<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Labo\Bundle\AdminBundle\services\aeData;
use Symfony\Component\HttpFoundation\Request;

use \Exception;

/**
 * generateController
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class generateController extends baseController {

	protected $classnames = null;

	public function indexAction($action = null, $entite = null) {
		$this->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->verifParamsFolder()->updateBundlesInConfig();
		$data = array();
		// $data['sitedata'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getSiteData();
		$data['action'] = $action;
		$data['entite'] = $entite;
		if($entite != null) $data['classname'] = $this->getClassname($entite);
		$data['created'] = array();
		$data['emptied'] = array();
		switch ($action) {
			case 'create':
				if($entite != null) $data['created'][$entite] = $this->generateEntite($entite);
				break;
			case 'empty':
				if($entite != null) $data['emptied'][$entite] = $this->get(aeData::PREFIX_CALL_SERVICE.'aefixtures')->emptyEntity($entite);
				break;
			default:
				break;
		}
		// view
		$em = $this->getDoctrine()->getManager();
		$entities = $this->getEntities();
		$data['info'] = array();
		$datainfo = array();
		$datainfofix = array();
		foreach ($entities as $name => $details) {
			$datainfo[$name]['classname'] = $details['classname'];
			$datainfo[$name]['class'] = $details['class'];
			try {
				$datainfo[$name]['size'] = $em->createQuery("SELECT COUNT(element.id) FROM ".$details['classname']." element")->getSingleScalarResult();
			} catch (Exception $e) {
				// Si l'entité ne possède pas d'Id => on la vire (panier…)
				unset($datainfo[$name]);
			}
		}
		// entités avec fixtures par ordre
		foreach($this->get(aeData::PREFIX_CALL_SERVICE.'aefixtures')->getInfoFiles() as $ord => $details) {
			$data['info'][$details['classname']] = array_merge(array('fixtures' => $details), $datainfo[$details['classname']]);
		}
		// entités sans fixtures à la fin
		foreach ($datainfo as $classname => $details) if(!isset($data['info'][$classname])) {
			$data['info'][$classname] = $details;
		}
		return $this->render('LaboAdminBundle:Default:install.html.twig', $data);
	}

	protected function getEntities() {
		return $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceBaseEntity')->getDetailedListOfEnties();
	}

	protected function getClassname($entite) {
		return isset($this->getEntities()[$entite]) ? $entite : false;
	}

	protected function generateEntite($entite = null) {
		set_time_limit(10000);
		$memory = $this->getParameter('memory_limit');
		ini_set("memory_limit", $memory);
		switch ($entite) {
			// case 'fileFormat':
			// 	return $this->get(aeData::PREFIX_CALL_SERVICE.'media')->initiateFormats(true);
			// 	break;
			case 'User':
				return $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceUser')->createUsers(true);
				break;
			
			default:
				// echo('<h2>Service : aetools.aefixtures</h2>');
				return $this->get(aeData::PREFIX_CALL_SERVICE.'aefixtures')->fillDataWithFixtures($entite);
				break;
		}
	}



}

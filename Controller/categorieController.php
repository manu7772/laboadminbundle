<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
use Symfony\Component\HttpFoundation\Request;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\Common\Collections\ArrayCollection;

use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\services\flashMessage;
use site\adminsiteBundle\Entity\categorie;
use Labo\Bundle\AdminBundle\Entity\nestedposition;

use \Exception;

/**
 * categorieController
 * @Security("has_role('ROLE_EDITOR')")
 */
class categorieController extends baseController {

	const ENTITE_NAME = 'site\adminsiteBundle\Entity\categorie';
	const ENTITE_SHORTNAME = 'categorie';

	// protected $entityService;

	// http://localhost/~emmanuel/domain/web/app_dev.php/fr/categorie/ajax-data
	public function ajaxDataAction($id = null, $types = 'all', $groups = null, Request $request) {
		// $request = $this->getRequest();
		$em = $this->getDoctrine()->getManager();
		if($id == null) {
			$id = $request->request->get('id');
		}
		if($id != null && $request->isXmlHttpRequest()) {
			// AJAX REQUEST
			$types = $request->request->get('types');
			$groups = $request->request->get('groups');
			$data = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceNested')->getRepo()->findArrayTree($id, $types, $groups);
			return new JsonResponse($data);
		} else if(!$request->isXmlHttpRequest()) {
			// TEST EN GET
			$data = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceNested')->getRepo()->findArrayTree($id, $types, $groups);
			return $this->render('LaboAdminBundle:superadmin:dump.html.twig', array('title' => 'Id '.$id, 'data' => $data));
		}
		return new JsonResponse('Error');
	}


	public function categorieListAction($type_related = null, $type_field = null, $type_values = null) {
		$data = $this->getEntiteData(self::ENTITE_NAME, self::LIST_ACTION, null, $type_related, $type_field, $type_values);
		return $this->render($this->getEntityTemplate($data), $data);
	}

	public function categorieRepoAction($method, $repoParams = null) {
		$data = $this->getEntiteData(self::ENTITE_NAME, self::LIST_ACTION, null, null, null, null, $method, $repoParams);
		return $this->render($this->getEntityTemplate($data), $data);
	}

	public function categorieShowAction($id) {
		$data = $this->getEntiteData(self::ENTITE_NAME, self::SHOW_ACTION, $id);
		return $this->render($this->getEntityTemplate($data), $data);
	}

	public function categorieEditAction($id, Request $request) {
		// set_time_limit(300);
		// $memory = $this->getParameter('memory_limit');
		// ini_set("memory_limit", $memory);

		$data = $this->getEntiteData(self::ENTITE_NAME, self::EDIT_ACTION, $id);
		// if(!is_object($data['entite'])) {
		// 	$classname = $data['classname'];
		// 	$data['entite'] = new $classname();
		// }
		$formType = null;
		$type = '';
		if(!is_object($data['entite']->getCategorieParent())) {
			$type = 'Root';
			if($data['entite']->getLvl() > 0)
				$this->get('flash_messages')->send(array(
					'title'		=> 'Erreurs de type',
					'type'		=> flashMessage::MESSAGES_WARNING,
					'text'		=> 'Cette catégorie est de type Type, mais de niveau supérieur à 0. Contactez l\'administrateur du site pour signaler ce risque de bug.',
				));
		}

		$formType = str_replace('Entity', 'Form', $data['classname'].$type.'Type');
		$data['form_action'] = $this->generateUrl('siteadmin_'.$data['action'].'_categorie', array('id' => $data['id']));
		$data[$data['action'].'_form'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeForms')->getEntityForm($data, $formType);
		if($data[$data['action'].'_form'] == false) {
			// erreur formulaire
			$this->get('flash_messages')->send(array(
				'title'		=> 'Erreur formulaire',
				'type'		=> flashMessage::MESSAGES_ERROR,
				'text'		=> 'Le formulaire d\'édition n\'a pu être généré.',
			));
			$data['action'] = self::SHOW_ACTION;
		} else {
			if($request->getMethod() == "POST") {
				// POST : retour formulaire
				$returnUrl = $this->computeReturnForm($data, $request);
				if($returnUrl !== false) return $this->redirect($returnUrl);
			}
			// else…
			// GET : création formulaire
		}
		if(isset($data[$data['action'].'_form']))
			$data[$data['action'].'_form'] = $data[$data['action'].'_form']->createView();
		return $this->render($this->getEntityTemplate($data), $data);
	}

	public function categorieCreateAction($rootParent = null, Request $request) {
		// set_time_limit(300);
		// $memory = $this->getParameter('memory_limit');
		// ini_set("memory_limit", $memory);

		$data = $this->getEntiteData(self::ENTITE_NAME, self::CREATE_ACTION, null);
		// $classname = $data['classname'];
		// $data['entite'] = new $classname();
		switch($rootParent) {
			case null:
				# new root type
				$data['entite']->setLvl(0);
				$formType = str_replace('Entity', 'Form', $data['classname'].'RootType');
				break;
			default:
				# new categorie has parent (so, is not root)
				$id = (integer) $rootParent;
				$rootParentObj = $this->entityService->getRepo()->find($id);
				if(!is_object($rootParentObj)) throw new Exception("Parent root #$id does not exists !", 1);
				$data['entite']->setCategorieParent($rootParentObj);
				$formType = null;
				break;
		}
		// Get form
		$data['form_action'] = $this->generateUrl('siteadmin_'.$data['action'].'_categorie', array('rootParent' => $rootParent));
		$data[$data['action'].'_form'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeForms')->getEntityForm($data, $formType);
		if($data[$data['action'].'_form'] == false) {
			// erreur formulaire
			$this->get('flash_messages')->send(array(
				'title'		=> 'Erreur formulaire',
				'type'		=> flashMessage::MESSAGES_ERROR,
				'text'		=> 'Le formulaire de création n\'a pu être généré.',
			));
			$data['action'] = self::DEFAULT_ACTION;
			$data['id'] = null;
		} else {

			if($request->getMethod() == "POST") {
				// POST : retour formulaire
				$returnUrl = $this->computeReturnForm($data, $request);
				if($returnUrl !== false) return $this->redirect($returnUrl);
			}
			// else…
			// GET : création formulaire
		}
		if(isset($data[$data['action'].'_form']))
			$data[$data['action'].'_form'] = $data[$data['action'].'_form']->createView();
		return $this->render($this->getEntityTemplate($data), $data);
	}

	protected function computeReturnForm(&$data, Request $request) {
		$data[$data['action'].'_form']->handleRequest($request);
		if($data[$data['action'].'_form']->isValid()) {
			// valid form : persist & flush
			$entite = $data['entite'];
			$data = $this->getHiddenDataFromForm($data['classname'], $request);
			$data['entite'] = $entite;
			// $this->entityService->checkAfterChange($data['entite']);
			$save = $this->entityService->save($data['entite']);
			if($save->getResult() == true) {
				// ENREGISTREMENT OK
				$this->getSuccessPersistFlashMessage($data);
				return $this->generateUrl('siteadmin_show_categorie', array('id' => $data['entite']->getId()));
				// return $this->redirectToRoute('siteadmin_show_categorie', array('id' => $data['entite']->getId()));
			} else {
				// erreur à l'enregistrement
				$this->get('flash_messages')->send(array(
					'title'		=> 'Erreurs enregistrement',
					'type'		=> flashMessage::MESSAGES_ERROR,
					'text'		=> $save->getMessage(),
				));
				// if(isset($data['onError'])) return $this->redirect($data['onError']);
			}
		} else {
			// invalid form
			$this->get('flash_messages')->send(array(
				'title'		=> 'Erreurs de saisie',
				'type'		=> flashMessage::MESSAGES_ERROR,
				'text'		=> 'La saisie de vos données contient des erreurs. Veuillez les corriger, svp.',
			));
		}
		return false;
	}

	protected function getHiddenDataFromForm($classname, Request $request) {
		$data = array();
		$entiteType = str_replace('Entity', 'Form', $classname.'Type');
		$entiteRootType = str_replace('Entity', 'Form', $classname.'RootType');
		$typeTmp = new $entiteType($this->container);
		$typeRootTmp = new $entiteRootType($this->container);
		// REQUEST
		// $request = $this->getRequest();
		// récupération hiddenData
		$req = $request->request->get($typeTmp->getName());
		$reqRoot = $request->request->get($typeRootTmp->getName());
		// echo('<pre>');
		// var_dump($req);
		if(isset($req["hiddenData"])) {
			$data = json_decode(urldecode($req["hiddenData"]), true);
			$isRoot = $entiteType;
			// var_dump($data);
		} else if(isset($reqRoot["hiddenData"])) {
			$data = json_decode(urldecode($reqRoot["hiddenData"]), true);
			$isRoot = $entiteRootType;
			// var_dump($data);
		} else {
			throw new Exception("PostFormAction : hiddenData absent ! (".$typeTmp->getName().' / '.$typeRootTmp->getName().")", 1);
		}
		// $data['entite'] = $this->entityService->getRepo()->find($data['id']);
		// echo('</pre>');
		if(count($data) < 1) throw new Exception('Données "hiddenData" vides !', 1);
		return $this->getEntiteData($data['entite'], $data['action'], $data['id'], $data['type']['type_related'], $data['type']['type_field'], $data['type']['type_values'], $data['repo']['method'], $data['repo']['repoParams']);
	}

	protected function getEntiteData($entite, $action = null, $id = null, $type_related = null, $type_field = null, $type_values = null, $method = null, $repoParams = null) {
		$data = array();
		$data['entite_name'] = self::ENTITE_SHORTNAME;
		$data['classname'] = self::ENTITE_NAME;
		$this->entityService = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceBaseEntity')->getEntityService($data['entite_name']);
		$repo = $this->entityService->getRepo();
		$data['id'] = $id;
		$data['entite'] = $entite;

		if($data['id'] !== null) {
			if(is_object($data['entite'])) {
				if((integer)$data['id'] !== (integer)$data['entite']->getId()) $data['entite'] = $repo->find($data['id']);
				$data['id'] = $data['entite']->getId();
			} else {
				$data['entite'] = $repo->find($data['id']);
			}
		} else if(is_string($data['entite'])) {
			if(class_exists($data['entite'])) $data['entite'] = $this->entityService->getNewEntity($data['entite']);
				else throw new Exception('Entity of class '.json_encode($data['entite']).' does not exist !', 1);
		}
		// if edit
		if($action === self::EDIT_ACTION) {
			$this->entityService->checkIntegrity($data['entite'], self::EDIT_ACTION);
		}

		// $data['sitedata'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getSiteData();
		$data['type']['type_related']	= $type_related;
		$data['type']['type_field']		= $type_field;
		$data['type']['type_values']	= $this->typeValuesToArray(urldecode($type_values));
		$data['repo']['method']	= $method;
		$data['repo']['repoParams']	= $repoParams;
		$data['action'] = $action;
		$data['entites'] = array();
		// $data['roots_list'] = $repo->findRoots();
		
		if($data['type']['type_related'] != null && $data['type']['type_field'] != null && $data['type']['type_values'] != null) {
			// recherche par valeurs ou liens
			if(method_exists($repo, 'findWithField')) {
				$data['entites'] = $repo->findWithField($data['type']);
			} else throw new Exception("Method \"findWithField\" does not exist in Repository \"".$data['classname']."\"", 1);
		} else if($data['repo']['method'] != null) {
			if(!method_exists($repo, $data['repo']['method'])) $data['repo']['method'] = self::LIST_REPO_METHOD;
			if(!method_exists($repo, $data['repo']['method'])) $data['repo']['method'] = self::LIST_REPO_DEFAULT;
			if(method_exists($repo, $data['repo']['method'])) {
				$data['entites'] = $repo->{$data['repo']['method']}($repoParams);
			}
		} else if($data['action'] == self::DEFAULT_ACTION) {
			// recherche all
			if(method_exists($repo, self::LIST_REPO_METHOD)) $method = self::LIST_REPO_METHOD;
				else $method = self::LIST_REPO_DEFAULT;
			$data['entites'] = $repo->$method();
		}
		// autres
		$data['typeSelf'] = self::TYPE_SELF;
		$data['type_value_joiner'] = self::TYPE_VALUE_JOINER;

		return $data;
	}

	public function checkCategoriesTypesAction(Request $request) {
		return $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCategorie')
			->checkCategoriesTypes()
			->getArrayJSONreponse()
			;
	}


}

<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\Entity\facture;

use Labo\Bundle\AdminBundle\services\flashMessage;

use \Exception;

/**
 * factureController
 * @Security("has_role('ROLE_ADMIN')")
 */
class factureController extends baseController {

	public function changeStateFactureAction($factureId, $state, Request $request) {
		$data['facture'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceFacture')->getRepo()->findOneById($factureId);
		if($data['facture'] instanceOf facture) {
			$data['facture']->setState($state);
			$em = $this->getDoctrine()->getManager();
			$em->flush();
		}
		$data['entites'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceFacture')->getRepo()->findAll();
		$data['request'] = $request;
		return $this->redirectToRoute('siteadmin_entite', array('entite' => 'facture', 'action' => 'list'));
		// return $this->render('LaboAdminBundle:entites:testsEntities.html.twig', $data);
	}


}

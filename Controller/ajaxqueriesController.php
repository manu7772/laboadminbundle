<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
use Symfony\Component\HttpFoundation\Request;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * ajaxqueriesController
 * @Security("has_role('ROLE_EDITOR')")
 */
class ajaxqueriesController extends baseController {

	public function ajaxconstructAction(Request $request) {
		if($request->isXmlHttpRequest()) {
			//
		}
	}

	public function ajaxsortAction(Request $request) {
		// $request = $this->getRequest();
		$data = array();
		if($request->isXmlHttpRequest()) {
			// AJAX REQUEST
			$data = $request->request->all();
			if(isset($data['entity']) && isset($data['children']) && isset($data['group'])) {
				// DATA OK
				$entityService = $this->get('aetools.aeServiceBaseEntity')->getEntityService($data['entity'][0]);
				if(method_exists($entityService, 'sortChildren')) {
					$data = $entityService->sortChildren($data);
					if(is_array($data)) return new JsonResponse($data);
					return $this->requErrors(500, $data);
				} else {
					return $this->requErrors(500, 'This entity is not nestable !');
				}
			} else {
				// ERROR
				$data['request']['method'] = $request->getMethod();
				$this->get('aetools.aeDebug')->debugNamedFile('ajaxqueries_ajaxsort', $data);
				return $this->requErrors(500, 'Request data not found');
			}
		} else if(!$request->isXmlHttpRequest()) {
			// TEST EN GET
			$data = array(
				'entity' => array('pageweb', '24'),
				'children' => array(
					array('section', '50'),
					array('section', '44'),
					),
				'group' => 'pageweb_sections',
				// 'entity' => array('article', '2639'),
				// 'children' => array(
				// 	array('article', '2638'),
				// 	array('article', '2639'),
				// 	array('article', '2636'),
				// 	array('article', '2637'),
				// 	),
				// 'group' => 'articles',
				);
			$entityService = $this->get('aetools.aeServiceBaseEntity')->getEntityService($data['entity'][0]);
			if(method_exists($entityService, 'sortChildren')) {
				$data = $entityService->sortChildren($data);
				echo('<pre>');
				var_dump($data);
				echo('</pre>');
				return new Response('- Results from non Ajax request.');
			}
		}
		return $this->requErrors(500, 'Uncognized system error !');
	}


	/***************************/
	/*** ERRORS              ***/
	/***************************/

	protected function requErrors($status, $message) {
		$response = new Response();
		$response->headers->set('Content-type', 'application/json');
		$response->setContent((string) $message);
		$response->setStatusCode((integer) $status);
		return $response;
	}

}

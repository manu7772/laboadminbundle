<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Labo\Bundle\AdminBundle\services\flashMessage;

use \Exception;

/**
 * menusController
 * @Security("has_role('ROLE_ADMIN')")
 */
class menusController extends baseController {

	public function indexAction() {
		$data = array();
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		$aeMenus = $this->get('aetools.aeMenus');
		$data['menus'] = $aeMenus->getInfoMenus();
		$data['bundles'] = $aeMenus->getBundles();
		return $this->render('LaboAdminBundle:menus:index.html.twig', $data);
	}

	public function actionAction($action, $bundle, $name = null, $id = null) {
		$data = array();
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		$data['action'] = $action;
		$data['bundle'] = $bundle;
		$data['name'] = $name;
		$aeMenus = $this->get('aetools.aeMenus');
		$data['translates'] = $aeMenus->getLanguagesInfo(); // "languages" - "catalogue"
		$data['bundles'] = $aeMenus->getBundles();
		switch ($action) {
			case 'add':
				$aeMenus->addNewItem($bundle, $name);
				return $this->redirectToRoute('siteadmin_menus_action', array('action' => 'edit', 'bundle' => $bundle, 'name' => $name));
				break;

			case 'create':
				# code...
				break;

			case 'edit':
				$data['menu'] = $aeMenus->getInfoMenu($bundle, $name);
				break;

			case 'delete':
				$aeMenus->deleteItem($bundle, $name, $id);
				return $this->redirectToRoute('siteadmin_menus_action', array('action' => 'edit', 'bundle' => $bundle, 'name' => $name));
				break;

			case 'copy':
				# code...
				break;

			default:
				// view
				$data['menu'] = $aeMenus->getInfoMenu($bundle, $name);
				break;
		}
		// $data['models'] = $this->get('aetools.aeServicePageweb')->getModels();
		$data['pagewebs'] = $this->get('aetools.aeServicePageweb')->getRepo()->findAll();
		return $this->render('LaboAdminBundle:menus:menu_action.html.twig', $data);
	}

	/**
	 * Ajax modification d'un menu
	 * @param string $bundle
	 * @param string $name
	 * @return boolean
	 */
	public function modifyAction($bundle, $name, Request $request) {
		$aeMenus = $this->get('aetools.aeMenus');
		// $request = $this->getRequest();
		$tree = $request->request->get('tree');
		$data = $aeMenus->setMenu($bundle, $name, $tree);
		return new JsonResponse($data);
	}

	/**
	 * Ajax modification de l'attribut maxDepth d'un menu
	 * @param string $bundle
	 * @param string $name
	 * @param string $value
	 * @return boolean
	 */
	public function changeMaxDepthAction($bundle, $name, $value) {
		$value = $this->get('aetools.aeMenus')->setMaxDepth($bundle, $name, $value);
		return new JsonResponse(array('value' => $value));
	}


}

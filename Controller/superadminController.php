<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
// use Doctrine\Common\Annotations\AnnotationReader;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Labo\Bundle\AdminBundle\services\flashMessage;
use Symfony\Component\HttpFoundation\Request;
use Labo\Bundle\AdminBundle\services\aeData;

use site\adminsiteBundle\Entity\categorie;

use \Exception;
use \ReflectionObject;
use \ReflectionClass;
use \ReflectionMethod;

/**
 * superadminController. 
 * Interface permettant de contrôler les données via le role SUPER ADMIN. 
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class superadminController extends baseController {

	/**
	 * Page d'accueil SUPER ADMIN
	 * @return Response
	 */
	public function indexAction() {
		$this->get('aetools.aeUrlroutes')->verifParamsFolder()->updateBundlesInConfig();
		$data = array();
		// Site data
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		// services
		$data['services'] = array(['nom' => 'fonction à développer…']);
		// $listOfServices = $this->get(aeData::PREFIX_CALL_SERVICE.'aeYaml')->getAppServices();
		// foreach($listOfServices as $service['callname']) {
		// 	/** DEV@not_perfect : fonction to optimize */
		// 	$data['services'][$service] = $this->get($service);
		// }
		$data['checkmessages'] = $this->get('request')->getSession()->getFlashBag()->get('checkmessages');
		// echo('<pre>');var_dump($data['checkmessages']);echo('</pre>');
		if(count($data['checkmessages']) < 1) {
			$data['checkmessages'][0] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceMessage')->checkMessagesCollaborators(false);
		}
		$data['aetools_Urlroutes'] = $this->get('aetools.aeUrlroutes');
		return $this->render('LaboAdminBundle:superadmin:index.html.twig', $data);
	}

	/**
	 * Check collaborators (user) messages
	 * @param boolean $repar = false
	 * @return Response
	 */
	public function checkMessagesCollaboratorsAction($repar = false) {
		$aeServiceMessage = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceMessage');
		$results = $aeServiceMessage->checkMessagesCollaborators($repar);
		$type = $results->getResult() ? flashMessage::MESSAGES_ERROR : flashMessage::MESSAGES_WARNING;
		$this->get('flash_messages')->send(array(
			'title'		=> 'Collaborator\'s messages',
			'type'		=> $type,
			'text'		=> $results->getMessage(),
			'grant'		=> 'ROLE_SUPER_ADMIN',
		));
		$this->get('request')->getSession()->getFlashBag()->add('checkmessages', $results);
		// echo('<pre>');var_dump($liste);echo('</pre>');
		// die();
		return $this->redirect($this->generateUrl('siteadmin_sadmin_index'));
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 * Page de test sur Panier
	 * @return Response
	 */
	public function panierTestAction() {
		$this->get('aetools.aeUrlroutes')->updateBundlesInConfig();
		$data = array();
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		$data['articles'] = $this->get('aetools.aeServiceBaseEntity')->getEntityService('article')->getRepo()->findAll();
		$data['panier_user'] = $this->getUser()->getPaniers();
		$data['panier_info'] = $this->get('aetools.aeServicePanier')->getInfosPanier($this->getUser());
		$data['panier_info_complete'] = $this->get('aetools.aeServicePanier')->getInfosPanier($this->getUser(), true);
		// $this->get('aetools.aeServicePanier')->getSerialized('ajaxlive', ['user' => $this->getUser()]);
		return $this->render('LaboAdminBundle:superadmin:panier_test.html.twig', $data);
	}

	public function sessiondataAction() {
		$aeServiceSessiondata = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSessiondata');

		$test = $aeServiceSessiondata->checkChangedSessionAjaxlivedata(json_encode('test2'));

		$data['sessiondatas'] = $aeServiceSessiondata->getRepo()->findAll();
		return $this->render('LaboAdminBundle:superadmin:sessiondata.html.twig', $data);
	}

	/**
	 * Page d'information sur les services    		
	 * @param string $service
	 * @return Response
	 */
	public function servicesAction($service) {
		$data = array();
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		$data['name'] = $service;
		$data['service_info'] = $this->get('aetools.aeClasses')->getClassProperties($this->get($service));
		return $this->render('LaboAdminBundle:superadmin:services.html.twig', $data);
	}


	/**
	 * Page d'information sur les routes
	 * @return Response
	 */
	public function routesAction() {
		$data = array();
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		// $data['params'] = $aetools->getRouteParameters();
		$data['aetools_Urlroutes'] = $this->get('aetools.aeUrlroutes');
		// via stack
		// $stack = $this->get('request_stack');
		// $masterRequest = $stack->getMasterRequest();
		// $data['routes'] = $masterRequest->get('_route');
		return $this->render('LaboAdminBundle:superadmin:routes.html.twig', $data);
	}

	/**
	 * Page d'information sur les bundles
	 * @return Response
	 */
	public function bundlesAction() {
		$data = array();
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		$data['bundles'] = $this->get('aetools.aeUrlroutes')->getBundlesList(true);
		$data['bundle'] = $this->get('aetools.aeUrlroutes')->getBundleName();
		return $this->render('LaboAdminBundle:superadmin:bundles.html.twig', $data);
	}

	/**
	 * Page d'information sur les entités
	 * @param string $entity = null
	 * @param string $field = null
	 * @return Response
	 */
	public function entitiesAction($entity = null, $field = null) {
		$data = array();
		$data['sitedata'] = $this->get('aetools.aeServiceSite')->getSiteData();
		$aeEntities = $this->get('aetools.aeServiceBaseEntity');
		$data['entities'] = $aeEntities->getCompleteDetailedListOfEnties();
		// général
		$level = '';

		// hierarchy
		$data['hierarchy'] = array();
		foreach ($data['entities'] as $name => $value) if(isset($value['object'])) {
			if(method_exists($value['object'], 'getParentsClassNames')) {
				$data['hierarchy'] = array_merge_recursive($data['hierarchy'], $value['object']->getParentsClassNames(true, true));
			}
		}

		if($entity != null) {
			// analyse d'une entité
			$level = '_entity';
			$entityClass = $aeEntities->getEntityClassName($entity);
			$data['entity']['shortname'] = $entity;
			$data['entity']['classname'] = $data['entities'][$entityClass]['classname'];
			$data['entity']['classinfo'] = $this->get('aetools.aeClasses')->getClassProperties($data['entity']['classname']);
			$fields = $data['entities'][$entityClass]['single'];
			$assoc = $data['entities'][$entityClass]['association'];
			// info sur champs…
			foreach ($fields as $fieldname => $values) {
				$data['entity']['single'][$fieldname] = array();
				$data['entity']['single'][$fieldname]['type'] = $aeEntities->getTypeOfField($fieldname, $entity);
				$data['entity']['single'][$fieldname]['nullable'] = $aeEntities->isNullableField($fieldname, $entity);
				$data['entity']['single'][$fieldname]['unique'] = $aeEntities->isUniqueField($fieldname, $entity);
				$data['entity']['single'][$fieldname]['isId'] = $aeEntities->isIdentifier($fieldname, $entity);
				$data['entity']['single'][$fieldname]['set'] = $aeEntities->getMethodOfSetting($fieldname, $entityClass, true);
				$data['entity']['single'][$fieldname]['get'] = $aeEntities->getMethodOfGetting($fieldname, $entityClass, true);
				$data['entity']['single'][$fieldname]['remove'] = $aeEntities->getMethodOfRemoving($fieldname, $entityClass, true);
			}
			foreach ($assoc as $assocname => $values) {
				$data['entity']['association'][$assocname] = array();
				$targetName = $aeEntities->getTargetEntity($assocname, $entity, true);
				// echo('<p>'.$entity." target for ".$assocname." = ".$targetName.'</p>');
				$data['entity']['association'][$assocname]['target']['classname'] = $targetName;
				$data['entity']['association'][$assocname]['target']['name'] = $aeEntities->getEntityShortName($data['entity']['association'][$assocname]['target']['classname']);
				$data['entity']['association'][$assocname]['set'] = $aeEntities->getMethodOfSetting($assocname, $entityClass, true);
				$data['entity']['association'][$assocname]['get'] = $aeEntities->getMethodOfGetting($assocname, $entityClass, true);
				$data['entity']['association'][$assocname]['remove'] = $aeEntities->getMethodOfRemoving($assocname, $entityClass, true);
				$data['entity']['association'][$assocname]['isId'] = $aeEntities->isIdentifier($assocname, $entity);
				$data['entity']['association'][$assocname]['nullable'] = $aeEntities->isNullableField($assocname, $entity);
				$data['entity']['association'][$assocname]['unique'] = $aeEntities->isUniqueField($assocname, $entity);
				$data['entity']['association'][$assocname]['unidir'] = $aeEntities->isAssociationWithSingleJoinColumn($assocname, $entity);
				$data['entity']['association'][$assocname]['bidir'] = $aeEntities->isBidirectional($assocname, $entity);
				$data['entity']['association'][$assocname]['isInverse'] = $aeEntities->isAssociationInverseSide($assocname, $entity);
				$data['entity']['association'][$assocname]['otherSideSource'] = $aeEntities->get_OtherSide_sourceField($assocname, $entity);
			}
		}

		if($field != null) {
			// analyse d'un champ
			$level = '_field';
		}
		return $this->render('LaboAdminBundle:superadmin:entities'.$level.'.html.twig', $data);
	}


// vakata-jstree-9770c67


	// TEST JSTREE

	public function jstree_testAction($rootslug = null) {
		$categorie = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCategorie')->getRepo()->findOneBySlug($rootslug);
		if(!($categorie instanceOf categorie)) throw new Exception("Can not find categorie ".$rootslug."!", 1);
		
	}



}









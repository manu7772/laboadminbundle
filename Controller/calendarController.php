<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;

use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\entity\calendar;
use site\adminsiteBundle\Entity\calendartier;

use \DateTime;

/**
 * calendarController
 * @Security("has_role('ROLE_TRANSLATOR')")
 */
class calendarController extends baseController {

	/**
	 * Page calendar pour tier
	 * @return Response
	 */
	public function calendarViewerAction($itemSlug, $class) {
		$data = array();
		$data['itemSlug'] = $itemSlug;
		$data['class'] = $class;
		$serviceEntity = $this->get('aetools.aeServiceBaseEntity')->getEntityService($class);
		$data['entity'] = $serviceEntity->getRepo()->findOneBySlug($itemSlug);
		return $this->render('LaboAdminBundle:calendar:calendar_'.$class.'_viewer.html.twig', $data);
	}

	/**
	 * Page calendar pour tier
	 * @return Response
	 */
	public function calendarEditAction($eventSlug) {
		$data = array();
		$data['eventSlug'] = $eventSlug;
		$data['event'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->findOneBySlug($eventSlug);
		$class = $data['event']->getOwner()['class'];
		return $this->render('LaboAdminBundle:calendar:calendar_'.$class.'_edit.html.twig', $data);
	}

	/**
	 * Calendar actions
	 * @return JsonResponse
	 */
	public function ajaxCalendarAction($itemSlug, $class, Request $request) {
		$global = $request->request->get('global');
		$event = null;
		$message = '';
		if($global !== null) {
			switch ($global) {
				case 'removeAll':
					$em = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getEm();
					// $entities = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->findAll();
					$entities = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->findCalendarsOfItem($itemSlug, $class);
					foreach ($entities as $key => $entity) $em->remove($entity);
					$em->flush();
					$message = 'Tous les évènements de cet agenda ont été supprimés.';
					break;
			}
		} else {
			$requestEvent = $request->request->get('event');
			switch($requestEvent['mode']) {
				case 'new':
					$event = $this->getNewCalendar($requestEvent);
					$this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->save($event);
					$event = $this->getFcFormat($event);
					$message = 'Evènement agenda enregistré.';
					// $message = json_encode($requestEvent);
					break;
				case 'remove':
					$event = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->find($requestEvent['id']);
					if($event == null)
						return $this->get(aeData::PREFIX_CALL_SERVICE.'aeReponse')
							->initAeReponse(false, null, 'Evènement agenda non trouvé.')
							->getArrayJSONreponse();
					$em = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getEm();
					$em->remove($event);
					$em->flush();
					$message = 'Evènement agenda supprimé.';
					break;
				case 'resize':
				case 'move':
				default:
					$event = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->find($requestEvent['id']);
					if($event == null)
						return $this->get(aeData::PREFIX_CALL_SERVICE.'aeReponse')
							->initAeReponse(false, null, 'Evènement agenda non trouvé.')
							->getArrayJSONreponse();
					$this->computeEvent($event, $requestEvent);
					$this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->save($event);
					// $event = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->find($event->getId());
					$event = $this->getFcFormat($event);
					$message = 'Evènement agenda modifié.';
					break;
			}
		}
		return $this->get(aeData::PREFIX_CALL_SERVICE.'aeReponse')
				->initAeReponse(true, $event, $message)
				->getArrayJSONreponse();
	}

	public function ajaxFeedAction($itemSlug, $class, Request $request) {
		$start = $request->query->get('start');
		$end = $request->query->get('end');
		// $entities = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->findAll();
		$entities = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getRepo()->findCalendarsOfItem($itemSlug, $class, new DateTime($start), new DateTime($end));
		return new JsonResponse($this->getFcFormat($entities));
	}


	protected function getNewCalendar($event, $itemSlug = null, $class = null) {
		if($itemSlug === null) $itemSlug = $event['owner']['slug'];
		if($class === null) $class = $event['owner']['class'];
		$newEvent = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCalendar')->getNewEntity('calendartier');
		$this->computeEvent($newEvent, $event, $itemSlug);
		return $newEvent;
	}

	protected function computeEvent(&$entity, $event, $itemSlug = null) {
		if(isset($event['title'])) $entity->setNom($event['title']);
		if(isset($event['start'])) $entity->setStartDate(new DateTime($event['start']));
		if(isset($event['end'])) {
			if($event['end'] != null) $entity->setEndDate(new DateTime($event['end']));
		}
		if(isset($event['allDay'])) {
			$entity->setAllday($event['allDay'] === 'true' || $event['allDay'] === 1 || $event['allDay'] === true);
		}
		if(isset($event['url'])) $entity->setUrl($event['url']);
		if(isset($event['color'])) $entity->setColor($event['color']);
		if($entity->getOwnerObject() === null) {
			if($itemSlug === null) $itemSlug = $event['owner']['slug'];
			$entity->setOwner($this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceTier')->getRepo()->findOneBySlug($itemSlug));
		}
		// return $entity;
	}

	protected function getFcFormat($entities) {
		$entities = $this->container->get('jms_serializer')->serialize($entities, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups(array('fullcalendar')));
		return json_decode($entities);
	}

	// protected function getRandomCalendars($itemSlug, $class, $data = null) {
	// 	if($data === null) {
	// 		$t = new DateTime();
	// 		$data['start'] = $t->format('Y-m-d H:i:s').' - 1 DAYS + 10 HOURS';
	// 		$data['end'] = $t->format('Y-m-d H:i:s').' - 1 DAYS + 13 HOURS';
	// 	}
	// 	$testHereStart = new DateTime($data['start'].' + 22 DAYS + 10 HOURS');
	// 	$testHereEnd = new DateTime($data['start'].' + 22 DAYS + 13 HOURS');
	// 	$time1 = new DateTime('+2 HOURS');
	// 	$time2 = new DateTime('+1 DAY +1 HOUR');
	// 	$time3start = new DateTime('Jan 19 2017 01:00:00 GMT+0100 (CET)');
	// 	$time3end = new DateTime('Jan 22 2017 01:00:00 GMT+0100 (CET)');
	// 	$list = array(
	// 		0 => array(
	// 			'id' => 1000,
	// 			'title' => 'Meeting',
	// 			'start' => $time1->format('Y-m-d H:i:s'),
	// 			'allDay' => false,
	// 			'color' => 'blue',
	// 			),
	// 		1 => array(
	// 			'id' => 2000,
	// 			'title' => 'Incentive',
	// 			'start' => $time2->format('Y-m-d H:i:s'),
	// 			'allDay' => false,
	// 			'color' => 'orange',
	// 			),
	// 		2 => array(
	// 			'id' => 3000,
	// 			'title' => 'Incentive 3 days',
	// 			'start' => $time3start->format('Y-m-d H:i:s'),
	// 			'end' => $time3end->format('Y-m-d H:i:s'),
	// 			'allDay' => true,
	// 			'color' => 'green',
	// 			),
	// 		3 => array(
	// 			'id' => 4000,
	// 			'title' => 'This is NOW',
	// 			'start' => $testHereStart->format('Y-m-d H:i:s'),
	// 			'end' => $testHereEnd->format('Y-m-d H:i:s'),
	// 			'allDay' => false,
	// 			'color' => 'pink',
	// 			),
	// 		);
	// 	// get entities
	// 	// $em = $this->getDoctrine()->getManager();
	// 	foreach ($list as $key => $item) {
	// 		$list[$key] = $this->getNewCalendar($item, $itemSlug, $class);
	// 		// $em->persist($list[$key]);
	// 	}
	// 	// $em->flush();
	// 	// JMSserialize them…
	// 	$return = $this->getFcFormat($list);
	// 	return $return;
	// }

}





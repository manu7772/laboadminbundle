<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Labo\Bundle\AdminBundle\Entity\magasin;
use Labo\Bundle\AdminBundle\Form\magasinType;
use site\adminsiteBundle\Entity\reseau;
use Labo\Bundle\AdminBundle\Form\reseauType;
use site\adminsiteBundle\Entity\marque;
use Labo\Bundle\AdminBundle\Form\marqueType;

use Labo\Bundle\AdminBundle\services\flashMessage;

use \Exception;

/**
 * baseController
 * @Security("has_role('ROLE_TRANSLATOR')")
 */
class baseController extends Controller {

	const TYPE_SELF 			= '_self';
	const DEFAULT_ACTION 		= 'list';
	const LIST_ACTION 			= 'list';
	const SHOW_ACTION 			= 'show';
	const CREATE_ACTION 		= 'create';
	const EDIT_ACTION 			= 'edit';
	const COPY_ACTION 			= 'copy';
	const DELETE_ACTION 		= 'delete';
	const ACTIVE_ACTION 		= 'active';
	const CHECK_ACTION 			= 'check';
	const TYPE_VALUE_JOINER 	= '___';
	const BUNDLE				= 'laboadmin';
	const LIST_REPO_METHOD		= 'findForList';
	const LIST_REPO_DEFAULT		= 'findAll';

	protected $bundle 			= null;



	protected function typeValuesToArray($type_values = null) {
		if($type_values != null) $type_values = explode(self::TYPE_VALUE_JOINER, $type_values);
		return $type_values;
	}
	protected function typeValuesToString($type_values = null) {
		if($type_values != null) $type_values = implode(self::TYPE_VALUE_JOINER, $type_values);
		return $type_values;
	}

	protected function fillEntityWithData(&$data) {
		$service = $this->get('aetools.aeServiceBaseEntity')->getEntityService($data['entite']);
		if(is_array($data['type']['type_values']) && count($data['type']['type_values']) > 0) {
			switch ($data['type']['type_related']) {
				case self::TYPE_SELF:
					// field
					if(!$service->hasAssociation($data['type']['type_field'], $data['entite'])) {
						$set = $service->getMethodOfSetting($data['type']['type_field'], $data['entite'], false);
						if(is_string($set)) {
							// ok setter
							if(preg_match('#^set#', $set)) $data['entite']->$set(reset($data['type']['type_values']));
							if(preg_match('#^add#', $set)) foreach($data['type']['type_values'] as $value) {
								$data['entite']->$set($value);
							}
						}
					} else {
						throw new Exception("Ce champ est de type association : field ".json_encode($data['type']['type_field'])." fourni !", 1);
					}
					break;
				default:
					// association
					$set = $service->getMethodOfSetting($data['type']['type_field'], $data['entite'], false);
					$related = explode(self::TYPE_VALUE_JOINER, $data['type']['type_related'], 2);
					if(is_string($set) && count($related) == 2) {
						// ok setter
						$related_service = $this->get('aetools.aeServiceBaseEntity')->getEntityService($related[0]);
						foreach ($data['type']['type_values'] as $value) {
							// find relateds
							$findMethod = $related_service->getMethodNameWith($related[1], 'findBy');
							if($findMethod != false) {
								$related_objects = $related_service->getRepo()->$findMethod($value);
								if(count($related_objects) > 0) $data['entite']->$set($related_objects);
							}
						}
					} else {
						throw new Exception("Ce champ n'a pas d'association : field ".json_encode($data['type']['type_field'])." fourni ! ".json_encode($set).". ".json_encode($related).".", 1);
					}
					break;
			}
		}
		return $data;
	}

	protected function getEntityTemplate($data) {
		$template = 'LaboAdminBundle:entites:'.$data['entite_name'].ucfirst($data['action']).'.html.twig';
		if(!$this->get('templating')->exists($template)) {
			$template = 'LaboAdminBundle:entites:'.'entite'.ucfirst($data['action']).'.html.twig';
			if(!$this->get('templating')->exists($template)) {
				throw new Exception("La page recherchée n'existe pas ".json_encode($template)." !", 1);
				// return $this->redirectToRoute('siteadmin_homepage');
			}
		}
		return $template;
	}

	/**
	 * Envoie un flash message après persist/update d'une entité
	 * @param array $data
	 */
	protected function getSuccessPersistFlashMessage($data) {
		$nom = $data['entite']->getId();
		if(method_exists($data['entite'], 'getName')) $nom = $data['entite']->getName();
		if(method_exists($data['entite'], 'getNom')) $nom = $data['entite']->getNom();
		if($data['action'] == "create") {
			$this->get('flash_messages')->send(array(
				'title'		=> 'Saisie enregistrée',
				'type'		=> flashMessage::MESSAGES_SUCCESS,
				'text'		=> 'Le nouvel élément "'.$nom.'" a bien été enregistré.',
			));
		} else {
			$this->get('flash_messages')->send(array(
				'title'		=> 'Saisie enregistrée',
				'type'		=> flashMessage::MESSAGES_SUCCESS,
				'text'		=> 'Les modification "'.$nom.'" ont bien été enregistrées.',
			));
		}
	}


}
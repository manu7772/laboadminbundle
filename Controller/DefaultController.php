<?php

namespace Labo\Bundle\AdminBundle\Controller;

use Labo\Bundle\AdminBundle\Controller\baseController;
// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\services\flashMessage;

use \Exception;

/**
 * DefaultController
 * @Security("has_role('ROLE_TRANSLATOR')")
 */
class DefaultController extends baseController {

	/**
	 * Page d'accueil adminstration
	 * @return Response
	 */
	public function indexAction() {
		// if site not installed
		if(count($this->get(aeData::PREFIX_CALL_SERVICE.'aeServicePageweb')->getDefaultPage()) < 1) return $this->redirectToRoute('generate');
		// else…
		$this->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->updateBundlesInConfig();
		$data = array();
		$data['sitedata'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getSiteData();
		$data['messages'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceMessage')->getRepo()->findNotRead(false);
		// $data['factures'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceFacture')->getRepo()->findByState(0);
		$data['nbmessages'] = count($data['messages']);
		$nbmessages = $this->getParameter('admin')['accueil_nb_messages'];
		$data['messages'] = array_slice($data['messages'], 0, $nbmessages); // max. : X messages
		$data['bundle'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->getBundleName();
		return $this->render('LaboAdminBundle:Default:index.html.twig', $data);
	}

	/**
	 * Page de support (help)
	 * @return Response
	 */
	public function supportAction() {
		$data = array();
		$data['sitedata'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getSiteData();
		$data['bundle'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->getBundleName();
		return $this->render('LaboAdminBundle:Default:support.html.twig', $data);
	}

	//**************//
	// BLOCKS       //
	//**************//

	public function headerAction($option = null) {
		$data = array();
		$stack = $this->get('request_stack');
		$masterRequest = $stack->getMasterRequest();
		$data['infoRoute']['_route'] = $masterRequest->get('_route');
		$data['infoRoute']['_route_params'] = $masterRequest->get('_route_params');
		$data['bundle'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->getBundleName();
		return $this->render('LaboAdminBundle:blocks:header.html.twig', $data);
	}

	public function sidebarAction($option = null) {
		$data = array();
		$data['sitedata'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getSiteData();
		$data['roles'] = $this->get('labo_user_roles')->getListOfRoles();
		// variables diverses
		$data['typeSelf'] = self::TYPE_SELF;
		$data['type_value_joiner'] = self::TYPE_VALUE_JOINER;
		$data['bundle'] = $this->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->getBundleName();
		return $this->render('LaboAdminBundle:blocks:sidebar.html.twig', $data);
	}


}

<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\Entity\message;

use \DateTime;
use \ReflectionClass;

/**
 * messageuser
 * @ORM\Table(name="message_user")
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\messageuserRepository")
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 *
 * @ExclusionPolicy("all")
 */
class messageuser {

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="messageusers")
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @MaxDepth(2)
	 */
	protected $collaborator;

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="site\adminsiteBundle\Entity\message", inversedBy="collaborators")
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @MaxDepth(2)
	 */
	protected $messageuser;

	/**
	 * @var DateTime
	 * @ORM\Column(name="messageread", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $read;

	/**
	 * @var DateTime
	 * @ORM\Column(name="messagesent", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $sent;

	/**
	 * @var DateTime
	 * @ORM\Column(name="messagecreated", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $messagecreated;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $updated;

	public function __construct() {
		$this->collaborator = null;
		$this->messageuser = null;
		$this->read = null;
		$this->sent = null;
		$this->messagecreated = null;
		$this->created = new DateTime();
		$this->updated = null;
	}

	public function __toString() {
		return $this->getId();
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }


	public function getId() {
		return $this->getCollaborator()->getId()."_".$this->getMessageuser()->getId();
	}

	/**
	 * Set collaborator
	 * @param LaboUser $collaborator
	 * @return 
	 */
	public function setCollaborator(LaboUser $collaborator = null) {
		$this->collaborator = $collaborator;
		if($collaborator != null) $collaborator->addMessageuser($this);
			else $collaborator->removeMessageuser($this);
		return $this;
	}

	/**
	 * Get collaborator
	 * @return LaboUser
	 */
	public function getCollaborator() {
		return $this->collaborator;
	}

	/**
	 * Set messageuser
	 * @param message $messageuser
	 * @return 
	 */
	public function setMessageuser(message $messageuser) {
		$this->messageuser = $messageuser;
		if($messageuser != null) {
			$messageuser->addCollaborator($this);
			$this->setMessagecreated($this->messageuser->getCreation());
		} else {
			$messageuser->removeCollaborator($this);
			$this->setMessagecreated(null);
		}
		return $this;
	}

	/**
	 * Get messageuser
	 * @return message
	 */
	public function getMessageuser() {
		return $this->messageuser;
	}

	/**
	 * Set sent
	 * @param DateTime $sent = null
	 * @return panier
	 */
	public function setSent(DateTime $sent = null) {
		$this->sent = $sent;
		// if($sent != null && $this->getMessageuser() != null) $this->getMessageuser()->setSent($sent);
		return $this;
	}

	/**
	 * Set sent now
	 * @return panier
	 */
	public function setSentnow() {
		$this->setSent(new DateTime());
		return $this;
	}

	/**
	 * Get sent
	 * @return DateTime|null
	 */
	public function getSent() {
		return $this->sent;
	}

	/**
	 * Is sent
	 * @return DateTime|null
	 */
	public function isSent() {
		return $this->sent instanceOf DateTime;
	}

	/**
	 * Set read
	 * @param DateTime $read = null
	 * @return panier
	 */
	public function setRead(DateTime $read = null) {
		$this->read = $read;
		if($read != null && $this->getMessageuser() != null) $this->getMessageuser()->setRead($read);
		return $this;
	}

	/**
	 * Set read now
	 * @return panier
	 */
	public function setReadnow() {
		$this->setRead(new DateTime());
		return $this;
	}

	/**
	 * Get read
	 * @return DateTime|null
	 */
	public function getRead() {
		return $this->read;
	}

	/**
	 * Is read
	 * @return DateTime|null
	 */
	public function isRead() {
		return $this->read instanceOf DateTime;
	}

	/**
	 * Set messagecreated
	 * @param DateTime $messagecreated
	 * @return panier
	 */
	public function setMessagecreated(DateTime $messagecreated = null) {
		$this->messagecreated = $messagecreated;
		return $this;
	}

	/**
	 * Get messagecreated
	 * @return DateTime 
	 */
	public function getMessagecreated() {
		return $this->messagecreated;
	}

	/**
	 * Set created
	 * @param DateTime $created
	 * @return panier
	 */
	public function setCreated(DateTime $created) {
		$this->created = $created;
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function updateDate() {
		$this->setUpdated(new DateTime());
	}

	/**
	 * Set updated
	 * @param DateTime $updated
	 * @return panier
	 */
	public function setUpdated(DateTime $updated = null) {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime|null
	 */
	public function getUpdated() {
		return $this->updated;
	}



}

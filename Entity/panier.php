<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\item;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\services\aeUnits;

use \DateTime;
use \ReflectionClass;

/**
 * panier
 * 
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
abstract class panier {

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="paniers")
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @Gedmo\SortableGroup
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @MaxDepth(2)
	 */
	protected $user;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 * @MaxDepth(2)
	 */
	protected $article;

	/**
	 * @ORM\Column(type="integer")
	 * https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/sortable.md
	 * @Gedmo\SortablePosition
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $position;

	/**
	 * @var integer
	 * @ORM\Column(name="quantite", type="integer", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 */
	protected $quantite;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 */
	protected $updated;

	/**
	 * @var string
	 * @ORM\Column(name="uniquid", type="string", nullable=false, unique=true)
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 */
	protected $uniquid;

	/**
	 * @Expose
	 * @Groups({"complete", "facture"})
	 * @Accessor(getter="getId")
	 */
	protected $id;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 * @Accessor(getter="getLocale")
	 */
	protected $locale;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 * @Accessor(getter="getPrixtotaltxt")
	 */
	protected $prixTotal;
	/**
	 * @Expose
	 * @Groups({"complete", "facture"})
	 * @Accessor(getter="getPrixUnitText")
	 */
	protected $prixTotalTxt;

	public function __construct() {
		$this->created = new DateTime();
		$this->user = null;
		$this->article = null;
		$this->updated = null;
		$this->quantite = 0;
		$this->position = 0;
		$this->uniquid = null;
		$this->locale = 'fr';
	}

	public function __toString() {
		return $this->getQuantite()." x ".$this->getArticle()->getNom();
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }


	public function getId() {
		$array = $this->getArrayForId();
		foreach($array as $key => $item) {
			if(is_object($item)) if(method_exists($item, 'getId'))$array[$key] = $item->getId();
		}
		return json_encode($array);
	}

	public function getArrayForId() {
		return array(
			'user' => $this->getUser(),
			'article' => $this->getArticle(),
		);
	}

	/**
	 * Set locale
	 * @param string $locale
	 * @return baseEntity
	 */
	public function setLocale($locale) {
		$this->locale = $locale;
		return $this;
	}

	/**
	 * Get locale
	 * @return string 
	 */
	public function getLocale() {
		return $this->locale;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 * Set uniquid
	 * @return panier
	 */
	public function setUniquid() {
		$this->uniquid = $this->getId();
	}

	public function getUniquid() {
		return $this->uniquid;
	}

	/**
	 * Set user
	 * @param User $user
	 * @return panier
	 */
	public function setUser(LaboUser $user) {
		$user->addPanier($this);
		$this->user = $user;
		return $this;
	}

	/**
	 * Get user
	 * @return User 
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Set article
	 * @param item $article
	 * @return panier
	 */
	public function setArticle(item $article) {
		$this->article = $article;
	
		return $this;
	}

	/**
	 * Set position
	 * @param integer $position
	 * @return panier
	 */
	public function setPosition($position) {
		$this->position = $position;
	}

	/**
	 * Get position
	 * @return integer
	 */
	public function getPosition() {
		return $this->position;
	}

	public function calculPrix($prix) {
		$aeUnits = new aeUnits;
		$ratio = $aeUnits->getRatio($this->article->getUnitPrix(), $this->article->getUnit());
		return $prix * $ratio * $this->getQuantite();
	}

	/**
	 * Get prixtotal
	 * @return float
	 */
	public function getPrixtotal() {
		return $this->calculPrix($this->article->getPrix());
	}

	/**
	 * Get prixtotal
	 * @return float
	 */
	public function getPrixtotalHT() {
		return $this->calculPrix($this->article->getPrixHT());
	}

	/**
	 * Get getPrixtotaltxt
	 * @return string
	 */
	public function getPrixtotaltxt() {
		return number_format($this->getPrixtotal(), 2, ",", "");
	}

	/**
	 * Get text of prix and devise (please, use raw filter in twig)
	 * @return string (of html code)
	 */
	public function getPrixUnitText() {
		$devise = $this->getArticle()->getDevise()['symb'];
		return $this->getPrixtotaltxt().'<small><sup>'.$devise.'TTC</sup></small>';
	}

	/**
	 * Get getPrixtotaltxt
	 * @return string
	 */
	public function getPrixtotalHTtxt() {
		return number_format($this->getPrixtotalHT(), 2, ",", "");
	}

	/**
	 * Get article
	 * @return item 
	 */
	public function getArticle() {
		return $this->article;
	}

	/**
	 * Set quantite
	 * @param integer $quantite
	 * @return panier
	 */
	public function setQuantite($quantite) {
		$this->quantite = $quantite;
		return $this;
	}

	/**
	 * ajouteQuantite
	 * @param integer $quantite
	 * @return panier
	 */
	public function ajouteQuantite($quantite) {
		$this->quantite += $quantite;
		return $this;
	}

	/**
	 * retireQuantite
	 * @param integer $quantite
	 * @return panier
	 */
	public function retireQuantite($quantite) {
		$this->quantite -= $quantite;
		if($this->quantite < 0) $this->quantite = 0;
		return $this;
	}

	/**
	 * Get quantite
	 * @return integer 
	 */
	public function getQuantite() {
		return $this->quantite;
	}

	/**
	 * Set created
	 * @param DateTime $created
	 * @return panier
	 */
	public function setCreated(DateTime $created) {
		$this->created = $created;
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function updateDate() {
		$this->setUpdated(new DateTime());
	}

	/**
	 * Set updated
	 * @param DateTime $updated
	 * @return panier
	 */
	public function setUpdated(DateTime $updated) {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime 
	 */
	public function getUpdated() {
		return $this->updated;
	}



}

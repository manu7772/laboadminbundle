<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\tier;
use Labo\Bundle\AdminBundle\Entity\baseevenement;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \DateTime;

/**
 * calendar
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\calendarRepository")
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * 
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
abstract class calendar extends baseEntity {

	// http://php.net/manual/en/class.datetime.php#datetime.constants.types
	// const FC_FORMAT = DateTime::ATOM;
	// const FC_FORMAT = 'Y-m-d H:i:s';

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Expose
	 * @Groups({"complete", "fullcalendar"})
	 * @Accessor(getter="getIdForFC")
	 */
	protected $id;

	/**
	 * @var DateTime
	 * @ORM\Column(name="start", type="datetime", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $startDate;

	/**
	 * @var DateTime
	 * @ORM\Column(name="end", type="datetime", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $endDate;

	/**
	 * @var boolean
	 * @ORM\Column(name="allday", type="boolean", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "fullcalendar"})
	 */
	protected $allDay;

	/**
	 * @var string
	 * @ORM\Column(name="url", type="string", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "fullcalendar"})
	 */
	protected $url;

	/**
	 * @var string
	 * @ORM\Column(name="color", type="string", length=32, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "fullcalendar"})
	 */
	protected $color;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\tier", inversedBy="calendars")
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete"})
	 * @MaxDepth(2)
	 */
	protected $tier;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\baseevenement", inversedBy="calendars")
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete"})
	 * @MaxDepth(2)
	 */
	protected $evenement;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="calendars")
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete"})
	 * @MaxDepth(2)
	 */
	protected $usercal;

	/**
	 * @Expose
	 * @Groups({"fullcalendar"})
	 * @Accessor(getter="getStartFcFormat")
	 */
	protected $start;

	/**
	 * @Expose
	 * @Groups({"fullcalendar"})
	 * @Accessor(getter="getTitle")
	 */
	protected $title;

	/**
	 * @Expose
	 * @Groups({"fullcalendar"})
	 * @Accessor(getter="getEndFcFormat")
	 */
	protected $end;

	/**
	 * @Expose
	 * @Groups({"complete", "fullcalendar"})
	 * @Accessor(getter="getOwner")
	 */
	protected $owner;

	/**
	 * @Expose
	 * @Groups({"complete", "fullcalendar"})
	 * @Accessor(getter="getLocale")
	 */
	protected $locale;


	public function __construct() {
		parent::__construct();
		$this->nom = '';
		$this->startDate = new DateTime();
		$this->endDate = null;
		$this->allDay = false;
		$this->url = null;
		$this->color = null;
		$this->tier = null;
		$this->evenement = null;
		$this->usercal = null;
	}

	public function __toString() {
		return $this->getNom();
	}

	public function getIdForFC() {
		if($this->id === null) return rand(100000, 999999).'#';
		return $this->id.'@';
	}

	/**
	 * Un élément par défaut dans la table est-il optionnel ?
	 * @return boolean
	 */
	public function isDefaultNullable() {
		return true;
	}

	/**
	 * Peut'on attribuer plusieurs éléments par défaut ?
	 * true 		= illimité
	 * integer 		= nombre max. d'éléments par défaut
	 * false, 0, 1 	= un seul élément
	 * @return boolean
	 */
	public function isDefaultMultiple() {
		return true;
	}

	/**
	 * Get title (nom)
	 * @return string 
	 */
	public function getTitle() {
		return $this->getNom();
	}

	/**
	 * Get start
	 * @return string 
	 */
	public function getStartFcFormat() {
		return $this->startDate->format(DATE_ATOM);
	}
	/**
	 * Get startDate
	 * @return DateTime 
	 */
	public function getStartDate() {
		return $this->startDate;
	}
	/**
	 * Set startDate
	 * @param DateTime $startDate
	 * @return calendar 
	 */
	public function setStartDate(DateTime $startDate) {
		$this->startDate = $startDate;
		return $this;
	}

	/**
	 * Get end
	 * @return string
	 */
	public function getEndFcFormat() {
		return $this->endDate instanceOf DateTime ? $this->endDate->format(DATE_ATOM) : null;
	}
	/**
	 * Get endDate
	 * @return DateTime  / null
	 */
	public function getEndDate() {
		return $this->endDate;
	}
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 * Verif endDate
	 * @return calendar
	 */
	public function verifDates() {
		if($this->endDate === null) {
			$this->endDate = clone $this->startDate;
			$this->endDate->modify('+1 hour');
		}
		if($this->endDate < $this->startDate) {
			$this->endDate = clone $this->startDate;
			$this->endDate->modify('+1 hour');
		}
		if($this->allDay) {
			$this->startDate->modify('today');
			$this->endDate->modify('today');
		}
		return $this;
	}
	/**
	 * Set endDate
	 * @param DateTime $endDate = null
	 * @return calendar
	 */
	public function setEndDate(DateTime $endDate = null) {
		$this->endDate = $endDate;
		// $this->verifDates();
		return $this;
	}

	/**
	 * Get allDay
	 * @return boolean 
	 */
	public function getAllDay() {
		return $this->allDay;
	}
	/**
	 * Set allDay
	 * @param boolean $allDay
	 * @return calendar 
	 */
	public function setAllDay($allDay) {
		$this->allDay = $allDay;
		return $this;
	}

	/**
	 * Get url
	 * @return string 
	 */
	public function getUrl() {
		return $this->url;
	}
	/**
	 * Set url
	 * @param string $url = null
	 * @return calendar 
	 */
	public function setUrl($url = null) {
		$this->url = $url;
		return $this;
	}

	/**
	 * Get color
	 * @return string 
	 */
	public function getColor() {
		return $this->color;
	}
	/**
	 * Set color
	 * @param string $color = null
	 * @return calendar 
	 */
	public function setColor($color = null) {
		$this->color = $color;
		return $this;
	}


	public function getOwnerObject() {
		if($this->tier instanceOf tier) return $this->tier;
		if($this->evenement instanceOf baseevenement) return $this->evenement;
		if($this->usercal instanceOf LaboUser) return $this->usercal;
		return null;
	}

	public function getOwner() {
		$prop = $this->getOwnerObject();
		if($prop !== null) return array(
			'slug' => $prop->getSlug(),
			'class' => $prop->getShortName(),
		);
		return $prop;
	}

	public function removeOwner() {
		if($this->getOwnerObject() instanceOf tier) return $this->removeTier();
			else if($this->getOwnerObject() instanceOf baseevenement) return $this->removeEvenement();
			else if($this->getOwnerObject() instanceOf LaboUser) return $this->removeUsercal();
		return $this;
	}

	/**
	 * set owner
	 * @param object $owner = null
	 * @return calendar
	 */
	public function setOwner($owner = null) {
		if($owner instanceOf tier) return $this->setTier($owner);
			else if($owner instanceOf baseevenement) return $this->setEvenement($owner);
			else if($owner instanceOf LaboUser) return $this->setUsercal($owner);
			else if($owner === null) $this->removeOwner();
			else throw new Exception("Owner for calendar is not valid! Must be instance of tier, baseevenement or LaboUser, or NULL.", 1);
		return $this;
	}

	/**
	 * set tier
	 * @param tier $tier
	 * @return tier / null
	 */
	public function getTier() {
		return $this->tier;
	}

	/**
	 * set tier
	 * @param tier $tier = null
	 * @return calendar
	 */
	public function setTier(tier $tier = null) {
		$this->removeOwner();
		if($tier instanceOf tier) {
			$this->tier = $tier;
			$tier->addCalendar($this);
		}
		return $this;
	}

	/**
	 * @ORM\PreRemove
	 * Remove tier
	 * @return boolean
	 */
	public function removeTier() {
		$r = false;
		if($this->tier instanceOf tier) $r = $this->tier->removeCalendar($this);
		if($r) $this->tier = null;
		return $r;
	}

	/**
	 * get baseevenement cal
	 * @return evenement / null
	 */
	public function getEvenement() {
		return $this->evenement;
	}

	/**
	 * set baseevenement cal
	 * @param baseevenement $evenement = null
	 * @return calendar
	 */
	public function setEvenement(baseevenement $evenement = null) {
		$this->removeOwner();
		if($evenement instanceOf baseevenement) {
			$this->evenement = $evenement;
			$evenement->addCalendar($this);
		}
		return $this;
	}

	/**
	 * @ORM\PreRemove
	 * Remove baseevenement
	 * @return boolean
	 */
	public function removeEvenement() {
		$r = false;
		if($this->evenement instanceOf baseevenement) $r = $this->evenement->removeCalendar($this);
		if($r) $this->evenement = null;
		return $r;
	}

	/**
	 * get user cal
	 * @return LaboUser / null
	 */
	public function getUsercal() {
		return $this->usercal;
	}

	/**
	 * set user cal
	 * @param LaboUser $usercal = null
	 * @return calendar
	 */
	public function setUsercal(LaboUser $usercal = null) {
		$this->removeOwner();
		if($usercal instanceOf LaboUser) {
			$this->usercal = $usercal;
			$usercal->addCalendar($this);
		}
		return $this;
	}

	/**
	 * @ORM\PreRemove
	 * Remove user cal
	 * @return boolean
	 */
	public function removeUsercal() {
		$r = false;
		if($this->usercal instanceOf LaboUser) $r = $this->usercal->removeCalendar($this);
		if($r) $this->usercal = null;
		return $r;
	}

}

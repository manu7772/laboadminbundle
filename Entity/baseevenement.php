<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;

use Labo\Bundle\AdminBundle\Entity\nested;
use Labo\Bundle\AdminBundle\Entity\calendar;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\Entity\userevenement;
use Labo\Bundle\AdminBundle\Entity\baseuserevenement;
use site\adminsiteBundle\Entity\adresse;

use \DateTime;
use \Exception;

class modelUser extends LaboUser {}

/**
 * baseevenement
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\baseevenementRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @ExclusionPolicy("all")
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 */
abstract class baseevenement extends nested {

	const NOFORM = '___NO_FORM___';

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="nom", type="string", length=128, nullable=false, unique=false)
	 * @Assert\NotBlank(message = "Vous devez remplir ce champ.")
	 * @Assert\Length(
	 *      min = "2",
	 *      max = "128",
	 *      minMessage = "Le nom doit comporter au moins {{ limit }} lettres.",
	 *      maxMessage = "Le nom doit comporter au maximum {{ limit }} lettres."
	 * )
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $nom;

	/**
	 * @var string
	 * @ORM\Column(name="soustitre", type="string", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $soustitre;

	/**
	 * @var string
	 * @ORM\Column(name="conditions", type="text", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $conditions;

	/**
	 * @var integer - PROPRIÉTAIRE
	 * @ORM\OneToOne(targetEntity="site\adminsiteBundle\Entity\adresse", cascade={"all"}, inversedBy="evenement")
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $adresse;

	/**
     * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\calendar", mappedBy="evenement")
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 * @ORM\OrderBy({"startDate" = "DESC"})
	 */
	protected $calendars;

	/**
	 * @var string
	 * @ORM\Column(name="telfixe", type="string", length=14, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $telfixe;

	/**
	 * @var string
	 * @ORM\Column(name="mobile", type="string", length=14, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $mobile;

	/**
	 * @var string
	 * @ORM\Column(name="email", type="string", length=128, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $email;

	/**
	 * - INVERSE
	 * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\baseuserevenement", mappedBy="evenement", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @ORM\OrderBy({"position" = "ASC"})
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $userevenements;

	/**
	 * @var DateTime
	 * @ORM\Column(name="debut", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $debut;

	/**
	 * @var DateTime
	 * @ORM\Column(name="fin", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $fin;

	/**
	 * @var float
	 * @ORM\Column(name="tarif", type="decimal", scale=2, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $tarif;

	/**
	 * @var array
	 * @ORM\Column(name="detailtarifs", type="json_array", nullable=true, unique=false)
	 */
	protected $detailtarifs;

	/**
	 * @var integer
	 * Délai pour modifications (en heures) avant l'évènement
	 * @ORM\Column(name="delaimodif", type="integer", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $delaimodif;

	/**
	 * @var integer
	 * @ORM\Column(name="places", type="integer", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "inforesa"})
	 */
	protected $places;

	/**
	 * @var integer
	 * @ORM\Column(name="multiplaces", type="integer", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $multiplaces;

	/**
	 * @var boolean
	 * @ORM\Column(name="listeattente", type="boolean", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $listeattente;

	/**
	 * @Expose
	 * @Groups({"complete", "inforesa"})
	 * @Accessor(getter="getNumberofusers")
	 */
	protected $numberofusers;

	/**
	 * @Expose
	 * @Groups({"complete", "inforesa"})
	 * @Accessor(getter="getNumberofreservations")
	 */
	protected $numberofreservations;

	/**
	 * @Expose
	 * @Groups({"complete", "inforesa"})
	 * @Accessor(getter="getRestnumberofreservations")
	 */
	protected $restnumberofreservations;

	/**
	 * @Expose
	 * @Groups({"complete"})
	 * @Accessor(getter="getTableautarifs")
	 */
	protected $tableautarifs;

	public function __construct() {
		parent::__construct();
		$this->soustitre = null;
		$this->conditions = null;
		$this->adresse = null;
		$this->telfixe = null;
		$this->mobile = null;
		$this->email = null;
		$this->calendars = new ArrayCollection();
		$this->userevenements = new ArrayCollection();
		$this->debut = null;
		$this->fin = null;
		$this->tarif = 295;
		$this->detailtarifs = array();
		$this->initTarifs();
		$this->delaimodif = 48; // 48 heures
		$this->places = 0;
		$this->listeattente = true;
		$this->multiplaces = 0;
	}

	/**
	 * Un élément par défaut dans la table est-il optionnel ?
	 * @return boolean
	 */
	public function isDefaultNullable() {
		return true;
	}

	/**
	 * On peut attribuer plusieurs éléments par défaut ?
	 * true 		= illimité
	 * integer 		= nombre max. d'éléments par défaut
	 * false, 0, 1 	= un seul élément
	 * @return boolean
	 */
	public function isDefaultMultiple() {
		return true;
	}

	/**
	 * Renvoie l'image principale
	 * @return image
	 */
	public function getMainMedia() {
		if($this->getImage() !== null) return $this->getImage();
		return null;
	}


	/**
	 * @ORM\PostLoad
	 */
	public function postLoad() {
		$this->repositionEvenements();
		if(!is_array($this->detailtarifs)) $this->initTarifs();
			else if(count($this->detailtarifs) < 1) $this->initTarifs();
		return $this;
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function verif() {
		if($this->multiplaces > $this->places) $this->setMultiplaces($this->places);
		if(!($this->fin instanceOf DateTime) && ($this->getDebut() instanceOf DateTime)) $this->fin = clone $this->getDebut();
		if($this->fin < $this->getDebut()) $this->fin = clone $this->getDebut();
		// $this->detailtarifs = array('test1' => "ok1", 'test2' => "ok2", 'test3' => array('ok3_1','ok3_2','ok3_3'));
	}

	public function getModelUser() {
		return new modelUser();
	}

	public function getNOFORMlabel() { return self::NOFORM; }

	public function initTarifs($setDefaults = true) {
		// data from user
		$modelUser = $this->getModelUser();
		$tarifsTableOptions = $this->getTarifsTableOptions();
		// echo('<h3>Initialisation Tarifs :</h3>');
		foreach($tarifsTableOptions['options'] as $ke1 => $option) {
			foreach($tarifsTableOptions['items'] as $ke2 => $item) {
				$value = $this->getNOFORMlabel();
				if(in_array($ke2, $option['items'])) {
					if(isset($option['default'])) $value = $option['default'];
						elseif(isset($item['default'])) $value = $item['default'];
				}
				// echo('<p>Value '.$ke1.' > '.$ke2.' : '.json_encode($value).'</p>');
				if(isset($option['field'])) {
					// FIELD
					$get = 'get'.ucfirst($option['field']).'s';
					if(method_exists($modelUser, $get)) {
						// multiple options
						$listOptions = $modelUser->$get();
						$this->detailtarifs[$option['field']]['_type'] = 'field_multiple';
						foreach($listOptions as $optionKey => $optionName) {
							$this->detailtarifs[$option['field']]['values'][$optionKey]['values'][$ke2] = $value;
							$this->detailtarifs[$option['field']]['values'][$optionKey]['name'] = $optionName;
						}
					} else {
						// single option
						$this->detailtarifs[$option['field']]['_type'] = 'field_single';
						$this->detailtarifs[$option['field']]['values'][$ke2] = $value;
					}
				} elseif(isset($option['special'])) {
					// SPECIAL
					$this->detailtarifs[$option['special']]['_type'] = 'special';
					$this->detailtarifs[$option['special']]['values'][$ke2] = $value;
				}
			}
		}
		if($setDefaults) {
			// default values…
			foreach ($modelUser->getStatutsocials() as $statutsocialKey => $statutsocialName) {
				if($statutsocialKey !== 0) {
					$this->detailtarifs['statutsocial']['values'][$statutsocialKey]['values']['user'] = '-25%';
					$this->detailtarifs['statutsocial']['values'][$statutsocialKey]['values']['invited'] = '-25%';
				}
			}
			foreach ($modelUser->getUniontypes() as $unionKey => $unionName) {
				if($unionKey !== $modelUser->getRemoveUniontype()) {
					$this->detailtarifs['Unionwithevenementhost']['values'][$unionKey]['values']['invited'] = '-25%';
				}
			}
			$this->detailtarifs['parrainage']['values']['user'] = '-10%';
			$this->detailtarifs['cumulatif']['values']['arrhes'] = true;
			$this->detailtarifs['default']['values']['arrhes'] = '50€';
			$this->detailtarifs['default']['values']['access'] = true;
		}
	}

	public function getTarifsTableOptions() {
		return array(
			'options' => array( // ROWS
				'parrainage' => array(
					'special' => 'parrainage',
					'name' => 'tableoptions.parrainage',
					'input_type' => 'text',
					'type' => 'percent',
					'items' => array('user', 'invited'),
					'default' => '0%',
					),
				'cumulatif' => array(
					'special' => 'cumulatif',
					'name' => 'tableoptions.cumulatif',
					'input_type' => 'checkbox',
					'type' => 'boolean',
					'items' => array('user', 'invited', 'arrhes'),
					'default' => false,
					),
				'default' => array(
					'special' => 'default',
					'name' => 'tableoptions.default',
					'items' => array('user', 'invited', 'arrhes', 'access'),
					'default' => '0%',
					),
				'statutsocial' => array(
					'field' => 'statutsocial',
					'name' => 'tableoptions.statutsocial',
					'unique' => false,
					'nullable' => false,
					'multiple' => false,
					'access_option' => true,
					'items' => array('user', 'invited', 'arrhes', 'access'),
					// 'default' => null,
					),
				'Unionwithevenementhost' => array(
					'field' => 'Unionwithevenementhost',
					'name' => 'tableoptions.union',
					'unique' => true,
					'nullable' => true,
					'multiple' => false,
					'access_option' => false,
					'items' => array('user', 'invited', 'arrhes'),
					// 'default' => null,
					),
				'sexe' => array(
					'field' => 'sexe',
					'name' => 'tableoptions.sexe',
					'unique' => false,
					'nullable' => false,
					'multiple' => false,
					'access_option' => true,
					'items' => array('user', 'invited', 'arrhes', 'access'),
					// 'default' => null,
					),
				),
			'items' => array( // COLUMNS
				'user' => array(
					'name' => 'tableoptions.user',
					'input_type' => 'text',
					'type' => 'percent',
					'default' => '0%',
					),
				'invited' => array(
					'name' => 'tableoptions.invited',
					'input_type' => 'text',
					'type' => 'percent',
					'default' => '0%',
					),
				'arrhes' => array(
					'name' => 'tableoptions.arrhes',
					'input_type' => 'text',
					'type' => 'money',
					'default' => '0€',
					),
				'access' => array(
					'name' => 'tableoptions.access',
					'input_type' => 'checkbox',
					'type' => 'boolean',
					'default' => true,
					),
				),
			);
	}


	public function setDetailtarifs($detailtarifs) {
		if(is_string($detailtarifs)) $detailtarifs = json_decode($detailtarifs, true);
		$this->detailtarifs = $detailtarifs;
		return $this;
	}

	public function getDetailtarifs($json_encoded = false) {
		return $json_encoded ? json_encode($this->detailtarifs) : $this->detailtarifs;
	}

	/**
	 * Set conditions
	 * @param string $conditions
	 * @return baseevenement
	 */
	public function setConditions($conditions = null) {
		$this->conditions = $conditions;
		return $this;
	}

	/**
	 * Get conditions
	 * @return string
	 */
	public function getConditions() {
		return $this->conditions;
	}

	/**
	 * Set adresse - PROPRIÉTAIRE
	 * @param adresse $adresse
	 * @return baseevenement
	 */
	public function setAdresse(adresse $adresse = null) {
		$this->adresse = $adresse;
		return $this;
	}

	/**
	 * Get adresse - PROPRIÉTAIRE
	 * @return adresse 
	 */
	public function getAdresse() {
		return $this->adresse;
	}

	protected function formatTel($tel) {
		// return preg_replace_callback(
		// 	'#(\d)#',
		// 	function($matches) {
		// 		return $matches[0];
		// 	},
		// 	$tel
		// );
		return $tel;
	}

	/**
	 * Get tarif
	 * @return float
	 */
	public function getTarif() {
		return $this->tarif;
	}

	/**
	 * Set tarif
	 * @param mixed $tarif
	 * @return baseevenement
	 */
	public function setTarif($tarif) {
		$this->tarif = floatval(str_replace(',', '.', $tarif));
		return $this;
	}

	/**
	 * Get telfixe
	 * @return string
	 */
	public function getTelfixe() {
		return $this->formatTel($this->telfixe);
	}

	/**
	 * Set telfixe
	 * @param string $telfixe
	 * @return baseevenement
	 */
	public function setTelfixe($telfixe) {
		$this->telfixe = $this->formatTel($telfixe);
		return $this;
	}


	/**
	 * Get mobile
	 * @return string
	 */
	public function getMobile() {
		return $this->formatTel($this->mobile);
	}

	/**
	 * Set mobile
	 * @param string $mobile
	 * @return baseevenement
	 */
	public function setMobile($mobile) {
		$this->mobile = $this->formatTel($mobile);
		return $this;
	}

	/**
	 * Get email
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set email
	 * @param string $email
	 * @return baseevenement
	 */
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Get calendars
	 * @return arrayCollection
	 */
	public function getCalendars() {
		return $this->calendars;
	}

	/**
	 * Set calendars
	 * @param ArrayCollection $calendars
	 * @return baseevenement
	 */
	public function setCalendars(ArrayCollection $calendars) {
		$this->calendars = $calendars;
		return $this;
	}

	/**
	 * add calendar
	 * @param calendar $calendar
	 * @return baseevenement
	 */
	public function addCalendar(calendar $calendar) {
		$this->calendars->add($calendar);
		return $this;
	}

	/**
	 * remove calendar
	 * @param calendar $calendar
	 * @return boolean
	 */
	public function removeCalendar(calendar $calendar) {
		return $this->calendars->removeElement($calendar);
	}



	public function repositionEvenements() {
		$userevenements = $this->userevenements->toArray();
		usort($userevenements, function($a, $b) {
			return $a->getCreated()->format('U') - $b->getCreated()->format('U');
		});
		$this->userevenements = new ArrayCollection($userevenements);
		$position = 1;
		foreach ($this->userevenements as $userevenement) {
			$userevenement->setPosition($position++);
		}
	}

	/**
	 * Add userevenement
	 * @param userevenement $userevenement
	 * @return baseevenement
	 */
	public function addUserevenement(baseuserevenement $userevenement) {
		if(!$this->userevenements->contains($userevenement)) $this->userevenements->add($userevenement);
		$this->repositionEvenements();
		return $this;
	}

	/**
	 * Remove user
	 * @param userevenement $userevenement
	 * @return boolean
	 */
	public function removeUserevenement(baseuserevenement $userevenement) {
		$result = $this->userevenements->removeElement($userevenement);
		$this->repositionEvenements();
		return $result;
	}

	/**
	 * Get userevenements
	 * @return ArrayCollection
	 */
	public function getUserevenements() {
		return $this->userevenements;
	}

	/**
	 * Set userevenements
	 * @param ArrayCollection $userevenements
	 * @return baseevenement
	 */
	public function setUserevenements(ArrayCollection $userevenements) {
		foreach($this->getUserevenements() as $userevenement)
			if(!$userevenements->contains($userevenement)) $userevenement->remove();
		foreach($userevenements as $userevenement)
			if(!$this->getUserevenements()->contains($userevenement)) $userevenement->setEvenement($this);
	}

	/**
	 * Get userevenement of user
	 * @param LaboUser $user
	 * @return userevenement
	 */
	public function getUserevenement(LaboUser $user) {
		foreach($this->getUserevenements() as $userevenement) {
			if($userevenement->getUser() === $user) return $userevenement;
		}
		return null;
	}

	/**
	 * @Assert\IsFalse(message="Certains inscrits sont en doublon pour cet évènement.")
	 */
	public function hasUserdouble() {
		$users = new ArrayCollection();
		$userevenements = $this->getUserevenements();
		foreach ($userevenements as $userevenement) {
			if($users->contains($userevenement->getUser())) return true;
			$users->add($userevenement->getUser());
		}
		return false;
	}

	/**
	 * Get users
	 * @return ArrayCollection
	 */
	public function getUsers() {
		$users = new ArrayCollection();
		foreach($this->getUserevenements() as $userevenement) $users->add($userevenement->getUser());
		return $users;
	}

	/**
	 * Get number of users
	 * @return integer
	 */
	public function getNumberofusers() {
		return count($this->getUsers());
	}

	/**
	 * Get number of reservations
	 * @return integer
	 */
	public function getNumberofreservations() {
		return count($this->getUserevenements());
	}

	/**
	 * Get number of resting reservations
	 * @return integer
	 */
	public function getRestnumberofreservations() {
		$rest = $this->getPlaces() - $this->getNumberofreservations();
		return $rest < 0 ? 0 : $rest;
	}

	/**
	 * Add user
	 * @param LaboUser $user
	 * @return boolean
	 */
	public function addUser(LaboUser $user, $state = null) {
		if(!$this->hasUser($user)) {
			$userevenement = new userevenement();
			$userevenement->setUser($user);
			$userevenement->setState($state);
			$userevenement->setEvenement($this);
			return true;
		}
		return false;
	}

	/**
	 * Remove user
	 * @param LaboUser $user
	 * @return boolean
	 */
	public function removeUser(LaboUser $user) {
		$userevenement = $this->getUserevenement($user);
		if($userevenement instanceOf userevenement) {
			$userevenement->remove();
			return true;
		}
		return false;
	}

	/**
	 * Set users
	 * @param arrayCollection $users
	 * @return baseevenement
	 */
	public function setUsers(ArrayCollection $users, $state = null) {
		$existantUsers = $this->getUsers();
		foreach($users as $user)
			if(!$existantUsers->contains($user)) $this->addUser($user, $state);
		foreach($existantUsers as $existantUser)
			if(!$users->contains($existantUser)) $this->removeUser($existantUser);
		return $this;
	}

	/**
	 * Has user
	 * @param LaboUser $user
	 * @return boolean
	 */
	public function hasUser(LaboUser $user) {
		return $this->getUsers()->contains($user);
	}






	/**
	 * Set debut DateTime
	 * @return baseevenement
	 */
	public function setDebut(DateTime $debut) {
		$this->debut = $debut;
		return $this;
	}

	/**
	 * Get debut DateTime
	 * @return DateTime
	 */
	public function getDebut() {
		return $this->debut;
	}

	public function isPast() {
		return new DateTime > $this->getDebut();
	}

	public function isTerminated() {
		return new DateTime > $this->getFin();
	}

	/**
	 * Is reservable
	 * @param LaboUser $user
	 * @return boolean
	 */
	public function isReservable(LaboUser $user = null) {
		$result = $this->places > 0;
		$limit = new DateTime('tomorrow'); // Ce soir à minuit !
		$result = $result && $this->debut >= $limit;
		if($user instanceOf LaboUser) {
			//
		}
		return $result;
	}

	/**
	 * Is changeable
	 * @param LaboUser $user
	 * @return boolean
	 */
	public function isChangeable(LaboUser $user = null) {
		$result = $this->isPast();
		$result = $result && count($this->getUserevenements()) < 1;
		if($user instanceOf LaboUser) {
			//
		}
		return $result;
	}

	/**
	 * Set fin DateTime = null
	 * @return baseevenement
	 */
	public function setFin(DateTime $fin = null) {
		if(!($fin instanceOf DateTime) && ($this->getDebut() instanceOf DateTime)) $fin = clone $this->getDebut();
		$this->fin = $fin;
		if($this->fin < $this->getDebut()) $this->fin = clone $this->getDebut();
		return $this;
	}

	/**
	 * Get fin DateTime
	 * @return DateTime
	 */
	public function getFin() {
		return $this->fin;
	}

	public function getInfoDuree() {
		$infos = array();
		$ecart = $this->debut->diff($this->fin);
		$infos['days'] = intval($ecart->format('%R%a')) + 1;
		$infos['hours'] = intval($ecart->format('%R%h'));
		$infos['minutes'] = intval($ecart->format('%R%i'));
		$infos['seconds'] = intval($ecart->format('%R%s'));
		return $infos;
	}

	/**
	 * Is evenement between two dates
	 * @param DateTime $dateDebut = null
	 * @param DateTime $dateFin = null
	 * @return boolean
	 */
	public function isEvenementBetween(DateTime $dateDebut = null, DateTime $dateFin = null) {
		if($dateDebut === null && $dateFin === null) return true;
		$evenementFin = $this->getFin();
		$evenementDebut = $this->getDebut();
		if(!($evenementFin instanceOf DateTime)) $evenementFin = clone $evenementDebut;
		if($dateFin === null) return $dateDebut < $evenementFin;
		if($dateDebut === null) return $dateFin < $evenementDebut;
	}

	public function setDelaimodif($delaimodif) {
		$this->delaimodif = intval($delaimodif);
		if($this->delaimodif < 0) $this->delaimodif = 0;
		return $this;
	}
	public function getDelaimodif() {
		return $this->delaimodif;
	}


	/**
	 * Get places
	 * @return integer
	 */
	public function getPlaces() {
		return $this->places;
	}

	/**
	 * Set places
	 * @param integer $places = 0
	 * @return baseevenement
	 */
	public function setPlaces($places = 0) {
		$this->places = intval($places);
		if($this->places < 0) $this->places = 0;
		return $this;
	}

	/**
	 * Get listeattente
	 * @return boolean
	 */
	public function getListeattente() {
		return $this->listeattente;
	}

	/**
	 * Set listeattente
	 * @param boolean $listeattente
	 * @return baseevenement
	 */
	public function setlisteattente($listeattente) {
		$this->listeattente = $listeattente;
		return $this;
	}

	/**
	 * Get multiplaces
	 * @return integer
	 */
	public function getMultiplaces() {
		return $this->multiplaces;
	}

	/**
	 * Set multiplaces
	 * @param integer $multiplaces = 0
	 * @return baseevenement
	 */
	public function setMultiplaces($multiplaces = 0) {
		$this->multiplaces = intval($multiplaces);
		if($this->multiplaces < 0) $this->multiplaces = 0;
		return $this;
	}



}




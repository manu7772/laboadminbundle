<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;

use Labo\Bundle\AdminBundle\Entity\nested;
use Labo\Bundle\AdminBundle\Entity\calendar;
use site\adminsiteBundle\Entity\categorie;
use site\adminsiteBundle\Entity\adresse;
use site\adminsiteBundle\Entity\image;
use site\adminsiteBundle\Entity\facture as siteFacture; // facture

use \DateTime;
use \Exception;

/**
 * tier
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\tierRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @ExclusionPolicy("all")
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 */
abstract class tier extends nested {

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $id;

	/**
	 * @var integer - PROPRIÉTAIRE
	 * @ORM\OneToOne(targetEntity="site\adminsiteBundle\Entity\adresse", cascade={"all"}, inversedBy="tier")
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $adresse;

	/**
     * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\calendar", orphanRemoval=true, mappedBy="tier")
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 * @ORM\OrderBy({"startDate" = "DESC"})
	 */
	protected $calendars;

	/**
     * @ORM\OneToOne(targetEntity="site\adminsiteBundle\Entity\image", orphanRemoval=true, cascade={"all"})
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 */
	protected $logo;

	/**
	 * @var string
	 * @ORM\Column(name="telfixe", type="string", length=14, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $telfixe;

	/**
	 * @var string
	 * @ORM\Column(name="mobile", type="string", length=14, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $mobile;

	/**
	 * @var string
	 * @ORM\Column(name="email", type="string", length=128, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 * @Assert\Email(strict=true, message="Le format de l'email est incorrect")
	 * @Assert\Email(checkMX=true, message="Aucun serveur mail n'a été trouvé pour ce domaine")
	 */
	protected $email;

	/**
	 * - INVERSE
	 * @ORM\OneToMany(targetEntity="site\adminsiteBundle\Entity\facture", mappedBy="boutique", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
	 * @ORM\OrderBy({"created" = "DESC"})
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $factures;


	public function __construct() {
		parent::__construct();
		$this->adresse = null;
		$this->telfixe = null;
		$this->mobile = null;
		$this->email = null;
		$this->calendars = new ArrayCollection();
		$this->factures = new ArrayCollection();
	}

	/**
	 * Renvoie l'image principale
	 * @return image
	 */
	public function getMainMedia() {
		if($this->getLogo() !== null) return $this->getLogo();
		if($this->getImage() !== null) return $this->getImage();
		return null;
	}

	/**
	 * Set adresse - PROPRIÉTAIRE
	 * @param adresse $adresse
	 * @return tier
	 */
	public function setAdresse(adresse $adresse = null) {
		// $adresse->setTier($this);
		$this->adresse = $adresse;
		return $this;
	}

	/**
	 * Get adresse - PROPRIÉTAIRE
	 * @return adresse 
	 */
	public function getAdresse() {
		return $this->adresse;
	}

	/**
	 * Set logo - PROPRIÉTAIRE
	 * @param image $logo
	 * @return tier
	 */
	public function setLogo(image $logo = null) {
		if($this->logo != null && $logo == null) {
			$this->logo->setElement(null);
		}
		$this->logo = $logo;
		if($this->logo != null) {
			$this->logo->setElement($this, 'logo');
			$this->logo->setStatut($this->getStatut());
		}
		return $this;
	}

	/**
	 * Get logo - PROPRIÉTAIRE
	 * @return image 
	 */
	public function getLogo() {
		return $this->logo;
	}

	protected function formatTel($tel) {
		// return preg_replace_callback(
		// 	'#(\d)#',
		// 	function($matches) {
		// 		return $matches[0];
		// 	},
		// 	$tel
		// );
		return $tel;
	}

	/**
	 * Get telfixe
	 * @return string
	 */
	public function getTelfixe() {
		return $this->formatTel($this->telfixe);
	}

	/**
	 * Set telfixe
	 * @param string $telfixe
	 * @return tier
	 */
	public function setTelfixe($telfixe) {
		$this->telfixe = $this->formatTel($telfixe);
		return $this;
	}


	/**
	 * Get mobile
	 * @return string
	 */
	public function getMobile() {
		return $this->formatTel($this->mobile);
	}

	/**
	 * Set mobile
	 * @param string $mobile
	 * @return tier
	 */
	public function setMobile($mobile) {
		$this->mobile = $this->formatTel($mobile);
		return $this;
	}

	/**
	 * Get email
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set email
	 * @param string $email
	 * @return tier
	 */
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Get calendars
	 * @return arrayCollection
	 */
	public function getCalendars() {
		return $this->calendars;
	}

	/**
	 * Set calendars
	 * @param ArrayCollection $calendars
	 * @return tier
	 */
	public function setCalendars(ArrayCollection $calendars) {
		$this->calendars = $calendars;
		return $this;
	}

	/**
	 * add calendar
	 * @param calendar $calendar
	 * @return tier
	 */
	public function addCalendar(calendar $calendar) {
		$this->calendars->add($calendar);
		return $this;
	}

	/**
	 * remove calendar
	 * @param calendar $calendar
	 * @return boolean
	 */
	public function removeCalendar(calendar $calendar) {
		return $this->calendars->removeElement($calendar);
	}

	/**
	 * Get factures - INVERSE
	 * @return ArrayCollection 
	 */
	public function getFactures() {
		return $this->factures;
	}

	/**
	 * Set factures - INVERSE
	 * @param ArrayCollection $factures
	 * @return tier
	 */
	public function setFactures(ArrayCollection $factures) {
		$this->factures = $factures;
		return $this;
	}

	/**
	 * Add facture - INVERSE
	 * @param siteFacture $facture
	 * @return tier
	 */
	public function addFacture(siteFacture $facture) {
		if(!$this->factures->contains($facture)) $this->factures->add($facture);
		return $this;
	}

	/**
	 * Remove facture - INVERSE
	 * @param siteFacture $facture
	 * @return boolean
	 */
	public function removeFacture(siteFacture $facture) {
		return $this->factures->removeElement($facture);
	}


}
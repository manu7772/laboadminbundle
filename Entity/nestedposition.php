<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\nested;
use Labo\Bundle\AdminBundle\services\aeData;

use \DateTime;
use \ReflectionClass;

/**
 * nestedposition
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 *
 * @ExclusionPolicy("all")
 *
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\nestedpositionRepository")
 * @ORM\Table(name="nested_position")
 * @ORM\HasLifecycleCallbacks
 */
class nestedposition {

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\nested", inversedBy="nestedpositionChilds", cascade={"persist"})
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=false, unique=false)
	 */
	protected $parent;

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\nested", inversedBy="nestedpositionParents", cascade={"persist"})
	 * @ORM\JoinColumn(name="child_id", referencedColumnName="id", nullable=false, unique=false)
	 */
	protected $child;

	/**
	 * @ORM\Column(name="nestedgroup", type="string", nullable=false, unique=false)
	 */
	protected $group;

	/**
	 * @ORM\Column(type="integer", nullable=false, unique=false)
	 */
	private $position;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
	protected $updated;


	public function __construct() {
		$this->parent = null;
		$this->child = null;
		$this->group = null;
		$this->created = new DateTime();
		$this->updated = null;
		$this->position = 0;
	}

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return $this->getId();
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }




	/**
	 * Get id
	 * @return string
	 */
	public function getId() {
		$parentId = $this->getParent() === null ? 'null' : $this->getParent()->getId();
		$childId = $this->getChild() === null ? 'null' : $this->getChild()->getId();
		$group = $this->getGroup() === null ? 'null' : $this->getGroup();
		return $parentId.'-'.$childId.'-'.$group;
	}


	/**
	 * @Assert\IsTrue(message="L'entité nestedposition n'est pas complete.")
	 */
	public function isNestedpositionValid() {
		// return ($this->getParent() !== null) && ($this->getChild() !== null) && ($this->group !== null);
		return true;
	}

	// public function isParentGroup($parent, $group) {
	// 	if($parent instanceOf nested) $parent = $parent->getId();
	// 	return ($this->getParent()->getId() === $parent) && ($group === $this->getGroup());
	// }

	/**
	 * @ORM\PreRemove
	 */
	public function onRemove() {
		$this->parent->removeNestedposition($this);
		$this->child->removeNestedposition($this);
		return $this;
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function check() {
		return $this;
	}

	/**
	 * Set parent and child at the same time
	 * @param nested $parent
	 * @param nested $child
	 * @return nestedposition
	 */
	public function setParentChild(nested $parent, nested $child, $group) {
		$this->parent = $parent;
		$this->setGroup($group);
		$this->child = $child;
		$this->parent->addNestedposition($this);
		$this->child->addNestedposition($this);
		return $this;
	}

	/**
	 * Set parent
	 * @param nested $parent
	 * @return nestedposition
	 */
	public function setParent(nested $parent, $group) {
		$this->parent = $parent;
		$this->setGroup($group);
		if($this->getChild() !== null && $this->getGroup() !== null) {
			$parent->addNestedposition($this);
		}
		return $this;
	}

	/**
	 * Get parent
	 * @return nested 
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * Set child
	 * @param nested $child
	 * @return nestedposition
	 */
	public function setChild(nested $child) {
		$this->child = $child;
		if($this->getParent() != null && $this->getGroup() !== null) {
			$child->addNestedposition($this);
		}
		return $this;
	}

	/**
	 * Get child
	 * @return nested 
	 */
	public function getChild() {
		return $this->child;
	}

	/**
	 * Set group
	 * @param string $group
	 * @return nestedposition
	 */
	public function setGroup($group) {
		$this->group = aeData::decamelize($group);
		return $this;
	}

	/**
	 * Get group
	 * @return string
	 */
	public function getGroup() {
		return $this->group;
	}

	/**
	 * Set position
	 * @param integer $position
	 * @return nestedposition
	 */
	public function setPosition($position) {
		$this->position = (integer)$position;
	}

	/**
	 * Get position
	 * @return integer
	 */
	public function getPosition() {
		return (integer)$this->position;
	}

	/**
	 * Set created
	 * @param DateTime $created
	 * @return nestedposition
	 */
	public function setCreated(DateTime $created) {
		$this->created = $created;
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function updateDate() {
		$this->setUpdated(new DateTime());
	}

	/**
	 * Set updated
	 * @param DateTime $updated
	 * @return nestedposition
	 */
	public function setUpdated(DateTime $updated) {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime 
	 */
	public function getUpdated() {
		return $this->updated;
	}



}

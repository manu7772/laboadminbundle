<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;

use Labo\Bundle\AdminBundle\Entity\statut;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \DateTime;
use \DateInterval;
use \ReflectionClass;

/**
 * message
 * 
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 * 
 * @ExclusionPolicy("all")
 */
abstract class message {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="nom", type="string", length=128, nullable=true, unique=false)
	 * @Assert\Length(
	 *      min = "3",
	 *      minMessage = "Le nom doit comporter au moins {{ limit }} lettres.",
	 * )
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $nom;

	/**
	 * @var string
	 * @ORM\Column(name="prenom", type="string", length=128, nullable=true, unique=false)
	 * @Assert\Length(
	 *      min = "3",
	 *      minMessage = "Le prénom doit comporter au moins {{ limit }} lettres.",
	 * )
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $prenom;

	/**
	 * @var string
	 * @Assert\Email(
	 *     message = "The email '{{ value }}' is not a valid email.",
	 *     checkMX = true
	 * )
	 * @ORM\Column(name="email", type="string", nullable=false, unique=false)
	 * @Gedmo\SortableGroup
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $email;

	/**
	 * @var string
	 * @ORM\Column(name="telephone", type="string", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $telephone;

	/**
	 * @var string
	 * @ORM\Column(name="objet", type="string", length=255, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $objet;

	/**
	 * @var string
	 * @ORM\Column(name="message", type="text", nullable=false, unique=false)
	 * @Assert\NotBlank(message = "entity.notblank.nom")
	 * @Assert\Length(
	 *      min = "3",
	 *      minMessage = "Le message doit comporter au moins {{ limit }} lettres.",
	 * )
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $message;

	/**
	 * @var DateTime
	 * @ORM\Column(name="creation", type="datetime", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $creation;

	/**
	 * @var string
	 * @ORM\Column(name="ip", type="string", length=32, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $ip;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\statut")
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $statut;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="messages")
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=true, unique=false, onDelete="SET NULL")
	 * @Gedmo\SortableGroup
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $user;

	/**
	 * @ORM\Column(type="integer")
	 * @Gedmo\SortablePosition
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $position;

	/**
	 * @ORM\Column(name="is_read", type="datetime", nullable=true, unique=false)
	 */
	protected $read;


	public function __construct() {
		$this->nom = null;
		$this->prenom = null;
		$this->email = null;
		$this->telephone = null;
		$this->objet = null;
		$this->message = null;
		$this->creation = new DateTime();
		$this->ip = null;
		$this->statut = null;
		$this->user = null;
		$this->read = null;
	}

	public function __toString() {
		return $this->getObjet()."/".$this->getNom();
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }





	/**
	 * Get id
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}


	/**
	 * Set read
	 * @param DateTime $DateTime = null
	 * @return message
	 */
	public function setRead(DateTime $DateTime = null) {
		if($DateTime == null) $this->read = new DateTime();
			else $this->read = $DateTime;
		return $this;
	}

	/**
	 * Get read
	 * @return DateTime
	 */
	public function getRead() {
		return $this->read;
	}

	/**
	 * Is read
	 * @return boolean
	 */
	public function isRead() {
		return $this->read != null;
	}

	/**
	 * Set nom
	 * @param string $nom
	 * @return message
	 */
	public function setNom($nom) {
		$this->nom = $nom;
		return $this;
	}

	/**
	 * Get nom
	 * @return string
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Set prenom
	 * @param string $prenom
	 * @return message
	 */
	public function setPrenom($prenom) {
		$this->prenom = $prenom;
		return $this;
	}

	/**
	 * Get prenom
	 * @return string
	 */
	public function getPrenom() {
		return $this->prenom;
	}

	/**
	 * Set email
	 * @param string $email
	 * @return message
	 */
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Get email
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set telephone
	 * @param string $telephone
	 * @return message
	 */
	public function setTelephone($telephone) {
		$this->telephone = $telephone;
		return $this;
	}

	/**
	 * Get telephone
	 * @return string
	 */
	public function getTelephone() {
		return $this->telephone;
	}

	/**
	 * Set objet
	 * @param string $objet
	 * @return message
	 */
	public function setObjet($objet) {
		$this->objet = $objet;
		return $this;
	}

	/**
	 * Get objet
	 * @return string
	 */
	public function getObjet() {
		return $this->objet;
	}

	/**
	 * Set message
	 * @param string $message
	 * @return message
	 */
	public function setMessage($message) {
		$this->message = $message;
		return $this;
	}

	/**
	 * Get message
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Set creation
	 * @param DateTime $creation
	 * @return message
	 */
	public function setCreation($creation) {
		$this->creation = $creation;
		return $this;
	}

	/**
	 * Get creation
	 * @return DateTime
	 */
	public function getCreation() {
		return $this->creation;
	}

	/**
	 * Set ip
	 * @param string $ip
	 * @return message
	 */
	public function setIp($ip) {
		$this->ip = $ip;
		return $this;
	}

	/**
	 * Get ip
	 * @return string
	 */
	public function getIp() {
		return $this->ip;
	}

	/**
	 * Set statut
	 * @param statut $statut
	 * @return message
	 */
	public function setStatut(statut $statut) {
		$this->statut = $statut;
		return $this;
	}

	/**
	 * Get statut
	 * @return statut 
	 */
	public function getStatut() {
		return $this->statut;
	}

	/**
	 * Set user
	 * @param user $user
	 * @return message
	 */
	public function setUser(LaboUser $user = null) {
		$this->user = $user;
		if(is_object($user)) {
			$this->setEmail($this->user->getEmail());
			$this->user->getNom() !== null ? $this->setNom($this->user->getNom()) : $this->setNom($this->user->getUsername());
			$this->setPrenom($this->user->getPrenom());
			$this->setTelephone($this->user->getTelephone());
		}
		return $this;
	}

	/**
	 * Get user
	 * @return user 
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Set position
	 * @param integer $position
	 * @return message
	 */
	public function setPosition($position) {
		$this->position = $position;
	}

	/**
	 * Get position
	 * @return integer
	 */
	public function getPosition() {
		return $this->position;
	}


}


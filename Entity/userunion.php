<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
// use JMS\Serializer\Annotation\Exclude;
// use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
// use JMS\Serializer\Annotation\Accessor;

use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \DateTime;
use \Exception;
use \ReflectionClass;

class userunion {

	const MAX_COUPLE = 2;
	const MIN_COUPLE = 2;

	/**
	 * @var string
	 */
	protected $id;

	/**
	 * @var Array
	 */
	protected $userunions;

	/**
	 * @var DateTime
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 */
	protected $updated;

	/**
	 * @var integer
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $uniontype;

	public function __construct() {
		$this->userunions = new ArrayCollection();
		$this->created = new DateTime();
		$this->updated = null;
		$this->setUniontype($this->getDefaultUniontype());
	}

    /**
     */
    public function controlUserunion() {
    	$this->setId();
    }

	public function isUserunionValid() {
		return
			$this->isUserunionnumberValid()
			&& $this->isUseruniontypeValid()
			;
	}

	/**
	 * @assert\IsTrue(message="Une union doit contenir 2 personnes.")
	 */
	public function isUserunionnumberValid() {
		$userunions = $this->getUserunions();
		return $userunions->count() <= self::MAX_COUPLE && $userunions->count() >= self::MIN_COUPLE;
	}

	/**
	 * @assert\IsTrue(message="Union ne peut être aucune.")
	 */
	public function isUseruniontypeValid() {
		return $this->getRemoveUniontype() !== $this->getUniontype();
	}

	public function __toString() {
		return $this->getId();
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }


    public function setId() {
    	if(!$this->isUserunionValid()) throw new Exception("userunion is not valid!", 1);
    		// return $this;
    	$this->id = $this->getUniontype();
    	foreach ($this->getUserunions() as $userunion) {
    		$this->id .='|'.$userunion->getId();
    	}
    	return $this;
    }

	public function getId() {
		return $this->id;
	}


	/**
	 */
	public function PreRemove() {
		$this->clearUserunions();
		return $this;
	}



	public function getUserunions() {
		return $this->userunions;
	}
	/**
	 * Get other user(s) of union
	 * @param LaboUser $LaboUser
	 * @return ArrayCollection
	 */
	public function getOtheruserunions(LaboUser $LaboUser) {
		$users = new ArrayCollection();
		foreach ($this->userunions as $user) {
			if($user !== $LaboUser) $users->add($user);
		}
		return $users;
	}
	public function addUserunion(LaboUser $LaboUser) {
		if(!$this->hasUserunion($LaboUser)) {
			$this->userunions->add($LaboUser);
			$LaboUser->setUserunion($this);
		}
		return $this;
	}
	public function hasUserunion(LaboUser $LaboUser) {
		return $this->userunions->contains($LaboUser);
	}
	public function setUserunions(ArrayCollection $LaboUsers, $uniontype = null) {
		foreach($this->userunions as $LaboUser) {
			if(!$LaboUsers->contains($LaboUser)) $this->removeUserunion($LaboUser);
		}
		foreach($LaboUsers as $LaboUser) $this->addUserunion($LaboUser);
		if($uniontype !== null) $this->setUniontype($uniontype);
		return $this;
	}
	public function clearUserunions() {
		foreach($this->userunions as $LaboUser) $this->removeUserunion($LaboUser);
		return $this;
	}
	public function removeUserunion(LaboUser $LaboUser) {
		if($this->hasUserunion($LaboUser)) {
			$this->userunions->removeElement($LaboUser);
			$LaboUser->setUserunion(null);
			$LaboUser->setUniontype(null);
		}
		return $this;
	}

	public function getUniontypes() {
		return array(
			0 => 'union.aucun',
			1 => 'union.conjoint',
			// 2 => 'union.concubin',
			// 3 => 'union.autre',
		);
	}

	public function getRemoveUniontype() {
		$keys = array_keys($this->getUniontypes());
		return reset($keys);
	}

	public function getDefaultUniontype() {
		$keys = array_keys($this->getUniontypes());
		return reset($keys);
	}

	public function getUniontype() {
		return $this->uniontype;
	}

	public function setUniontype($uniontype) {
		if(array_key_exists($uniontype, $this->getUniontypes())) $this->uniontype = $uniontype;
			else throw new Exception("This union type ".json_encode($uniontype)." does not exist in ".json_encode($this->getUniontypes())." in Labo\\Bundle\\AdminBundle\\Entity\\userunion entity.", 1);
		return $this;
	}






	/**
	 * Set created
	 * @param DateTime $created
	 * @return userunion
	 */
	public function setCreated(DateTime $created) {
		$this->created = $created;
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 */
	public function createDate() {
		$this->setCreated(new DateTime());
	}

	/**
	 */
	public function updateDate() {
		$this->setUpdated(new DateTime());
	}

	/**
	 * Set updated
	 * @param DateTime $updated
	 * @return userunion
	 */
	public function setUpdated(DateTime $updated) {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime 
	 */
	public function getUpdated() {
		return $this->updated;
	}


}

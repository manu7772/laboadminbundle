<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;

use Labo\Bundle\AdminBundle\Entity\nested;

use \DateTime;
use \Exception;

/**
 * item
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\itemRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * 
 * @ExclusionPolicy("all")
 */
abstract class item extends nested {

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;


}
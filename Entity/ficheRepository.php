<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Labo\Bundle\AdminBundle\Entity\itemRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * ficheRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ficheRepository extends itemRepository {

	public function defaultVal() {
		return array();
	}

}

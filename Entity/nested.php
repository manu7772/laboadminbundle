<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;

use Labo\Bundle\AdminBundle\Entity\subentity;
use Labo\Bundle\AdminBundle\Entity\nestedposition;
use site\adminsiteBundle\Entity\categorie;
use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\services\aeSnake;

use \DateTime;
use \Exception;

/**
 * nested
 *
 * @ExclusionPolicy("all")
 *
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\nestedRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 */
abstract class nested extends subentity {

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "menu"})
	 */
	protected $id;


	// NESTED

	/**
	 * @var array
	 * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\nestedposition", orphanRemoval=true, mappedBy="child", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 * @MaxDepth(2)
	 */
	protected $nestedpositionParents;

	/**
	 * @var array
	 * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\nestedposition", orphanRemoval=true, mappedBy="parent", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 * @ORM\OrderBy({"position" = "ASC"})
	 * @Expose
	 * @Groups({"complete"})
	 * @MaxDepth(2)
	 */
	protected $nestedpositionChilds;

	protected $nestedAttributesParameters;
	protected $aeSnake;

	/**
	 * @Expose
	 * @Groups({"menu"})
	 * @MaxDepth(4)
	 * @Accessor(getter="getChildrenForMenu")
	 */
	protected $children;

	/**
	 * @Expose
	 * @Groups({"menu"})
	 * @MaxDepth(4)
	 * @Accessor(getter="getNestedsForMenu")
	 */
	protected $nesteds;

	/**
	 * @Expose
	 * @Groups({"menu"})
	 * @Accessor(getter="getShortname")
	 */
	protected $shortname;

	public function __construct() {
		parent::__construct();
		$this->nestedpositionParents = new ArrayCollection();
		$this->nestedpositionChilds = new ArrayCollection();
		$this->aeSnake = null;
	}

	/*
	NAMES OF VARIABLES :
	$group								nesteds / categorie_nested / … (short name)
	$Ggroup								group_nesteds (long name)
	$Ggroup_hierarchy / $attribute		group_nesteds_parents (-> virtual attribute)
	$set_Ggroup_hierarchy				setGroup_nesteds_parents / addGroup_nesteds_parents (!! « s »)
	$hierarchy							parent(s) / child(s)
	*/


	// AESNAKE

	public function setNestedAttributesParameters($nestedAttributesParameters) {
		$this->aeSnake = new aeSnake($nestedAttributesParameters);
		return $this;
	}

	public function getNestedAttributesParameters() {
		return $this->getSnake()->getNestedAttributesParameters();
	}

	public function getNestedAttributes() {
		return $this->getSnake()->getNestedAttributeNames(false);
	}

	public function hasNestedAttribute($attribute) {
		return $this->getSnake()->hasGgroupOrGroup($attribute);
	}

	public function getSnake() {
		if(!is_object($this->aeSnake)) throw new Exception("nestedAttributesParameters is not initialized.", 1);
		return $this->aeSnake;
	}


	/**
	 * @Assert\IsTrue(message="L'entité nested n'est pas conforme.")
	 */
	public function isNestedValid() {
		return true;
	}

	/**
	 * Get nestedposition
	 * @param mixed $parent - object nested or id of parent
	 * @param mixed $child - object nested or id of child
	 * @param string $group
	 * @return nestedposition
	 */
	public function getNestedposition($parent, $child, $group) {
		if($parent instanceOf nested) $parent = $parent->getId();
		if($child instanceOf nested) $child = $child->getId();
		foreach (array_merge($this->getNestedpositionParents()->toArray(), $this->getNestedpositionChilds()->toArray()) as $link) {
			if($link->getId() === $parent.'-'.$child.'-'.$group) return $link;
		}
		return false;
	}

	/**
	 * Get nested parents by group FOR $this
	 * @param string $group
	 * @param categorie $categorie
	 * @param array|string $types = []
	 * @return array
	 */
	public function getParentsByGroupForCategorie($group, categorie $categorie, $level = 1, $types = []) {
		if($categorie->getLvl() == $level) $childs = $categorie->getAllCategorieChilds();
			else $childs = $categorie->getParentOfLevel($level, $types)->getAllCategorieChilds();
		$list = $this->getParentsByGroup($group, $types);
		foreach($list as $key => $value) {
			if(!in_array($value, $childs)) unset($list[$key]);
		}
		return $list;
	}

	/**
	 * Get nested parents by group
	 * @param string $group
	 * @param array|string $types = []
	 * @return array
	 */
	public function getParentsByGroup($group, $types = []) {
		$group = $this->getSnake()->getGroupName($group, false);
		// $group = (array)$group;
		$class = $this->getNestedAttributesParameters()[$group]['class'];
		$list = array();
		foreach($this->getNestedpositionParents() as $link) {
			if(
				preg_match('#^(.+)-'.$this->getId().'-'.$this->getSnake()->getGroupName($group, false).'$#', $link->getId())
				&& in_array($link->getParent()->getShortName(), $class)
				&& ((array)$types == [] || in_array($link->getParent()->getType(), (array)$types))
			)
				$list[] = $link->getParent();
		}
		return array_unique($list);
	}

	/**
	 * Get ALL nested parents by group
	 * @param string $group
	 * @param array|string $types = []
	 * @return array
	 */
	public function getAllParentsByGroup($group, $types = []) {
		$parents = $this->getParentsByGroup($group, $types);
		// only categorie parents / recursion warning in not
		$subparents = array();
		if($group === 'categorie_nested') {
			foreach($parents as $parent) if($parent->getShortName() === 'categorie') {
				$subparents = array_merge($subparents, $parent->getCategorieParents());
			}
			// var_dump($subparents);
		}
		return array_unique(array_merge($parents, $subparents));
	}

	public function getParentOfLevel($level = 1, $types = []) {
		$parents = $this->getAllParentsByGroup('categorie_nested', $types);
		foreach ($parents as $parent) {
			if($parent->getLvl() == (integer)$level) return $parent;
		}
		return null;
	}

	/**
	 * Lien parent <=> child existe ?
	 * @param mixed $parent
	 * @param mixed $child
	 * @param string $group
	 * @return boolean
	 */
	public function hasNestedposition($parent, $child, $group) {
		return is_object($this->getNestedposition($parent, $child, $group));
	}

	public function getPositionFromHisParent($parent, $group) {
		foreach($this->getNestedpositionParents() as $link) {
			if($link->getId() === $parent.'-'.$this->getId().'-'.$group) return $link->getPosition();
		}
		return false;
	}

	/**
	 * has Child ?
	 * @param mixed $child - nested or id of child
	 * @return boolean
	 */
	public function hasChild($child, $group) {
		if($child instanceOf nested) $child = $child->getId();
		foreach($this->getNestedpositionChilds() as $link) {
			if(preg_match('#^'.$this->getId().'-'.$child.'-'.$group.'$#', $link->getId())) return true;
		}
		return false;
	}

	/**
	 * Get first child in group
	 * @return nested
	 */
	public function getFirstChild($group) {
		$results = array();
		foreach($this->getNestedpositionChilds() as $link) {
			if(preg_match('#^'.$this->getId().'-(.+)-'.$group.'$#', $link->getId()) && $link->getPosition() === 0) $results[] = $link;
		}
		if(count($results) > 0) return $results[0]->getChild();
		// if(count($results) > 1) throw new Exception($group.' group with parent '.$this->getNom().' (id#'.$this->getId().') has more than one first child !! (position = 0).', 1);
		// 	else if(count($results) == 1) return $results[0]->getChild();
		return null;
	}

	public function getChildsByGroup($group, $class = null) {
		// echo('<p>getChildsByGroup '.json_encode($group).' in "'.$this->getNom().'" with class '.json_encode($class).'</p>');
		$group = $this->getSnake()->getGroupName($group, false);
		// $shortcutClass = count((array)$class) < 1;
		if($class == 'all') {
			$shortcutClass = true;
		} else {
			$shortcutClass = false;
			$class = (array)$class;
			if(count($class) < 1 && isset($this->getNestedAttributesParameters()[$group])) $class = $this->getNestedAttributesParameters()[$group]['class'];
		}
		$results = array();
		foreach($this->getNestedpositionChilds() as $link) {
			if(preg_match('#^'.$this->getId().'-(.+)-'.$this->getSnake()->getGroupName($group, false).'$#', $link->getId()) && (in_array($link->getChild()->getShortName(), (array)$class) || $shortcutClass)) {
				$results[] = $link->getChild();
			}
		}
		return $results;
	}


	public function addGetIfNoAction(&$method) {
		if($this->getSnake()->getAction($method) == false) $method = 'get'.ucfirst($method);
		return $method;
	}


	/**
	 * Property exists ? (required for Twig)
	 * @param string $attribute
	 * @return boolean
	 */
	public function __isset($attribute) {
		// echo('<p>Test isset on '.$attribute.' -> '.$this->getSnake()->stripActionAndHierarchy($attribute).'</p>');
		return $this->getSnake()->hasGgroup($this->getSnake()->stripActionAndHierarchy($attribute));
	}

	/**
	 * Get property (with entity class filtering)
	 * @param string $property
	 * @return ArrayCollection or false
	 */
	public function __get($property) {
		if($this->getSnake()->hasGgroup($property)) {
			$group = $this->getSnake()->getGroupName($property);
			$hierarchy = $this->getSnake()->getHierarchy($property);
			// echo('<p>__get group "'.json_encode($group).'" in '.json_encode($hierarchy).' ('.json_encode($property).')</p>');
			if(!$group || !$hierarchy) {
				$get = 'get'.ucfirst($property);
				if(method_exists($this, $get)) return $this->$get();
				throw new Exception('Error on __get '.json_encode($property).' : not a valid property or has no getter. Searched with group "'.json_encode($this->getSnake()->getGroupName($property)).'" and hierarchy "'.json_encode($this->getSnake()->getHierarchy($property)).'".', 1);
			}
			// usual getter
			if(!preg_match('#s$#', $hierarchy)) $hierarchy += 's';
			$get = 'get'.ucfirst($hierarchy).'ByGroup';
			return new ArrayCollection($this->$get($group));
		}
		return false;
	}

	/**
	 * Set values for a property
	 * @param string $property
	 * @param array $values
	 * @return nested
	 */
	public function __set($property, $values) {
		if($this->getSnake()->hasGgroup($property)) {
			$group = $this->getSnake()->getGroupName($property, false);
			if(count((array)$values) < 1) $values = new ArrayCollection();
			if(is_array($values)) {
				if($values[0] instanceOf ArrayCollection) $values = $values[0];
				if(is_array($values[0])) $values = new ArrayCollection($values[0]);
			}
			// SET PARENT
			if($this->getSnake()->isNestParent($property)) {
				// remove
				foreach($this->getParentsByGroup($group) as $value) if(!$values->contains($value)) {
					$nestedposition = $this->getNestedposition($value, $this, $group);
					if($nestedposition != false) $this->removeNestedposition($nestedposition);
				}
				// add/set
				foreach($values as $value) {
					if(in_array($value->getShortName(), $this->getNestedAttributesParameters()[$group]['class'])) {
						if(!$this->hasNestedposition($value, $this, $group)) {
							$nestedposition = new nestedposition();
							$nestedposition->setParentChild($value, $this, $group);
							// echo('<p>-('.$nestedposition->getId().') "'.$this->getNom().'" : add as parent '.json_encode($value->getNom()).' in "'.$group.'"</p>');
							// $this->addNestedposition($nestedposition);
						}
					}
				}
			}
			// SET CHILD
			if($this->getSnake()->isNestChild($property)) {
				// remove
				foreach($this->getChildsByGroup($group) as $value) if(!$values->contains($value)) {
					$nestedposition = $this->getNestedposition($this, $value, $group);
					if($nestedposition != false) $this->removeNestedposition($nestedposition);
				}
				// add/set
				foreach($values as $value) {
					if(in_array($value->getShortName(), $this->getNestedAttributesParameters()[$group]['class'])) {
						if(!$this->hasNestedposition($this, $value, $group)) {
							$nestedposition = new nestedposition();
							$nestedposition->setParentChild($this, $value, $group);
							// echo('<p>-('.$nestedposition->getId().') "'.$this->getNom().'" : add as child '.json_encode($value->getNom()).' in "'.$group.'"</p>');
							// $this->addNestedposition($nestedposition);
						}
					}
				}
			}
		} 
		return $this;
	}

	public function __call($method, $arguments) {
		$this->addGetIfNoAction($method);
		if($this->getSnake()->isNestParent($method)) {
			// le groupe est reconnu pour opérations sur les parents
			switch ($this->getSnake()->getAction($method)) {
				case 'add':
					if(!$arguments[0] instanceOf nestedposition) throw new Exception('Error while calling '.json_encode($method).' : first argument must be instance of Labo\Bundle\AdminBundle\Entity\nestedposition', 1);
					return $this->addNestedposition($arguments[0]);
					break;
				case 'remove':
					if(!$arguments[0] instanceOf nestedposition) throw new Exception('Error while calling '.json_encode($method).' : first argument must be instance of Labo\Bundle\AdminBundle\Entity\nestedposition', 1);
					return $this->removeNestedposition($arguments[0]);
				case 'set':
					// echo('<p>call "set" : '.json_encode($method).' '.count($arguments).' (Hierarchy : '.json_encode($hierarchy).')</p>');
					$attribute = $this->getSnake()->stripAction($method);
					return $this->$attribute = $arguments; // call __set()
					break;
				case 'get':
					// echo('<p>call "get" : '.json_encode($method).' '.count($arguments).' (Hierarchy : '.json_encode($hierarchy).')</p>');
					$attribute = $this->getSnake()->stripAction($method);
					return $this->$attribute; // call __get()
					break;
				default:
					throw new Exception('Action impossible '.json_encode($method), 1);
					break;
			}
		} else if($this->getSnake()->isNestChild($method)) {
			// le groupe est reconnu pour opérations sur les enfants
			switch ($this->getSnake()->getAction($method)) {
				case 'add':
					if(!$arguments[0] instanceOf nestedposition) throw new Exception('Error while calling '.json_encode($method).' : first argument must be instance of Labo\Bundle\AdminBundle\Entity\nestedposition', 1);
					return $this->addNestedposition($arguments[0]);
					break;
				case 'remove':
					if(!$arguments[0] instanceOf nestedposition) throw new Exception('Error while calling '.json_encode($method).' : first argument must be instance of Labo\Bundle\AdminBundle\Entity\nestedposition', 1);
					return $this->removeNestedposition($arguments[0]);
				case 'set':
					// echo('<p>call "set" : '.json_encode($method).' '.count($arguments).' (Hierarchy : '.json_encode($hierarchy).')</p>');
					$attribute = $this->getSnake()->stripAction($method);
					return $this->$attribute = $arguments; // call __set()
					break;
				case 'get':
					// echo('<p>call "get" : '.json_encode($method).' '.count($arguments).' (Hierarchy : '.json_encode($hierarchy).')</p>');
					$attribute = $this->getSnake()->stripAction($method);
					return $this->$attribute; // call __get()
					break;
				default:
					throw new Exception('Action impossible '.json_encode($method), 1);
					break;
			}
		}
		// so, error…
		// $trace = debug_backtrace();
		// trigger_error('Error in '.$trace[0]['function'].' line '.$trace[0]['line'].' : '.json_encode($this->getSnake()->getHierarchy($method))." is not a valid hierarchy ! (method was : ".json_encode($method).")", E_USER_ERROR);
		// throw new Exception(json_encode($hierarchy)." is not a valid hierarchy !", 1);
		return false;
	}

	/**
	 * check positions of childs (of group) and (optionallly) add a new child at first position
	 * @param string $group
	 * @param nestedposition $changeposition = null
	 * @param integer $position = 0
	 * @return integer (next available position)
	 */
	public function checkChildsPositions($group, nestedposition $changeposition = null, $position = 0) {
		if(is_object($changeposition))
			if($changeposition->getParent() !== $this) throw new Exception('Nested::checkChildsPositions error : nestedposition (id : '.json_encode($changeposition->getId()).') element has not this entity '.json_encode($this->getNom()).' (id : '.$this->getId().') for parent !!', 1);
		$childs = $this->getNestedpositionChilds($group);
		$indexs = array();
		// get index (as positions) for chilren
		foreach($childs as $nestedposition) if($nestedposition !== $changeposition) {
			$pos = $nestedposition->getPosition();
			while(array_key_exists($pos, $indexs)) $pos++;
			$indexs[$pos] = $nestedposition;
		}
		unset($childs);
		ksort($indexs);
		// sort
		$cntpos = 0;
		foreach($indexs as $nestedposition) {
			if($cntpos === $position) {
				$changeposition->setPosition($cntpos++);
			}
			if($nestedposition !== $changeposition) {
				$nestedposition->setPosition($cntpos++);
			}
		}
		// last position if -1
		if($changeposition instanceOf nestedposition && $position === -1) {
			$changeposition->setPosition($cntpos++);
		}
		return $cntpos;
	}

	/**
	 * set parent position at $position / returns true if operation done, if not, returns false
	 * @param mixed $parent - object nested or id of parent
	 * @param string $group
	 * @param integer $position
	 * @return boolean
	 */
	public function setNestedPosition_position($parent, $group, $position) {
		$link = $this->getNestedposition($parent, $this, $group);
		if(!($parent instanceOf nested)) $parent = $link->getParent();
		if($link instanceOf nestedposition) {
			// $link->setPosition((integer)$position);
			$parent->checkChildsPositions($group, $link, $position);
			return true;
		}
		return false;
	}

	/**
	 * set first in parent position
	 * @param mixed $parent - object nested or id of parent
	 * @param string $group
	 * @return integer 
	 */
	public function setNestedPosition_first($parent, $group) {
		return $this->setNestedPosition_position($parent, $group, 0);
	}

	/**
	 * set last in parent position
	 * @param mixed $parent - object nested or id of parent
	 * @param string $group
	 * @return integer 
	 */
	public function setNestedPosition_last($parent, $group) {
		return $this->setNestedPosition_position($parent, $group, -1);
	}



	/**
	 * Add nestedposition
	 * @param nestedposition $nestedposition
	 * @return nested
	 */
	public function addNestedposition(nestedposition $nestedposition) {
		// if(!$nestedposition->isNestedpositionValid()) throw new Exception('Incomplete nestedposition '.json_encode($nestedposition->getId()).' !', 1);
		if($nestedposition->getParent() === $this) {
			// add as parent
			if(!$this->hasNestedposition($this, $nestedposition->getChild(), $nestedposition->getGroup())) $this->getNestedpositionChilds()->add($nestedposition);
		} else if($nestedposition->getChild() === $this) {
			// add as children
			if(!$this->hasNestedposition($nestedposition->getParent(), $this, $nestedposition->getGroup())) $this->getNestedpositionParents()->add($nestedposition);
		}
		return $this;
	}

	/**
	 * Remove nestedposition
	 * @param nestedposition $nestedposition
	 * @return boolean
	 */
	public function removeNestedposition(nestedposition $nestedposition) {
		$r = false;
		if($nestedposition->getParent() === $this) {
			// remove as parent
			if($this->getNestedpositionChilds()->contains($nestedposition)) {
				$r = $this->getNestedpositionChilds()->removeElement($nestedposition);
			}
		} else if($nestedposition->getChild() === $this) {
			// remove as children
			if($this->getNestedpositionParents()->contains($nestedposition)) {
				$r = $this->getNestedpositionParents()->removeElement($nestedposition);
			}
		}
		return $r;
	}

	/**
	 * Get nestedpositionParents
	 * @return ArrayCollection 
	 */
	public function getNestedpositionParents($group = null) {
		$nestedpositions = $this->nestedpositionParents;
		if(is_string($group)) {
			foreach ($nestedpositions as $key => $nestedposition) {
				if($nestedposition->getGroup() !== $group) unset($nestedpositions[$key]);
			}
		}
		return $nestedpositions;
	}

	/**
	 * Get nestedpositionChilds
	 * @return ArrayCollection 
	 */
	public function getNestedpositionChilds($group = null) {
		$nestedpositions = $this->nestedpositionChilds;
		if(is_string($group)) {
			foreach ($nestedpositions as $key => $nestedposition) {
				if($nestedposition->getGroup() !== $group) unset($nestedpositions[$key]);
			}
		}
		return $nestedpositions;
	}

	/**
	 * Get children for menu
	 * @return ArrayCollection 
	 */
	public function getChildrenForMenu() {
		return $this->getChildsByGroup('nesteds');
	}

	/**
	 * Get nesteds for menu
	 * @return ArrayCollection 
	 */
	public function getNestedsForMenu() {
		return $this->getChildsByGroup('categorie_nested');
	}


}
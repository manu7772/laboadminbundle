<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;

use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \DateTime;

/**
 * statut
 *
 * @ExclusionPolicy("all")
 *
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\statutRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields={"nom"}, message="statut.existe")
 */
class statut extends baseEntity {

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="nom", type="string", length=100, nullable=false, unique=true)
	 * @Assert\NotBlank(message = "Vous devez remplir ce champ.")
	 * @Assert\Length(
	 *      min = "3",
	 *      max = "100",
	 *      minMessage = "Le nom doit comporter au moins {{ limit }} lettres.",
	 *      maxMessage = "Le nom doit comporter au maximum {{ limit }} lettres."
	 * )
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $nom;

	/**
	 * @var string
	 * @ORM\Column(name="descriptif", type="text", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $descriptif;

	/**
	 * @var string
	 * @ORM\Column(name="niveau", type="string", length=32, nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $niveau;

	/**
	 * @var array
	 * @ORM\Column(name="bundles", type="array", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $bundles;

	/**
	 * @var string
	 * @ORM\Column(name="couleur", type="string", length=24, nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $couleur;

	// protected $couleurs;
	protected $bundleChoices;
	protected $roleChoices;

	public function __construct() {
		parent::__construct();
		$this->nom = null;
		$this->descriptif = null;
		$this->niveau = null;
		$this->bundles = array();
		$this->couleur = "transparent";
	}

	/**
	 * Un élément par défaut dans la table est-il optionnel ?
	 * @return boolean
	 */
	public function isDefaultNullable() {
		return false;
	}

	/**
	 * Peut'on attribuer plusieurs éléments par défaut ?
	 * true 		= illimité
	 * integer 		= nombre max. d'éléments par défaut
	 * false, 0, 1 	= un seul élément
	 * @return boolean
	 */
	public function isDefaultMultiple() {
		return false;
	}

	public function injectRolesHierarchy($role_hierarchy, LaboUser $user = null) {
		$this->roleChoices = $role_hierarchy;
		$grants = $user != null ? $user->getGrants() : $grants = array('IS_AUTHENTICATED_ANONYMOUSLY', 'ROLE_USER'); // ROLE_USER ???????
		$grantedRoles = array();
		foreach($this->roleChoices as $role => $item) {
			if(in_array($role, $grants)) $grantedRoles[$role] = $item;
		}
		$this->roleChoices = $grantedRoles;
		// echo('<pre>injectRolesHierarchy / Roles choices : ');var_dump($this->roleChoices);echo('</pre>');
		if($this->niveau == null) $this->niveau = reset($this->roleChoices);
		return $this;
	}

	public function injectBundles($bundles) {
		$this->bundleChoices = $bundles;
		// $this->bundles = array();
		if(count($bundles) < 1) {
			foreach($this->bundleChoices as $bundle) {
				$this->addBundle($bundle);
			}
		}
		return $this;
	}

	/**
	 * Set niveau
	 * @param string $niveau
	 * @return statut
	 */
	public function setNiveau($niveau) {
		$this->niveau = $niveau;
		return $this;
	}

	/**
	 * Get niveau
	 * @return string 
	 */
	public function getNiveau() {
		return $this->niveau;
	}

	/**
	 * Add bundle
	 * @param string $bundle
	 * @return statut
	 */
	public function addBundle($bundle) {
		if(!is_array($this->bundles)) $this->bundles = array();
		// if(is_string($bundle)) $bundle = array($bundle);
		foreach((array)$bundle as $bun) {
			if(!in_array($bun, $this->bundles)) $this->bundles[] = $bun;
		}
		return $this;
	}

	/**
	 * Remove bundle
	 * @param string $bundle
	 * @return boolean
	 */
	public function removeBundle($bundle) {
		if(!is_array($this->bundles)) $this->bundles = array();
		$r = false;
		if(in_array($bundle, $this->bundles)) {
			foreach ($this->bundles as $key => $bun) {
				if($bun == $bundle) {
					unset($this->bundles[$key]);
					$r = true;
				}
			}
		}
		return $r;
	}

	/**
	 * Set bundles
	 * @param array $bundles
	 * @return statut
	 */
	public function setBundles($bundles) {
		if(is_array($bundles)) $this->bundles = $bundles;
			else $this->bundles = array();
		return $this;
	}

	/**
	 * Get bundles
	 * @return array 
	 */
	public function getBundles() {
		if(!is_array($this->bundles)) $this->bundles = array();
		return $this->bundles;
	}

	/**
	 * Set descriptif
	 * @param string $descriptif
	 * @return statut
	 */
	public function setDescriptif($descriptif = null) {
		$this->descriptif = $descriptif;
		return $this;
	}

	/**
	 * Get descriptif
	 * @return string 
	 */
	public function getDescriptif() {
		return $this->descriptif;
	}

	/**
	 * Set couleur
	 * @param string $couleur
	 * @return statut
	 */
	public function setCouleur($couleur) {
		// if(in_array($couleur, $this->couleurs)) $this->couleur = $couleur;
		// 	else $this->couleur = reset($this->couleurs);
		$this->couleur = $couleur;
		return $this;
	}

	/**
	 * Get couleur
	 * @return string 
	 */
	public function getCouleur() {
		return $this->couleur;
	}

	/**
	 * Get role choices
	 * @return array 
	 */
	public function getRoleChoices() {
		// echo('<pre>getRoleChoices / Roles choices : ');var_dump($this->roleChoices);echo('</pre>');
		return $this->roleChoices;
	}

	/**
	 * Get bundle choices
	 * @return array 
	 */
	public function getBundleChoices() {
		return $this->bundleChoices;
	}

}

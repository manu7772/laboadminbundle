<?php
 
namespace Labo\Bundle\AdminBundle\Entity;

use FOS\UserBundle\Model\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Yaml\Parser;
use Labo\Bundle\AdminBundle\services\aeData;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;

use site\adminsiteBundle\Entity\image as siteImage; // image
use site\adminsiteBundle\Entity\adresse as siteAdresse; // adresse
use site\adminsiteBundle\Entity\site as siteSite; // site
use site\adminsiteBundle\Entity\facture as siteFacture; // facture
use site\adminsiteBundle\Entity\message as siteMessage; // message
use site\adminsiteBundle\Entity\panier as sitePanier; // panier
use Labo\Bundle\AdminBundle\Entity\messageuser;
use Labo\Bundle\AdminBundle\Entity\baseuserevenement;
use Labo\Bundle\AdminBundle\Entity\userevenement;
use Labo\Bundle\AdminBundle\Entity\invitedevenement;

use \DateTime;
use \Exception;
use \ReflectionClass;

/**
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\LaboUserRepository")
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * 
 * @ORM\HasLifecycleCallbacks
 * @ExclusionPolicy("all")
 */
abstract class LaboUser extends User {

	const MAIL_TEMP_EXT = "@xxx.xx";

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="nom", type="string", length=50, nullable=true, unique=false)
	 * @Assert\Length(
	 *      min = "2",
	 *      max = "50",
	 *      minMessage = "Votre nom doit comporter au moins {{ limit }} lettres.",
	 *      maxMessage = "Votre nom peut comporter au maximum {{ limit }} lettres."
	 * )
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $nom;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 * @Assert\NotBlank(message="Indiquez votre nom d'utilisateur.")
	 */
	protected $username;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 * @Assert\Email(strict=true, message="Le format de l'email est incorrect")
	 * @Assert\Email(checkMX=true, message="Aucun serveur mail n'a été trouvé pour ce domaine")
	 * @Assert\NotBlank(message="Indiquez un email valide.")
	 */
	protected $email;

	/**
	 * @var string
	 * @ORM\Column(name="prenom", type="string", length=100, nullable=true, unique=false)
	 * @Assert\Length(
	 *      min = "2",
	 *      max = "50",
	 *      minMessage = "Votre prénom doit comporter au moins {{ limit }} lettres.",
	 *      maxMessage = "Votre prénom peut comporter au maximum {{ limit }} lettres."
	 * )
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $prenom;

	/**
	 * @var integer - PROPRIÉTAIRE
	 * @ORM\OneToOne(targetEntity="site\adminsiteBundle\Entity\adresse", inversedBy="user", cascade={"all"})
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=true, unique=true, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete", "facture"})
	 * @Assert\Valid()
	 */
	protected $adresse;

	/**
	 * @var integer - PROPRIÉTAIRE
	 * @ORM\OneToOne(targetEntity="site\adminsiteBundle\Entity\adresse", inversedBy="userLivraison", cascade={"all"})
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=true, unique=true, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete", "facture"})
	 * @Assert\Valid()
	 */
	protected $adresseLivraison;

	/**
	 * @var string
	 * @ORM\Column(name="telephone", type="string", length=24, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $telephone;

	/**
	 * @var integer
	 * @ORM\Column(name="statutsocial", type="integer", nullable=true, unique=false)
	 * @Assert\Expression(
	 *		"(value === 0 and this.getProfession() !== null) or (value > 0)",
	 *		message="Si vous êtes actif, merci de renseigner votre profession, svp."
	 * )
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $statutsocial;

	/**
	 * - INVERSE
	 * @ORM\OneToMany(targetEntity="site\adminsiteBundle\Entity\panier", mappedBy="user", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
	 * @ORM\OrderBy({"position" = "ASC"})
	 * @Exclude
	 * @Groups({"facture"})
	 */
	protected $paniers;

	/**
	 * - INVERSE
	 * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\baseuserevenement", mappedBy="user", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
	 * @Exclude
	 * @Groups({"complete"})
	 */
	protected $evenements;

	/**
	 * - INVERSE
	 * @ORM\OneToMany(targetEntity="site\adminsiteBundle\Entity\facture", mappedBy="user", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
	 * @ORM\OrderBy({"position" = "ASC"})
	 */
	protected $factures;

	/**
	 * - INVERSE
	 * @ORM\OneToMany(targetEntity="site\adminsiteBundle\Entity\message", mappedBy="user", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 * @ORM\OrderBy({"position" = "DESC"})
	 */
	protected $messages;

	/**
	 * - INVERSE
	 * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\messageuser", mappedBy="collaborator", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 * @ORM\OrderBy({"messagecreated" = "DESC"})
	 */
	protected $messageusers;

	/**
	 * @var boolean
	 * @ORM\Column(name="adminhelp", type="boolean", nullable=false, unique=false)
	 */
	protected $adminhelp;

	/**
	 * @var integer
	 * @ORM\Column(name="sexe", type="integer", nullable=true, unique=false)
	 */
	protected $sexe;

	/**
	 * @var integer
	 * @ORM\OneToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="userunionChild", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $userunionParent;

	/**
	 * @ORM\OneToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", mappedBy="userunionParent", cascade={"persist"})
	 * @ORM\JoinTable(name="unionLinks",
	 *     joinColumns={@ORM\JoinColumn(name="userunionChild_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="userunionParent_id", referencedColumnName="id")}
	 * )
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $userunionChild;

	/**
	 * @var boolean
	 * @ORM\Column(name="unionconfirmed", type="boolean", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $unionconfirmed;

	/**
	 * @var integer
	 * @ORM\Column(name="uniontype", type="integer", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $uniontype;
	protected $uniontypes;

	/**
	 * @var boolean
	 * @ORM\Column(name="mailSitemessages", type="boolean", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="getMailSitemessages", setter="setMailSitemessages")
	 */
	protected $mail_sitemessages;

	/**
	 * @ORM\Column(name="admintheme", type="string", length=32, unique=false, nullable=true)
	 */
	protected $admintheme;

    /**
     * - PROPRIÉTAIRE
     * @ORM\OneToOne(targetEntity="site\adminsiteBundle\Entity\image", orphanRemoval=true, inversedBy="userAvatar", cascade={"all"})
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
     */
    protected $avatar;

	/**
	 * - INVERSE
	 * @ORM\ManyToMany(targetEntity="site\adminsiteBundle\Entity\site", mappedBy="collaborateurs")
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 */
	protected $sites;

	/**
	 * @ORM\Column(name="langue", type="string", length=32, unique=false, nullable=true)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $langue;

	/**
     * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\calendar", orphanRemoval=true, mappedBy="usercal")
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 */
	protected $calendars;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="isCollaborator")
	 */
	protected $isCollaborator;

	/**
	 * @var string
	 * @ORM\Column(name="profession", type="string", length=128, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $profession;

	/**
	 * @var boolean
	 * @ORM\Column(name="cookies", type="boolean", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $cookies;

	/**
	 * @var boolean
	 * @ORM\Column(name="publicite", type="boolean", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $publicite;

	/**
	 * @var string
	 * @ORM\Column(name="temppassword", type="string", length=48, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $temppassword;

	protected $grantedRoles;
	protected $rolesHierarchy;

	/**
	 * @assert\IsFalse(message="Ce compte existe déjà. Si c'est le vôtre, veuillez vous connecter.")
	 */
	protected $recognizedbutnotconnected;

	protected $class_name;

	public function __construct() {
		$this->id = null;
		parent::__construct();
		$this->evenements = new ArrayCollection();
		$this->paniers = new ArrayCollection();
		$this->factures = new ArrayCollection();
		$this->messages = new ArrayCollection();
		$this->messageusers = new ArrayCollection();
		$this->calendars = new ArrayCollection();
		$this->adminhelp = true;
		$this->mail_sitemessages = true;
		$this->admintheme = $this->getDefaultAdminskin();
		$this->avatar = null;
		$this->adresse = null;
		$this->adresseLivraison = null;
		$this->sites = new ArrayCollection();
		$this->langue = 'fr';
		$this->grantedRoles = array();
		$this->rolesHierarchy = array();
		$this->statutsocial = $this->getDefaultStatutsocial();
		$this->sexe = $this->getDefaultSexe();
		$this->userunionParent = null;
		$this->userunionChild = null;
		$this->setUnionconfirmed(false);
		$this->uniontype = $this->getDefaultUniontype();
		$this->profession = null;
		$this->cookies = null;
		$this->publicite = true;
		$this->recognizedbutnotconnected = false;
		$this->setTemppassword();
	}

	public function getMailTempExt() {
		return self::MAIL_TEMP_EXT;
	}

	public function isTempMail($test = null) {
		$test = (string)$test === '' ? (string) $this->email : (string) $test;
		return preg_match("#".$this->getMailTempExt()."$#", $test);
	}

	public function getAdminskins() {
		return array(
			"skin-0" => "Default",
			"skin-1" => "Blue light",
			"skin-2" => "Yellow and purple",
			"skin-3" => "Material design",
		);
	}

	public function getDefaultAdminskin() {
		$skins = $this->getAdminskins();
		return reset($skins);
	}

	public function __toString() {
		return $this->getUsername();
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }


    /**
	 * @ORM\PostLoad
     */
    public function postLoad() {
    	$this->recognizedbutnotconnected = false;
    	$this->uniontype = $this->uniontype === null ? $this->getRemoveUniontype() : $this->uniontype;
    	$this->setUnionconfirmed($this->getUnionconfirmed());
    }

    /**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
     */
    public function controlUser() {
    	if($this->userunionParent instanceOf LaboUser && $this->userunionChild instanceOf LaboUser) throw new Exception("Two users with this one in union as parent and child is no possible.", 1);
    	$this->uniontype = $this->getUniontype();
    }

    public function isRecognizedbutnotconnected() {
    	return !$this->recognizedbutnotconnected;
    }

    public function setRecognizedbutnotconnected($bool) {
    	$this->recognizedbutnotconnected = (boolean) $bool;
    }

	/**
	 * @assert\IsTrue(message="Les informations de profil sont incomplètes.")
	 */
	public function isLaboUserValid() {
		$valid = true;
		return $valid
			&& (string) $this->getUsername() !== ''
			&& (string) $this->getEmail() !== ''
			&& (string) $this->getNom() !== ''
		;
	}

	/**
	 * Get id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set rolesHierarchy
	 * @param array $rolesHierarchy
	 * @return LaboUser
	 */
	public function setRolesHierarchy($rolesHierarchy) {
		$this->rolesHierarchy = $rolesHierarchy;
		// echo('<pre>- setRolesHierarchy : '.$this->getUsername().'<br>');var_dump($this->rolesHierarchy);echo('</pre>');
		return $this;
	}

	/**
	 * Get rolesHierarchy
	 * @return array 
	 */
	public function getRolesHierarchy() {
		return $this->rolesHierarchy;
	}

	public function getValidRoles() {
		// echo('<pre>- getValidRoles : '.$this->getUsername().'<br>');var_dump($this->rolesHierarchy);echo('</pre>');
		return is_array($this->rolesHierarchy) ? array_keys($this->rolesHierarchy) : array('IS_AUTHENTICATED_ANONYMOUSLY','ROLE_USER','ROLE_TESTER','ROLE_TRANSLATOR','ROLE_EDITOR','ROLE_ADMIN','ROLE_SUPER_ADMIN');
	}

	public function getStatutsocials() {
		return array(
			0 => 'statutsocial.actif',
			1 => 'statutsocial.demandeurdemploi',
			2 => 'statutsocial.enfant',
			3 => 'statutsocial.etudiant',
			4 => 'statutsocial.retraite',
		);
	}

	public function getDefaultStatutsocial() {
		$statutsocials = $this->getStatutsocials();
		$keys = array_keys($statutsocials);
		return reset($keys);
	}

	public function getSexes() {
		return array(
			0 => 'sexe.femme',
			1 => 'sexe.homme',
			// 2 => 'sexe.autre',
		);
	}

	public function getDefaultSexe() {
		$sexes = $this->getSexes();
		$keys = array_keys($sexes);
		return reset($keys);
	}

	public function getUniontypes() {
		return array(
			0 => 'union.aucun',
			1 => 'union.conjoint',
			2 => 'union.concubin',
			3 => 'union.autre',
		);
	}

	public function getRemoveUniontype() {
		return 0;
	}

	public function getDefaultUniontype() {
		return 1;
	}


	public function getSexe() {
		return $this->sexe;
	}
	public function setSexe($sexe) {
		$this->sexe = $sexe;
		return $this;
	}
	public function getSexename() {
		return isset($this->getSexes()[$this->sexe]) ? $this->getSexes()[$this->sexe] : null;
	}

	public function getStatutsocial() {
		return $this->statutsocial;
	}
	public function setStatutsocial($statutsocial) {
		$this->statutsocial = $statutsocial;
		return $this;
	}
	public function getStatutsocialname() {
		return isset($this->getStatutsocials()[$this->statutsocial]) ? $this->getStatutsocials()[$this->statutsocial] : null;
	}







	public function setUserunion(LaboUser $LaboUser = null) {
		if($LaboUser instanceOf LaboUser) {
			if($this->getUserunionParent() !== $LaboUser && $this->getUserunionChild() !== $LaboUser) $this->setUserunionChild($LaboUser);
		} else {
			$this->removeUserunion();
		}
		return $this;
	}

	public function getUserunion() {
		return $this->getUserunionChild() instanceOf LaboUser ? $this->getUserunionChild() : $this->getUserunionParent();
	}

	public function removeUserunion() {
		if($this->getUserunion() instanceOf LaboUser) {
			if($this->getUserunion()->getUniontype() !== $this->getRemoveUniontype()) $this->getUserunion()->setUniontype($this->getRemoveUniontype());
		}
		if($this->getUserunionChild() instanceOf LaboUser) {
			$this->getUserunionChild()->setUserunionParent(null);
		}
		$this->userunionParent = null;
		$this->userunionChild = null;
		return $this;
	}

	public function getUserunionParent() {
		return $this->userunionParent;
	}
	public function setUserunionParent(LaboUser $LaboUser = null) {
		if($LaboUser === $this) throw new Exception("Your union parent can not be the yourself!", 1);
		if($LaboUser instanceOf LaboUser) {
			// remove child if exists
			if($this->userunionChild instanceOf LaboUser) $this->userunionChild->setUserunionParent(null);
			$this->userunionChild = null;
			if($this->userunionParent !== $LaboUser) {
				// remove old parent if exists
				// if($this->userunionParent instanceOf LaboUser) $this->userunionParent->setUserunionChild(null);
				// set new parent
				$this->userunionParent = $LaboUser;
				if($this->userunionParent->getUniontype() !== $this->getRemoveUniontype()) $this->setUniontype($this->userunionParent->getUniontype());
				$this->setUnionconfirmed(false);
			}
		} else {
			// null
			// remove old parent if exists
			// if($this->userunionParent instanceOf LaboUser) $this->userunionParent->setUserunionChild(null);
			$this->userunionParent = null;
			$this->setUniontype($this->getRemoveUniontype());
			$this->setUnionconfirmed(false);
		}
		return $this;
	}

	public function getUserunionChild() {
		return $this->userunionChild;
	}
	public function setUserunionChild(LaboUser $LaboUser = null) {
		if($LaboUser === $this) throw new Exception("Your union child can not be the yourself!", 1);
		if($LaboUser instanceOf LaboUser) {
			// remove parent if exists
			// if($this->userunionParent instanceOf LaboUser) $this->userunionParent->setUserunionChild(null);
			$this->userunionParent = null;
			if($this->userunionChild !== $LaboUser) {
				// remove old child if exists
				if($this->userunionChild instanceOf LaboUser) $this->userunionChild->setUserunionParent(null);
				$this->userunionChild = $LaboUser;
				$this->userunionChild->setUserunionParent($this);
				$this->setUnionconfirmed(true);
			}
		} else {
			// null
			// remove old child if exists
			if($this->userunionChild instanceOf LaboUser) $this->userunionChild->setUserunionParent(null);
			$this->setUniontype($this->getRemoveUniontype());
			$this->setUnionconfirmed(false);
		}
		return $this;
	}

	public function synchronizeUniontype() {
		if($this->getUserunion() instanceOf LaboUser) {
			$this->getUserunion()->setUniontype($this->getUniontype());
		}
		return $this;
	}

	public function setUniontype($uniontype = null) {
		if($uniontype === null) $uniontype = $this->getUniontype();
		if(!array_key_exists($uniontype, $this->getUniontypes())) throw new Exception("Type d'union invalide (".$uniontype.").", 1);
		$this->uniontype = $uniontype;
		if($this->uniontype === $this->getRemoveUniontype()) $this->removeUserunion();
		if($this->getUserunion() instanceOf LaboUser && $this->getUserunion()->getUniontype() !== $this->uniontype) $this->getUserunion()->setUniontype($this->uniontype);
		return $this;
	}
	public function getUniontype() {
		return $this->uniontype === null ? $this->getRemoveUniontype() : $this->uniontype;
	}
	public function getUniontypename() {
		return $this->getUniontypes()[$this->getUniontype()];
	}

	public function isGlobalunionValid() {
		return $this->isUserunionValid() && $this->isUniontypeValid() && $this->isUniontypeSynchro() && $this->isUnionsameValid();
	}

	/**
	 * @assert\IsTrue(message="La relation est incomplète.")
	 */
	public function isUserunionValid() {
		if($this->userunionParent instanceOf LaboUser && $this->userunionChild instanceOf LaboUser) return false;
		if($this->userunionParent instanceOf LaboUser) {
			return $this->userunionParent->getUserunionChild() === $this;
		}
		if($this->userunionChild instanceOf LaboUser) {
			return $this->userunionChild->getUserunionParent() === $this;
		}
		return true;
	}

	/**
	 * @assert\IsTrue(message="Votre parent ne vous a pas comme enfant.")
	 */
	public function isUserunionParentValid() {
		if($this->userunionParent instanceOf LaboUser) {
			return $this->userunionParent->getUserunionChild() === $this;
		}
		return true;
	}

	/**
	 * @assert\IsTrue(message="Votre enfant ne vous a pas comme parent.")
	 */
	public function isUserunionChildValid() {
		if($this->userunionChild instanceOf LaboUser) {
			return $this->userunionChild->getUserunionParent() === $this;
		}
		return true;
	}

	/**
	 * @assert\IsTrue(message="Vous devez préciser le type de relation.")
	 */
	public function isUniontypeValid() {
		if(!($this->getUserunion() instanceOf LaboUser)) return true;
		return $this->getUniontype() !== $this->getRemoveUniontype();
	}
	public function isUniontypeSynchro() {
		if(!($this->getUserunion() instanceOf LaboUser)) return true;
		return $this->getUserunion()->getUniontype() === $this->getUniontype();
	}

	/**
	 * @assert\IsTrue(message="Vous ne pouvez pas mettre un utilisateur en relation avec lui-même.")
	 */
	public function isUnionsameValid() {
		return ($this->getUserunion() !== $this);
	}

	public function setUniontypewithuser(LaboUser $LaboUser, $uniontype = null) {
		if($this->getUserunion() === $LaboUser) setUniontype($uniontype);
	}
	public function getUniontypewithuser(LaboUser $LaboUser) {
		return $this->getUserunion() === $LaboUser ? $this->getUniontype() : $this->getRemoveUniontype();
	}

	public function setUnionconfirmed($unionconfirmed) {
		$this->unionconfirmed = $unionconfirmed;
		return $this;
	}
	public function getUnionconfirmed() {
		return $this->unionconfirmed === null ? false : $this->unionconfirmed;
	}




	/**
	 * Set grantedRoles
	 * @param array $grantedRoles
	 * @return LaboUser
	 */
	public function setGrantedRoles($grantedRoles) {
		$this->grantedRoles = $grantedRoles;
		return $this;
	}

	/**
	 * Get grantedRoles
	 * @return array 
	 */
	public function getGrantedRoles($all = true) {
		return $all ? $this->grantedRoles : array_diff($this->grantedRoles, array('IS_AUTHENTICATED_ANONYMOUSLY', 'ROLE_ALLOWED_TO_SWITCH', 'ERROR'));
	}

	/**
	 * Set nom
	 * @param string $nom
	 * @return LaboUser
	 */
	public function setNom($nom) {
		$this->nom = $nom;
		return $this;
	}

	/**
	 * Get nom
	 * @return string 
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Set prenom
	 * @param string $prenom
	 * @return LaboUser
	 */
	public function setPrenom($prenom) {
		$this->prenom = $prenom;
		return $this;
	}

	/**
	 * Get prenom
	 * @return string 
	 */
	public function getPrenom() {
		return $this->prenom;
	}

	/**
	 * Set telephone
	 * @param string $telephone
	 * @return LaboUser
	 */
	public function setTelephone($telephone) {
		$this->telephone = $telephone;
		return $this;
	}

	/**
	 * Get telephone
	 * @return string 
	 */
	public function getTelephone() {
		return $this->telephone;
	}

	/**
	 * Get paniers - INVERSE
	 * @return ArrayCollection 
	 */
	public function getPaniers() {
		return $this->paniers;
	}

	/**
	 * Add panier - INVERSE
	 * @param panier $panier
	 * @return LaboUser
	 */
	public function addPanier(sitePanier $panier) {
		$this->paniers->add($panier);
		return $this;
	}

	/**
	 * Remove panier - INVERSE
	 * @param panier $panier
	 * @return boolean
	 */
	public function removePanier(sitePanier $panier) {
		return $this->paniers->removeElement($panier);
	}

	/**
	 * Renvoie le nombre total d'articles dans le panier de l'utilisateur
	 * @return integer
	 */
	public function getArticlesPanier() {
		$Q = 0;
		$paniers = $this->getPaniers();
		foreach ($paniers as $panier) {
			$Q += $panier->getQuantite();
		}
		return $Q;
	}

	/**
	 * Get factures - INVERSE
	 * @return ArrayCollection 
	 */
	public function getFactures() {
		return $this->factures;
	}

	/**
	 * Get factures by states
	 * @return ArrayCollection 
	 */
	public function getfacturesByStates($states) {
		$states = (array)$states;
		$list = new ArrayCollection();
		foreach ($this->factures as $facture) {
			if(in_array($facture->getState(), $states)) $list->add($facture);
		}
		return $list;
	}

	/**
	 * Set factures - INVERSE
	 * @param ArrayCollection $factures
	 * @return LaboUser
	 */
	public function setFactures(ArrayCollection $factures) {
		$this->factures = $factures;
		return $this;
	}

	/**
	 * Add facture - INVERSE
	 * @param siteFacture $facture
	 * @return LaboUser
	 */
	public function addFacture(siteFacture $facture) {
		if(!$this->factures->contains($facture)) $this->factures->add($facture);
		return $this;
	}

	/**
	 * Remove facture - INVERSE
	 * @param siteFacture $facture
	 * @return boolean
	 */
	public function removeFacture(siteFacture $facture) {
		return $this->factures->removeElement($facture);
	}

	/**
	 * Add message - INVERSE
	 * @param message $message
	 * @return LaboUser
	 */
	public function addMessage(siteMessage $message) {
		$this->messages->add($message);
		return $this;
	}

	/**
	 * Remove message - INVERSE
	 * @param message $message
	 * @return boolean
	 */
	public function removeMessage(siteMessage $message) {
		return $this->messages->removeElement($message);
	}

	/**
	 * Get messages - INVERSE
	 * @return ArrayCollection 
	 */
	public function getMessages($onlyVisible = false) {
		// all
		if($onlyVisible === false) return $this->messages;
		// only visible
		$role = $this->getBestRoleValue();
		$validRoles = array_flip($this->getValidRoles());
		$messages = new ArrayCollection();
		foreach ($this->messages as $message) {
			if($role >= $validRoles[$message->getStatut()->getNiveau()]) $messages->add($message);
			// if(!in_array($message->getStatut()->getNiveau(), array('IS_AUTHENTICATED_ANONYMOUSLY', 'ROLE_USER'))) $messages->add($message);
		}
		return $messages;
	}

	/**
	 * Get messageusers
	 * @return ArrayCollection
	 */
	public function getMessageusers() {
		return $this->messageusers;
	}

	/**
	 * Get messageuser with message (or id)
	 * @return messageuser
	 */
	public function getMessageuser($messageId) {
		if(is_object($messageId)) $messageId = $messageId->getId();
		foreach ($this->getMessageusers() as $mu) {
			if($mu->getMessageuser()->getId() === $messageId) return $mu;
		}
		return null;
	}

	public function getMessageUserMessage($messageId) {
		foreach ($this->getMessageusers() as $mu) {
			if($mu->getMessageuser()->getId() === $messageId) return $mu->getMessageuser();
		}
		return null;
	}

	public function hasMessageUserMessage($message) {
		if(is_object($message)) $message = $message->getId();
		return !is_null($this->getMessageUserMessage($message));
	}

	/**
	 * Get number of messageusers
	 * @return integer
	 */
	public function getNumberOfMessageusers() {
		return $this->messageusers->count();
	}

	/**
	 * Set messageusers
	 * @param ArrayCollection $messageusers
	 * @return message
	 */
	public function setMessageusers(ArrayCollection $messageusers) {
		$this->messageusers = $messageusers;
		return $this;
	}

	/**
	 * Add messageuser
	 * @param messageuser $messageuser
	 * @return message
	 */
	public function addMessageuser(messageuser $messageuser) {
		$this->messageusers->add($messageuser);
		return $this;
	}

	/**
	 * Remove messageuser
	 * @param messageuser $messageuser
	 * @return boolean
	 */
	public function removeMessageuser(messageuser $messageuser) {
		return $this->messageusers->RemoveElement($messageuser);
	}

	/**
	 * Empty messageuser
	 * @return boolean
	 */
	public function emptyMessageusers() {
		return $this->messageusers->clear();
	}


	/**
	 * Set adminhelp
	 * @param boolean $adminhelp
	 * @return LaboUser
	 */
	public function setAdminhelp($adminhelp) {
		$this->adminhelp = $adminhelp;
		return $this;
	}

	/**
	 * Get adminhelp
	 * @return boolean 
	 */
	public function getAdminhelp() {
		return $this->adminhelp;
	}

	/**
	 * Set mail_sitemessages
	 * @param boolean $mail_sitemessages
	 * @return LaboUser
	 */
	public function setMailSitemessages($mail_sitemessages) {
		if($this->sites->count() < 1) $mail_sitemessages = false;
		$this->mail_sitemessages = $mail_sitemessages;
		return $this;
	}

	/**
	 * Get mail_sitemessages
	 * @return boolean 
	 */
	public function getMailSitemessages() {
		return $this->mail_sitemessages && $this->sites->count() > 0;
	}

	/**
	 * Set admintheme
	 * @param string $admintheme
	 * @return LaboUser
	 */
	public function setAdmintheme($admintheme) {
		$skins = $this->getAdminskins();
		if(array_key_exists($admintheme, $skins)) $this->admintheme = $admintheme;
			else $this->admintheme = $this->getDefaultAdminskin();
		return $this;
	}

	/**
	 * Get admintheme
	 * @return string 
	 */
	public function getAdmintheme() {
		return $this->admintheme;
	}

	/**
	 * Set avatar - PROPRIÉTAIRE
	 * @param media $avatar
	 * @return pageweb
	 */
	public function setAvatar(siteImage $avatar = null) {
		if($this->getAvatar() != null) $this->getAvatar()->setUserAvatar(null);
		if($avatar != null) $avatar->setUserAvatar($this);
		$this->avatar = $avatar;
		return $this;
	}

	/**
	 * Get avatar
	 * @return media 
	 */
	public function getAvatar() {
		return $this->avatar;
	}

	/**
	 * Set baseEntity - PROPRIÉTAIRE
	 * @param baseEntity $adresse
	 * @return LaboUser
	 */
	public function setAdresse(siteAdresse $adresse = null) {
		$this->adresse = $adresse;
		if($adresse instanceOf siteAdresse) $adresse->setUser($this);
		return $this;
	}

	/**
	 * Get adresse - PROPRIÉTAIRE
	 * @return baseEntity 
	 */
	public function getAdresse() {
		return $this->adresse;
	}

	/**
	 * Set adresseLivraison - PROPRIÉTAIRE
	 * @param baseEntity $adresseLivraison
	 * @return LaboUser
	 */
	public function setAdresseLivraison(siteAdresse $adresseLivraison = null) {
		$this->adresseLivraison = $adresseLivraison;
		if($adresseLivraison instanceOf siteAdresse) $adresseLivraison->setUserLivraison($this);
		return $this;
	}

	/**
	 * Get adresseLivraison - PROPRIÉTAIRE
	 * @return baseEntity 
	 */
	public function getAdresseLivraison() {
		return $this->adresseLivraison;
	}

	/**
	 * Add site
	 * @param subentity $site
	 * @return LaboUser
	 */
	public function addSite(siteSite $site) {
		$this->sites->add($site);
		return $this;
	}

	/**
	 * Remove site
	 * @param subentity $site
	 * @return boolean
	 */
	public function removeSite(siteSite $site) {
		$result = $this->sites->removeElement($site);
		if($this->sites->count() < 1) $this->setMailSitemessages(false);
		return $result;
	}

	/**
	 * Get sites
	 * @return ArrayCollection
	 */
	public function getSites() {
		if($this->sites->count() < 1) $this->setMailSitemessages(false);
		return $this->sites;
	}

	public function isCollaborator() {
		return $this->sites->count() > 0;
	}

	/**
	 * get langue
	 * @return string
	 */
	public function getLangue() {
		return $this->langue;
	}

	/**
	 * set langue
	 * @param string $langue
	 * @return LaboUser
	 */
	public function setLangue($langue = null) {
		if($langue !== null) $this->langue = $langue;
		return $this;
	}

	// /**
	//  * Renvoie le nom du plus haut role d'un user (ou de l'user de cette entité)
	//  * @param LaboUser $user = null
	//  * @return string
	//  */
	// public function getBestRole(LaboUser $user = null) {
	// 	if($user === null) $user = $this;
	// 	$user_roles = $user->getRoles();
	// 	$best_role = null;
	// 	foreach($this->getValidRoles() as $value => $roleToTest) {
	// 		if(in_array($roleToTest, $user_roles)) $best_role = $roleToTest;
	// 	}
	// 	if($best_role === null) $best_role = reset($this->getValidRoles());
	// 	return $best_role;
	// }

	// /**
	//  * Renvoie les granted de User
	//  * @return array
	//  */
	// public function getGrants() {
	// 	// return $this->getRolesHierarchy()[$this->getBestRole()];
	// 	return array_merge(array($this->getBestRole()), getValidRoles()[$this->getBestRole()]);
	// }

	/**
	 * Renvoie le nom du plus haut role d'un user (ou de l'user de cette entité)
	 * @param LaboUser $user = null
	 * @return string
	 */
	public function getBestRole(LaboUser $user = null) {
		if($user === null) $user = $this;
		$user_roles = $user->getRoles();
		$best_role = null;
		$validRoles = array(0 => 'IS_AUTHENTICATED_ANONYMOUSLY', 1 => 'ROLE_USER', 2 => 'ROLE_TRANSLATOR', 3 => 'ROLE_EDITOR', 4 => 'ROLE_ADMIN', 5 => 'ROLE_SUPER_ADMIN');
		foreach($validRoles as $value => $roleToTest) {
			if(in_array($roleToTest, $user_roles)) $best_role = $roleToTest;
		}
		if($best_role === null) $best_role = reset($validRoles);
		return $best_role;
	}

	/**
	 * Renvoie les granted de User
	 * @return array
	 */
	public function getGrants() {
		$pathToSecurity = __DIR__.'/../../../../../../../app/config/security.yml';
		$yaml = new Parser();
		$rolesArray = $yaml->parse(file_get_contents($pathToSecurity));
		return array_merge(array($this->getBestRole()), $rolesArray['security']['role_hierarchy'][$this->getBestRole()]);
	}

	/**
	 * Renvoie la valeur du plus haut role d'un user (ou de l'user de cette entité)
	 * @param LaboUser $user = null
	 * @return integer
	 */
	public function getBestRoleValue(LaboUser $user = null) {
		$nom_role = $this->getBestRole($user);
		$results = array_keys($this->getValidRoles(), $nom_role);
		if(count($results) > 0) return reset($results);
		return 0;
	}

	/**
	 * Renvoie true si l'utilisateur a des droits au moins identiques sur l'User passé en paramètre
	 * @param LaboUser $user
	 * @return boolean
	 */
	public function haveRight(LaboUser $user) {
		if($this->getId() === null) return false; 
		if($user->getId() == $this->getId()) return true;
		return $this->getBestRoleValue() >= $user->getBestRoleValue();
	}




	/**
	 * Get calendars
	 * @return arrayCollection
	 */
	public function getCalendars() {
		return $this->calendars;
	}

	/**
	 * Set calendars
	 * @param ArrayCollection $calendars
	 * @return LaboUser
	 */
	public function setCalendars(ArrayCollection $calendars) {
		$this->calendars = $calendars;
		return $this;
	}

	/**
	 * add calendar
	 * @param calendar $calendar
	 * @return LaboUser
	 */
	public function addCalendar(calendar $calendar) {
		$this->calendars->add($calendar);
		return $this;
	}

	/**
	 * remove calendar
	 * @param calendar $calendar
	 * @return boolean
	 */
	public function removeCalendar(calendar $calendar) {
		return $this->calendars->removeElement($calendar);
	}







	/**
	 * Get evenements
	 * @return ArrayCollection
	 */
	public function getEvenements() {
		return $this->evenements;
	}

	/**
	 * Get evenements as direct registered
	 * @return ArrayCollection
	 */
	public function getDirectregisteredEvenements() {
		$evenements = new ArrayCollection();
		foreach ($this->evenements as $evenement) {
			if($evenement instanceOf userevenement) $evenements->add($evenement);
		}
		return $evenements;
	}

	/**
	 * Get evenements as invited
	 * @return ArrayCollection
	 */
	public function getInvitedEvenements() {
		$evenements = new ArrayCollection();
		foreach ($this->evenements as $evenement) {
			if($evenement instanceOf invitedevenement) $evenements->add($evenement);
		}
		return $evenements;
	}

	/**
	 * Add baseuserevenement
	 * @param baseuserevenement $baseuserevenement
	 * @return LaboUser
	 */
	public function addEvenement(baseuserevenement $baseuserevenement) {
		if(!$this->evenements->contains($baseuserevenement)) $this->evenements->add($baseuserevenement);
		return $this;
	}

	/**
	 * Remove baseuserevenement
	 * @param baseuserevenement $baseuserevenement
	 * @return LaboUser
	 */
	public function removeEvenement(baseuserevenement $baseuserevenement) {
		return $this->evenements->removeElement($baseuserevenement);
	}

	// /**
	//  * Set evenements
	//  * @param ArrayCollection $evenements
	//  * @return LaboUser
	//  */
	// public function setEvenements(ArrayCollection $evenements) {
	// 	foreach($this->getEvenements() as $userevenement)
	// 		if(!$evenements->contains($userevenement)) $this->removeUserevenement($userevenement);
	// 	foreach($evenements as $userevenement)
	// 		if(!$this->getEvenements()->contains($userevenement)) $this->addUserevenement($userevenement);
	// 	return $this;
	// }

	/**
	 * Get user evenements in interval of dates
	 * @param DateTime $dateDebut = null
	 * @param DateTime $dateFin = null
	 * @return ArrayCollection
	 */
	public function getIntervalEvenements(DateTime $dateDebut = null, DateTime $dateFin = null) {
		$evenements = new ArrayCollection();
		foreach($this->getEvenements() as $evenement) {
			if($evenement->isEvenementBetween($dateDebut, $dateFin)) $evenements->add($evenement->getEvenement());
		}
		return $evenements;
	}

	/**
	 * Get user evenements in interval of dates
	 * @param DateTime $dateDebut = null
	 * @param DateTime $dateFin = null
	 * @return ArrayCollection
	 */
	public function getIntervalDirectregisteredEvenements(DateTime $dateDebut = null, DateTime $dateFin = null) {
		$evenements = new ArrayCollection();
		foreach($this->getDirectregisteredEvenements() as $evenement) {
			if($evenement->isEvenementBetween($dateDebut, $dateFin)) $evenements->add($evenement->getEvenement());
		}
		return $evenements;
	}

	/**
	 * Get user evenements in interval of dates
	 * @param DateTime $dateDebut = null
	 * @param DateTime $dateFin = null
	 * @return ArrayCollection
	 */
	public function getIntervalInvitedEvenements(DateTime $dateDebut = null, DateTime $dateFin = null) {
		$evenements = new ArrayCollection();
		foreach($this->getInvitedEvenements() as $evenement) {
			if($evenement->isEvenementBetween($dateDebut, $dateFin)) $evenements->add($evenement->getEvenement());
		}
		return $evenements;
	}




	public function getProfession() {
		return $this->profession;
	}

	public function setProfession($profession = null) {
		$this->profession = $profession;
		return $this;
	}

	public function getCookies() {
		return $this->cookies;
	}

	public function setCookies($cookies = null) {
		$this->cookies = $cookies;
		return $this;
	}

	public function getPublicite() {
		return $this->publicite;
	}

	public function setPublicite($publicite = true) {
		$this->publicite = $publicite;
		return $this;
	}

	public function setTemppassword($temppassword = null) {
		if(!is_string($temppassword) && $temppassword !== null) throw new Exception("temppassword must be a string (or null) while creating new User!", 1);		
		$this->temppassword = $temppassword;
		return $this;
	}

	public function getTemppassword() {
		return $this->temppassword;
	}





}
<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\services\aeArrays;
use \ReflectionClass;
use \Exception;
use \DateTime;

/**
 * baseEntity
 * 
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * @ORM\MappedSuperclass
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
abstract class baseEntity {

	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="nom", type="string", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $nom;

	/**
	 * @var string
	 * @ORM\Column(name="icon", type="string", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $icon;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $updated;

	/**
	 * @Gedmo\Slug(fields={"nom"})
	 * @ORM\Column(length=128, unique=true)
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "fullcalendar"})
	 */
	protected $slug;

	/**
	 * @var boolean
	 * @ORM\Column(name="default_value", type="boolean", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $default;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture", "fullcalendar"})
	 * @Accessor(getter="getLocale")
	 */
	protected $locale;

	protected $listIcons;

	public function __construct() {
		$this->nom = null;
		$this->icon = null;
		$this->created = new DateTime(); // but updated on @PrePersist
		$this->updated = null;
		$this->default = false;
		$this->slug = null;
		$this->listIcons = null;
		$this->locale = 'fr';
	}

	public function __toString() {
		return (string)$this->getNom();
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function check() {
		// do nothing, here…
		$this->setUpdated(new DateTime());
	}

	/**
	 * @Assert\IsTrue(message="L'entité de base n'est pas conforme.")
	 */
	public function isBaseEntityValid() {
		return true;
	}

	/**
	 * Get id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	public function getListIcons($flat = false) {
		if(!(bool)$flat) {
			return $this->listIcons;
		} else {
			$aeArrays = new aeArrays();
			return $aeArrays->getFlatList($this->listIcons);
		}
	}

	public function setListIcons($array) {
		$this->listIcons = $this->transformIconList($array);
	}

	protected function transformIconList($iconList) {
		$list = array();
		if(is_array($iconList)) {
			foreach ($iconList as $key => $icon) {
				if(is_array($icon)) $list[$key] = $this->transformIconList($icon);
					else if(is_string($icon)) $list[$icon] = preg_replace('#^fa-#', '', $icon);
			}
		} else if(is_string($iconList)) $list[$iconList] = preg_replace('#^fa-#', '', $iconList);
		return $list;
	}

	/**
	 * Un élément par défaut dans la table est-il optionnel ?
	 * @return boolean
	 */
	public function isDefaultNullable() {
		return false;
	}

	/**
	 * Peut'on attribuer plusieurs éléments par défaut ?
	 * true 		= illimité
	 * integer 		= nombre max. d'éléments par défaut
	 * false, 0, 1 	= un seul élément
	 * @return boolean
	 */
	public function isDefaultMultiple() {
		return false;
	}

	/**
	 * Set nom
	 * @param string $nom
	 * @return baseEntity
	 */
	public function setNom($nom) {
		$this->nom = $nom;
		return $this;
	}

	/**
	 * Get nom
	 * @return string 
	 */
	public function getNom() {
		return $this->nom;
	}

	/**
	 * Set locale
	 * @param string $locale
	 * @return baseEntity
	 */
	public function setLocale($locale) {
		$this->locale = $locale;
		return $this;
	}

	/**
	 * Get locale
	 * @return string 
	 */
	public function getLocale() {
		return $this->locale;
	}

	/**
	 * Set icon
	 * @param string $icon
	 * @return baseEntity
	 */
	public function setIcon($icon = null) {
		$this->icon = $icon;
		return $this;
	}

	/**
	 * Get icon
	 * @return string 
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Set default
	 * @param string $default
	 * @return baseEntity
	 */
	public function setDefault($default) {
		$this->default = (bool) $default;
		return $this;
	}

	/**
	 * Get default
	 * @return boolean
	 */
	public function getDefault() {
		return $this->default;
	}

	/**
	 * Set created
	 * @param DateTime $created
	 * @return baseEntity
	 */
	public function setCreated(DateTime $created) {
		$this->created = $created;
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function updateCreation() {
		$this->setCreated(new DateTime());
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function updateDate() {
		$this->setUpdated(new DateTime());
	}

	/**
	 * Set updated
	 * @param DateTime $updated
	 * @return baseEntity
	 */
	public function setUpdated(DateTime $updated) {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime 
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * Set slug
	 * @param integer $slug
	 * @return baseEntity
	 */
	public function setSlug($slug) {
		$this->slug = $slug;
		return $this;
	}    

	/**
	 * Get slug
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}


}
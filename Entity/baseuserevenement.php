<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
// Slug
// use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\baseevenement;
use Labo\Bundle\AdminBundle\Entity\invitedevenement;
use Labo\Bundle\AdminBundle\Entity\userevenement;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \DateTime;
use \ReflectionClass;
use \Exception;


/**
 * baseuserevenement
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\baseuserevenementRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 */
abstract class baseuserevenement {

	/**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var integer
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="evenements", cascade={"persist","refresh","detach"})
	 * @ORM\JoinColumn(name="user_id", nullable=false, unique=false)
	 */
	protected $user;

	/**
	 * @var integer
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\baseevenement", inversedBy="userevenements", cascade={"persist"})
	 * @ORM\JoinColumn(name="evenement_id", nullable=false, unique=false)
	 */
	protected $evenement;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser")
	 * @ORM\JoinColumn(nullable=true, unique=false)
	 */
	protected $parrain;

	/**
	 * @var string
	 * @ORM\Column(name="parrainmailorname", type="string", nullable=true, unique=false)
	 */
	protected $parrainmailorname;

	/**
	 * @ORM\Column(name="position", type="integer", nullable=false, unique=false)
	 */
	protected $position;

	/**
	 * @var array
	 * @ORM\Column(name="history", type="json_array", nullable=true, unique=false)
	 */
	protected $history;
	protected $temp_history;

	/**
	 * @var float
	 * @ORM\Column(name="reglement", type="float", nullable=false, unique=false)
	 */
	protected $reglement;

	/**
	 * @var string
	 * @ORM\Column(name="note", type="text", nullable=true, unique=false)
	 */
	protected $note;

	/**
	 * @var boolean
	 * @ORM\Column(name="sendemail", type="boolean", nullable=false, unique=false)
	 */
	protected $sendemail;

	/**
	 * @var string
	 * @ORM\Column(name="message", type="text", nullable=true, unique=false)
	 */
	protected $message;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
	protected $updated;

	/**
	 * @assert\IsFalse(message="Vous ne pouvez pas vous inviter vous-même.")
	 */
	protected $sameashost;
	/**
	 * @assert\IsFalse(message="Un utilisateur participe déjà à cet évènement.")
	 */
	protected $allreadyparticipant;
	/**
	 * @Assert\IsFalse(message="Un utilisateur est en doublon.")
	 */
	protected $double;

	public function __construct() {
		$this->position = null;
		$this->created = new DateTime();
		$this->updated = null;
		$this->reglement = 0;
		$this->note = null;
		$this->history = array();
		$this->temp_history = array();
		$this->sendemail = true;
		$this->message = null;
		$this->user = null;
		$this->evenement = null;
		$this->sameashost = false;
		$this->allreadyparticipant = false;
		$this->double = false;
	}

	/**
	 * @ORM\PostLoad
	 */
	public function load() {
		$this->sameashost = false;
		$this->allreadyparticipant = false;
		$this->setDouble();
		$this->temp_history = $this->newTemp_history();
		return $this;
	}


	/**
	 * @ORM\PrePersist
	 */
	public function PrePersist() {
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function PreUpdate() {
		$oldTemp_history = $this->temp_history;
		$this->setUpdated(new DateTime());
		$this->newTemp_history();
		if($oldTemp_history !== $this->temp_history) $this->newHystory();
	}

	/**
	 * @assert\IsTrue(message="Vous ne pouvez pas vous parrainer vous-même !")
	 */
	public function isUserevenement_userandparrain_Valid() {
		return $this->getUser() !== $this->getParrain() || $this->getParrain() === null;
	}

	protected function newTemp_history() {
		// NE PAS SUPPRIMER !!!
	}

	protected function newHystory() {
		$date = new DateTime();
		$this->history[$date->format(DATE_ATOM)] = $this->temp_history;
	}

	public function getHistory() {
		$history = $this->history;
		foreach ($history as $key => $value) {
			$value['created'] = is_string($value['created']) ? strtotime($value['created']) : null;
			$value['updated'] = is_string($value['updated']) ? strtotime($value['updated']) : null;
		}
		return $history;
	}

	public function __toString() {
		return (string) $this->getId();
	}

	public function getId() {
		return $this->id;
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }


	/**
	 * @ORM\PreRemove
	 * pre remove userevenement
	 */
	public function PreRemove() {
		$this->setUser(null);
		$this->setEvenement(null);
		return $this;
	}

	public function setSameashost($same) {
		$this->sameashost = (boolean) $same;
	}
	public function setAllreadyparticipant($participant) {
		$this->allreadyparticipant = (boolean) $participant;
	}

	/**
	 * Set user
	 * @param LaboUser $user
	 * @return userevenement
	 */
	public function setUser(LaboUser $user = null) {
		if($user instanceOf LaboUser) {
			// if($this->user !== $user) {
				if($this->user !== null) $this->user->removeEvenement($this);
				$user->addEvenement($this);
				$this->user = $user;
			// }
		} else {
			// null
			if($this->user instanceOf LaboUser) $this->user->removeEvenement($this);
			$this->user = $user;
		}
		return $this;
	}

	/**
	 * Get user
	 * @return User 
	 */
	public function getUser() {
		return $this->user;
	}



	/**
	 * Set sendemail
	 * @param boolean $sendemail
	 * @return baseuserevenement
	 */
	public function setSendemail($sendemail) {
		$this->sendemail = $sendemail;
		return $this;
	}

	/**
	 * Get sendemail
	 * @return boolean 
	 */
	public function getSendemail() {
		return $this->sendemail;
	}

	/**
	 * @Assert\IsTrue(message="Vous devez indiquer un email valide pour pouvoir faire parvenir le mail d'invitation.")
	 */
	public function isSendemail() {
		if($this->getUser() === null) return true;
		return !($this->getSendemail() && ((string)$this->getUser()->getEmail() === '' || $this->getUser()->isTempMail()));
	}

	public function setDouble($double = null) {
		if($double === null) $this->getDouble();
			else $this->double = $double;
		return $this;
	}
	public function getDouble() {
		$this->double = $this->getEvenement()->hasUserdouble();
		return $this->double;
	}


	/**
	 * Set parrain User
	 * @param LaboUser $parrain
	 * @return userevenement
	 */
	public function setParrain(LaboUser $parrain = null) {
		$this->parrain = $parrain;
		if($this->parrain instanceOf LaboUser && !($this->parrain->isTempMail())) $this->parrainmailorname = $this->parrain->getEmail();
		return $this;
	}

	/**
	 * Get parrain
	 * @return LaboUser / null 
	 */
	public function getParrain() {
		return $this->parrain;
	}


	/**
	 * Set parrainmailorname
	 * @param string $parrainmailorname = null
	 * @return userevenement
	 */
	public function setParrainmailorname($parrainmailorname = null) {
		$parrainmailorname = trim(strip_tags((string)$parrainmailorname));
		$this->parrainmailorname = strlen($parrainmailorname) > 0 ? $parrainmailorname : null;
		if($this->parrainmailorname === null) $this->setParrain(null);
		return $this;
	}

	/**
	 * Get parrainmailorname
	 * @return string / null 
	 */
	public function getParrainmailorname() {
		return $this->parrainmailorname;
	}


	/**
	 * Set evenement
	 * @param baseevenement $evenement
	 * @return userevenement
	 */
	public function setEvenement(baseevenement $evenement = null) {
		if($evenement instanceOf baseevenement) {
			if($this->evenement !== $evenement) {
				if($this->evenement !== null) $this->evenement->removeUserevenement($this);
				$evenement->addUserevenement($this);
				$this->evenement = $evenement;
			}
		} else {
			// null
			if($this->evenement instanceOf baseevenement) $this->evenement->removeUserevenement($this);
			$this->evenement = $evenement;
		}
		return $this;
	}

	/**
	 * Get evenement
	 * @return baseevenement 
	 */
	public function getEvenement() {
		return $this->evenement;
	}

	/**
	 * Set position
	 * @param integer $position
	 * @return userevenement
	 */
	public function setPosition($position) {
		$this->position = $position;
		return $this;
	}

	/**
	 * Get position
	 * @return integer
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * Is changeable
	 * @return boolean
	 */
	public function isChangeable() {
		return true;
		$now = new DateTime();
		$debut = clone $this->getEvenement()->getDebut();
		$limit = $debut->modify('-'.$this->getEvenement()->getDelaimodif().' hours');
		$result = ($limit >= $now) && ($this->getEvenement()->isReservable());
		// $result = $this->isNew() || $result;
		return $result;
	}

	public function isNew() {
		return $this->id === null;
	}

	/**
	 * set Note
	 * @param string $note
	 * @return baseuserevenement
	 */
	public function setNote($note = null) {
		$this->note = trim((string)$note.'') != '' ? (string)$note : null;
		return $this;
	}
	/**
	 * get Note
	 * @return string | null
	 */
	public function getNote() {
		return $this->note;
	}


	/**
	 * set Message
	 * @param string $message = null
	 * @return baseuserevenement
	 */
	public function setMessage($message = null) {
		$message = trim(strip_tags((string)$message));
		$this->message = strlen($message) > 0 ? $message : null;
		return $this;
	}
	/**
	 * get Message
	 * @return string | null
	 */
	public function getMessage() {
		return $this->message;
	}




	/********************/
	/*** REGLEMENT    ***/
	/********************/

	/**
	 * Set Reglement
	 * @param float $reglement
	 * @return baseuserevenement
	 */
	public function setReglement($reglement) {
		$this->reglement = (float)$reglement;
		return $this;
	}
	/**
	 * Add to Reglement
	 * @param float $reglement
	 * @return baseuserevenement
	 */
	public function addReglement($reglement) {
		$this->reglement += (float)$reglement;
		return $this;
	}
	/**
	 * solde Reglement
	 * @return baseuserevenement
	 */
	public function soldeReglement() {
		$this->reglement = $this->getMontantReglement();
		return $this;
	}
	/**
	 * Get Reglement
	 * @return float
	 */
	public function getReglement() {
		return $this->reglement;
	}
	/**
	 * Get Reglement rest
	 * @return float
	 */
	public function getRestReglement() {
		return $this->getMontantReglement() - $this->getReglement();
	}
	/**
	 * Arrhes complete ?
	 * @return boolean
	 */
	public function isReglementComplete() {
		return $this->getReglement() >= $this->getMontantReglement();
	}


	/********************/
	/*** ARRHES       ***/
	/********************/

	public function getArrhes() {
		$montantArrhes = $this->getMontantArrhes();
		return $this->getReglement() >= $montantArrhes ? $montantArrhes : $this->getReglement();
	}

	public function getRestArrhes() {
		$montantArrhes = $this->getMontantArrhes();
		return $this->getReglement() >= $montantArrhes ? 0 : $montantArrhes - $this->getReglement();
	}

	public function soldeArrhes() {
		if($this->getRestArrhes() > 0) {
			$this->reglement = $this->getMontantArrhes();
		}
		return $this;
	}

	/**
	 * Arrhes complete ?
	 * @return boolean
	 */
	public function isArrhesComplete() {
		return $this->getReglement() >= $this->getMontantArrhes();
	}







	public function getMontantReglement() {		
		$evenement = $this->getEvenement();
		$userreglement = 0;
		if($evenement instanceOf baseevenement) {
			$tarifs = $evenement->getDetailtarifs();
			$tarifsOptions = $evenement->getTarifsTableOptions();
			$prix = $evenement->getTarif();
			$remisesTable = array('percent' => array(0), 'money' => array(0));
			// $NFlabel = $evenement->getNOFORMlabel();
			$userreglement = $prix;

			foreach($tarifs as $name => $details) {
				$getter = 'get'.ucfirst($name);
				switch($details['_type']) {
					case 'field_multiple':
						$values = array();
						if(method_exists($this->getUser(), $getter)) $values = (array) $this->getUser()->$getter($this);
						$index = 0;
						foreach($values as $value) {
							$remisesTable[$tarifsOptions['items']['user']['type']][$name.'_'.$index++] = floatval($details['values'][$value]['values']['user']);
						}
						break;
					case 'special':
						switch($name) {
							case 'parrainage':
								$index = 0;
								if($this instanceOf userevenement) {
									$values = $this->getInvitedevenements();
									foreach($values as $value) {
										$type = isset($tarifsOptions['options'][$name]['type']) ? $tarifsOptions['options'][$name]['type'] : $tarifsOptions['items']['user']['type'];
										$remisesTable[$type][$name.'_'.$index++] = floatval($details['values']['user']);
									}
								} else {
									$type = isset($tarifsOptions['options'][$name]['type']) ? $tarifsOptions['options'][$name]['type'] : $tarifsOptions['items']['invited']['type'];
									$remisesTable[$type][$name.'_'.$index++] = floatval($details['values']['invited']);									
								}
								break;
							case 'default':
								$index = 0;
								$type = isset($tarifsOptions['options'][$name]['type']) ? $tarifsOptions['options'][$name]['type'] : $tarifsOptions['items']['user']['type'];
								$remisesTable[$type][$name.'_'.$index++] = floatval($details['values']['user']);
								break;
							case 'cumulatif':
								break;
						}
						break;
				}
			}
			if($tarifs['cumulatif']['values']['user']) {
				// cumulatif
				$totalPercent = array_sum($remisesTable['percent']);
				$totalMoney = array_sum($remisesTable['money']);
				$userreglement = $userreglement + $totalMoney;
				$userreglement = $userreglement + ($userreglement / 100 * $totalPercent);
			} else {
				// NOT cumulatif
				$minPercent = min($remisesTable['percent']);
				$minMoney = min($remisesTable['money']);
				$minPrixPercent = $userreglement + ($userreglement / 100 * $minPercent);
				$minPrixMonay = $userreglement + $minMoney;
				$userreglement = $minPrixPercent < $minPrixMonay ? $minPrixPercent : $minPrixMonay;
			}
			// echo('<pre>');
			// var_dump($remisesTable);
			// echo('</pre>');
		}
		return $userreglement;
	}

	public function getMontantArrhes() {		
		$evenement = $this->getEvenement();
		$userarrhes = 0;
		if($evenement instanceOf baseevenement) {
			$tarifs = $evenement->getDetailtarifs();
			$tarifsOptions = $evenement->getTarifsTableOptions();
			// $NFlabel = $evenement->getNOFORMlabel();
			$arrhesTable = array('money' => array(0));
			// $arrhesTable = array('percent' => array(0), 'money' => array(0));

			foreach($tarifs as $name => $details) {
				$getter = 'get'.ucfirst($name);
				switch($details['_type']) {
					case 'field_multiple':
						$values = array();
						if(method_exists($this->getUser(), $getter)) $values = (array) $this->getUser()->$getter($this);
						$index = 0;
						foreach($values as $value) {
							$arrhesTable[$tarifsOptions['items']['arrhes']['type']][$name.'_'.$index++] = floatval($details['values'][$value]['values']['arrhes']);
						}
						break;
					case 'special':
						switch($name) {
							case 'parrainage':
								$index = 0;
								if($this instanceOf userevenement) {
									$values = $this->getInvitedevenements();
									foreach($values as $value) {
										$arrhesTable[$tarifsOptions['items']['arrhes']['type']][$name.'_'.$index++] = floatval($details['values']['arrhes']);
									}
								} else {
									$arrhesTable[$tarifsOptions['items']['arrhes']['type']][$name.'_'.$index++] = floatval($details['values']['arrhes']);
								}
								break;
							case 'default':
								$arrhesTable[$tarifsOptions['items']['arrhes']['type']][$name] = floatval($details['values']['arrhes']);
								break;
							case 'cumulatif':
								break;
						}
						break;
				}
			}
			if($tarifs['cumulatif']['values']['arrhes']) {
				// cumulatif
				// $totalPercent = array_sum($arrhesTable['percent']);
				$totalMoney = array_sum($arrhesTable['money']);
				$userarrhes = $userarrhes + $totalMoney;
				// $userarrhes = $userarrhes + ($userarrhes / 100 * $totalPercent);
			} else {
				// NOT cumulatif
				// $minPercent = min($arrhesTable['percent']);
				$minMoney = min($arrhesTable['money']);
				// $minPrixPercent = $userarrhes + ($userarrhes / 100 * $minPercent);
				$minPrixMonay = $userarrhes + $minMoney;
				// $userarrhes = $minPrixPercent < $minPrixMonay ? $minPrixPercent : $minPrixMonay;
			}
			$userreglement = $this->getMontantReglement();
			if($userarrhes > $userreglement) $userarrhes = $userreglement;
			// echo('<pre>');
			// var_dump($tarifs);
			// var_dump($arrhesTable);
			// echo('</pre>');
		}
		return $userarrhes;
	}

	public function getTotalreglement() {
		return $this->getMontantReglement();
	}

	public function getTotalarrhes() {
		return $this->getMontantArrhes();
	}





	/**
	 * Set created
	 * @param DateTime $created
	 * @return baseuserevenement
	 */
	public function setCreated(DateTime $created) {
		$this->created = $created;
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set updated
	 * @param DateTime $updated
	 * @return baseuserevenement
	 */
	public function setUpdated(DateTime $updated) {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime 
	 */
	public function getUpdated() {
		return $this->updated;
	}


}

<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\baseevenement;
use Labo\Bundle\AdminBundle\Entity\invitedevenement;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\Entity\baseuserevenement;

use \DateTime;
use \ReflectionClass;
use \Exception;


/**
 * userevenement
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\userevenementRepository")
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * @ORM\HasLifecycleCallbacks
 */
class userevenement extends baseuserevenement {

	/**
	 * @ORM\OneToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\invitedevenement", mappedBy="userevenement", orphanRemoval=true, cascade={"persist","remove"})
	 * @Assert\Valid()
	 */
	protected $invitedevenements;

	public function __construct() {
		parent::__construct();
		$this->sendemail = true;
		$this->invitedevenements = new ArrayCollection();
	}

	protected function newTemp_history() {
		// parent::newTemp_history();
		$this->temp_history = array(
			'username' => $this->user->getUsername(),
			'position' => $this->position,
			'quantity' => $this->getQuantity(),
			'note' => $this->note,
			'message' => $this->message,
			'parrainmailorname' => $this->parrainmailorname,
			'parrain' => $this->parrain instanceOf LaboUser ? array(
				'id' => $this->parrain->getId(),
				'username' => $this->parrain->getUsername(),
				'email' => $this->parrain->getEmail()
				) : null,
			'created' => $this->getCreated()->format(DATE_ATOM),
			'updated' => $this->getUpdated() instanceOf DateTime ? $this->getUpdated()->format(DATE_ATOM) : null,
			'reglements' => array(
				// règlement
				'reglement' => $this->getReglement(),
				'reglement_rest' => $this->getRestReglement(),
				'reglement_complete' => $this->isReglementComplete(),
				'reglement_montant' => $this->getMontantReglement(),
				// arrhes
				'arrhes' => $this->getArrhes(),
				'arrhes_rest' => $this->getRestArrhes(),
				'arrhes_complete' => $this->isArrhesComplete(),
				'arrhes_montant' => $this->getMontantArrhes(),
				),
		);
	}


	/**
	 * Get quantity
	 * @return integer
	 */
	public function getQuantity() {
		return count($this->getInvitedevenements()) + 1;
	}

	/**
	 * Get invitedevenements
	 * @return ArrayCollection
	 */
	public function getInvitedevenements() {
		return $this->invitedevenements;
	}

	/**
	 * Add invitedevenement
	 * @param invitedevenement $invitedevenement
	 * @return userevenement
	 */
	public function addInvitedevenement(invitedevenement $invitedevenement) {
		if(!$this->invitedevenements->contains($invitedevenement)) {
			$this->invitedevenements->add($invitedevenement);
		}
		$invitedevenement->setUserevenement($this);
		return $this;
	}

	/**
	 * Set invitedevenements
	 * @param ArrayCollection $invitedevenements
	 * @return userevenement
	 */
	public function setInvitedevenements(ArrayCollection $invitedevenements) {
		foreach ($this->invitedevenements as $invitedevenement) {
			if(!$invitedevenements->contains($invitedevenement)) $this->removeInvitedevenement($invitedevenement);
		}
		foreach ($invitedevenements as $invitedevenement) $this->addInvitedevenement($invitedevenement);
		return $this;
	}

	/**
	 * Remove invitedevenement
	 * @param invitedevenement $invitedevenement
	 * @return booelan
	 */
	public function removeInvitedevenement(invitedevenement $invitedevenement) {
		if($invitedevenements->contains($invitedevenement)) {
			$invitedevenement->setUserevenement(null);
			return $this->invitedevenements->removeElement($invitedevenement);
		}
		return false;
	}

	/**
	 * Clear invitedevenements
	 * @return userevenement
	 */
	public function clearInvitedevenements() {
		$this->invitedevenements->clear();
		return $this;
	}


	public function getTotalreglement() {
		$total = $this->getMontantReglement();
		foreach ($this->getInvitedevenements() as $invitedevenement) {
			$total += $invitedevenement->getMontantReglement();
		}
		return $total;
	}

	public function getTotalarrhes() {
		$total = $this->getMontantArrhes();
		foreach ($this->getInvitedevenements() as $invitedevenement) {
			$total += $invitedevenement->getMontantArrhes();
		}
		return $total;
	}


}

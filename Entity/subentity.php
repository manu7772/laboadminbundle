<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\subentityposition;
use Labo\Bundle\AdminBundle\Entity\tag;
use site\adminsiteBundle\Entity\image;
use Labo\Bundle\AdminBundle\Entity\statut;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \DateTime;
use \Exception;

/**
 * subentity
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\subentityRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 * 
 * @ExclusionPolicy("all")
 */
abstract class subentity extends baseEntity {

	// const VIRTUALGROUPS_PARENTS_PATTERN = '#^(set|add|remove|get)(Group_)(.{3,})(Parent)(s)?$#';
	// const VIRTUALGROUPS_CHILDS_PATTERN = '#^(set|add|remove|get)(Group_)(.{3,})(Child)(s)?$#';
	// const VIRTUALGROUPS_ALL_PATTERN = '#^(group_)(.{3,})(Parent|Child)(s)?$#';

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "menu"})
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="nom", type="string", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture", "menu"})
	 */
	protected $nom;

	/**
	 * @var string
	 * @ORM\Column(name="icon", type="string", length=24, nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "menu"})
	 */
	protected $icon;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $updated;

	/**
	 * @Gedmo\Slug(fields={"nom"})
	 * @ORM\Column(length=128, unique=true)
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "menu"})
	 */
	protected $slug;

	/**
	 * @var boolean
	 * @ORM\Column(name="default_value", type="boolean", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $default;
	/**
	 * @ORM\ManyToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="sites")
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 */
	protected $collaborateurs;

	/**
	 * @var string
	 * @ORM\Column(name="descriptif", type="text", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $descriptif;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\statut")
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @Expose
	 * @MaxDepth(2)
	 * @Groups({"complete", "ajaxlive", "menu"})
	 */
	protected $statut;

	/**
	 * @var boolean
	 * @ORM\Column(name="deletable", type="boolean", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $deletable;

	/**
	 * @var array
	 * @ORM\ManyToMany(targetEntity="Labo\Bundle\AdminBundle\Entity\tag", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, unique=false, onDelete="SET NULL")
	 * @Groups({"complete"})
	 */
	protected $tags;

    /**
     * @ORM\OneToOne(targetEntity="site\adminsiteBundle\Entity\image", orphanRemoval=true, cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 * @Groups({"complete"})
	 * @MaxDepth(1)
     */
    protected $image;

	/**
	 * @var string
	 * @ORM\Column(name="couleur", type="string", length=24, nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "menu"})
	 */
	protected $couleur;

	/**
	 * @var integer
	 * @ORM\Column(name="typentite", type="integer", nullable=true, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $typentite;

	protected $listeTypentites = array();

	protected $class_name;

	public function __construct() {
		parent::__construct();
		$this->collaborateurs = new ArrayCollection();
		$this->descriptif = null;
		$this->statut = null;
		$this->deletable = true;
		$this->tags = new ArrayCollection();
		$this->image = null;
		$this->couleur = "rgba(255,255,255,1)";
		$this->setTypentite($this->getDefaultTypentite()); // Niveau par défaut
	}


	/**
	 * Renvoie l'image principale
	 * @return image
	 */
	public function getMainMedia() {
		return $this->getImage();
	}

	/**
	 * Get keywords
	 * @return string
	 */
	public function getKeywords() {
		$tags = array();
		foreach ($this->getTags() as $tag) $tags[] = $tag->getNom();
		return implode($tags, ', ');
	}

	/**
	 * @Assert\IsTrue(message="L'entité subentity n'est pas conforme.")
	 */
	public function isSubentityValid() {
		return true;
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function check() {
		// parent
		parent::check();
	}

	/**
	 * Set collaborateurs
	 * @param arrayCollection $collaborateurs
	 * @return subentity
	 */
	public function setCollaborateurs(ArrayCollection $collaborateurs) {
		// $this->collaborateurs->clear();
		// incorporation avec "add" et "remove" au cas où il y aurait des opérations (inverse notamment)
		foreach ($this->getCollaborateurs() as $collaborateur) if(!$collaborateurs->contains($collaborateur)) $this->removeCollaborateur($collaborateur); // remove
		foreach ($collaborateurs as $collaborateur) $this->addCollaborateur($collaborateur); // add
		return $this;
	}

	/**
	 * Add collaborateur
	 * @param User $collaborateur
	 * @return site
	 */
	public function addCollaborateur(LaboUser $collaborateur) {
		$this->collaborateurs->add($collaborateur);
		// $collaborateur->addSite($this);
		return $this;
	}

	/**
	 * Remove collaborateur
	 * @param User $collaborateur
	 * @return boolean
	 */
	public function removeCollaborateur(LaboUser $collaborateur) {
		// $collaborateur->removeSite($this);
		return $this->collaborateurs->removeElement($collaborateur);
	}

	/**
	 * Get collaborateurs
	 * @return ArrayCollection
	 */
	public function getCollaborateurs() {
		return $this->collaborateurs;
	}

	/**
	 * Set descriptif
	 * @param string $descriptif
	 * @return subentity
	 */
	public function setDescriptif($descriptif = null) {
		$this->descriptif = $descriptif;
		if(strip_tags(preg_replace('#([[:space:]])+#', '', $this->descriptif)) == '') $this->descriptif = null;
		return $this;
	}

	/**
	 * Get descriptif
	 * @return string 
	 */
	public function getDescriptif() {
		return $this->descriptif;
	}

	/**
	 * Set statut
	 * @param statut $statut
	 * @return subentity
	 */
	public function setStatut(statut $statut) {
		$this->statut = $statut;
		return $this;
	}

	/**
	 * Get statut
	 * @return statut 
	 */
	public function getStatut() {
		return $this->statut;
	}

	/**
	 * Set deletable
	 * @param string $deletable
	 * @return baseEntity
	 */
	public function setDeletable($deletable) {
		$this->deletable = (bool) $deletable;
		return $this;
	}

	/**
	 * Get deletable
	 * @return boolean
	 */
	public function getDeletable() {
		return $this->deletable;
	}

	/**
	 * Set tags
	 * @param arrayCollection $tags
	 * @return subentity
	 */
	public function setTags(ArrayCollection $tags) {
		// $this->tags->clear();
		// incorporation avec "add" et "remove" au cas où il y aurait des opérations (inverse notamment)
		foreach($this->getTags() as $tag) if(!$tags->contains($tag)) $this->removeTag($tag); // remove
		foreach($tags as $tag) $this->addTag($tag); // add
		return $this;
	}

	/**
	 * Add tag
	 * @param tag $tag
	 * @return subentity
	 */
	public function addTag(tag $tag) {
		$this->tags->add($tag);
		return $this;
	}

	/**
	 * Remove tag
	 * @param tag $tag
	 * @return boolean
	 */
	public function removeTag(tag $tag) {
		return $this->tags->removeElement($tag);
	}

	/**
	 * Get tags
	 * @return ArrayCollection 
	 */
	public function getTags() {
		return $this->tags;
	}

	/**
	 * Get tags as a string
	 * @return string 
	 */
	public function getTagsText($glue = ', ') {
		if(!is_string($glue)) $glue = ', ';
		return implode($glue, $this->tags);
	}

	/**
	 * Set image
	 * @param image $image
	 * @return subentity
	 */
	public function setImage(image $image = null) {
		if($this->image != null && $image == null) {
			$this->image->setElement(null);
		}
		$this->image = $image;
		if($this->image != null) {
			$this->image->setElement($this);
			$this->image->setStatut($this->getStatut());
		}
		return $this;
	}

	/**
	 * Get image
	 * @return image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Set couleur
	 * @param string $couleur
	 * @return subentity
	 */
	public function setCouleur($couleur) {
		$this->couleur = $couleur;
		return $this;
	}

	/**
	 * Get couleur
	 * @return string 
	 */
	public function getCouleur() {
		return $this->couleur;
	}

	/**
	 * Get list of typentites
	 * @return array 
	 */
	public function getListeTypentites() {
		return $this->listeTypentites;
	}
	/**
	 * Get default typentite
	 * @return integer
	 */
	public function getDefaultTypentite() {
		$list = array_keys($this->getListeTypentites());
		return count($list) > 0 ? $list[0] : null;
	}

	/**
	 * Set typentite
	 * @param string $typentite
	 * @return fiche
	 */
	public function setTypentite($typentite = null) {
		if($typentite != null) if(!in_array($typentite, array_keys($this->getListeTypentites()))) $typentite = null;
		$this->typentite = $typentite;
		return $this;
	}

	/**
	 * Get typentite
	 * @return string 
	 */
	public function getTypentite() {
		return $this->typentite;
	}

	/**
	 * Get typentite text
	 * @return string 
	 */
	public function getTypentiteText() {
		return isset($this->listeTypentites[$this->typentite]) ? $this->listeTypentites[$this->typentite] : null ;
	}


}
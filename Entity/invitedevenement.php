<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;
use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\Entity\baseevenement;
use Labo\Bundle\AdminBundle\Entity\userevenement;
use Labo\Bundle\AdminBundle\Entity\baseuserevenement;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use site\UserBundle\Entity\User;
use site\UserBundle\Entity\TempUser;

use \DateTime;
use \ReflectionClass;
use \Exception;

/**
 * invitedevenement
 * 
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\invitedevenementRepository")
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * @ORM\HasLifecycleCallbacks
 */
class invitedevenement extends baseuserevenement {

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\userevenement", inversedBy="invitedevenements", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 */
	protected $userevenement;

	/**
	 * @var boolean
	 * @ORM\Column(name="sendemail", type="boolean", nullable=false, unique=false)
	 */
	protected $sendemail;


	public function __construct() {
		parent::__construct();
		$this->setUser(new TempUser());
		// $this->setNom('test');
		$this->userevenement = null;
		$this->sendemail = true;
	}

	/**
	 * @ORM\PreRemove
	 */
	public function PreRemove() {
		parent::PreRemove();
		$this->setParrain(null);
		$this->setUserevenement(null);
	}

	protected function newTemp_history() {
		// parent::newTemp_history();
		$this->temp_history = array(
			'username' => $this->user->getUsername(),
			'position' => $this->position,
			// 'quantity' => $this->getQuantity(),
			'note' => $this->note,
			'message' => $this->message,
			'parrainmailorname' => $this->parrainmailorname,
			'parrain' => $this->parrain instanceOf LaboUser ? array(
				'id' => $this->parrain->getId(),
				'username' => $this->parrain->getUsername(),
				'email' => $this->parrain->getEmail()
				) : null,
			'created' => $this->getCreated()->format(DATE_ATOM),
			'updated' => $this->getUpdated() instanceOf DateTime ? $this->getUpdated()->format(DATE_ATOM) : null,
			'reglements' => array(
				// règlement
				'reglement' => $this->getReglement(),
				'reglement_rest' => $this->getRestReglement(),
				'reglement_complete' => $this->isReglementComplete(),
				'reglement_montant' => $this->getMontantReglement(),
				// arrhes
				'arrhes' => $this->getArrhes(),
				'arrhes_rest' => $this->getRestArrhes(),
				'arrhes_complete' => $this->isArrhesComplete(),
				'arrhes_montant' => $this->getMontantArrhes(),
				),
		);
	}

	public function setUserevenement(userevenement $userevenement = null) {
		$this->userevenement = $userevenement;
		if($this->userevenement instanceOf userevenement) {
			$this->setParrain($userevenement->getUser());
			$this->setEvenement($userevenement->getEvenement());
			// $userevenement->addInvitedevenement($this);
		} else {
			$this->setParrainmailorname(null);
			$this->setEvenement(null);
			// if($this->userevenement instanceOf userevenement) $this->userevenement->removeInvitedevenement($this);
		}
	}

	/**
	 * @assert\IsTrue(message="Le compte de cet utilisateur a été désactivé !")
	 */
	public function isUserenabled() {
		return $this->user->isEnabled() || $this->user instanceOf TempUser;
	}

	public function getUserevenement() {
		return $this->userevenement;
	}

	public function getUniontypes() {
		return $this->user->getUniontypes();
	}

	public function getDefaultUniontype() {
		return $this->user->getDefaultUniontype();
	}

	public function getUniontypename() {
		return $this->user->getUniontypename();
	}

	public function getEmail() {
		return $this->user->isTempMail() ? null : $this->user->getEmail();
	}
	public function setEmail($email) {
		$this->user->setEmail($email);
		return $this;
	}

	public function getNom() {
		return $this->user->getNom();
	}
	public function setNom($nom) {
		$this->user->setNom($nom);
		return $this;
	}

	public function getPrenom() {
		return $this->user->getPrenom();
	}
	public function setPrenom($prenom) {
		$this->user->setPrenom($prenom);
		return $this;
	}

	public function getStatutsocial() {
		return $this->user->getStatutsocial();
	}
	public function setStatutsocial($statutsocial) {
		$this->user->setStatutsocial($statutsocial);
		return $this;
	}


	public function isUserunioneditable() {
		// return true;
		if(!($this->userevenement instanceOf userevenement)) return $this->user->getUserunion() === null;
		return ($this->user->getUserunion() === $this->userevenement->getUser()) || ($this->user->getUserunion() === null);
	}

	public function getUserunion() {
		return $this->isUserunioneditable() ? $this->user->getUserunion() : null;
	}
	public function setUserunion($userunion) {
		if($this->isUserunioneditable()) $this->user->setUserunion($userunion);
		return $this;
	}
	public function getUniontype() {
		return $this->isUserunioneditable() ? $this->user->getUniontype() : $this->user->getRemoveUniontype();
	}
	public function setUniontype($uniontype) {
		if($this->isUserunioneditable()) $this->user->setUniontype($uniontype);
		return $this;
	}

	// public function getUnionwithevenementhost() {
	// 	return $this->user->getUniontypewithuser($this->getUserevenement()->getUser());
	// }
	// public function setUnionwithevenementhost($uniontype) {
	// 	$this->user->setUserforuserunions($uniontype, $this->getUserevenement()->getUser());
	// 	return $this;
	// }


}

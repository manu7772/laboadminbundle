<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
// use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;

use \DateInterval;
use \DateTime;

/**
 * sessiondata
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\sessiondataRepository")
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class sessiondata {

	/**
	 * @var string
	 * @ORM\Id
	 * @ORM\Column(name="id", type="string", nullable=false, unique=true)
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(name="ajaxlive", type="text", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $ajaxlive;

	/**
	 * @var string
	 * @ORM\Column(name="user", type="text", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $user;

	/**
	 * @var string
	 * @ORM\Column(name="ip", type="text", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $ip;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $created;

	/**
	 * @var DateTime
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $updated;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="getCreatedSince")
	 */
	protected $createdSince;
	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="getUpdatedSince")
	 */
	protected $updatedSince;
	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="isDeprecated")
	 */
	protected $isDeprecated;
	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="isCurrentUser", setter="setCurrentUser")
	 */
	protected $isCurrentUser;

	public function __construct() {
		$this->ajaxlive = null;
		$this->user = null;
		$this->ip = null;
		$this->created = new DateTime(); // but updated on @PrePersist
		$this->updated = null;
		$this->setCurrentUser(false);
	}

	/**
	 * Set id
	 * @param string $id
	 * @return sessiondata
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * Get id
	 * @return string
	 */
	public function getid() {
		return $this->id;
	}

	/**
	 * Set ajaxlive
	 * @param string $ajaxlive
	 * @return sessiondata
	 */
	public function setAjaxlive($ajaxlive) {
		$this->ajaxlive = crc32($ajaxlive);
		return $this;
	}

	/**
	 * Get ajaxlive
	 * @return string
	 */
	public function getAjaxlive() {
		return $this->ajaxlive;
	}

	/**
	 * Check if $ajaxlive is different than $ajaxlive
	 * @param string $ajaxlive
	 * @return boolean
	 */
	public function hasAjaxliveChanged($ajaxlive) {
		return crc32($ajaxlive) != $this->getAjaxlive();
	}

	/**
	 * Set user
	 * @param LaboUser $user
	 * @return sessiondata
	 */
	public function setUser(LaboUser $user = null) {
		$this->user = $user instanceOf LaboUser ? $user->getUsername() : 'anon.';
		return $this;
	}

	/**
	 * Get user
	 * @return string
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Set ip
	 * @param string $ip
	 * @return sessiondata
	 */
	public function setIp($ip) {
		$this->ip = $ip;
		return $this;
	}

	/**
	 * Get ip
	 * @return string
	 */
	public function getIp() {
		return $this->ip;
	}

	/**
	 * Set created
	 * @param DateTime $created
	 * @return sessiondata
	 */
	public function setCreated(DateTime $created) {
		$this->created = $created;
		return $this;
	}

	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function updateCreation() {
		$this->setCreated(new DateTime());
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function updateDate() {
		$this->setUpdated(new DateTime());
	}

	/**
	 * Set updated
	 * @param DateTime $updated
	 * @return sessiondata
	 */
	public function setUpdated(DateTime $updated) {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * Get updated
	 * @return DateTime 
	 */
	public function getUpdated() {
		return $this->updated;
	}




	public function is_olderThan($date, $seconds = 600, $time = 'NOW') {
		return $this->getOld($date, $time) >= $seconds;
	}

	public function getOld($date, $time = 'NOW') {
		if(!($date instanceOf DateTime)) $date = new DateTime('NOW');
		$lapse = new DateIntervalEnhanced(date_diff(new DateTime($time), $date));
		return $lapse->to_seconds();
	}

	/**
	 * Created since (in seconds)
	 * @return DateTime 
	 */
	public function getCreatedSince() {
		// $datetime = new DateTime('+ 24 HOURS');
		// return $datetime->format('H:i:s') - $this->getCreated()->format('H:i:s');
		return $this->getOld($this->getCreated());
	}

	/**
	 * Changed since (in seconds)
	 * @return DateTime 
	 */
	public function getUpdatedSince() {
		// $datetime = new DateTime('+ 24 HOURS');
		// return $this->getUpdated() instanceOf DateTime ? $datetime->format('H:i:s') - $this->getUpdated()->format('H:i:s') : null;
		$date = $this->getUpdated();
		if($date == null) $date = $this->getCreated();
		return $this->getOld($date);
	}

	/**
	 * Is deprecated (updated over than 600 sec. - 10 min.)
	 * @return boolean 
	 */
	public function isDeprecated() {
		$date = $this->getUpdated();
		if($date == null) $date = $this->getCreated();
		return $this->is_olderThan($date, 600);
	}

	/**
	 * Set if is current user session
	 * @return sessiondata 
	 */
	public function setCurrentUser($boolean) {
		$this->isCurrentUser = $boolean;
		return $this;
	}

	/**
	 * Is current user session
	 * @return boolean 
	 */
	public function isCurrentUser() {
		return $this->isCurrentUser;
	}



}

class DateIntervalEnhanced {

	private $interval;

	public function __construct(DateInterval $interval) {
		$this->interval = $interval;
		return $this;
	}

	/* Keep in mind that a year is seen in this class as 365 days, and a month is seen as 30 days.         
	   It is not possible to calculate how many days are in a given year or month without a point of  
	   reference in time.*/ 
	public function to_seconds($diff = null) {
		if(!($diff instanceOf DateInterval)) $diff = $this->interval;
		return ($diff->y * 365 * 24 * 60 * 60) + 
			($diff->m * 30 * 24 * 60 * 60) + 
			($diff->d * 24 * 60 * 60) + 
			($diff->h * 60 * 60) + 
			($diff->i * 60) + 
			$diff->s; 
	} 
	
	public function recalculate() { 
		$seconds = $this->to_seconds(); 
		$this->interval->y = floor($seconds/60/60/24/365); 
		$seconds -= $this->interval->y * 31536000; 
		$this->interval->m = floor($seconds/60/60/24/30); 
		$seconds -= $this->interval->m * 2592000; 
		$this->interval->d = floor($seconds/60/60/24); 
		$seconds -= $this->interval->d * 86400; 
		$this->interval->h = floor($seconds/60/60); 
		$seconds -= $this->interval->h * 3600; 
		$this->interval->i = floor($seconds/60); 
		$seconds -= $this->interval->i * 60; 
		$this->interval->s = $seconds; 
	}

	public function format($format) {
		return $this->interval->format($format);
	}
} 

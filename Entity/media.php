<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;

use Labo\Bundle\AdminBundle\Entity\nested;
use Labo\Bundle\AdminBundle\Entity\rawfile;

use \DateTime;
use \Exception;
use \SplFileInfo;

/**
 * media
 *
 * @ORM\Entity(repositoryClass="Labo\Bundle\AdminBundle\Entity\mediaRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @ExclusionPolicy("all")
 * 
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\InheritanceType("JOINED")
 */
abstract class media extends nested {

	const CLASS_IMAGE		= "image";
	const CLASS_PDF			= "pdf";

	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * Nom du fichier original du media
	 * @var string
	 * @ORM\Column(name="originalnom", type="string", length=255, nullable=true, unique=false)
	 */
	protected $originalnom;
		
	/**
	 * Contenu numérique du media
	 * @var string
	 * @ORM\Column(name="binaryFile", type="blob", nullable=true, unique=false)
	 */
	protected $binaryFile;
	
	/**
	 * Type mime (d'origine) du media
	 * @var string
	 * @ORM\Column(name="format", type="string", length=128, nullable=true, unique=false)
	 */
	protected $format;

	/**
	 * Extension originale du nom de fichier du media
	 * @var string
	 * @ORM\Column(name="extension", type="string", length=8, nullable=true, unique=false)
	 */
	protected $extension;

	/**
	 * Stockage du media : 'database' / 'file'
	 * @var string
	 * @ORM\Column(name="stockage", type="string", length=16, nullable=false, unique=false)
	 */
	protected $stockage;

	/**
	 * Informations de recadrage cropper
	 * @var string
	 * @ORM\Column(name="croppingInfo", type="text", nullable=true, unique=false)
	 */
	protected $croppingInfo;

	/**
	 * Taille du fichier d'origine du media
	 * (ou taille du champ "binaryFile" -> à développer)
	 * @var int
	 * @ORM\Column(name="file_size", type="integer", length=10, nullable=true, unique=false)
	 */
	protected $fileSize;

	/**
	 * upload file
	 */
	public $upload_file;


	/****************************/
	/*** DOING WITH RAW FILE ***/
	/**************************/

	/**
	 * @var array
	 * @ORM\OneToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\rawfile", orphanRemoval=true, cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, unique=true, onDelete="SET NULL")
	 */
	protected $rawfile;

	
	protected $infoForPersist;
	protected $authorizedFormatsByType;
	protected $schemaData;
	protected $schemaBase;
	protected $preferedStockage;
	protected $mediaType;

	public function __construct() {
		parent::__construct();
		$this->setCroppingInfo(null);
		$this->setInfoForPersist(null);
		$this->authorizedFormatsByType = null;
		$this->schemaData = null;
		$this->schemaBase = null;
		$this->preferedStockage = null;
		$this->mediaType = null;
	}


	/**
	 * @ORM\PostLoad
	 */
	public function onLoad($construct = false) {
		$this->setInfoForPersist(null);
		$this->authorizedFormatsByType = null;
		$this->schemaData = null;
		$this->schemaBase = null;
		$this->preferedStockage = null;
		$this->mediaType = null;
		// nom
		if($this->getNom() == null) $this->setNom($this->getOriginalnom());
	}

	/**
	 * @Assert\IsTrue(message="Le média n'est pas conforme.")
	 */
	public function isValid() {
		if($this->getStockage() === 'database' && $this->getBinaryFile() == null) return false;
		if($this->preferedStockage === null) return false;
		return true;
	}

	public function getStockageList() {
		return array('database', 'file');
	}

	public function setPreferedStockage($stockage) {
		$stockageList = $this->getStockageList();
		if(in_array($stockage, $stockageList)) $this->preferedStockage = $stockage;
			else throw new Exception('Prefered stockage : '.$stockage.' is not available in '.json_encode($stockageList), 1);
		$this->authorizedFormatsByType = array(
			self::CLASS_IMAGE	=> array('png', 'jpg', 'jpeg', 'gif'),
			self::CLASS_PDF		=> array('pdf'),
			);
		// CLASS_IMAGE
		$this->schemaData = '#^(data:image/('.implode("|", $this->authorizedFormatsByType[self::CLASS_IMAGE]).');base64,)#';
		$this->schemaBase = 'data:__FORMAT__;base64,';
		$this->setStockage($this->getPreferedStockage());
		return $this;
	}

	public function getPreferedStockage() {
		return $this->preferedStockage;
	}

	public function getShemaBase($format = null) {
		// $this->schemaBase = 'data:image/***;base64,';
		if(!is_string($format)) {
			$format = 'png';
			if($this->getFormat() != null) {
				$format = $this->getFormat();
			}
		}
		return preg_replace('#(__FORMAT__)#', $format, $this->schemaBase);
	}

	public function getAuthorizedFormatsByType($type = null) {
		if($type == null) return $this->authorizedFormatsByType;
		return isset($this->authorizedFormatsByType[$type]) ? $this->authorizedFormatsByType[$type] : false;
	}

	public function getTypeOf($typeMime) {
		$exp = explode('/', $typeMime);
		if($exp[0] == self::CLASS_IMAGE) return self::CLASS_IMAGE;
		if($exp[1] == self::CLASS_PDF) return self::CLASS_PDF;
		return 'inconnu';
	}

	public function getExtByMime($typeMime) {
		return explode('/', $typeMime)[1];
	}

	public function upLoad(){
		// $info = $this->getInfoForPersist();
		// if($this->getId() != null) {
		// 	// test only on update…
		// 	if(null === $this->upload_file && null === $this->getBinaryFile() && $info === null) return;
		// }
		return;
	}



	/**
	 * set infoForPersist
	 * @param json/array $infoForPersist = null
	 * @return media
	 */
	public function setInfoForPersist($infoForPersist = null) {
		if(!is_string($infoForPersist)) $infoForPersist = json_encode($infoForPersist);
		$this->infoForPersist = $infoForPersist;
		return $this;
	}

	/**
	 * get infoForPersist
	 * @return array
	 */
	public function getInfoForPersist() {
		return json_decode($this->infoForPersist, true);
	}




	/**
	 * set croppingInfo
	 * Returns false if croppingInfo are not the same
	 * @param json/array $croppingInfo = null
	 * @return boolean
	 */
	public function setCroppingInfo($croppingInfo = null) {
		$oldcroppingInfo = $this->croppingInfo;
		if(!is_string($croppingInfo)) {
			$croppingInfo = json_encode($croppingInfo);
		}
		$this->croppingInfo = $croppingInfo;
		return $this->croppingInfo === $oldcroppingInfo;
	}

	/**
	 * get croppingInfo
	 * @return array
	 */
	public function getCroppingInfo() {
		return json_decode($this->croppingInfo, true);
	}

	/**
	 * get croppingInfo in JSON
	 * @return string
	 */
	public function getJsonCroppingInfo() {
		return $this->croppingInfo;
	}



	/**
	 * Get upload file
	 * @return string 
	 */
	public function getUploadFile() {
		return $this->upload_file;
	}

	/**
	 * Get upload file name
	 * @return string 
	 */
	public function getUploadFile_typemime() {
		if (null === $this->upload_file) return false;
		// http://api.symfony.com/2.0/Symfony/Component/HttpFoundation/File/UploadedFile.html
		return $this->upload_file->getMimeType();
	}

	/**
	 * Get upload file extension
	 * @return string 
	 */
	public function getUploadFile_extension() {
		if (null === $this->upload_file) return false;
		// http://api.symfony.com/2.0/Symfony/Component/HttpFoundation/File/UploadedFile.html
		return $this->upload_file->guessExtension();
	}

	/**
	 * Set binaryFile
	 * @param string $binaryFile
	 * @return media
	 */
	public function setBinaryFile($binaryFile) {
		$this->binaryFile = $binaryFile;
		return $this;
	}

	/**
	 * Get binaryFile
	 * @return string 
	 */
	public function getBinaryFile() {
		if(is_resource($this->binaryFile)) {
			rewind($this->binaryFile);
			return stream_get_contents($this->binaryFile);
		}
		return $this->binaryFile;
	}

	/**
	 * Set rawfile
	 * @param rawfile $rawfile
	 * @return media
	 */
	public function setRawfile(rawfile $rawfile = null) {
		$this->rawfile = $rawfile;
		return $this;
	}

	/**
	 * Get rawfile
	 * @return rawfile 
	 */
	public function getRawfile() {
		return $this->rawfile;
	}




	/**
	 * Define nom
	 * @return media
	 */
	public function defineNom() {
		if($this->nom == null) {
			$date = new DateTime();
			$defaultVersion = $date->format('d-m-Y_H-i-s')."_".rand(10000,99999);
		}
		return $this;
	}



	/**
	 * Set originalnom
	 * @param string $originalnom
	 * @return media
	 */
	public function setOriginalnom($originalnom) {
		$this->originalnom = $originalnom;
		return $this;
	}

	/**
	 * Get originalnom
	 * @return string
	 */
	public function getOriginalnom() {
		return $this->originalnom;
	}

	/**
	 * Set format
	 * @param fileFormat $format
	 * @return media
	 */
	public function setFormat($format) {
		$this->format = $format;
		return $this;
	}

	/**
	 * Get format
	 * @return fileFormat
	 */
	public function getFormat() {
		return $this->format;
	}

	/**
	 * Set extension
	 * @param string $extension
	 * @return media
	 */
	public function setExtension($extension) {
		$this->extension = strtolower($extension);
		return $this;
	}

	/**
	 * Get extension
	 * @return string
	 */
	public function getExtension() {
		return $this->extension;
	}

	/**
	 * Set mediaType
	 * @param string $mediaType
	 * @return media
	 */
	public function setMediaType($mediaType) {
		$this->mediaType = strtolower($mediaType);
		return $this;
	}

	/**
	 * Get mediaType
	 * @return string
	 */
	public function getMediaType() {
		return $this->mediaType;
	}

	/**
	 * Set stockage
	 * @param string $stockage
	 * @return media
	 */
	public function setStockage($stockage) {
		if(in_array($stockage, $this->getStockageList())) {
			$this->stockage = $stockage;
		} else throw new Exception("Stock Support not recognized : ".$stockage.". Need : ".json_encode($this->getStockageList()), 1);
		return $this;
	}

	/**
	 * Get stockage
	 * @return string
	 */
	public function getStockage() {
		return $this->stockage;
	}

	/**
	 * Set fileSize
	 * @param integer $fileSize
	 * @return media
	 */
	public function setFileSize($fileSize) {
		$this->fileSize = $fileSize;
		return $this;
	}

	/**
	 * Get fileSize
	 * @return integer
	 */
	public function getFileSize() {
		return $this->fileSize;
	}




	/**
	 * is media a screenable IMAGE type ?
	 * @return boolean
	 */
	public function isScreenableImage(){
		return ($this->isImage() && ($this->getBinaryFile() != null));
	}

	/**
	 * is media an IMAGE type ?
	 * @return boolean
	 */
	public function isImage(){
		return $this->getShortName(true) == self::CLASS_IMAGE;
	}

	/**
	 * is media a PDF type ?
	 * @return boolean
	 */
	public function isPdf(){
		return $this->getShortName(true) == self::CLASS_PDF;
	}



	/**
	 * Get id
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}




}

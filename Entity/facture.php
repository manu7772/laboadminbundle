<?php

namespace Labo\Bundle\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
// JMS Serializer
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Accessor;
// Slug
use Gedmo\Mapping\Annotation as Gedmo;

use Labo\Bundle\AdminBundle\Entity\item;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\services\aeUnits;

use \DateTime;
use \ReflectionClass;
use \Exception;

/**
 * facture
 * @ORM\EntityListeners({"Labo\Bundle\AdminBundle\services\baseEntityListener"})
 *
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
abstract class facture {

	/**
	 * @var string
	 * @ORM\Id
	 * @ORM\Column(name="id", type="string", nullable=false, unique=true)
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $id;

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\LaboUser", inversedBy="factures")
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @Gedmo\SortableGroup
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @MaxDepth(2)
	 */
	protected $user;

	/**
	 * @ORM\ManyToOne(targetEntity="Labo\Bundle\AdminBundle\Entity\tier", inversedBy="factures")
	 * @ORM\JoinColumn(nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete"})
	 * @MaxDepth(2)
	 */
	protected $boutique;

	/**
	 * @ORM\Column(type="integer")
	 * https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/sortable.md
	 * @Gedmo\SortablePosition
	 * @Expose
	 * @Groups({"complete"})
	 */
	protected $position;

	/**
	 * @var string
	 * @ORM\Column(name="frozenlocale", type="string", length=16, nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $frozenlocale;

	/**
	 * @ORM\Column(type="integer")
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $state;

	/**
	 * @var DateTime
	 * @ORM\Column(name="delailivraison", type="datetime", nullable=true)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 */
	protected $delailivraison;

	/**
	 * @var DateTime
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 */
	protected $created;

	/**
	 * @var string
	 * @ORM\Column(name="panier", type="text", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="getPanier")
	 */
	protected $panier;

	/**
	 * @var string
	 * @ORM\Column(name="frozenuser", type="text", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 * @Accessor(getter="getFrozenuser")
	 */
	protected $frozenuser;

	/**
	 * @var string
	 * @ORM\Column(name="frozenboutique", type="text", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "facture"})
	 * @Accessor(getter="getFrozenuser")
	 */
	protected $frozenboutique;

	/**
	 * @Expose
	 * @Groups({"complete", "ajaxlive", "facture"})
	 * @Accessor(getter="getStateslist")
	 */
	protected $stateslist;

	/**
	 * @var string
	 * @ORM\Column(name="deviseslist", type="text", nullable=false, unique=false)
	 * @Expose
	 * @Groups({"complete", "ajaxlive"})
	 * @Accessor(getter="getDeviseslist")
	 */
	protected $deviseslist;

	/**
	 * @Expose
	 * @Groups({"complete", "facture", "ajaxlive"})
	 * @Accessor(getter="getDevise")
	 */
	protected $devise;

	public function __construct() {
		$this->id = null;
		$this->user = null;
		$this->boutique = null;
		$this->position = 0;
		$this->state = 0;
		$this->delailivraison = null;
		$this->created = new DateTime();
		$this->panier = null;
		$this->frozenuser = null;
		$this->frozenboutique = null;
		$this->deviseslist = null;
		$this->devise = null;
		$this->locale = null;
		$this->frozenlocale = null;
	}

	public function __toString() {
		return $this->getId();
	}

	public function getNom() {
		return $this->getId();
	}

	/**
	 * @ORM\PrePersist
	 * NEVER PERSIST IF STATE == 6 !!!
	 * Please, change state before persist
	 */
	public function crashIfState6() {
		if($this->state === 6) {
			$this->setUser($this->user);
			$this->setBoutique($this->boutique);
			throw new Exception("It is impossible to persist a facture with state = 6 !! Please, change state before.", 1);
		}
		return $this;
	}

    // Get class name
    public function getShortName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getShortName();
    }

    // Get class name
    public function getClassName() {
        $class = new ReflectionClass(get_called_class());
        return $class->getName();
    }

	/**
	 * Set id
	 * @param string $numero = null
	 * @return facture
	 */
	public function setId($numero = null) {
		// auto / uniquement à la création
		if($this->getId() == null) {
			$date = new DateTime('NOW');
			$this->created = $date;
			$date = $date->format('-Ymd-His');
			if($numero != null) $numero = '-'.(string)$numero;
			$this->id = 'fac-'.$this->getUser()->getId().$date.$numero;
		}
		return $this;
	}

	/**
	 * Set locale
	 * @param string $locale
	 * @return facture
	 */
	public function setLocale($locale) {
		$this->locale = $locale;
		if($this->frozenlocale === null) $this->frozenlocale = $locale;
		return $this;
	}

	/**
	 * Get locale
	 * @return string 
	 */
	public function getLocale() {
		return $this->locale;
	}

	/**
	 * Set frozenlocale
	 * @param string $frozenlocale
	 * @return facture
	 */
	public function setFrozenlocale($frozenlocale) {
		$this->frozenlocale = $frozenlocale;
		return $this;
	}

	/**
	 * Get frozenlocale
	 * @return string 
	 */
	public function getFrozenlocale() {
		return $this->frozenlocale;
	}


	public function getId() {
		return $this->id;
	}

	/**
	 * Set user
	 * @param User $user
	 * @return facture
	 */
	public function setUser(LaboUser $user) {
		if($this->state !== 6) {
			$user->addFacture($this);
			if($user->hasRole('ROLE_TESTER')) $this->setStateTest();
		} else if($user->getFactures()->contains($this)) {
			$user->removeFacture($this);
		}
		$this->user = $user;
		$this->setPanier($this->user->getPaniers());
		return $this;
	}

	/**
	 * Get user
	 * @return User 
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Set boutique
	 * @param tier $boutique
	 * @return facture
	 */
	public function setBoutique(tier $boutique) {
		if($this->state !== 6) {
			$boutique->addFacture($this);
		} else if($boutique->getFactures()->contains($this)) {
			$boutique->removeFacture($this);
		}
		$this->boutique = $boutique;
		return $this;
	}

	/**
	 * Get boutique
	 * @return tier 
	 */
	public function getBoutique() {
		return $this->boutique;
	}

	/**
	 * Set position
	 * @param integer $position
	 * @return facture
	 */
	public function setPosition($position) {
		$this->position = $position;
		return $this;
	}

	/**
	 * Get position
	 * @return integer
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * Set deviseslist
	 * @param array $deviseslist
	 * @return facture
	 */
	public function setDeviseslist($deviseslist) {
		$this->deviseslist = serialize($deviseslist);
		return $this;
	}

	/**
	 * Get deviseslist
	 * @return array
	 */
	public function getDeviseslist() {
		return unserialize($this->deviseslist);
	}

	/**
	 * Get devise
	 * @return array
	 */
	public function getDevise() {
		$devises = $this->getDeviseslist();
		return $devises[$this->getFrozenlocale()];
	}

	/**
	 * Set state
	 * @param integer $state
	 * @return facture
	 */
	public function setState($state) {
		if(!array_key_exists($state, $this->getStateslist())) {
			throw new Exception('Facture::setState() : state '.json_encode($state).' does not exist. Choose it in this list: '.json_encode(array_keys($this->getStateslist())).', please.', 1);
		} else {
			$this->state = $state;
			if($this->state === 6) {
				if($this->getUser() !== null) $this->setUser($this->getUser());
				if($this->getBoutique() !== null) $this->setBoutique($this->getBoutique());
			}
		}
		return $this;
	}

	/**
	 * Get state
	 * @return integer
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * Get state name
	 * @return string
	 */
	public function getStatename() {
		return $this->getStateslist()[$this->state]['name'];
	}

	/**
	 * Get state color
	 * @return string
	 */
	public function getStatecolor() {
		return $this->getStateslist()[$this->state]['color'];
	}

	/**
	 * Get state
	 * @return integer
	 */
	public function getStateslist() {
		$this->stateslist = array(
			0 => array(
				'name' => 'state.preparation',
				'color' => 'danger',
				),
			1 => array(
				'name' => 'state.devis',
				'color' => 'warning',
				),
			2 => array(
				'name' => 'state.valid',
				'color' => 'success',
				),
			3 => array(
				'name' => 'state.archived',
				'color' => 'muted',
				),
			4 => array(
				'name' => 'state.canceled',
				'color' => 'muted',
				),
			5 => array(
				'name' => 'state.test',
				'color' => 'info',
				),
			6 => array( // TEMP PANIER : jamais persistée !!!
				'name' => 'state.TEMP',
				'color' => 'info',
				),
			);
		return $this->stateslist;
	}

	public function setStatePrepar() { $this->setState(0); }
	public function setStateDevis() { $this->setState(1); }
	public function setStateValid() { $this->setState(2); }
	public function setStateArchived() { $this->setState(3); }
	public function setStateCanceled() { $this->setState(4); }
	public function setStateTest() { $this->setState(5); }
	public function setStateTEMP() { $this->setState(6); }


	/**
	 * Set details of articles
	 * CAUTION !!! use JMSserializer (with 'json' and group 'facture') on an array of panier
	 * @param string $panier
	 * @return facture
	 */
	public function setPanier($panier) {
		$this->panier = $panier;
	}

	/**
	 * get articles detail
	 * @return array
	 */
	public function getPanier($asJson = false) {
		if(!is_string($this->panier)) return $this->panier;
		return $asJson ? $this->panier : json_decode($this->panier);
	}

	/**
	 * Set details of user
	 * CAUTION !!! use JMSserializer (with 'json' and group 'facture') on a User(LaboUser)
	 * @param string $frozenuser
	 * @return facture
	 */
	public function setFrozenuser($frozenuser) {
		$this->frozenuser = $frozenuser;
	}

	/**
	 * get user(LaboUser) detail
	 * @param boolean $asJson = false
	 * @return array or json
	 */
	public function getFrozenuser($asJson = false) {
		return $asJson ? $this->frozenuser : json_decode($this->frozenuser);
	}

	/**
	 * Set details of user
	 * CAUTION !!! use JMSserializer (with 'json' and group 'facture') on a boutique(tier)
	 * @param string $frozenboutique
	 * @return facture
	 */
	public function setFrozenboutique($frozenboutique) {
		$this->frozenboutique = $frozenboutique;
	}

	/**
	 * get boutique(tier) detail
	 * @param boolean $asJson = false
	 * @return array or json
	 */
	public function getFrozenboutique($asJson = false) {
		return $asJson ? $this->frozenboutique : json_decode($this->frozenboutique);
	}




	/*****************************/
	/** données calculées       **/
	/*****************************/

	public function getNbArticles() {
		return count($this->getPanier());
	}

	public function getNbArticlesDifferents() {
		$paniers = $this->getPanier();
		
	}

	// public function calculPrix($prix) {
	// 	$aeUnits = new aeUnits;
	// 	$ratio = $aeUnits->getRatio($this->article->getUnitPrix(), $this->article->getUnit());
	// 	return $prix * $ratio * $this->getQuantite();
	// }

	/**
	 * Get prixtotal TTC
	 * @return float
	 */
	public function getPrixtotal() {
		$prixtotal = 0;
		$paniers = $this->getPanier(true);
		// echo('<pre>');var_dump($paniers);die('</pre>');
		if(is_string($paniers)) {
			// serialized
			$paniers = json_decode($paniers, true);
			foreach($paniers as $panier) $prixtotal += floatval(str_replace(',', '.', $panier['prix_total']));
		} else {
			foreach($paniers as $panier) $prixtotal += floatval($panier->getPrixtotal());
		}
		return $prixtotal;
	}

	/**
	 * Get prixtotal HT
	 * @return float
	 */
	public function getPrixtotalHT() {
		$prixtotalHT = 0;
		$paniers = $this->getPanier(true);
		// echo('<pre>');var_dump($paniers);die('</pre>');
		if(is_string($paniers)) {
			// serialized
			$paniers = json_decode($paniers, true);
			foreach($paniers as $panier) $prixtotalHT += floatval(str_replace(',', '.', $panier['prix_total_txt']));
		} else {
			foreach($paniers as $panier) $prixtotalHT += floatval($panier->getPrixtotalHT());
		}
		return $prixtotalHT;
	}

	/**
	 * Get getPrixtotal TTC as text
	 * @return string
	 */
	public function getPrixtotaltxt() {
		return number_format($this->getPrixtotal(), 2, ",", "");
	}

	/**
	 * Get getPrixtotal HT as text
	 * @return string
	 */
	public function getPrixtotalHTtxt() {
		return number_format($this->getPrixtotalHT(), 2, ",", "");
	}




	/**
	 * Get created
	 * @return DateTime 
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Set delailivraison
	 * @param DateTime $delailivraison = null
	 * @return facture
	 */
	public function setDelailivraison(DateTime $delailivraison = null) {
		$this->delailivraison = $delailivraison;
		return $this;
	}

	/**
	 * Get delailivraison
	 * @return DateTime 
	 */
	public function getDelailivraison() {
		return $this->delailivraison;
	}



}

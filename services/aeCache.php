<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeData;
use Symfony\Component\DependencyInjection\ContainerInterface;
// yaml parser
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Exception\ParseException;
// aeDates
use Labo\Bundle\AdminBundle\services\aeDates;

use \DateTime;
use \Exception;

/**
 * Service aeCache
 * - Gestion cache
 * class: Labo\Bundle\AdminBundle\services\aeCache
 * arguments: [@service_container]
 */
class aeCache {

	const NAME					= 'aeCache'; 			// nom du service
	const CALL_NAME				= 'aetools.aeCache';	// comment appeler le service depuis le controller/container

	const CACHE_FOLDER			= 'labo_cache';			// cache folder (in web/)
	const YAML_LEVELS			= 32;					// niveaux yaml

	protected $container;			// container
	protected $cachePath;			// path for cache folder
	protected $aeDates;				// service aeDates
	protected $aeDebug;				// service debug
	protected $aeSystemfiles;		// service Systemfiles


	public function __construct(ContainerInterface $container = null) {
		$this->container = $container;
		$this->aeDates = $this->container->get('aetools.aeDates');
		$this->aeSystemfiles = $this->container->get('aetools.aeSystemfiles');
		$this->aeDebug = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeDebug');
		$this->cachePath = 'web/'.self::CACHE_FOLDER;
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IDENTIFICATION CLASSE
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}




	/**
	 * Réduit les objets à leur slug, id ou autre propriété pour format YML
	 * @param array &$array
	 * @return array
	 */
	public function nameObjectsInArray(&$array) {
		foreach ($array as $key => $item) {
			if(is_array($item)) $array[$key] = $this->nameObjectsInArray($item);
			if(is_object($item)) $array[$key] = $this->getBestPropertyOFObject($item);
		}
		return $array;
	}

	/**
	 * Renvoie une string correspondant à l'une des propriétés de l'objet $object
	 * @param object $object
	 * @param string $methods = []
	 * @return string
	 */
	public function getBestPropertyOFObject($object, $methods = []) {
		if(is_string($methods)) $methods = array($methods);
		$methods = array_unique($methods + array('getSlug', 'getId', 'getNom'));
		foreach ($methods as $method) {
			if(method_exists($object, $method)) return $object->$method();
		}
		return get_class($object);
	}

	/**
	 * Écriture un fichier d'après path
	 * @param string $path
	 * @param array $array
	 * @return boolean (nb d'octets si success)
	 */
	public function dump_yaml_toFile($path, $array, $testEnv = true) {
		$launch = true;
		if($this->container !== null && $testEnv === true) {
			$env = $this->container->get('kernel')->getEnvironment();
			if(!in_array($env, array('dev', 'test'))) $launch = false;
		}
		if($launch == true) {
			$this->reduceArray($array, 0);
			$dumper = new Dumper();
			$r = @file_put_contents(
				$path,
				$dumper->dump($this->nameObjectsInArray($array), self::YAML_LEVELS)
			);
		}
		return $r;
	}

	/**
	 * Écriture un fichier d'après path
	 * @param array $array
	 * @return boolean (nb d'octets si success)
	 */
	public function cacheFile($array, $testEnv = true) {
		$this->reduceArray($array, 0);
		$launch = true;
		$r = null;
		if($this->container !== null && $testEnv === true) {
			$env = $this->container->get('kernel')->getEnvironment();
			if(!in_array($env, array('dev', 'test'))) $launch = false;
		}
		if($launch == true) {
			$date = new DateTime();
			$dumper = new Dumper();
			$inc = 0;
			$file = $this->aeSystemfiles->getRootServer().$this->cachePath.'/cacheFile_'.$date->format(aeDates::FORMAT_DATEFILE).'-'.$inc.'.yml';
			while (@file_exists($file)) {
				$file = $this->aeSystemfiles->getRootServer().$this->cachePath.'/cacheFile_'.$date->format(aeDates::FORMAT_DATEFILE).'-'.$inc++.'.yml';
			}
			$r = @file_put_contents(
				$file,
				$dumper->dump($this->nameObjectsInArray($array), self::YAML_LEVELS)
			);
		}
		return $r;
	}

	/**
	 * Renvoie les données en cache du fichier $name. 
	 * $closerDate =
	 * 			- true : renvoie uniquement les données les plus récentes
	 * 			- false : renvoie toutes les données
	 * 			- string :
	 * 				• "NOW" : renvoie les données les plus récentes (plus proches de "NOW")
	 * 				• format ""
	 * @param string $name
	 * @param mixed $closerDate = false
	 * @return array / null
	 */
	public function getCacheNamedFile($name, $closerDate = false) {
		$result = null;
		$file = $this->aeSystemfiles->getRootServer().$this->cachePath.'/'.$name.'.yml';
		if(@file_exists($file)) {
			$parser = new Parser();
			$result = $parser->parse(@file_get_contents($file));
			// echo('<p>cache file '.$name.' : '.gettype($closerDate).'</p>');
			if(count($result) > 0) {
				switch (gettype($closerDate)) {
					case 'boolean':
						if($closerDate === true) {
							$date = $this->aeDates->findDate(array_keys($result), null);
							$result = isset($result[(string)$date]) ? $result[(string)$date] : null;
						}
						break;
					case 'string':
						$date = $this->aeDates->findDate(array_keys($result), $closerDate);
						$result = isset($result[(string)$date]) ? $result[(string)$date] : null;
						break;
					default:
						$date = $this->aeDates->findDate(array_keys($result), null);
						$result = isset($result[(string)$date]) ? $result[(string)$date] : null;
						break;
				}
			}
		}
		// if($name == 'menuNavigation') echo('<p>Date : '.json_encode($result).'</p>');
		return $result;
	}

	/**
	 * Écriture un fichier de nom $name d'après path. Ajoute les infos en fin de fichier, avec date. 
	 * Préciser le nom du fichier uniquement, sans chemin ni extension (.yml)
	 * Le ficher est placé dans le dossier web/debug/
	 * @param string $name
	 * @param array $array
	 * @return boolean (nb d'octets si success)
	 */
	public function cacheNamedFile($name, $array, $testEnv = true, $erase = true) {
		// (boolean) $erase === true ? $mode = 'w' : $mode = 'c'; // c = ajout en début de fichier (et non à la fin, avec 'a')
		(boolean) $erase === true ? $mode = 'w' : $mode = 'a';
		$launch = true;
		$r = true;
		if($this->container !== null && $testEnv === true) {
			$env = $this->container->get('kernel')->getEnvironment();
			if(!in_array($env, array('dev', 'test'))) $launch = false;
		}
		if($launch == true) {
			$dumper = new Dumper();
			$date = new DateTime();
			$dateTxt = $date->format(aeDates::FORMAT_DATETIME);
			$array = array($dateTxt => $this->reduceArray($array, 0));
			$file = $this->aeSystemfiles->getRootServer().$this->cachePath.'/'.$name.'.yml';
			if(file_exists($file)) {
				if(is_writable($file)) {
					$fop = @fopen($file, $mode);
					$r = $r && $fop;
					$r = $r && fwrite($fop, $dumper->dump($this->nameObjectsInArray($array), self::YAML_LEVELS));
					$r = $r && fclose($fop);
					if($r != true) throw new Exception("Fichier Cache n'a pas pu être ajouté en écriture : ".$file, 1);
				} else throw new Exception("Fichier Cache non accessible en écriture : ".$file, 1);
			} else {
				$r = $r && file_put_contents(
					$file,
					$dumper->dump($this->nameObjectsInArray($array), self::YAML_LEVELS)
				);
				if($r != true) throw new Exception("Fichier Cache n'a pas pu être créé en écriture : ".$file, 1);
			}
		}
		return $r;
	}

	public function deleteCacheNamedFile($name) {
		$file = $this->aeSystemfiles->getRootServer().$this->cachePath.'/'.$name.'.yml';
		if(file_exists($file)) {
			if(is_writable($file)) {
				@unlink($file);
				return true;
			}
		}
		return false;
	}

	/**
	 * Réduit la taille des chaînes trop longues dans un tableau. Méthode résursive.
	 * @param array &$array
	 * @param integer $maxSize = 50
	 * @return array
	 */
	protected function reduceArray(&$array, $maxSize = 120) {
		foreach ($array as $key => $item) {
			if(is_string($item)) if(strlen($item) > $maxSize && $maxSize > 0) $array[$key] = substr($item, 0, $maxSize).' ('.strlen($item).' char.)';
			if(is_array($item)) $array[$key] = $this->reduceArray($array[$key], $maxSize);
		}
		return $array;
	}

}


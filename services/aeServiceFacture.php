<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
// use Doctrine\ORM\Event\PreUpdateEventArgs;
// use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializationContext;

use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;

use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\item;
// use Labo\Bundle\AdminBundle\Entity\facture;
use site\UserBundle\Entity\User;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\Entity\tier;
use site\adminsiteBundle\Entity\article;
use site\adminsiteBundle\Entity\facture;

use Labo\Bundle\AdminBundle\services\aeReponse;
use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\services\aeDates;

use \Datetime;

class aeServiceFacture extends aeServiceBaseEntity {

	const NAME					= 'aeServiceFacture';			// nom du service
	const CALL_NAME				= 'aetools.aeServiceFacture';	// comment appeler le service depuis le controller/container
	const CLASS_ENTITY			= 'site\adminsiteBundle\Entity\facture';
	const CLASS_SHORT_ENTITY	= 'facture';
	const TEST					= false;


	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		parent::__construct($container, $EntityManager);
		$this->defineEntity(self::CLASS_ENTITY);
		return $this;
	}


	/**
	 * Get complete panier of user(LaboUser) as a facture
	 * @param LaboUser $user
	 * @return facture
	 */
	public function getUserPanier(LaboUser $user) {
		return $this->getNewUserFacture($user, null, 6);
	}

	/**
	 * Save a new facture of user(LaboUser) and empty panier
	 * @param LaboUser $user
	 * @return aeReponse
	 */
	public function saveUserFacture(LaboUser $user, tier $boutique, $emptyPanier = true) {
		$result = true;
		$facture = $this->getNewUserFacture($user, $boutique);
		$this->getEm()->persist($facture);
		// save
		try {
			$this->getEm()->flush();
		} catch (Exception $e) {
			$result = false;
			$message = 'Erreur enregistrement de facture.<br>Veuillez recommancer, svp., ou contactez l\'administrateur.';
			if($this->aeUrlroutes->isDevOrSadmin()) $message = $e->getMessage();
		}
		if(!isset($message)) $message = 'Facture '.json_encode($facture->getId()).' enregistrée.';
		if($emptyPanier && $result) foreach ($user->getPaniers() as $panier) $this->getEm()->remove($panier);
		return $this
			->container->get(aeData::PREFIX_CALL_SERVICE.'aeReponse')
			->setResult($result)
			->setMessage($message)
			->setData($facture)
			// ->setData(array('id' => $facture->getId()))
			// ->getArrayReponse()
			;
	}

	public function saveFacture($facture) {
		$this->_em->persist($facture);
		$this->_em->flush();
	}

	/**
	 * Get a new facture of user(LaboUser)
	 * @param LaboUser $user
	 * @return facture
	 */
	public function getNewUserFacture(LaboUser $user, tier $boutique = null, $state = 0) {
		$facture = $this->getNewEntity(self::CLASS_ENTITY);
		// if($user->hasRole('ROLE_TESTER')) $state = 5;
		$facture->setState($state);
		$facture->setUser($user);
		if($boutique !== null) $facture->setBoutique($boutique);
		if($state !== 6) {
			$facture->setId();
		} else {
			// $facture->setLocale($this->_locale);
			// $deviseslist = $this->container->getParameter('marketplace')['devises'];
			// $facture->setDeviseslist($deviseslist);
		}
		return $facture;
	}

	/**
	 * Check entity integrity in context
	 * @param item $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
	 * @param $eventArgs = null
	 * @return aeServiceItem
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		parent::checkIntegrity($entity, $context, $eventArgs);
		// if($entity instanceOf item) {
			switch(strtolower($context)) {
				case 'new':
				case 'postload':
					$deviseslist = $this->container->getParameter('marketplace')['devises'];
					$entity->setDeviseslist($deviseslist);
					// $entity->setFrozenlocale($entity->getLocale());
					break;
				case 'prepersist':
					break;
				case 'onflush::insertions':
					// frozen user
					$frozenuser = $this->container->get('jms_serializer')->serialize($entity->getUser(), 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups(["facture"]));
					$entity->setFrozenuser($frozenuser);
					// frozen boutique
					$frozenboutique = $this->container->get('jms_serializer')->serialize($entity->getBoutique(), 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups(["facture"]));
					$entity->setFrozenboutique($frozenboutique);
					// set panier
					if(!is_string($entity->getPanier())) {
						$paniers = $entity->getUser()->getPaniers();
						$paniers = $this->container->get('jms_serializer')->serialize($paniers, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups(["facture"]));
						$entity->setPanier($paniers);
					}
					if(intval($entity->getState()) === 2) $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeEmail')->confirmCdeReadyUser($entity);
					// $entity->setId();
					break;
				case 'postpersist':
					break;
				case 'preupdate':
					break;
				case 'onflush::updates':
					if(intval($entity->getState()) === 2) $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeEmail')->confirmCdeReadyUser($entity);
					break;
				case 'postupdate':
					break;
				case 'preremove':
					break;
				case 'postremove':
					break;
				default:
					break;
			}
		// }
		return $this;
	}

}
<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\HttpFoundation\JsonResponse;

use Labo\Bundle\AdminBundle\services\aeData;

class aeReponse {

    const NAME                  = 'aeReponse';        // nom du service
    const CALL_NAME             = 'aetools.aeReponse'; // comment appeler le service depuis le controller/container

	/**
	 * Données de l'objet aeReponse
	 */
	private $data = array();

	/**
	 * Constructeur
	 * @param boolean $result = true
	 * @param mixed $data = null
	 * @param string $message = null
	 * @return aeReponse
	 */
	public function __construct($result = true, $data = null, $message = null) {
		$this->initAeReponse($result, $data, $message);
		return $this;
	}

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}



	/**
	 * Initialise le service
	 * @param boolean $result = true
	 * @param mixed $data = null
	 * @param string $message = null
	 * @return aeReponse
	 */
	public function initAeReponse($result = true, $data = null, $message = null) {
		$this->setResult((boolean) $result);
		$this->setData($data);
		$this->setMessage((string) $message);
		return $this;
	}

	// GETTERS

	/**
	 * Get result
	 * @return boolean
	 */
	public function getResult() {
		return $this->data["result"];
	}

	/**
	 * Get message
	 * @return string
	 */
	public function getMessage() {
		return $this->data["message"];
	}

	/**
	 * Get data
	 * @param string $name = null
	 * @return mixed
	 */
	public function getData($name = null) {
		if(is_string($name) && isset($this->data["data"][$name])) return $this->data["data"][$name];
		return $this->data["data"];
	}

	/**
	 * Get data type
	 * @return string
	 */
	public function getDataType() {
		return gettype($this->data["data"]);
	}

	/**
	 * Get JsonResponse as JSON encoded data
	 * @param boolean $reset = false
	 * @return string
	 */
	public function getJSONreponse($reset = false) {
		$r = new JsonResponse(json_encode($this->data));
		if((boolean) $reset) $this->initAeReponse();
		return $r;
	}

	/**
	 * Get JsonResponse as array data
	 * @param boolean $reset = false
	 * @return string
	 */
	public function getArrayJSONreponse($reset = false) {
		$r = new JsonResponse($this->data);
		if((boolean) $reset) $this->initAeReponse();
		return $r;
	}

	/**
	 * Get aeReponse as array data
	 * @param boolean $reset = false
	 * @return string
	 */
	public function getArrayReponse($reset = false) {
		if((boolean) $reset) $this->initAeReponse();
		return $this->data;
	}


	// SETTERS

	/**
	 * Set result
	 * @param boolean $result
	 * @return aeReponse
	 */
	public function setResult($result) {
		$this->data["result"] = (boolean) $result;
		return $this;
	}

	/**
	 * Add result
	 * @param boolean $result
	 * @return aeReponse
	 */
	public function addResult($result) {
		$this->data["result"] = $this->data["result"] && (boolean) $result;
		return $this;
	}

	/**
	 * Set message
	 * @param string $message
	 * @return aeReponse
	 */
	public function setMessage($message = null) {
		if($message == null) $message = "";
		$this->data["message"] = trim((string)$message);
		return $this;
	}

	/**
	 * Set data
	 * @param mixed $data
	 * @return aeReponse
	 */
	public function setData($data = null) {
		$this->data["data"] = $data;
		return $this;
	}

	/**
	 * Add data with name
	 * @param mixed $data
	 * @return aeReponse
	 */
	public function addData($name, $data = null) {
		if($this->data["data"] == null) $this->data["data"] = array();
		if(is_array($this->data["data"])) $this->data["data"][$name] = $data;
		return $this;
	}


}
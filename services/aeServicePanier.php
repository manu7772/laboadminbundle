<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
// use Doctrine\ORM\Event\PreUpdateEventArgs;
// use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializationContext;

use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;

use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\item;
use Labo\Bundle\AdminBundle\Entity\panier;
use site\UserBundle\Entity\User;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use site\adminsiteBundle\Entity\article;

use Labo\Bundle\AdminBundle\services\aeReponse;
use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\services\aeDates;

use \Datetime;

class aeServicePanier extends aeServiceBaseEntity {

	const NAME					= 'aeServicePanier';			// nom du service
	const CALL_NAME				= 'aetools.aeServicePanier';	// comment appeler le service depuis le controller/container
	const CLASS_ENTITY			= 'site\adminsiteBundle\Entity\panier';
	const CLASS_SHORT_ENTITY	= 'panier';
	const TEST					= false;

	protected $router;
	protected $trans;

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		parent::__construct($container, $EntityManager);
		$this->defineEntity(self::CLASS_ENTITY);
		$this->router = $this->container->get('router');
		$this->trans = $this->container->get('translator');
		return $this;
	}


	// /**
	//  * messageNonConnecte
	//  * @return string
	//  */
	// protected function messageNonConnecte() {
	// 	$login = '<a href="'.$this->router->generate('fos_user_security_login').'"><button type="button" class="btn btn-warning">LOGIN</button></a>';
	// 	$regis = '<a href="'.$this->router->generate('fos_user_registration_register').'"><button type="button" class="btn btn-danger">Créer mon compte</button></a>';
	// 	return "Vous devez vous connecter à votre compte pour acheter en ligne.<br />Si vous n'en avez pas, vous pouvez créer un compte facilement.<br /><br />".$login."&nbsp;".$regis;
	// }

	public function checkPanier(LaboUser $user) {
		if(self::TEST == false) {
			$aeReponse = new aeReponse();
			$aeReponse->setData(array());
			$paniers = $user->getPaniers();
			$message = array();
			$aeServiceArticle = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceArticle');
			foreach($paniers as $panier) {
				$article = $panier->getArticle();
				if(!$article->isPanierable()) {
					$aeReponse->addData($article->getId(), $article->getNom());
					$message[] = $article->getNom();
					$this->SupprimeArticle($article, $user);
				}
			}
			if(count($message) > 0) {
				// items deleted…
				$message = $this->trans->transchoice("actions.articleverified", count($message), array('%count%' => count($message), '%articles%' => implode(', ', $message)), self::CLASS_SHORT_ENTITY);
				$aeReponse->setMessage($message);
				$aeReponse->setResult(false);
			}
			return $aeReponse;
		}
		return new aeReponse(true, array('TEST' => self::TEST), 'Phase de test');
	}

	/**
	 * Get info from $postParams['panier'] ( = panier uniquid)
	 * @param array &$postParams
	 * @return $postParams
	 */
	public function computePanier(&$postParams) {
		// User
		if(isset($postParams['user'])) {
			if(!($postParams['user'] instanceOf LaboUser)) {
				if(isset($postParams['user']['id'])) $postParams['user'] = $postParams['user']['id'];
				$postParams['user'] = (integer)$postParams['user'];
				if($postParams['user'] > 0) $postParams['user'] = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceUser')->getRepo()->find($postParams['user']);
			}
		} else {
			$postParams['user'] = null;
		}
		if(!($postParams['user'] instanceOf LaboUser)) $postParams['user'] = $this->container->get('security.token_storage')->getToken()->getUser();

		if($postParams['action'] != 'empty') {
			if(isset($postParams['panier'])) {
				// $postParams = array_merge($postParams, json_decode($postParams['panier']));
				// panier
				$postParams['panier'] = $this->getRepo()->getOneArticleOfUser($postParams);
				if($postParams['panier'] instanceOf panier) {
					$postParams = array_merge($postParams, $postParams['panier']->getArrayForId());
				}
			}
			// article
			if(isset($postParams['article'])) {
				if(!($postParams['article'] instanceOf article))
					if(isset($postParams['article']['id'])) $postParams['article'] = $postParams['article']['id'];
					$postParams['article'] = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceArticle')->getRepo()->find((integer)$postParams['article']);
			}
			// panier
			if(!isset($postParams['panier'])) $postParams['panier'] = $this->getRepo()->getOneArticleOfUser($postParams);
			if(!($postParams['panier'] instanceOf panier)) $postParams['panier'] = null;
		}
	}

	protected function verifPanierBeforeChange($postParams) {
		if(self::TEST == false) {
			if(!$postParams['article']->isPanierable()) {
				// article is not panierable
				$add = $this->trans->trans("actions.articlenotpanierable", array('%article%' => $postParams['article']->getNom()), self::CLASS_SHORT_ENTITY);
				$r = $this->checkPanier($postParams['user']);
				if($r->getResult() == false) {
					$message = $r->getMessage();
					$r->setMessage($message.' / '.$add);
				} else {
					$r = new aeReponse(false, $this->getInfosPanier($postParams['user'], true), $add);
				}
				return $r;
			}
			if(!($postParams['user'] instanceOf LaboUser)) return new aeReponse(false, null, $this->trans->trans("errors.usernotfound", [], self::CLASS_SHORT_ENTITY));
			// verif par action
			switch ($postParams['action']) {
				case 'supp':
				case 'remove':
					if(!($postParams['panier'] instanceOf panier)) return new aeReponse(false, $this->getInfosPanier($user, true), $this->trans->trans("actions.articlenotexist", array('%article%' => $postParams['article']->getNom()), self::CLASS_SHORT_ENTITY));
					break;
				case 'add':
					if(!($postParams['article'] instanceOf article)) return new aeReponse(false, $this->getInfosPanier($user, true), $this->trans->trans("errors.articlenotfound", array('%article%' => $postParams['article']->getNom()), self::CLASS_SHORT_ENTITY));
					break;
			}
		}
		return new aeReponse(true, array('TEST' => self::TEST), 'Phase de test');
	}

	/**
	 * ajoute article
	 * @param array postParams
	 * @return aeReponse
	 */
	public function ajouteArticle($postParams) {
		$verif = $this->verifPanierBeforeChange($postParams);
		if($verif->getResult() == false) return $verif;

		if($postParams['panier'] === null) {
			// article non présent dans le panier
			$shortname = $this->getEntityClassName('panier');
			$postParams['panier'] = new $shortname();
			$postParams['panier']->setUser($postParams['user']);
			$postParams['panier']->setArticle($postParams['article']);
			$postParams['panier']->setQuantite($postParams['quantite']);
			$postParams['panier']->setVolume($postParams['volume']);
			$postParams['panier']->setUnit($postParams['unit']);
			$this->getEm()->persist($postParams['panier']);
		} else {
			// article déjà présent au moins 1 fois
			if($postParams['article']->getGroupbasket()) {
				$postParams['panier']->setVolume($postParams['panier']->getVolume() + $postParams['volume']);
			} else {
				$postParams['panier']->ajouteQuantite($postParams['quantite']);
			}
		}
		$this->getEm()->flush();
		return new aeReponse(true, $this->getInfosPanier($postParams['user'], true), $this->trans->transchoice("actions.articleadded", $postParams['quantite'], array('%count%' => $postParams['quantite'], '%article%' => $postParams['article']->getNom().' '.$postParams['volume'].$postParams['unit']), self::CLASS_SHORT_ENTITY));
	}

	/**
	 * reduitArticle
	 * @param array postParams
	 * @return aeReponse
	 */
	public function reduitArticle($postParams) {
		$verif = $this->verifPanierBeforeChange($postParams);
		if($verif->getResult() == false) return $verif;

		// article déjà présent au moins 1 fois
		$postParams['panier']->retireQuantite($postParams['quantite']);
		if($postParams['panier']->getQuantite() < 1) {
			// plus d'articles…
			$this->getEm()->remove($postParams['panier']);
			$r = new aeReponse(true, $this->getInfosPanier($postParams['user'], true), $this->trans->trans("actions.articledeleted", array('%article%' => $postParams['article']->getNom()), self::CLASS_SHORT_ENTITY));
		} else {
			if(!$postParams['article']->isPanierable()) {
				$r = $this->checkPanier($postParams['user']);
			} else {
				// reste encore un ou des articles…
				$r = new aeReponse(true, $this->getInfosPanier($postParams['user'], true), $this->trans->transchoice("actions.articledretired", $postParams['quantite'], array('%count%' => $postParams['quantite'], '%article%' => $postParams['article']->getNom()), self::CLASS_SHORT_ENTITY));
			}
		}
		if($r->getResult()) $this->getEm()->flush();
		return $r;
	}

	/**
	 * SupprimeArticle
	 * @param array postParams
	 * @return aeReponse
	 */
	public function SupprimeArticle($postParams) {
		$verif = $this->verifPanierBeforeChange($postParams);
		if($verif->getResult() == false) return $verif;

		$this->getEm()->remove($postParams['panier']);
		$this->getEm()->flush();
		return new aeReponse(true, $this->getInfosPanier($user, true), $this->trans->trans("actions.articledeleted", array('%article%' => $postParams['article']->getNom()), self::CLASS_SHORT_ENTITY));
	}

	/**
	 * Vide le panier de l'utilisateur $user (courant si non précisé)
	 * @param LaboUser $user
	 * @return aeReponse
	 */
	public function videPanier(LaboUser $user = null) {
		if(!($user instanceOf LaboUser)) return new aeReponse(false, null, $this->trans->trans("errors.usernotfound", [], self::CLASS_SHORT_ENTITY));
		if(count($user->getPaniers()) < 1) {
			// Le panier est déjà vide
			$r = new aeReponse(false, $this->getInfosPanier($user, true), $this->trans->trans("actions.contenuempty", [], self::CLASS_SHORT_ENTITY));
		} else {
			// Le panier contient au moins 1 article
			foreach($user->getPaniers() as $panier) $this->getEm()->remove($panier);
			$this->getEm()->flush();
			$r = new aeReponse(true, $this->getInfosPanier($user, true), $this->trans->trans("actions.contenudeleted", [], self::CLASS_SHORT_ENTITY));
		}
		return $r;
	}

	// /**
	//  * getArticlesOfUser
	//  * @param LaboUser $user
	//  * @return array of article
	//  */
	// public function getArticlesOfUser(LaboUser $user = null) {
	// 	if($user instanceOf LaboUser) {
	// 		return $this->getRepo()->getUserArticles($user->getId());
	// 	} else return null;
	// }

    public function getSerialized($requirements = [], LaboUser $user = null, $shortname = null) {
    	$this->initSerializeRequirements($requirements, $user, $shortname);
		//
		$paniers = array();
		if($requirements['user'] instanceOf LaboUser && $requirements['user_condition']) {
			// $paniers = $this->getInfosPanier($requirements['user']);
			$paniers['paniers'] = $requirements['user']->getPaniers();
			$paniers["quantite"] = 0;
			$paniers["prixtotal"] = 0;
			$paniers["prixtotalHT"] = 0;
			$paniers["created"] = null;
			$paniers["updated"] = null;
			$articles = array();
			foreach($paniers['paniers'] as $key => $panier) {
				$article = $panier->getArticle();
				if(!isset($articles[$article->getId()])) $articles[$article->getId()] = 0;
				$articles[$article->getId()]++;
				//
				$paniers['complement'][$panier->getId()]['prixtotal'] = $panier->getPrixtotal();
				$paniers['complement'][$panier->getId()]['prixtotalHT'] = $panier->getPrixtotalHT();
				// $paniers['complement'][$panier->getId()]['prixtotaltxt'] = $panier->getPrixtotaltxt();
				// $paniers['complement'][$panier->getId()]['prixtotalHTtxt'] = $panier->getPrixtotalHTtxt();
				$paniers['complement'][$panier->getId()]['url_article'] = $this->router->generate('site_pageweb_article', array('itemSlug' => $article->getSlug()));
				//
				$paniers["quantite"] += $panier->getQuantite();
				$paniers["prixtotal"] += $panier->getPrixtotal();
				$paniers["prixtotalHT"] += $panier->getPrixtotalHT();
				// created
				if($paniers["created"] instanceOf Datetime) {
					if($paniers["created"] < $panier->getCreated()) $paniers["created"] = $panier->getCreated();
				} else {
					$paniers["created"] = $panier->getCreated();
				}
				// updated
				if($panier->getUpdated() instanceOf Datetime) {
					$paniers["updated"] = $panier->getUpdated();
					if($paniers["updated"] instanceOf Datetime) {
						if($paniers["updated"] > $panier->getUpdated()) $paniers["updated"] = $panier->getUpdated();
					} else {
						$paniers["updated"] = $panier->getUpdated();
					}
				}
				//
			}
			$paniers["prixtotal"] = number_format($paniers["prixtotal"], 2, ',', '');
			$paniers["prixtotalHT"] = number_format($paniers["prixtotalHT"], 2, ',', '');
			$paniers["bytype"] = count($paniers['paniers']);
			$paniers["byarticle"] = count($articles);
			// serialize
			return $this->container->get('jms_serializer')->serialize($paniers, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups($requirements['JmsGroups']));
			// echo('<pre>');var_dump($paniers);die('</pre>');
			// return $paniers;
		}
		return json_encode($paniers);
	}

	/**
	 * getInfosPanier
	 * Renvoie le nombre d'articles dans un array :
	 * -> "bytype" = nombre d'articles différents
	 * -> "quantite" = nombre total d'articles
	 * @param LaboUser $user = null
	 * @return array
	 */
	public function getInfosPanier(LaboUser $user = null) {
		if($user instanceOf LaboUser) {
			$paniers = $user->getPaniers();
			$articles = array();
			$infopanier["quantite"] = 0;
			$infopanier["prixtotal"] = 0;
			$infopanier["created"] = null;
			$infopanier["updated"] = null;
			foreach($paniers as $panier) {
				$infopanier["quantite"] += $panier->getQuantite();
				$infopanier["prixtotal"] += $panier->getPrixtotal();
				// created
				if($infopanier["created"] instanceOf Datetime) {
					if($infopanier["created"] < $panier->getCreated()) $infopanier["created"] = $panier->getCreated()->format(aeDates::FORMAT_DATETIME);
				} else {
					$infopanier["created"] = $panier->getCreated()->format(aeDates::FORMAT_DATETIME);
				}
				// updated
				if($panier->getUpdated() instanceOf Datetime) {
					$infopanier["updated"] = $panier->getUpdated()->format(aeDates::FORMAT_DATETIME);
					if($infopanier["updated"] instanceOf Datetime) {
						if($infopanier["updated"] > $panier->getUpdated()) $infopanier["updated"] = $panier->getUpdated()->format(aeDates::FORMAT_DATETIME);
					} else {
						$infopanier["updated"] = $panier->getUpdated()->format(aeDates::FORMAT_DATETIME);
					}
				}
				$article = $panier->getArticle();
				if(!isset($articles[$article->getId()])) $articles[$article->getId()] = 0;
				$articles[$article->getId()]++;
				// informations complètes
				$infopanier["paniers"][$panier->getUniquid()]['article'] = $article;
				// $infopanier["paniers"][$panier->getUniquid()]['article'] = $article->getDataForPanier(false);
				$infopanier["paniers"][$panier->getUniquid()]['panier'] = $panier;
				// $infopanier["paniers"][$panier->getUniquid()]['panier']['position'] = $panier->getPosition();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']['created'] = $panier->getCreated()->format(aeDates::FORMAT_DATETIME);
				// $infopanier["paniers"][$panier->getUniquid()]['panier']["volume"] = $panier->getVolume();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']["unit_panier"] = $panier->getUnit();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']["quantite"] = $panier->getQuantite();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']["prixtotal"] = $panier->getPrixtotal();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']["prixtotaltxt"] = $panier->getPrixtotaltxt();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']["prixtotalHT"] = $panier->getPrixtotalHT();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']["prixtotalHTtxt"] = $panier->getPrixtotalHTtxt();
				// $infopanier["paniers"][$panier->getUniquid()]['panier']['updated'] = $panier->getUpdated() instanceOf Datetime ? $panier->getUpdated()->format(aeDates::FORMAT_DATETIME) : null;
				$infopanier["paniers"][$panier->getUniquid()]['url'] = $this->router->generate('site_pageweb_article', array('itemSlug' => $article->getSlug()));
			}
			$infopanier["prixtotal"] = number_format($infopanier["prixtotal"], 2, ',', '');
			$infopanier["bytype"] = count($paniers);
			$infopanier["byarticle"] = count($articles);
			return $infopanier;
		} else return null;
	}


}
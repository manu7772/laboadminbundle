<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Labo\Bundle\AdminBundle\Entity\baseEntity;

/** call in controller with $this->get('aetools.aeSubentity'); */
class aeServiceSubentity extends aeServiceBaseEntity {

	const NAME                  = 'aeServiceSubentity';				// nom du service
	const CALL_NAME             = 'aetools.aeServiceSubentity';		// comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\subentity';

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		parent::__construct($container, $EntityManager);
		$this->defineEntity(self::CLASS_ENTITY);
		return $this;
	}


	// /**
	//  * Check entity integrity in context
	//  * @param subentity $entity
	//  * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove', 'PreFlush')
	//  * @param $eventArgs = null
	//  * @return aeServiceSubentity
	//  */
	// public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
	// 	parent::checkIntegrity($entity, $context, $eventArgs);
	// 	// if($entity instanceOf subentity) {
	// 		switch(strtolower($context)) {
	// 			case 'new':
	// 				break;
	// 			case 'postload':
	// 				break;
	// 			case 'prepersist':
	// 				break;
	// 			case 'postpersist':
	// 				break;
	// 			case 'preupdate':
	// 				break;
	// 			case 'postupdate':
	// 				break;
	// 			case 'preremove':
	// 				break;
	// 			case 'postremove':
	// 				break;
	// 			case 'preflush':
	// 				break;
	// 			default:
	// 				break;
	// 		}
	// 	// }
	// 	return $this;
	// }




}
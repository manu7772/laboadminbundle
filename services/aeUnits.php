<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use \Exception;

/**
 * Service aeUnits
 * - fonctionnalités d'unités
 * class: Labo\Bundle\AdminBundle\services\aeUnits
 * arguments: null
 */
class aeUnits {

    const NAME                  = 'aeUnits';			// nom du service
    const CALL_NAME             = 'aetools.aeUnits';	// comment appeler le service depuis le controller/container

	protected $units;
	protected $default;

	public function __construct() {
		$this->units = array(
			// quantité
			'p' => array(
				'name' => 'pièce',
				'group' => 'Quantités',
				'corresp' => array(
					'p' => 1,
					'x3' => 3,
					'x6' => 6,
					'x12' => 12,
					'x20' => 20,
					'x50' => 50,
					'x100' => 100,
					),
				),
			'x3' => array(
				'name' => 'les 3',
				'group' => 'Quantités',
				'corresp' => array(
					'p' => 1/3,
					'x3' => 3/3,
					'x6' => 6/3,
					'x12' => 12/3,
					'x20' => 20/3,
					'x50' => 50/3,
					'x100' => 100/3,
					),
				),
			'x6' => array(
				'name' => 'les 6',
				'group' => 'Quantités',
				'corresp' => array(
					'p' => 1/6,
					'x3' => 3/6,
					'x6' => 6/6,
					'x12' => 12/6,
					'x20' => 20/6,
					'x50' => 50/6,
					'x100' => 100/6,
					),
				),
			'x12' => array(
				'name' => 'douzaine',
				'group' => 'Quantités',
				'corresp' => array(
					'p' => 1/12,
					'x3' => 3/12,
					'x6' => 6/12,
					'x12' => 12/12,
					'x20' => 20/12,
					'x50' => 50/12,
					'x100' => 100/12,
					),
				),
			'x20' => array(
				'name' => 'vingtaine',
				'group' => 'Quantités',
				'corresp' => array(
					'p' => 1/20,
					'x3' => 3/20,
					'x6' => 6/20,
					'x12' => 12/20,
					'x20' => 20/20,
					'x50' => 50/20,
					'x100' => 100/20,
					),
				),
			'x50' => array(
				'name' => 'cinquantaine',
				'group' => 'Quantités',
				'corresp' => array(
					'p' => 1/50,
					'x3' => 3/50,
					'x6' => 6/50,
					'x12' => 12/50,
					'x20' => 20/50,
					'x50' => 50/50,
					'x100' => 100/50,
					),
				),
			'x100' => array(
				'name' => 'centaine',
				'group' => 'Quantités',
				'corresp' => array(
					'p' => 1/100,
					'x3' => 3/100,
					'x6' => 6/100,
					'x12' => 12/100,
					'x20' => 20/100,
					'x50' => 50/100,
					'x100' => 100/100,
					),
				),
			// poids
			'Kg' => array(
				'name' => 'kilogramme',
				'group' => 'Poids',
				'corresp' => array(
					'Kg' => 1,
					'g' => 0.001,
					),
				),
			'g' => array(
				'name' => 'gramme',
				'group' => 'Poids',
				'corresp' => array(
					'Kg' => 1000,
					'g' => 1,
					),
				),
			// volume
			'L' => array(
				'name' => 'litre',
				'group' => 'Volumes',
				'corresp' => array(
					'L' => 1,
					'dl' => 0.1,
					'cl' => 0.01,
					'ml' => 0.001,
					),
				),
			'dl' => array(
				'name' => 'décilitre',
				'group' => 'Volumes',
				'corresp' => array(
					'L' => 10,
					'dl' => 1,
					'cl' => 0.1,
					'ml' => 0.01,
					),
				),
			'cl' => array(
				'name' => 'centilitre',
				'group' => 'Volumes',
				'corresp' => array(
					'L' => 100,
					'dl' => 10,
					'cl' => 1,
					'ml' => 0.1,
					),
				),
			'ml' => array(
				'name' => 'millilitre',
				'group' => 'Volumes',
				'corresp' => array(
					'L' => 1000,
					'dl' => 100,
					'cl' => 10,
					'ml' => 1,
					),
				),
			);
		$keys = $this->getListOfUnits();
		$this->setDefaultUnit(reset($keys));
	}

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}


	/**
	 * Get list of unit short names
	 * @return array
	 */
	public function getListOfUnits() {
		return array_keys($this->units);
	}

	/**
	 * Get grouped list of unit short names
	 * @return array
	 */
	public function getListOfUnitsByGroups() {
		$list = array();
		foreach ($this->units as $key => $unit) {
			if(!isset($list[$unit['group']])) $list[$unit['group']] = array();
			$list[$unit['group']][] = $key;
		}
		return $list;
	}

	public function isSameGroup($unit1, $unit2 = null) {
		if($unit2 == null) $unit2 = $unit1;
		$unit1 = $this->getUnitInfo($unit1);
		$unit2 = $this->getUnitInfo($unit2);
		return $unit1['group'] == $unit2['group'];
	}

	public function getRatio($unit1, $unit2 = null) {
		if($unit2 == null) return 1;
		// if($unit2 == null) $unit2 = $unit1;
		$info = $this->getUnitInfo($unit1);
		if(!isset($info['corresp'][$unit2])) throw new Exception('Error aeUnits::getRatio() : units "'.$unit1.'" and "'.$unit2.'" are not corresponding.', 1);
		return $info['corresp'][$unit2];
	}

	/**
	 * Get list of unit names
	 * @return array
	 */
	public function getListOfUnitNames() {
		$labels = array();
		foreach ($this->units as $short => $unit) $labels[] = $unit['name'];
		return $labels;
	}

	/**
	 * Get grouped list of unit names
	 * @return array
	 */
	public function getListOfUnitNamesByGroups() {
		$list = array();
		foreach ($this->units as $key => $unit) {
			if(!isset($list[$unit['group']])) $list[$unit['group']] = array();
			$list[$unit['group']][] = $unit['name'];
		}
		return $list;
	}

	/**
	 * Unit short name exists
	 * @param string $unit
	 * @return boolean
	 */
	public function unitExists($unit) {
		return isset($this->units[$unit]);
	}

	/**
	 * Get unit name with unit short name (or default unit name if $unit == null)
	 * @param string $unit = null
	 * @return string
	 */
	public function getUnitName($unit = null) {
		$unit = $unit == null ? $this->getDefaultUnit() : $unit;
		if(!$this->unitExists($unit)) throw new Exception('aeUnits::getUnitName() error : unit '.json_encode($unit).' does not exist. Should be in '.json_encode($this->getListOfUnits()), 1);
		return $this->units[$unit]['name'];
	}

	/**
	 * Get unit info with unit short name (or default unit info if $unit == null)
	 * @param string $unit = null
	 * @return string
	 */
	public function getUnitInfo($unit = null) {
		$unit = $unit == null ? $this->getDefaultUnit() : $unit;
		if(!$this->unitExists($unit)) throw new Exception('aeUnits::getUnitInfo() error : unit '.json_encode($unit).' does not exist. Should be in '.json_encode($this->getListOfUnits()), 1);
		return $this->units[$unit];
	}

	public function getCorresp($unit = null, $detail = false) {
		$unit = $unit == null ? $this->getDefaultUnit() : $unit;
		if(!$this->unitExists($unit)) throw new Exception('aeUnits::getCorresp() error : unit '.json_encode($unit).' does not exist. Should be in '.json_encode($this->getListOfUnits()), 1);
		if(!$detail) return $this->units[$unit]['corresp'];
		$list = array();
		foreach ($this->units[$unit]['corresp'] as $key => $oneUnit) $list[$key] = $this->units[$key];
		return $list;
	}

	/**
	 * Set default unit
	 * @param string $unit
	 * @return aeUnits
	 */
	public function setDefaultUnit($unit) {
		if(!$this->unitExists($unit)) throw new Exception('aeUnits::setDefaultUnit() error : unit '.json_encode($unit).' does not exist. Should be in '.json_encode($this->getListOfUnits()), 1);
		$this->default = $unit;
		return $this;
	}

	/**
	 * Get default unit short name
	 * @return string
	 */
	public function getDefaultUnit() {
		return $this->default;
	}

	/**
	 * Get ChoiceList for form select options
	 * @return ChoiceList
	 */
	public function getChoiceListOfUnits() {
		return new ChoiceList($this->getListOfUnitsByGroups(), $this->getListOfUnitNamesByGroups());
	}

}
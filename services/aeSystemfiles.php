<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeData;

/**
 * Service aeSystemfiles
 * - Gestion des fichers/dossiers
 * class: Labo\Bundle\AdminBundle\services\aeSystemfiles
 * arguments: null
 */
class aeSystemfiles {

	const NAME					= 'aeSystemfiles';				// nom du service
	const CALL_NAME				= 'aetools.aeSystemfiles';		// comment appeler le service depuis le controller/container

	protected $gotoroot;
	protected $rootPath;
	protected $aslash;
	protected $recursiveTree;
	protected $memo = '__self';
	protected $currentPath;

	/**
	 * Constructeur
	 * @return aeSystemfiles
	 */
	public function __construct() {
		// slashes
		switch(strtoupper(aeData::SERVEUR_TYPE)) {
			case "WINDOWS":
				$this->aslash = aeData::WIN_ASLASH;
				break;
			case "UNIX/LINUX":
			default:
				$this->aslash = aeData::ASLASH;
				break;
		}
		$this->gotoroot = __DIR__.aeData::GO_TO_ROOT;
		return $this;
	}

	public function __destruct() {
		$this->close();
	}




	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}




	/**
	 * Ajoute un slash en fin de path s'il n'existe pas
	 * @param string $path
	 * @return string
	 */
	protected function addEndSlash($path) {
		// ajoute un slash en fin si non existant
		if(substr($path, -1, 1) != aeData::SLASH) $path .= aeData::SLASH;
		// supprime les slashes doubles…
		$path = preg_replace("#".aeData::SLASH."(".aeData::SLASH.")+"."#", aeData::SLASH, $path);
		// echo($path."<br>");
		return $path;
	}

	/**
	 * Renvoie le chemin vers la racine du site
	 * @return string
	 */
	public function getRootServer() {
		return $this->gotoroot;
	}

	/**
	 * Définit un nouveau path à partir du dossier WEB
	 * @param string $path
	 * @return string
	 */
	public function setWebPath($path = "") {
		$rootPath = $this->addEndSlash($this->gotoroot.aeData::WEB_PATH.$path);
		if(file_exists($rootPath)) {
			$this->close();
			$this->rootPath = $rootPath;
			$this->currentPath = $rootPath;
			$this->recursiveTree = array(dir($this->currentPath));
			$this->rewind();
			return $this;
		} else return false;
	}

	/**
	 * Définit un nouveau path à partir du dossier ROOT
	 * @param string $path
	 * @return string
	 */
	public function setRootPath($path = "") {
		$rootPath = $this->addEndSlash($this->gotoroot.$path);
		if(file_exists($rootPath)) {
			$this->close();
			$this->rootPath = $rootPath;
			$this->currentPath = $rootPath;
			$this->recursiveTree = array(dir($this->currentPath));
			$this->rewind();
			return $this;
		} else return false;
	}

	public function createFolderInWeb($name) {
		$path = $this->currentPath;
		$this->setWebPath();
		$this->verifDossierAndCreate($name);
		$this->setRootPath($this->currentPath);
		return $this->getRootPath();
	}

	/**
	 * Renvoie le contenu à partir d'un path (ou path courant)
	 * !!! insensible à la casse par défaut
	 * renvoie un tableau : 
	 * 		["path"]	= chemin
	 * 		["nom"]		= nom du fichier
	 * 		["full"]	= chemin + nom
	 *		["type"]	= fichier / dossier
	 * @param string/null - path à analyser (currentPath par défaut) (si "/" au début : "/web/" ou "/", on reprend à la racine du site)
	 * @param string $motif - motif preg pour recherche de nom
	 * @param string $genre - "fichiers" ou "dossiers" ou null (null = tous)
	 * @param boolean $recursive - true par défaut / recherche récursive (true = recherche dans les sous-dossiers également)
	 * @param boolean $casseSensitive - true par défaut
	 * @return array
	 */
	public function exploreDir($path = null, $motif = null, $genre = null, $recursive = true, $casseSensitive = true) {
		$path = $this->addEndSlash($path);
		$this->savePath();
		$this->setRootPath($path);
		$this->liste = array();
		while (false !== ($entry = $this->exploreDirectory($path, $motif, $genre, $recursive, $casseSensitive))) {
			$this->liste[] = $entry;
		}
		$this->close();
		$this->restoreSavedPath();
		return $this->liste;
	}

	protected function exploreDirectory($path = null, $motif = null, $genre = null, $recursive = true, $casseSensitive = true) {
		$path = $this->addEndSlash($path);
		$path2 = array();
		// motif
		if($motif === null) $motif = ".+";
		// genre fichier/dossier
		$genre === "dossiers" ? $fichier = false : $fichier = true ;
		$genre === "fichiers" ? $dossier = false : $dossier = true ;
		// casseSensitive
		$casseSensitive === false ? $sens = "i" : $sens = "" ;
		// parcours…
		while(count($this->recursiveTree) > 0) {
			$d = end($this->recursiveTree);
			if(false !== ($entry = $d->read())) {
				if(!preg_match("#".aeData::NOFILES."#", $entry)) {
					if((is_file($d->path.$entry)) && (preg_match("#".$motif."#".$sens, $entry)) && ($fichier === true)) {
						// fichier
						$path2["path"] = $d->path;
						$path2["nom"]  = $entry;
						$path2["full"] = $d->path.$entry;
						$path2["sitepath"] = str_replace($this->getRootServer(), "", $path2["path"]);
						$path2["type"] = "fichier";
						return $path2;
					}
					if(is_dir($this->addEndSlash($d->path.$entry))) {
						if((preg_match("#".$motif."#".$sens, $entry)) && ($dossier === true)) {
							// dossier
							$path2["path"] = $d->path;
							$path2["nom"]  = $entry;
							$path2["full"] = $d->path.$entry;
							$path2["sitepath"] = str_replace($this->getRootServer(), "", $path2["path"]);
							$path2["type"] = "dossier";
						}
						// sous-dossiers
						if($recursive === true) {
							if(false !== ($child = dir($d->path.$entry.aeData::SLASH))) {
								// $this->currentPath = $d->path.$entry.$this->aslash;
								$this->recursiveTree[] = $child;
							}
						}
						if(count($path2) > 0) {
							return $path2;
						}
					}
				}
			} else {
				// supprime le dernier élément de recusriveTree en le fermant (close)
				array_pop($this->recursiveTree)->close();
			}
		}
		return false;
	}
	
	/**
	 * read - OBSOLETE
	 * Recherche un fichier $type dans le dossier courant ou ses enfants
	 * !!! insensible à la casse par défaut
	 * renvoie un tableau : 
	 * 		["path"]	= chemin
	 * 		["nom"]		= nom du fichier
	 * 		["full"]	= chemin + nom
	 * @param string $type (expression régulière)
	 * @return array
	 */
	public function read($type = null, $casseSensitive = false) {
		if($casseSensitive === false) $sens = "i"; else $sens = "";
		while(count($this->recursiveTree)>0) {
			$d = end($this->recursiveTree);
			if((false !== ($entry = $d->read()))) {
				if(!preg_match("#".aeData::NOFILES."#", $entry)) {
					$path["path"] = $d->path;
					$path["nom"]  = $entry;
					$path["full"] = $d->path.$entry;
					
					if(is_file($d->path.$entry)) {
						if($type !== null) $r=preg_match('#'.$type.'#'.$sens, $entry); else $r = true;
						if($r == true || $r == 1) return $path;
					}
					else if(is_dir($d->path.$entry.$this->aslash)) {
						// $this->currentPath = $d->path.$entry.$this->aslash;
						if($child = @dir($d->path.$entry.$this->aslash)) {
							$this->recursiveTree[] = $child;
						}
					}
				}
			} else {
				array_pop($this->recursiveTree)->close();
			}
		}
		return false;
	}

	/**
	 * readAll - OBSOLETE
	 * renvoie la liste de tous les fichiers contenus dans le dossier et ses enfants
	 * !!! insensible à la casse par défaut
	 * renvoie un tableau : 
	 * 		["path"]	= chemin
	 * 		["nom"]		= nom du fichier
	 * 		["full"]	= chemin + nom
	 * @return array
	 */
	public function readAll($type = null, $path = null, $casseSensitive = true) {
		// if(null !== $path) $this->setWebPath($path);
		// 	else $this->setWebPath($this->rootPath); // réinitialise
		if(null !== $path) $this->setRootPath($path);
			// else $this->setRootPath(); // réinitialise
		$this->liste = array();
		// echo "<span style='color:white;'> Path : ".$this->getRootPath()."</span><br /><br />";
		while (false !== ($entry = $this->read($type, $casseSensitive))) {
			// echo $entry["path"]."<span style='color:pink;'>".$entry["nom"]."</span><br />";
			$this->liste[] = $entry;
		}
		$this->close();
		return $this->liste;
	}

	protected function rewind() {
		$this->closeChildren();
		$this->rewindCurrent();
	}

	protected function rewindCurrent() {
		return end($this->recursiveTree)->rewind();
	}

	protected function close() {
		if(is_array($this->recursiveTree)) while(true === ($d = array_pop($this->recursiveTree))) {
			$d->close();
		}
	}

	protected function closeChildren() {
		while(count($this->recursiveTree) > 1 && false !== ($d = array_pop($this->recursiveTree))) {
			$d->close();
			return true;
		}
		return false;
	}

	/**
	 * getRootPath
	 * Renvoie le dossier racine
	 * @return string
	 */
	public function getRootPath() {
		return isset($this->rootPath) ? $this->rootPath : false ;
	}

	/**
	 * getCurrentPath
	 * Renvoie le dossier courant
	 * @return string
	 */
	public function getCurrentPath() {
		return isset($this->currentPath) ? $this->currentPath : false ;
	}

	/**
	 * Retrouve les fichiers $file dans le dossier courant et tous les dossiers enfants
	 * @param array $files (peut être des expressions régulières => voir la méthode "read()")
	 * @return array
	 */
	public function findFilesEverywhere($files) {
		$r = array();
		if(is_string($files)) $files = array($files);
		foreach($files as $file) {
			$search = $this->readAll($file, null, true);
			if(count($search) > 0) foreach($search as $found) {
				$r[] = $found;
			}
		}
		return $r;
	}

	/**
	 * Retrouve et efface les fichiers $file dans le dossier courant et tous les dossiers enfants
	 * @param array $files (peut être des expressions régulières => voir la méthode "read()")
	 * @return array
	 */
	public function deleteFilesEverywhere($files) {
		$r = array();
		if(is_string($files)) $files = array($files);
		foreach($files as $file) {
			$search = $this->readAll($file, null, false);
			if(count($search) > 0) foreach($search as $erase) {
				$t = $this->deleteFile($erase["full"]);
				if($t === true) $r['succes'][] = $t;
					else $r['echec'][] = $t;
			}
		}
		return $r;
	}

	/**
	 * Efface le fichier $fileName s’il est dans le dossier courant (ou préciser le chemin !)
	 * @param $fileName
	 * @return boolean
	 */
	public function deleteFile($fileName) {
		$r = false;
		if(is_string($fileName)) {
			if(file_exists($fileName)) {
				if(@unlink($fileName)) $r = true;
			}
		}
		return $r;
	}

	/**
	 * Efface tous les fichiers contenus dans le tableau $files, depuis le dossier courant (ou préciser les chemins !)
	 * @param array $files
	 * @return array
	 */
	public function deleteFiles($files) {
		$r = array();
		if(is_string($files)) { $f = $files; $files = array(); $files[0] = $f; }
		$err = 0;
		foreach($files as $file) {
			$res = $this->deleteFile($file);
			if($res === false) $err++; else $r[] = $res;
		}
		if($err > 0) $r = false;
		return $r;
	}

	/**
	 * Efface le dossier $dir (préciser le chemin !)
	 * @param array $files
	 * @param boolean $deleteIn - efface les fichiers contenus avant
	 * @return array
	 */
	public function deleteDir($dir, $deleteIn = false) {
		$r = false;
		if((file_exists($dir)) && (is_dir($dir))) {
			if($deleteIn === true) {
				// efface les fichiers contenus // Ne marche pas POUR L'INSTANT !!!
				$this->findAndDeleteFiles(aeData::ALL_FILES, $dir);
			}
			if(@rmdir($dir)) $r = true;
				else $r = false;
		} else $r = false;
		return $r;
	}

	/**
	 * Recherche et efface tous les fichiers contenus dans $files
	 * (préciser le chemin de départ ou utilise la valeur de $rootPath)
	 * @param array/string $files
	 * @param string $path - depuis root site
	 */
	public function findAndDeleteFiles($files, $path = null) {
		$r = array();
		if(null !== $path) $this->setRootPath($path);
			// else $this->setWebPath($this->rootPath); // réinitialise
		if(is_string($files)) $files = array($files);
		$err = 0;
		foreach($files as $file) {
			// $this->readAll("^".$file."$");
			$this->readAll($file); // --> dans $this->liste
			if(count($this->liste) > 0) foreach($this->liste as $fichier) {
				$res = $this->deleteFile($fichier['full']);
				if($res === false) $err++; else $r[] = $res;
			}
		}
		if($err > 0) $r = false;
		return $r;
	}

	///// Créations/suppressions de dossiers

	public function createTreeFoldersInWeb($tree, $path = "") {
		foreach ($tree as $item) {
			if(is_string($item)) $this->createFolderInWeb($path.$item);
			if(is_array($item)) foreach ($item as $folder => $subtree) {
				$this->createFolderInWeb($path.$folder);
				$this->createTreeFoldersInWeb($subtree, $path.$folder.aeData::SLASH);
			}
		}
		return $this;
	}

	/**
	 * Crée un dossier s'il n'existe pas. 
	 * Crée tout les dossiers intermédiaires si besoin. 
	 * ex. : pour "web/images/thumbnails/mini/" => créera "thumbnails", puis "mini" s'ils n'existent pas
	 * @param string $dossier
	 * @param integer $chmod (en mode octal)
	 * @return boolean / string
	 */
	public function verifDossierAndCreate($dossier, $chmod = null) {
		$result = true;
		$dossiers = preg_split('#['.aeData::SLASH.']+#', $dossier, -1, PREG_SPLIT_NO_EMPTY);
		if(!preg_match('#^[0-7]{4}$#', (string)$chmod)) $chmod = aeData::DEFAULT_CHMOD;
		// création des dossiers
		$cumul = "";
		foreach ($dossiers as $dossier) {
			$doss = $this->getCurrentPath().$cumul.$dossier;
			if(!file_exists($doss)) {
				if(!is_dir($doss)) {
					if(!mkdir($doss, $chmod, true)) {
						return false;
					}
				}
			}
			$cumul .= $dossier.aeData::SLASH;
		}
		return $result;
	}

	/**
	 * avance de $path depuis le path courant
	 * @param string $path
	 * @return boolean
	 */
	public function gotoFromCurrentPath($path = null) {
		$rootPath = $this->getCurrentPath().$path;
		if(file_exists($rootPath)) {
			$this->close();
			$this->rootPath = $rootPath;
			$this->currentPath = $rootPath;
			$this->recursiveTree = array(dir($this->currentPath));
			$this->rewind();
			return $this;
		} else return false;
	}

	/**
	 * Vérifie si un dossier existe (le crée si nécessaire) et s'y place en tant que dossier courant
	 * @param string $type - type de rapport
	 * @return string - chemin courant
	 */
	public function verifAndGotoFromCurrentPath($type = null) {
		$this->rootpath = $this->fmparameters['dossiers']['pathrapports'];
		// vérifie la présence du dossier pathrapports et pointe dessus
		$this->setWebPath();
		$this->verifDossierAndCreate($this->rootpath);
		$this->setWebPath($this->rootpath);
		if(is_string($type)) {
			$path = $this->rootpath.$type.aeData::SLASH;
			$this->verifDossierAndCreate($type);
			$this->setWebPath($path);
			// echo('Current path : '.$this->getCurrentPath().'<br>');
			return $path;
		}
		return $this->rootpath;
	}


	///// Mémorisations de chemins courants (paths)

	/**
	 * sauvegarde le chemin courant avec un nom
	 * @param $nom
	 * @return aetools
	 */
	public function savePath($nom = null) {
		if(!is_string($nom)) $nom = $this->memo;
		$this->pathMemo[$nom] = $this->getCurrentPath();
		return $this;
	}

	/**
	 * Récupère la liste des paths sauvagardés
	 * @return array
	 */
	public function getSavedPaths() {
		return $this->pathMemo;
	}

	/**
	 * Récupère la liste des noms des paths sauvagardés
	 * @return array
	 */
	public function getSavedPathNames() {
		return array_keys($this->pathMemo);
	}

	/**
	 * Supprime les paths sauvegardés (tous, ou celui nommé / ceux nommés)
	 * si $nom = true, supprime tous les paths
	 * @param mixed $nom
	 * @return aetools
	 */
	public function reinitSavePath($nom = null) {
		if(!is_string($nom) && $nom !== true) $nom = $this->memo;
		if($nom !== true) {
			if(is_string($nom)) $nom = array($nom);
			foreach($nom as $n) if(isset($this->pathMemo[$n])) {
				$this->pathMemo[$n] = null;
				unset($this->pathMemo[$n]);
			}
		} else {
			$this->pathMemo = array();
		}
		return $this;
	}

	/**
	 * Revient au chemin sauvegardé, avec un nom
	 * @param $nom
	 * @return string
	 */
	public function restoreSavedPath($nom = null) {
		if(!is_string($nom)) $nom = $this->memo;
		if(isset($this->pathMemo[$nom])) {
			$rootPath = $this->pathMemo[$nom];
			if(file_exists($rootPath)) {
				$this->close();
				$this->rootPath = $rootPath;
				$this->currentPath = $rootPath;
				$this->recursiveTree = array(dir($this->rootPath));
				$this->rewind();
				return $this->rootPath;
			}
		}
		return false;
	}

	///// Xxxxxxxxxx

	///// Xxxxxxxxxx

	///// Xxxxxxxxxx

	///// Xxxxxxxxxx

	///// Xxxxxxxxxx

	///// Xxxxxxxxxx


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// STRUCTURE DES DOSSIERS
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Renvoie la liste des dossiers de src (donc la liste des groupes)
	 * @param boolean $path = true
	 * @return array
	 */
	public function getSrcGroupes($path = true) {
		// return $this->getDirs("/src/");
		$groupesPaths = $this->exploreDir("src/", null, "dossiers", false);
		if($path) return $groupesPaths;
		$groupesNames = array();
		foreach($groupesPaths as $name) $groupesNames[] = $name['nom'];
		return $groupesNames;
	}

	public function getDirs($path = null) {
		$this->savePath();
		if($path !== null) $this->setRootPath($path);
		$list = array();
		// lecture du contenu du dossier
		while($file = @readdir()) {
			if(is_dir($file) && !preg_match("#".aeData::NOFILES."#", $file)) $list[] = $file;
		}
		$this->restoreSavedPath();
		return $list;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ARRAY FUNCTIONS
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function array_unique_in_array2_recursive($array1, $array2) {
		$result = array();
		foreach ($array2 as $key => $value) {
			if(is_array($array2[$key]) && isset($array1[$key])) {
				if(is_array($array1[$key])) $result[$key] = $this->array_unique_in_array2_recursive($array1[$key], $array2[$key]);
			} else if(is_array($array1)) {
				if(!in_array($array2[$key], $array1)) $result[$key] = $array2[$key];
			}
		}
		return $result;
	}





}
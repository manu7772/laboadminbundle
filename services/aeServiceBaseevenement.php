<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceNested;
use Doctrine\ORM\EntityManager;
// use Doctrine\ORM\Event\PreUpdateEventArgs;
// use Doctrine\ORM\Event\LifecycleEventArgs;
use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\Entity\baseevenement;
use Labo\Bundle\AdminBundle\Entity\baseEntity;

// call in controller with $this->get('aetools.aeServiceBaseevenement');
class aeServiceBaseevenement extends aeServiceNested {

	const NAME                  = 'aeServiceBaseevenement';        // nom du service
	const CALL_NAME             = aeData::PREFIX_CALL_SERVICE.'aeServiceBaseevenement'; // comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\baseevenement';
	const CLASS_SHORT_ENTITY    = 'baseevenement';

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		parent::__construct($container, $EntityManager);
		$this->defineEntity(self::CLASS_ENTITY);
		return $this;
	}



}
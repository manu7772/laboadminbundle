<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\Entity\statut;
use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\subentity;

// call in controller with $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceStatut');
class aeServiceStatut extends aeServiceBaseEntity {

    const NAME                  = 'aeServiceStatut';        // nom du service
    const CALL_NAME             = aeData::PREFIX_CALL_SERVICE.'aeServiceStatut'; // comment appeler le service depuis le controller/container
    const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\statut';
    const CLASS_SHORT_ENTITY    = 'statut';

    public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
        parent::__construct($container, $EntityManager);
        $this->defineEntity(self::CLASS_ENTITY);
        return $this;
    }


    /**
     * Check entity integrity in context
     * @param statut $entity
     * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
     * @param $eventArgs = null
     * @return aeServiceStatut
     */
    public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
        parent::checkIntegrity($entity, $context, $eventArgs);
        // if($entity instanceOf statut) {
            switch(strtolower($context)) {
                case 'new':
                case 'postload':
                    $roles = $this->container->get(aeData::PREFIX_CALL_SERVICE.'RolesService')->getListOfRolesForSelect();
                    $user = $this->container->get('security.token_storage')->getToken()->getUser();
                    if($user == 'anon.') $user = null;
                    $entity->injectRolesHierarchy($roles, $user);
                    $bundles = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->getBundlesList(false, true);
                    $entity->injectBundles($bundles);
                    break;
                case 'prepersist':
                    break;
                case 'postpersist':
                    break;
                case 'preupdate':
                    break;
                case 'postupdate':
                    break;
                case 'preremove':
                    break;
                case 'postremove':
                    break;
                default:
                    break;
            }
        // }
        return $this;
    }


    /**
     * Passe l'$entity en mode temps
     * @param &$entity
     * @return aeServiceStatut
     */
    public function setTemp(&$entity) {
        $set = $this->getMethodOfSetting('statut', $entity, true);
        if($set !== false) {
            $temp = $this->getRepo()->findTemp();
            $entity->$set($temp);
            return true;
        }
        return false;
    }

    /**
     * Passe l'$entity en mode actif
     * @param &$entity
     * @return aeServiceStatut
     */
    public function setActif(&$entity) {
        $set = $this->getMethodOfSetting('statut', $entity, true);
        if($set !== false) {
            $actif = $this->getRepo()->findActif();
            $entity->$set($actif);
            return true;
        }
        return false;
    }

    /**
     * Passe l'$entity en mode connected
     * @param &$entity
     * @return aeServiceStatut
     */
    public function setConnected(&$entity) {
        $set = $this->getMethodOfSetting('statut', $entity, true);
        if($set !== false) {
            $connected = $this->getRepo()->findConnected();
            $entity->$set($connected);
            return true;
        }
        return false;
    }

    /**
     * Passe l'$entity en mode inactif
     * @param &$entity
     * @return aeServiceStatut
     */
    public function setInactif(&$entity) {
        $set = $this->getMethodOfSetting('statut', $entity, true);
        if($set !== false) {
            $inactif = $this->getRepo()->findInactif();
            $entity->$set($inactif);
            return true;
        }
        return false;
    }

    /**
     * Passe l'$entity en mode expired
     * @param &$entity
     * @return aeServiceStatut
     */
    public function setExpired(&$entity) {
        $set = $this->getMethodOfSetting('statut', $entity, true);
        if($set !== false) {
            $expired = $this->getRepo()->findExpired();
            $entity->$set($expired);
            return true;
        }
        return false;
    }

    /**
     * Passe l'$entity en mode webmaster
     * @param &$entity
     * @return aeServiceStatut
     */
    public function setWebmaster(&$entity) {
        $set = $this->getMethodOfSetting('statut', $entity, true);
        if($set !== false) {
            $webmaster = $this->getRepo()->findWebmaster();
            $entity->$set($webmaster);
            return true;
        }
        return false;
    }

    /**
     * Passe l'$entity en mode deleted
     * @param &$entity
     * @return aeServiceStatut
     */
    public function setDeleted(&$entity) {
        $set = $this->getMethodOfSetting('statut', $entity, true);
        if($set !== false) {
            $deleted = $this->getRepo()->findDeleted();
            $entity->$set($deleted);
            return true;
        }
        return false;
    }


}
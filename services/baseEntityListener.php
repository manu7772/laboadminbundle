<?php

namespace Labo\Bundle\AdminBundle\services;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Symfony\Component\DependencyInjection\Container;
use Labo\Bundle\AdminBundle\Entity\baseEntity;

use site\adminsiteBundle\Entity\pageweb;

class baseEntityListener {

    const NAME                  = 'baseEntityListener';		// nom du service

	protected $container;
	protected $uow;
	protected $counter;

	public function __construct(Container $container) {
		$this->container = $container;
		$this->counter = 0;
		return $this;
	}

	public function __toString() { return $this->getNom(); }
	public function getNom() { return self::NAME; }


	// /**
	//  * recomputeEntity
	//  * UPDATE : recompute l'entité pour enregistrement
	//  */
	// protected function recomputeEntity(&$entity, EntityManager $em) {
	// 	$this->uow = $em->getUnitOfWork();
	// 	$this->uow->recomputeSingleEntityChangeSet(
	// 		$em->getClassMetadata(get_class($entity)),
	// 		$entity
	// 	);
	// 	$this->uow->computeChangeSets();
	// }


	/** @ORM\PreFlush() */
	public function PreFlushHandler($entity, PreFlushEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PreFlushHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PreFlush', $eventArgs)
			;

		// echo('<h1>PreFlushHandler on '.get_class($entity).' #'.$entity->getId().' ('.$this->counter++.')</h1>');
		// if(get_class($entity) == 'site\adminsiteBundle\Entity\pageweb') {
		// if($entity instanceOf pageweb) {
			// $em = $eventArgs->getEntityManager();
			// $uow = $em->getUnitOfWork();
			// $this->printChangeSets($entity, $eventArgs);
			// $uow->recomputeSingleEntityChangeSet(
			// 	$eventArgs->getEntityManager()->getClassMetadata(get_class($entity)),
			// 	$entity
			// );
			// $this->printChangeSets($entity, $eventArgs);
			// $uow->computeChangeSets();

		// // test de création
		// if($entity instanceOf pageweb) {
		// 	$newentity = $this->container->get('aetools.aeServiceBaseEntity')->getNewEntity(get_class($entity));
		// 	$newentity->setNom($entity->getNom().rand(100000,999999));
		// 	$em->persist($newentity);
		// 	$uow->computeChangeSet($em->getClassMetadata(get_class($newentity)), $newentity);
		// }


			// changeSet
			// echo('<h3>ChangeSet on '.get_class($entity).' #'.$entity->getId().'</h3>');
			// echo('<pre>');var_dump($uow->getEntityChangeSet($entity));echo('</pre>');
			// die('PreFlushHandler after verifyDefault END');
		// }
	}

	// private function printChangeSets($entity, $eventArgs) {
	// 	$em = $eventArgs->getEntityManager();
	// 	$uow = $em->getUnitOfWork();
	// 	echo('<h3>Identify Map in Unit of Work on '.get_class($entity).' #'.$entity->getId().'</h3>');
	// 	foreach ($uow->getIdentityMap() as $key => $value) {
	// 		if(is_object($value)) echo('<p style="margin:0px;">- Object '.get_class($value).' #'.$value->getId().'</p>');
	// 		if(is_array($value)) {
	// 			echo('<p style="margin:0px;">- Array '.count($value).'</p>');
	// 			foreach ($value as $key => $entity2) {
	// 				// $mappd = count($uow->getEntityChangeSet($entity2)) > 0; $title = 'count of changeSets';
	// 				$mappd = $uow->isEntityScheduled($entity2); $title = 'isEntityScheduled';
	// 				// $mappd = $uow->isInIdentityMap($entity2); $title = 'isInIdentityMap';
	// 				$color = $mappd ? 'lightgreen' : 'red';
	// 				// if(is_array($entity2)) echo('<p style="margin:0px;"><i>--- Array '.count($entity2).'</i></p>');
	// 				// if(is_string($entity2)) echo('<p style="margin:0px;"><i>--- String '.$entity2.'</i></p>');
	// 				if(is_object($entity2)) {
	// 					$default = null;
	// 					if(method_exists($entity2, 'getDefault')) $default = $entity2->getDefault();
	// 					echo('<p style="margin:0px;">--- Object / '.$title.' : <span style="color:'.$color.';">'.json_encode($mappd).'</span><i> / Changes : <span style="color:orange;">'.count($uow->getEntityChangeSet($entity2)).'</span> /'.get_class($entity2).' #'.$entity2->getId().' => default : '.json_encode($default).'</i></p>');
	// 				}
	// 				if(!$mappd) {
	// 					$uow->scheduleForUpdate($entity2);
	// 					$Scheduled = $uow->isEntityScheduled($entity2); $title = 'isEntityScheduled';
	// 					$color = $Scheduled ? 'lightgreen' : 'red';
	// 					// $uow->computeChangeSet($eventArgs->getEntityManager()->getClassMetadata(get_class($entity2)), $entity2);
	// 					$uow->recomputeSingleEntityChangeSet(
	// 						$em->getClassMetadata(get_class($entity2)),
	// 						$entity2
	// 					);
	// 					echo('<p style="margin:0px;"><i>------ recomputeSingleEntityChangeSet on '.get_class($entity2).' #'.$entity2->getId().' / '.$title.' : <span style="color:'.$color.';">'.json_encode($Scheduled).'</span> => default : '.json_encode($default).'</i></p>');
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	/** @ORM\PostLoad() */
	public function PostLoadHandler($entity, LifecycleEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PostLoadHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PostLoad', $eventArgs)
			;
	}

	/** @ORM\PrePersist() */
	public function PrePersistHandler($entity, LifecycleEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PrePersistHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PrePersist', $eventArgs)
			;
	}

	/** @ORM\PostPersist() */
	public function PostPersistHandler($entity, LifecycleEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PostPersistHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PostPersist', $eventArgs)
			;
	}

	/** @ORM\PreUpdate() */
	public function PreUpdateHandler($entity, PreUpdateEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PreUpdateHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PreUpdate', $eventArgs)
			;
		//////////////////////////// IMPORTANT ///////////////////////////////
		// Recompute suite modifs entity (nécessaire dans le cas d'Update) !!!
		// $this->recomputeEntity($entity, $eventArgs->getEntityManager());
	}

	/** @ORM\PostUpdate() */
	public function PostUpdateHandler($entity, LifecycleEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PostUpdateHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PostUpdate', $eventArgs)
			;
	}

	/** @ORM\PreRemove() */
	public function PreRemoveHandler($entity, LifecycleEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PreRemoveHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PreRemove', $eventArgs)
			;
	}

	/** @ORM\PostRemove() */
	public function PostRemoveHandler($entity, LifecycleEventArgs $eventArgs) {
		// echo('<p style="margin:0px;">BEGIN >>> PostRemoveHandler for '.$entity->getNom().'/#'.$entity->getId().' of class '.$entity->getShortName().'</p>');
		$this->container
			->get('aetools.aeServiceBaseEntity')
			->getEntityService($entity)
			->checkIntegrity($entity, 'PostRemove', $eventArgs)
			;
	}









}
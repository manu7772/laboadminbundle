<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\services\aeSystemfiles;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Exception\ParseException;

use \Exception;
use \DateTime;

/**
 * Service aeYaml
 * - fonctionnalités de lecture/écriture en fichiers YAML
 * class: Labo\Bundle\AdminBundle\services\aeYaml
 */
class aeYaml {

    const NAME					= 'aeYaml';			// nom du service
    const CALL_NAME				= 'aetools.aeYaml';	// comment appeler le service depuis le controller/container

	protected $aeSystemfiles;

	/**
	 * Constructeur
	 * @return aeYaml
	 */
	public function __construct() {
    	$this->aeSystemfiles = new aeSystemfiles();
		return $this;
    }


	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// YML FILES
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function getYmlContent($file = 'config.yml') {
		$pathFile = $this->aeSystemfiles->getRootServer().'app/config/'.$file;
		if(file_exists($pathFile)) {
			$yaml = new Parser();
			return $yaml->parse(file_get_contents($pathFile));
		}
		else throw new Exception("Le fichier YML ".$file." n'a pu être trouvé.", 1);
	}

	public function getAppServices() {
		$yaml = $this->getYmlContent('services.yml');
		$services = $yaml["services"];
		$parameters = $yaml["parameters"];
		$search = array();
		$replace = array();
		foreach($parameters as $alias => $path) {
			$search[] = '%'.$alias.'%';
			$replace[] = $path;
		}
		$list = array();
		foreach($services as $callname => $class) {
			$className = str_replace($search, $replace, $class['class']);
			unset($class['class']);
			$list[$className] = array_merge(array('callname' => $callname), $class);
		}
		return $list;
	}

	public function getConfigParameters($file, $data = null) {
		$content = $this->getYmlContent($file);
		if(is_string($data)) {
			if(isset($content['parameters'][$data])) return $content['parameters'][$data];
				else throw new Exception("aeYaml::getConfigParameters() : le paramètre \"".$data."\" n'existe pas dans le fichier ".$file.".", 1);
		}
		return $content['parameters'];
	}

	public function getLaboParam($data = null, $file = 'labo_parameters.yml') {
		$content = $this->getYmlContent($file);
		if(is_string($data)) {
			if(isset($content['parameters'][$data])) return $content['parameters'][$data];
				else throw new Exception("aeYaml::getConfigParameters() : le paramètre \"".$data."\" n'existe pas dans le fichier ".$file.".", 1);
		}
		return $content['parameters'];
	}

	// languages

	/**
	 * Renvoie la locale par défaut stockée dans parameters.yml ou config.yml
	 * @return string
	 */
	public function getLocaleInParameters() {
		$content = $this->getYmlContent('parameters.yml');
		if(isset($content['parameters']['locale'])) {
			return $content['parameters']['locale'];
		} else {
			$content = $this->getYmlContent('config.yml');
			if(isset($content['parameters']['locale'])) {
				return $content['parameters']['locale'];
			}
		}
		throw new Exception('Locale absente dans "parameters.yml" et "config.yml" !', 1);
	}

	/**
	 * @dev voir pour charger la locale par défaut via Parser sur le fichier app/config.yml si le controller est absent
	 * Get parsed translation files
	 * @param string $domain = "messages"
	 * @param string $lang = null
	 * @param string $path = "src"
	 * @return array
	 */
	public function getTranslations($domain = "messages", $lang = null, $path = "src") {
		// $this->aeSystemfiles->setRootPath($path);
		// echo('<p>Get translation domain : "'.$domain.'.'.$lang.'.yml" in "'.$this->aeSystemfiles->getCurrentPath().'"</p>');
		if($lang == null) $lang = $this->getLocaleInParameters();
		$aeSystemfiles = new aeSystemfiles();
		$files = $aeSystemfiles->exploreDir($path, $domain.'.'.$lang.'.yml', 'fichiers', true, true);
		// echo('<pre><h3>Get translation files :</h3>');var_dump($files);echo('</pre>');
		$result = array();
		$yaml = new Parser();
		foreach ($files as $file) {
			// echo('<p>Get translation file : '.$file['full'].'</p>');
			$result[] = $yaml->parse(file_get_contents($file['full']));
		}
		// echo('<pre><h3>Translation(s) on "'.$domain.'.'.$lang.'.yml"</h3>');var_dump($result);echo('</pre>');
		return $result;
	}



}
<?php

namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;
use \Exception;

class MaintenanceListener {

	private $container;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	public function onKernelRequest(GetResponseEvent $event) {
		$debug = in_array($this->container->get('kernel')->getEnvironment(), array('test', 'dev'));
		// $debug = false;
		// $data = array();
		if(!preg_match('#^(test\.|admin\.|testadmin\.|localhost)#i', @$_SERVER['SERVER_NAME']) && !$debug) {
			$data = $this->container->hasParameter('sleep') ? $this->container->getParameter('sleep') : null;
			if(is_array($data)) {
				if(!isset($data['duration'])) $data['duration'] = $data['duration_fr'] = false;
				if(!isset($data['mode'])) $data['mode'] = 'maintenance';

				if($data['duration'] !== false) {
					$data['duration_fr'] = $data['duration'];
					$date_elems = explode(' ', $data['duration_fr']);
					$date_jour = explode('/', $date_elems[0]);
					if(is_string($data['duration_fr'])) {
						$data['duration'] = new DateTime($date_jour[2].'-'.$date_jour[1].'-'.$date_jour[0].' '.$date_elems[1]);
						$data['duration_fr'] = $date_jour[1].'-'.$date_jour[0].'-'.$date_jour[2].' '.$date_elems[1];
						if($data['duration'] <= new DateTime('NOW')) return;
					} else {
						$data['duration'] = $data['duration_fr'] = null;
					}
					$engine = $this->container->get('templating');
					$content = $engine->render(':sleep:sleep.html.twig', $data);
					$event->setResponse(new Response($content, 200));
					$event->stopPropagation();
				}
			}
		}
	}


}
<?php

namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;
use \Swift_Message;
use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\Entity\message;
use Labo\Bundle\AdminBundle\Entity\facture;

use \DateTime;

class aeEmail {

	const BUNDLE = 'LaboAdminBundle';

	protected $container;
	protected $twig;
	protected $mailer;
	protected $from;
	protected $site;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->twig = $this->container->get('twig');
		$this->mailer = $this->container->get('mailer');
		$this->from = '';
		$this->site = $this->container->get('aetools.aeServiceSite')->getRepo()->findOneByDefault(1);
		$this->twig->addGlobal('site', $this->site);

		if($this->container->hasParameter('mail')) {
			$mail = $this->container->getParameter('mail');
			if(isset($mail['address'])) $this->from = $mail['address'];
		}
		if(!filter_var($this->from, FILTER_VALIDATE_EMAIL)) {
			if($this->container->hasParameter('site_domains')) {
				$main_domain = $this->container->getParameter('site_domains');
				if(isset($main_domain['main_domain'])) $this->from = 'noreply@'.$this->site->getNom();
			}
		}
		if(!filter_var($this->from, FILTER_VALIDATE_EMAIL)) throw new Exception("Error : no sender for mail defined in congig.yml.", 1);
	}


	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}

	/**
	 * Get a Swift_Message filled with data
	 * @param string $subject
	 * @param string $mailDest
	 * @param string $contenu
	 * @param string $from = null
	 * @return Swift_Message
	 */
	protected function getNewHtmlMessage($subject, $mailDest, $contenu, $from = null) {
		$message = \Swift_Message::newInstance()->setContentType('text/html');
			if(filter_var($from, FILTER_VALIDATE_EMAIL)) $message->setFrom($from);
				else $message->setFrom(array($this->from => $this->site->getNom()));
			$message->setSubject($subject);
			$message->setTo($mailDest);
			// $message->setBcc($mailDest);
			$message->setBody($contenu);
			// echo('<p>From : '.json_encode($message->getFrom()).'</p>');
		return $message;
	}


	/**
	 * Send a copy mail of a message
	 * @param mixed $users (LaboUser or array of LaboUser)
	 * @param message $mail_message
	 * @return boolean
	 */
	public function emailCollatoratorMessage(message $mail_message) {
		$users = $this->getAbonnedMailCollaborateurs();
		if(count($users) > 0) {
			$dest = array();
			foreach ($users as $key => $user) {
				$dest[$user->getEmail()] = $user->getUsername();
				// set as sent
				$this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceUser')->setUserMessageSent($user, $mail_message, new DateTime('NOW'));
			}
			$contenu = $this->twig->render(self::BUNDLE.":email:message.html.twig", array("message" => $mail_message));
			$message = $this->getNewHtmlMessage(
				"Message sur le site ".$this->site->getNom(),
				$dest, 
				$contenu
				);
			return $this->mailer->send($message);
		}
		return false;
	}

	public function emailCopyToUserAfterMessage(message $mail_message) {
		$dest = array();
		if($mail_message->getUser() instanceOf LaboUser) {
			$dest[$mail_message->getUser()->getEmail()] = $mail_message->getUser()->getUsername();
			// set as sent
			$this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceUser')->setUserMessageSent($mail_message->getUser(), $mail_message, new DateTime('NOW'));
		} else {
			$nom = trim($mail_message->getNom());
			if(strlen($nom.'') > 0) {
				if(strlen(trim($mail_message->getPrenom()).'')) $nom = $mail_message->getPrenom().' '.$nom;
			} else {
				$nom = $mail_message->getEmail();
			}
			$dest[$mail_message->getEmail()] = $mail_message->getNom();
		}
		$contenu = $this->twig->render(self::BUNDLE.":email:message_to_sender.html.twig", array("message" => $mail_message));
		$message = $this->getNewHtmlMessage(
			"Votre message sur le site ".$this->site->getNom(),
			$dest, 
			$contenu
			);
		return $this->mailer->send($message);
	}

	// PANIER / FACTURES

	public function emailToUserAfterCommand(LaboUser $user, facture $facture) {
		$contenu = $this->twig->render(self::BUNDLE.":email:confirmCdeUser.html.twig", array("facture" => $facture, 'user' => $user));
		$message = $this->getNewHtmlMessage(
			$user->getUsername()." : Commande en ligne - ".$facture->getId(), 
			$user->getEmail(), 
			$contenu
			);
		return $this->mailer->send($message);
	}

	public function confirmCdeReadyUser(facture $facture) {
		$contenu = $this->twig->render(self::BUNDLE.":email:confirmCdeReadyUser.html.twig", array("facture" => $facture));
		$message = $this->getNewHtmlMessage(
			$facture->getUser()->getUsername()." : Votre commande est prête ! - ".$facture->getId(), 
			$facture->getUser()->getEmail(),
			$contenu
			);
		return $this->mailer->send($message);
	}

	public function emailConfirmCdeCollaborateurs(LaboUser $user, facture $facture) {
		$collaborateurs = $this->getAbonnedMailCollaborateurs();
		if(count($collaborateurs) > 0) {
			$dest = array();
			foreach ($collaborateurs as $collaborateur) {
				$dest[$collaborateur->getEmail()] = $collaborateur->getUsername();
			}
			$contenu = $this->twig->render(self::BUNDLE.":email:confirmCdeCollaborateurs.html.twig", array("facture" => $facture, 'user' => $user, 'collaborateur' => $collaborateur));
			$message = $this->getNewHtmlMessage(
				"Commande en ligne de  ".$user->getUsername().' - Facture '.$facture->getId(),
				$dest, 
				$contenu
				);
			return $this->mailer->send($message);
		}
		return false;
	}




	protected function getAbonnedMailCollaborateurs() {
		$users = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getAllCollaborateurs($this->site->getId());
		// if($users instanceOf LaboUser) $users = array($users);
		foreach($users as $key => $user) {
			if(!$user->getMailSitemessages()) unset($users[$key]);
		}
		return $users;
	}


	// EVENEMENTS / RÉSERVATIONS

	public function confirmReservationUser($data) {
		$data['title'] = $data['user']->getUsername()." : votre réservation est enregistrée";
		$contenu = $this->twig->render(self::BUNDLE.":email:confirmReservationUser.html.twig", $data);
		$message = $this->getNewHtmlMessage(
			$data['title'],
			$data['user']->getEmail(),
			$contenu
			);
		$result = $this->mailer->send($message);
		// collaborators
		$collaborateurs = $this->getAbonnedMailCollaborateurs();
		if(count($collaborateurs) > 0) {
			$dest = array();
			foreach ($collaborateurs as $collaborateur) {
				$dest[$collaborateur->getEmail()] = $collaborateur->getUsername();
			}
			$contenu = $this->twig->render(self::BUNDLE.":email:confirmReservationCollaborator.html.twig", $data);
			$message = $this->getNewHtmlMessage(
				"Réservation en ligne de  ".$data['user']->getUsername(),
				$dest,
				$contenu
				);
			$this->mailer->send($message);
		}

		return $result;
	}



}


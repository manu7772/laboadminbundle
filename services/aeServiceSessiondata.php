<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Labo\Bundle\AdminBundle\Entity\sessiondata;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\services\aeData;

/** call in controller with $this->get('aetools.aeServiceSessiondata'); */
class aeServiceSessiondata extends aeServiceBaseEntity {

	const NAME                  = 'aeServiceSessiondata';				// nom du service
	const CALL_NAME             = 'aetools.aeServiceSessiondata';		// comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\sessiondata';

	protected $sessionId;
	protected $sessionEntity;

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
	    parent::__construct($container, $EntityManager);
	    $this->defineEntity(self::CLASS_ENTITY);
	    $this->sessionId = $this->container->get('request')->getSession()->getId();
	    $this->sessionEntity = null;
	    return $this;
	}

	/** 
	 * Get sessiondata of current session ID
	 * @return sessiondata
	 */
	public function getSessionEntity() {
		if($this->sessionEntity == null) $this->sessionEntity = $this->getRepo()->find($this->sessionId);
		return $this->sessionEntity;
	}

	/**
	 * Create a new sessiondata if not exists. 
	 * returns sessiondata if a new sessiondata has been created / else returns false
	 * @param string $sessionAjaxlivedata
	 * @param boolean $update = true
	 * @return sessiondata | false
	 */
	public function createAjaxlivedataNewIfNotExists($sessionAjaxlivedata, $update = true) {
		if(!($this->getSessionEntity() instanceOf sessiondata)) {
			// does not exist : create
			$newsessiondata = $this->getNewEntity(self::CLASS_ENTITY);
			$newsessiondata->setId($this->sessionId);
			$newsessiondata->setAjaxlive($sessionAjaxlivedata);
			$user = null;
			if(is_object($this->container->get('security.token_storage')->getToken())) $user = $this->container->get('security.token_storage')->getToken()->getUser();
			if(!($user instanceOf LaboUser)) $user = null;
			$newsessiondata->setUser($user);
			$newsessiondata->setIp($this->container->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes')->getIp());
			$this->getEm()->persist($newsessiondata);
			$this->getEm()->flush();
			return $newsessiondata;
		} else if($update) {
			// if exists, update
			$this->getSessionEntity()->setAjaxlive($sessionAjaxlivedata);
			$this->getEm()->flush();
		}
		return false;
	}

	/**
	 * Check if $sessiondata has changed
	 * @param string $sessionAjaxlivedata
	 * @param boolean $createIfNotFound = true
	 * @return boolean
	 */
	public function checkChangedSessionAjaxlivedata($sessionAjaxlivedata, $createIfNotFound = true) {
		if($this->getSessionEntity() instanceOf sessiondata) {
			$changed = $this->getSessionEntity()->hasAjaxliveChanged($sessionAjaxlivedata);
			if($changed) {
				$this->getSessionEntity()->setAjaxlive($sessionAjaxlivedata);
				$this->getEm()->flush();
			}
			return $changed;
		} else if($createIfNotFound) {
			// sessiondata does not exist, so, create it
			$this->createAjaxlivedataNewIfNotExists($sessionAjaxlivedata, true);
			return true;
		}
		return true;
	}

	/**
	 * Remove expired sessiondata
	 * returns true if some sessiondata has been removed
	 * @return boolean
	 */
	public function removeDeprecated() {
		$list = $this->getRepo()->findAll();
		$count = 0;
		foreach ($list as $key => $value) {
			if($value->isDeprecated()) {
				$this->getEm()->remove($value);
				$count++;
			}
		}
		if($count > 0) $this->getEm()->flush();
		return $count > 0;
	}

	/**
	 * Check entity integrity in context
	 * @param sessiondata $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
	 * @param $eventArgs = null
	 * @return aeServiceSessiondata
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		parent::checkIntegrity($entity, $context, $eventArgs);
		// if($entity instanceOf sessiondata) {
			// echo('<p>checkIntegrity '.$context.'::'.self::NAME.'/'.json_encode($entity->getNom()).'/'.$this->getEntityShortName($entity).'</p>');
			switch(strtolower($context)) {
				case 'new':
				case 'postload':
					$sessionId = $entity->getId();
					$entity->setCurrentUser($this->sessionId === $sessionId);
					break;
				case 'prepersist':
					break;
				case 'postpersist':
					break;
				case 'preupdate':
					break;
				case 'postupdate':
					$this->removeDeprecated();
					break;
				case 'preremove':
					break;
				case 'postremove':
					break;
				case 'preflush':
					break;
				case 'postflush':
					break;
				default:
					break;
			}
		// }
		return $this;
	}


}
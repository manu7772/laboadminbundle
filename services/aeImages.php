<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeReponse;
use \Exception;

class aeImages {

	const IMAGE_MANQUANTE_TEXT = 'Image###Manquante';

	public function __construct() {
		return $this;
	}

	/**
	* thumb_image
	* Crée et enregistre un tumbnail de l'image
	* @param image $image
	* @param integer $Xsize = null
	* @param integer $Ysize = null
	* @param string $mode = "no"
	* @return image
	*/
	public function thumb_image($image, $Xsize = null, $Ysize = null, $mode = "no", $getString = false, $format = null) {
		// $mode =
		// cut      : remplit le format avec l'image et la coupe si besoin
		// in       : inclut l'image pour qu'elle soit entièrerement visible
		// deform   : déforme l'image pour qu'elle soit exactement à la taille
		// no       : ne modifie pas la taille de l'image
		// calcul…
		// set_time_limit(300);
		// ini_set('memory_limit', '2048M');

		// if Raw data (string)
		if(is_string($image)) $image = @imagecreatefromstring($image);
		if(!is_resource($image)) {
			$Rimage = $this->getMissingImage($Xsize, $Ysize);
		} else {
			$x = imagesx($image);
			$y = imagesy($image);
			$ratio = $x / $y;

			if($Xsize == null && $Ysize == null) {
				$Xsize = $x;
				$Ysize = $y;
			}
			if($Xsize == null) $Xsize = $Ysize * $ratio;
			if($Ysize == null) $Ysize = $Xsize / $ratio;

			$Dratio = $Xsize / $Ysize;

			// echo('<p>BEGIN : Size X : '.$Xsize.' px</p>');
			// echo('<p>BEGIN : Size X : '.$Ysize.' px</p>');

			if(($x != $Xsize) || ($y != $Ysize)) {
				switch($mode) {
					case('deform') :
						$nx = $Xsize;
						$ny = $Ysize;
						$posx = $posy = 0;
					break;
					case('cut') :
						if($ratio > $Dratio) {
							$posx = ($x - ($y * $Dratio)) / 2;
							$posy = 0;
							$x = $y * $Dratio;
						} else {
							$posx = 0;
							$posy = ($y - ($x / $Dratio)) / 2;
							$y = $x / $Dratio;
						}
						$nx = $Xsize;
						$ny = $Ysize;
					break;
					case('in') :
						if($x > $Xsize || $y > $Xsize) {
							if($x > $y) {
								$nx = $Xsize;
								$ny = $y/($x/$Xsize);
							} else {
								$nx = $x/($y/$Xsize);
								$ny = $Xsize;
							}
						} else {
							$nx = $x;
							$ny = $y;
						}
						$posx = $posy = 0;
					break;
					default: // "no" et autres…
						$posx = $posy = 0;
						$nx = $x;
						$ny = $y;
					break;
				}
				$Rimage = imagecreatetruecolor($nx, $ny);
				imagealphablending($Rimage, false);
				imagesavealpha($Rimage, true);
				imagecopyresampled($Rimage, $image, 0, 0, $posx, $posy, $nx, $ny, $x, $y);
			} else {
				$Rimage = imagecreatetruecolor($x, $y);
				imagealphablending($Rimage, false);
				imagesavealpha($Rimage, true);
				imagecopy($Rimage, $image, 0, 0, 0, 0, $x, $y);
			}
			imagedestroy($image);
		}

		if($getString !== false) $this->setAsString($Rimage, $format);
		return $Rimage;
	}

	public function setAsString(&$resource, $format = null) {
		if(!is_resource($resource)) throw new Exception("aeImages::setAsString() line ".__LINE__." : first parameter is not a resource (".gettype($resource)." given).", 1);
		ob_start();
		switch($format) {
			case 'jpeg':
			case 'jpg': imagejpeg($resource); break;
			case 'gif': imagegif($resource); break;
			case 'png': imagepng($resource); break;
			default: imagepng($resource); break;
		}
		$resource = ob_get_contents();
		ob_end_clean();
	}

	public function getCropped($image, $w, $h, $x, $y, $width, $height, $rotate = 0) {
		// set_time_limit(300);
		// ini_set('memory_limit', '2048M');
		$reponse = new aeReponse();
		// echo('<p>- w : '.$w.'<br>'); 
		// echo('- h : '.$h.'<br>');
		// echo('- x : '.$x.'<br>');
		// echo('- y : '.$y.'<br>');
		// echo('- width : '.$width.'<br>');
		// echo('- height : '.$height.'<br>');
		// echo('- rotate : '.$rotate.'°</p>');
		$message = '';
		if(is_string($image)) {
			try {
				$image = @imagecreatefromstring($image);
			} catch (Exception $e) {
				$message = $e->getMessage();
			} finally {
				if(!is_resource($image)) {
					$image = array('image' => $this->getMissingImage($width, $height), 'width' => $width, 'height' => $height);
					return $reponse->initAeReponse(true, $image, 'Génération de l\'image impossible. Une image de remplacement a été générée. '.$message);				
				}
			}
		}
		if($rotate != 0) imagerotate($image, $rotate, 0, 0);
		$message = '';
		try {
			$Rimage = @imagecreatetruecolor($w, $h);
		} catch (Exception $e) {
			$message = $e->getMessage();
		} finally {
			if(!is_resource($Rimage)) {
				$Rimage = array('image' => $this->getMissingImage($width, $height), 'width' => $width, 'height' => $height);
				return $reponse->initAeReponse(true, $Rimage, 'Dimensionnement de l\'image impossible. Une image de remplacement a été générée. '.$message);				
			}
		}
		imagealphablending($Rimage, false);
		imagesavealpha($Rimage, true);
		$reponse->setResult(imagecopyresampled($Rimage, $image, 0, 0, $x, $y, $w, $h, $width, $height));
		if($reponse->getResult() == true) {
			// OK
			$message = 'Génération de l\'image réussie. ';
			$beaucoup = ' légèrement';
			if($width < ($x / 2) || $height < ($y / 2)) $beaucoup = ' beaucoup';
			if($width < $x || $height < $y) $message .= 'Attention, l\'image a été'.$beaucoup.' agrandie. Sa résolution ne sera pas suffisante pour une qualité d\'affichage optimale.';
			$reponse->setMessage($message);
			$reponse->setData(array('image' => $Rimage, 'width' => $width, 'height' => $height));
		} else {
			// ERROR
			$reponse->setMessage('Une erreur s\'est produite pendant la génération. Veuillez recommencer l\'opération.');
		}
		return $reponse;
	}

	public function computeXandY($originX, $originY, &$x = null, &$y = null) {
		if((integer)$x < 1 && (integer)$y < 1) {
			$x = $originX;
			$y = $originY;
		} else if((integer)$y < 1) {
			$y = $originY / $originX * $x;
		} else if((integer)$x < 1) {
			$x = $originX / $originY * $y;
		}
	}

	public function getMissingImage($x, $y, $message = null) {
		if($message === null) $message = self::IMAGE_MANQUANTE_TEXT;
		if((integer)$x == 0 || (integer)$y == 0) $x = $y = 80;
		$image = @imagecreatetruecolor($x, $y);
		$col_back = imagecolorallocate($image, 200, 200, 200);
		imagefill($image, 0, 0, $col_back);
		$col_text = imagecolorallocate($image, 100, 100, 100);
		$messages = explode('###', (string)$message);
		$decal = 14;
		$cptDecal = -floor((count($messages) * $decal / 2) - ($decal / 2));
		foreach ($messages as $text) {
			$this->imageCenteredString($image, $text, 2, 0, $cptDecal, $col_text);
			$cptDecal += $decal;
		}
		return $image;
	}

	/**
	 * Add text in image resource, with optional x and y shift
	 * @param resource &$image
	 * @param integer $font
	 * @param integer $x
	 * @param integer $y
	 * @param string $str
	 * @param resource $col_text = null
	 */
	public function imageCenteredString(&$image, $str, $font = 2, $x = 0, $y = 0, $col_text = null) {
		if($col_text == null) $col_text = imagecolorallocate($image, 100, 100, 100);
		$xLoc = floor((imagesx($image) - (imagefontwidth($font) * strlen($str))) / 2 + $x);
		$yLoc = floor((imagesy($image) - imagefontheight($font)) / 2 + $y);
		imagestring($image, $font, $xLoc, $yLoc, $str, $col_text);
	}

}










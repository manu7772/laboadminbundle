<?php
namespace Labo\Bundle\AdminBundle\services;

use Doctrine\Common\Collections\ArrayCollection;

use \DateTime;
use \Exception;

class aeArrays {

	const NAME					= 'aeArrays'; 				// nom du service
	const CALL_NAME				= 'aetools.aeArrays';		// comment appeler le service depuis le controller/container



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IDENTIFICATION CLASSE
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}


	/**
	 * Get an array with $items
	 * Returns with $items type:
	 * - true : array(1) { [0]=> bool(true) }
	 * - false : array(1) { [0]=> bool(false) }
	 * - null : array(0) { }
	 * - object : array(1) { [0]=> object(classname)#1 … }
	 * - ArrayCollection : array with his method ->toArray()
	 * - array : the same array
	 * - others (string, integer…) : force (array)
	 * @param mixed $items
	 * @return array
	 */
	public function asArray($items) {
		if(is_object($items)) {
			return method_exists($items, 'toArray') ? $items->toArray() : array($items);
		}
		return (array)$items;
	}

	/**
	 * Réduit la taille des chaînes trop longues dans un tableau. Méthode résursive.
	 * @param array &$array
	 * @param integer $maxSize = 50
	 * @return array
	 */
	public function reduceArray(&$array, $maxSize = 120) {
		if(is_array($array)) {
			foreach($array as $key => $item) {
				if(is_string($item)) if(strlen($item) > $maxSize) $array[$key] = substr($item, 0, $maxSize).' ('.strlen($item).' char.)';
				if(is_array($item)) $array[$key] = $this->reduceArray($array[$key]);
			}
		}
		return $array;
	}

	/**
	 * Transform nested array to flat array
	 * @param array $array
	 * @return array
	 */
	protected function getFlatList($array) {
		$flat_list = array();
		if(is_array($array)) {
			foreach ($array as $value) {
				if(is_array($value)) $flat_list = array_merge($flat_list, $this->getFlatList($value));
					else if(is_string($value)) $flat_list[] = $value;
			}
		} else if(is_string($array)) $flat_list[] = $value;
		return $flat_list;
	}



}
<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceNested;
use Doctrine\ORM\EntityManager;
// use Doctrine\ORM\Event\PreUpdateEventArgs;
// use Doctrine\ORM\Event\LifecycleEventArgs;
use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\Entity\tier;
use Labo\Bundle\AdminBundle\Entity\baseEntity;

// call in controller with $this->get('aetools.aeServiceTier');
class aeServiceTier extends aeServiceNested {

	const NAME                  = 'aeServiceTier';        // nom du service
	const CALL_NAME             = aeData::PREFIX_CALL_SERVICE.'aeServiceTier'; // comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\tier';
	const CLASS_SHORT_ENTITY    = 'tier';

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		parent::__construct($container, $EntityManager);
		$this->defineEntity(self::CLASS_ENTITY);
		return $this;
	}



}
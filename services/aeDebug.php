<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeData;
use Symfony\Component\DependencyInjection\ContainerInterface;
// yaml parser
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Exception\ParseException;
// aetools
use Labo\Bundle\AdminBundle\services\aetools;

use \DateTime;
use \Exception;

/**
 * Service aeDebug
 * - Gestion debug
 */
class aeDebug {

	const DEBUG_FOLDER = 'debug';
	protected $container; 			// container
	protected $debugPath;			// path for debug folder
	protected $aeArrays;			// service arrays
	protected $aeSystemfiles;		// service aeSystemfiles
	protected $chrono;				// chono

	public function __construct(ContainerInterface $container = null) {
		$this->container = $container;
		$this->debugPath = aeData::WEB_PATH.self::DEBUG_FOLDER;
		$this->aeArrays = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeArrays');
		$this->aeSystemfiles = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeSystemfiles');
		$this->startChrono();
	}


	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}

	/**
	 * Réduit les objets à leur slug, id ou autre propriété pour format YML
	 * @param array &$array
	 * @return array
	 */
	public function nameObjectsInArray(&$array) {
		foreach ($array as $key => $item) {
			if(is_array($item)) $array[$key] = $this->nameObjectsInArray($item);
			if(is_object($item)) $array[$key] = $this->getBestPropertyOFObject($item);
		}
		return $array;
	}

	/**
	 * Renvoie une string correspondant à l'une des propriétés de l'objet $object
	 * @param object $object
	 * @param string $methods = []
	 * @return string
	 */
	public function getBestPropertyOFObject($object, $methods = []) {
		if(is_string($methods)) $methods = array($methods);
		$methods = array_unique($methods + array('getSlug', 'getId', 'getNom'));
		foreach ($methods as $method) {
			if(method_exists($object, $method)) return $object->$method();
		}
		return get_class($object);
	}

	/**
	 * Écriture un fichier d'après path
	 * @param string $path
	 * @param array $array
	 * @return boolean (nb d'octets si success)
	 */
	public function dump_yaml_toFile($path, $array, $testEnv = null) {
		if(!is_bool($testEnv)) $testEnv = aeData::TEST_ENVIRONNEMENT;
		$launch = true;
		if($this->container !== null && $testEnv === true) {
			$env = $this->container->get('kernel')->getEnvironment();
			if(!in_array($env, array('dev', 'test'))) $launch = false;
		}
		if($launch) {
			$this->aeArrays->reduceArray($array);
			$dumper = new Dumper();
			$r = @file_put_contents(
				$path,
				$dumper->dump($this->nameObjectsInArray($array), aeData::YAML_LEVELS)
			);
		}
		return $r;
	}

	/**
	 * Écriture un fichier d'après path
	 * @param array $array
	 * @return boolean (nb d'octets si success)
	 */
	public function debugFile($array, $testEnv = null) {
		if(!is_bool($testEnv)) $testEnv = aeData::TEST_ENVIRONNEMENT;
		$this->aeArrays->reduceArray($array);
		$launch = true;
		$r = null;
		if($this->container !== null && $testEnv === true) {
			$env = $this->container->get('kernel')->getEnvironment();
			if(!in_array($env, array('dev', 'test'))) $launch = false;
		}
		if($launch == true) {
			$date = new DateTime();
			$dumper = new Dumper();
			$inc = 0;
			$file = $this->aeSystemfiles->getRootServer().$this->debugPath.aeData::SLASH.'debugFile_'.$date->format('Ymd_His').'-'.$inc.'.yml';
			while (@file_exists($file)) {
				$file = $this->aeSystemfiles->getRootServer().$this->debugPath.aeData::SLASH.'debugFile_'.$date->format('Ymd_His').'-'.$inc++.'.yml';
			}
			$r = @file_put_contents(
				$file,
				$dumper->dump($this->nameObjectsInArray($array), aeData::YAML_LEVELS)
			);
		}
		return $r;
	}

	/**
	 * Écriture un fichier de nom $name. Ajoute les infos en fin de fichier, avec date. 
	 * Préciser le nom du fichier uniquement, sans chemin ni extension (.yml)
	 * Le ficher est placé dans le dossier web/debug/
	 * @param string $name
	 * @param array $array
	 * @return boolean (nb d'octets si success)
	 */
	public function debugNamedFile($name, $array, $testEnv = null, $erase = false) {
		if(!is_bool($testEnv)) $testEnv = aeData::TEST_ENVIRONNEMENT;
		// (boolean) $erase === true ? $mode = 'w' : $mode = 'c'; // c = ajout en début de fichier (et non à la fin, avec 'a')
		$erase ? $mode = 'w' : $mode = 'a';
		$launch = true;
		$r = true;
		if($this->container !== null && $testEnv === true) {
			$env = $this->container->get('kernel')->getEnvironment();
			if(!in_array($env, array('dev', 'test'))) $launch = false;
		}
		if($launch == true) {
			$dumper = new Dumper();
			$date = new DateTime();
			$dateTxt = $date->format('Y-m-d H:i:s');
			$array = array(array($dateTxt => $this->aeArrays->reduceArray($array)));
			$file = $this->aeSystemfiles->getRootServer().$this->debugPath.aeData::SLASH.$name.'.yml';
			if(file_exists($file)) {
				if(is_writable($file)) {
					$fop = @fopen($file, $mode);
					$r = $r && $fop;
					$r = $r && @fwrite($fop, $dumper->dump($this->nameObjectsInArray($array), aeData::YAML_LEVELS));
					$r = $r && @fclose($fop);
					if($r != true) throw new Exception("Fichier Debug n'a pas pu être ajouté en écriture : ".$file, 1);
				} else throw new Exception("Fichier Debug non accessible en écriture : ".$file, 1);
			} else {
				$r = @file_put_contents(
					$file,
					$dumper->dump($this->nameObjectsInArray($array), aeData::YAML_LEVELS)
				);
				if($r != true) throw new Exception("Fichier Debug n'a pas pu être créé en écriture : ".$file, 1);
			}
		}
		return $r;
	}

	/**
	 * Delete debug file named $name
	 * @param string $name
	 * @return boolean
	 */
	public function deleteDebugNamedFile($name) {
		$file = $this->aeSystemfiles->getRootServer().$this->debugPath.aeData::SLASH.$name.'.yml';
		if(@file_exists($file)) return @unlink($file);
		return false;
	}


	// TEST TIMESTAMP

	public function microtime_float() {
	    list($usec, $sec) = explode(" ", microtime());
	    return ((float)$usec + (float)$sec) * 1000;
	}

	public function startChrono() {
		$this->chrono = $this->microtime_float();
	}

	public function getChrono($restart = false) {
		$a = number_format($this->microtime_float() - $this->chrono, 3,',','');
		if($restart) $this->startChrono();
		return $a;
	}

	public function printChrono($txt, $restart = false) {
		// if($this->isDevOrSadmin()) echo('<h5>'.$txt.' : <i>'.$this->getChrono().'ms</i></h5>');
	}

}


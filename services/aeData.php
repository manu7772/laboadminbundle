<?php
namespace Labo\Bundle\AdminBundle\services;

// use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
// use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Service aeData
 * - Gestion des données globales fixes du site
 * class: Labo\Bundle\AdminBundle\services\aeData
 * arguments: null
 */
class aeData {

	const NAME					= 'aeData'; 			// nom du service
	const CALL_NAME				= 'aetools.aeData';		// comment appeler le service depuis le controller/container

	const SERVEUR_TYPE			= 'UNIX/LINUX';			// Type de serveur
	const SLASH					= '/';					// slash
	const ASLASH 				= '\\';					// anti-slashes
	const WIN_ASLASH			= '/';					// anti-slashes Windows
	const ALL_FILES 			= "^.+$";				// motif PCRE pour tous textes
	const NOFILES 				= "^\.";				// motif PCRE pour no file
	const EOLine				= "\n";					// End of line Terminal
	const ARRAY_GLUE 			= '___';
	const PROXY_PREFIX			= '#^(Proxies\\\\__CG__\\\\)#';
	// site
	const SITE_DATA 			= 'sitedata';
	// Paths
	const GO_TO_ROOT 			= '/../../../../../../../';
	const SOURCE_FILES 			= 'src/';
	const WEB_PATH				= 'web/';
	const FOLD_RESOURCES 		= 'Resources';
	const BUNDLE_EXTENSION 		= 'Bundle';
	const PARAMS_FOLDER			= 'params';
	const FOLD_DATAFIXTURES		= 'DataFixtures';
	const FOLD_ORM				= 'ORM';
	// chmod
	const DEFAULT_CHMOD			= 0755;
	// DateTime
	const FORMAT_DATETIME_SQL	= "Y-m-d H:i:s";
	const DATE_ZERO				= "0000-00-00";
	const TIME_ZERO				= "0:0:0";
	// YAML
	const MAX_YAML_LEVEL 		= 100;			// max levels for yaml
	const YAML_LEVELS 			= 32;			// levels for yaml
	// services
	const PREFIX_CALL_SERVICE	= "aetools.";
	// logs
	const LOG_NAME				= 'logs_dev';	// log file name
	// environnement
	const TEST_ENVIRONNEMENT	= false;
	// entities
	const TYPE_SELF 			= '_self';
	const DEFAULT_ACTION 		= 'list';
	const LIST_ACTION 			= 'list';
	const SHOW_ACTION 			= 'show';
	const CREATE_ACTION 		= 'create';
	const EDIT_ACTION 			= 'edit';
	const COPY_ACTION 			= 'copy';
	const DELETE_ACTION 		= 'delete';
	const ACTIVE_ACTION 		= 'active';
	const CHECK_ACTION 			= 'check';
	const TYPE_VALUE_JOINER 	= '___';
	const BUNDLE				= 'laboadmin';
	const LIST_REPO_METHOD		= 'findForList';
	const LIST_REPO_DEFAULT		= 'findAll';


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IDENTIFICATION CLASSE
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}

	// http://stackoverflow.com/questions/1993721/how-to-convert-camelcase-to-camel-case
	// see also http://symfony.com/doc/current/components/serializer.html#camelcase-to-snake-case
	static public function decamelize(&$attribute) {
		if(!is_string($attribute)) return $attribute;
		$attribute = preg_replace("/(_){2,}/", '_', $attribute);
		return $attribute = strtolower(preg_replace_callback(
			"/(^|[a-z])([A-Z])/",
			function($m) { return strtolower(strlen($m[1]) ? "$m[1]_$m[2]" : "$m[2]"); },
			$attribute
		));
	}

	static public function camelize(&$attribute) {
		if(!is_string($attribute)) return $attribute;
		return $attribute = lcfirst(preg_replace_callback(
			"/(^|_)+([a-z])/",
			function($m) { return strtoupper("$m[2]"); },
			strtolower($attribute)
		));
	} 


}
<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializationContext;

use Labo\Bundle\AdminBundle\Entity\baseEntity;

use \DateTime;

/** call in controller with $this->get('aetools.aeCalendar'); */
class aeServiceCalendar extends aeServiceBaseEntity {

	const NAME                  = 'aeServiceCalendar';				// nom du service
	const CALL_NAME             = 'aetools.aeServiceCalendar';		// comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\calendar';

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		parent::__construct($container, $EntityManager);
		$this->defineEntity(self::CLASS_ENTITY);
		return $this;
	}


	/**
	 * Check entity integrity in context
	 * @param Calendar $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove', 'PreFlush')
	 * @param $eventArgs = null
	 * @return aeServiceCalendar
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		// DO NOTHING
		return $this;
	}

	public function verifDate(DateTime $date, $class, $itemSlug, $label = null, $serialized = false) {
		$test = null;
		$now = new DateTime;

		if($date->format('Y-m-d') === $now->format('Y-m-d')) {
			// même jour…
			$debut = clone $now;
			$whenReady = clone $now;
			$whenReady->modify('+3 hours');			
		} else {
			// autre jour, on met l'heure à minuit…
			$debut = clone $date;
			$debut->modify('today');
			$whenReady = clone $debut;
		}
		$fin = clone $date;
		$fin->modify('tomorrow');

		// agendas de la journée $date (3h après maintenant)
		$restAllDay = $this->getRepo()->findCalendarsOfItem($itemSlug, $class, $whenReady, $fin, $label);
		// prochain enlèvement possible (3h après maintenant)
		$nextOpen = $this->getRepo()->findNextCal($itemSlug, $class, $whenReady, $label);

		$triAmPm = array();
		$triAmPm['matin'] = array();
		$triAmPm['aprem'] = array();
		if(count($restAllDay) > 0) {
			$message = null;
			$limitAmPm = 13;
			$commandeready = array('matin' => null, 'aprem' => null); // heure à laquelle l'enlèvement du panier est possible
			foreach ($restAllDay as $key => $agenda) {
				// tri matin / après-midi (limite = 13h)
				$h_debut = intval($agenda->getStartDate()->format('H'));
				$h_fin = intval($agenda->getEndDate()->format('H'));
				// heure d'enlèvement…
				// tri
				if($h_debut < $limitAmPm) {
					$triAmPm['matin'][] = $serialized ? $this->getJMSerializedAsArray($agenda) : $agenda;
					if($commandeready['matin'] === null) {
						if($whenReady > $agenda->getStartDate()) $commandeready['matin'] = clone $whenReady;
							else $commandeready['matin'] = $agenda->getStartDate();
					}
				}
				if($h_fin >= $limitAmPm) {
					$triAmPm['aprem'][] = $serialized ? $this->getJMSerializedAsArray($agenda) : $agenda;
					if($commandeready['aprem'] === null) {
						if($whenReady > $agenda->getStartDate()) $commandeready['aprem'] = clone $whenReady;
							else $commandeready['aprem'] = $agenda->getStartDate();
					}
				}
			}
			if($serialized) $nextOpen = $this->getJMSerializedAsArray($nextOpen);
			if($serialized) $restAllDay = $this->getJMSerializedAsArray($restAllDay);
			$test = array('commandeready' => array('matin' => $commandeready['matin'] === null ? array() : $commandeready['matin']->format(DATE_ATOM), 'aprem' => $commandeready['aprem'] === null ? array() : $commandeready['aprem']->format(DATE_ATOM)), 'next_open' => $nextOpen, 'date_origin' => $date->format(DATE_ATOM), 'am_and_pm' => $triAmPm);
			$aeReponse = new aeReponse(true, $test, $message);
		} else {
			$message = 'La boutique n\'est pas ouverte à cette date, ou la date n\'est pas encore disponible. Veuillez choisir une autre date, s.v.p.';
			// test on nextOpen
			if(count($nextOpen) > 0) {
				// one more chance !!
				return $this->verifDate($nextOpen[0]->getStartDate(), $class, $itemSlug, $label, $serialized)->setResult(false)->setMessage($message);
			}
			if($serialized) $nextOpen = $this->getJMSerializedAsArray($nextOpen);
			if($serialized) $restAllDay = $this->getJMSerializedAsArray($restAllDay);
			$test = array('commandeready' => null, 'next_open' => $nextOpen, 'date_origin' => $date->format(DATE_ATOM), 'am_and_pm' => $triAmPm);
			$aeReponse = new aeReponse(false, $test, $message);
		}
		return $aeReponse;
	}

	protected function getJMSerializedAsArray($data, $group = 'fullcalendar') {
		return json_decode($this->container->get('jms_serializer')->serialize($data, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups(array($group))));
	}

}
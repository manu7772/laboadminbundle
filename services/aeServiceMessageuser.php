<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
// use Doctrine\ORM\Event\PreUpdateEventArgs;
// use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializationContext;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;

use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\Entity\message;
use Labo\Bundle\AdminBundle\Entity\messageuser;

class aeServiceMessageuser extends aeServiceBaseEntity {

    const NAME                  = 'aeServiceMessageuser';        // nom du service
    const CALL_NAME             = 'aetools.aeServiceMessageuser'; // comment appeler le service depuis le controller/container
    const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\messageuser';

    public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
        parent::__construct($container, $EntityManager);
        // $this->defineEntity(self::CLASS_ENTITY);
        return $this;
    }

    /**
     * Check entity integrity in context
     * @param message $entity
     * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
     * @param $eventArgs = null
     * @return aeServiceMessageuser
     */
    public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
        parent::checkIntegrity($entity, $context, $eventArgs);
        // if($entity instanceOf message) {
            switch(strtolower($context)) {
                case 'new':
                    break;
                case 'postload':
                    break;
                case 'prepersist':
                    break;
                case 'postpersist':
                    break;
                case 'preupdate':
                    break;
                case 'postupdate':
                    break;
                case 'preremove':
                    break;
                case 'postremove':
                    break;
                default:
                    break;
            }
        // }
        return $this;
    }


}
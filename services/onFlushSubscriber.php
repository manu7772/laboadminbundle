<?php

namespace Labo\Bundle\AdminBundle\services;

// use Doctrine\ORM\Mapping as ORM;
// use Doctrine\ORM\EntityManager;
// use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\UnitOfWork;

use Labo\Bundle\AdminBundle\services\aeData;


class onFlushSubscriber implements EventSubscriber {

	const NAME                  = 'onFlushSubscriber';		// nom du service

	protected $container;

	public function __construct(Container $container) {
		$this->container = $container;
		return $this;
	}

	public function __toString() { return $this->getNom(); }
	public function getNom() { return self::NAME; }

    public function getSubscribedEvents() {
        return array(
            Events::onFlush,
            Events::postFlush,
        );
    }

    // http://www.doctrine-project.org/api/orm/2.3/class-Doctrine.ORM.UnitOfWork.html
    // https://github.com/doctrine/doctrine2/blob/master/lib/Doctrine/ORM/UnitOfWork.php

    // https://doctrine-orm.readthedocs.io/en/latest/reference/events.html#onflush
	public function onFlush(OnFlushEventArgs $event) {
		// $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeDebug')->deleteDebugNamedFile('onFlushSubscriber');
		$em = $event->getEntityManager();
		$uow = $em->getUnitOfWork();

		// $this->printUOW($em);

		// echo('<h3>'.Events::onFlush.' entitites (size : '.$uow->size().')</h3>');
		foreach(['updates','insertions','deletions'] as $action) {
			$get = 'getScheduledEntity'.ucfirst($action);
			foreach($uow->$get() as $id => $entity) {
				// echo('<p style="margin:0px;"><i><em>'.$id.'</em></i> <strong>'.$action.'</strong> entity <i>'.get_class($entity).' #'.json_encode($entity->getId()).'</i></p>');
				$entityService = $this->container->get('aetools.aeServiceBaseEntity')->getEntityService($entity);
				$entityService->checkIntegrity($entity, 'onFlush::'.$action, $event);
				if($action == 'insertions' && $uow->isEntityScheduled($entity)) {
					$uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
					// echo('<p>Default for '.json_encode($entity->getNom()).' : '.json_encode($entity->getDefault()).'</p>');
				}
			}
		}
		foreach($uow->getIdentityMap() as $className => $entities) {
			foreach ($entities as $entity) {
				if(!$uow->isEntityScheduled($entity)) $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
			}
		}

		// $this->printUOW($em);

		// $this->printChangeSets($event);

		// echo('<h3>'.Events::onFlush.' entitites (size : '.$uow->size().')</h3>');
		// foreach(['updates','insertions','deletions'] as $action) {
		// 	$get = 'getScheduledEntity'.ucfirst($action);
		// 	foreach($uow->$get() as $id => $entity) {
		// 		echo('<p style="margin:0px;"><i>'.$id.'</i> <strong>'.$action.'</strong> entity <i>'.get_class($entity).' #'.json_encode($entity->getId()).'</i></p>');
		// 		// $entityService = $this->container->get('aetools.aeServiceBaseEntity')->getEntityService($entity);
		// 		// $entityService->checkIntegrity($entity, 'onFlush::'.$action, $event);
		// 	}
		// }
		// $this->printChangeSets($event);


		// // UPDATES
		// foreach($uow->getScheduledEntityUpdates() as $scheduledEntityUpdate) {
		// 	// echo('<p>onFlush:preUpdate <span style="color:orange;">'.$scheduledEntityUpdate->getNom().'#'.$scheduledEntityUpdate->getId().'</span></p>');
		// 	$this->entityService = $this->container->get('aetools.aeServiceBaseEntity')->getEntityService($scheduledEntityUpdate);
		// 	$this->entityService->checkIntegrity($scheduledEntityUpdate, 'onFlush::update', $event);
		// 	$uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($scheduledEntityUpdate)), $scheduledEntityUpdate);
		// }
		// // INSERTIONS
		// foreach($uow->getScheduledEntityInsertions() as $scheduledEntityInsertion) {
		// 	// echo('<p>onFlush:Insert NEW <span style="color:orange;">'.$scheduledEntityInsertion->getNom().'#'.$scheduledEntityInsertion->getId().'</span></p>');
		// 	$this->entityService = $this->container->get('aetools.aeServiceBaseEntity')->getEntityService($scheduledEntityInsertion);
		// 	$this->entityService->checkIntegrity($scheduledEntityInsertion, 'onFlush::update', $event);
		// 	$uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($scheduledEntityInsertion)), $scheduledEntityInsertion);
		// }
		// $uow->computeChangeSets();
		// // echo('<p>onFlush Changes ('.count($uow->getScheduledEntityUpdates()).') : '.implode(', ', $uow->getScheduledEntityUpdates()).'</p>');
		// // echo('<p>onFlush Insertions ('.count($uow->getScheduledEntityInsertions()).') : '.implode(', ', $uow->getScheduledEntityInsertions()).'</p>');
		// die('<p style="color:red;">END >>> onFlushSubscriber::onFlush</p>');
	}

	private function printUOW($em) {
		$uow = $em->getUnitOfWork();
		/*
		UnitOfWork::STATE_MANAGED		= 1
		UnitOfWork::STATE_NEW			= 2
		UnitOfWork::STATE_DETACHED		= 3
		UnitOfWork::STATE_REMOVED		= 4
		UnitOfWork::HINT_DEFEREAGERLOAD	= 'deferEagerLoad'

		322            public function commit($entity = null)
		517            public function & getEntityChangeSet($entity)
		563  @internal public function computeChangeSet(ClassMetadata $class, $entity)
		754            public function computeChangeSets()
		924   INTERNAL public function recomputeSingleEntityChangeSet(ClassMetadata $class, $entity)
		1194           public function scheduleForInsert($entity)
		1231           public function isScheduledForInsert($entity)
		1245           public function scheduleForUpdate($entity)
		1276  INTERNAL public function scheduleExtraUpdate($entity, array $changeset)
		1299           public function isScheduledForUpdate($entity)
		1311           public function isScheduledForDirtyCheck($entity)
		1326  INTERNAL public function scheduleForDelete($entity)
		1362           public function isScheduledForDelete($entity)
		1374           public function isEntityScheduled($entity)
		1398  INTERNAL public function addToIdentityMap($entity)
		1430           public function *** getEntityState($entity, $assume = null) *** GETTER
		1513  INTERNAL public function removeFromIdentityMap($entity)
		1548  INTERNAL public function *** getByIdHash($idHash, $rootClassName) *** GETTER
		1565  INTERNAL public function tryGetByIdHash($idHash, $rootClassName)
		1581           public function isInIdentityMap($entity)
		1610  INTERNAL public function containsIdHash($idHash, $rootClassName)
		1622           public function persist($entity)
		1699           public function remove($entity)
		1773           public function merge($entity)
		1949           public function detach($entity)
		2010           public function refresh($entity)
		2299           public function lock($entity, $lockMode, $lockVersion = null)
		2358           public function *** getCommitOrderCalculator() *** GETTER
		2370           public function clear($entityName = null)
		2410  INTERNAL public function scheduleOrphanRemoval($entity)
		2425  INTERNAL public function cancelOrphanRemoval($entity)
		2438  INTERNAL public function scheduleCollectionDeletion(PersistentCollection $coll)
		2454  INTERNAL public function isCollectionScheduledForDeletion(PersistentCollection $coll)
		2491  INTERNAL public function createEntity($className, array $data, &$hints = array())
		2779           public function triggerEagerLoads()
		2811           public function loadCollection(PersistentCollection $collection)
		2834           public function *** getIdentityMap() *** GETTER
		2847           public function *** getOriginalEntityData($entity) *** GETTER
		2864           public function setOriginalEntityData($entity, array $data)
		2881  INTERNAL public function setOriginalEntityProperty($oid, $property, $value)
		2896           public function *** getEntityIdentifier($entity) *** GETTER
		2910           public function *** getSingleIdentifierValue($entity) *** GETTER
		2935           public function tryGetById($id, $rootClassName)
		2953           public function scheduleForDirtyCheck($entity)
		2965           public function hasPendingInsertions()
		2976           public function size()
		2990           public function *** getEntityPersister($entityName) *** GETTER
		3034           public function *** getCollectionPersister(array $association) *** GETTER
		3070  INTERNAL public function registerManaged($entity, array $id, array $data)
		3093  INTERNAL public function clearEntityChangeSet($oid)
		3110           public function propertyChanged($entity, $propertyName, $oldValue, $newValue)
		3134           public function *** getScheduledEntityInsertions() *** GETTER
		3144           public function *** getScheduledEntityUpdates() *** GETTER
		3154           public function *** getScheduledEntityDeletions() *** GETTER
		3164           public function *** getScheduledCollectionDeletions() *** GETTER
		3174           public function *** getScheduledCollectionUpdates() *** GETTER
		3186           public function initializeObject($obj)
		3223           public function markReadOnly($object)
		3241           public function isReadOnly($object)
		3441 @internal public function hydrationComplete()
		*/

		// GLOBALS
		$data = array(
			'size'			=> $uow->size(),
			'identityMap'	=> $uow->getIdentityMap(),
			);

		// INSERTIONS
		$data['insertions']['hasPendingInsertions'] = $uow->hasPendingInsertions();
		$data['insertions']['getScheduledEntityInsertions']['count'] = count($uow->getScheduledEntityInsertions());
		foreach($uow->getScheduledEntityInsertions() as $key => $entity) {
			$data['insertions']['getScheduledEntityInsertions']['entities'][$key]['id'] = $entity->getId();
			$data['insertions']['getScheduledEntityInsertions']['entities'][$key]['nom'] = $entity->getNom();
			$data['insertions']['getScheduledEntityInsertions']['entities'][$key]['class'] = get_class($entity);
			$data['insertions']['getScheduledEntityInsertions']['entities'][$key]['state'] = $uow->getEntityState($entity);
			$infos = array();
			if($uow->isScheduledForInsert($entity)) $infos[] = 'Up';
			if($uow->isScheduledForDirtyCheck($entity)) $infos[] = 'Dc';
			$data['insertions']['getScheduledEntityInsertions']['entities'][$key]['infos'] = $infos;
		}

		// UPDATES
		$data['updates']['getScheduledEntityUpdates']['count'] = count($uow->getScheduledEntityUpdates());
		foreach($uow->getScheduledEntityUpdates() as $key => $entity) {
			$data['updates']['getScheduledEntityUpdates']['entities'][$key]['id'] = $entity->getId();
			$data['updates']['getScheduledEntityUpdates']['entities'][$key]['nom'] = $entity->getNom();
			$data['updates']['getScheduledEntityUpdates']['entities'][$key]['class'] = get_class($entity);
			$data['updates']['getScheduledEntityUpdates']['entities'][$key]['changeset'] = $uow->getEntityChangeSet($entity);
			$data['updates']['getScheduledEntityUpdates']['entities'][$key]['state'] = $uow->getEntityState($entity);
			$infos = array();
			if($uow->isScheduledForUpdate($entity)) $infos[] = 'Up';
			if($uow->isScheduledForDirtyCheck($entity)) $infos[] = 'Dc';
			$data['updates']['getScheduledEntityUpdates']['entities'][$key]['infos'] = $infos;
		}
		// COLLECTIONS UPDATES
		$data['updates']['getScheduledCollectionUpdates']['count'] = count($uow->getScheduledCollectionUpdates());
		foreach($uow->getScheduledCollectionUpdates() as $key => $collection) {
			$data['updates']['getScheduledCollectionUpdates']['collections'][$key]['class'] = get_class($collection);
			$data['updates']['getScheduledCollectionUpdates']['collections'][$key]['mapping'] = $collection->getMapping();
		}

		// DELETIONS
		$data['deletions']['getScheduledEntityDeletions']['count'] = count($uow->getScheduledEntityDeletions());
		foreach($uow->getScheduledEntityDeletions() as $key => $entity) {
			$data['deletions']['getScheduledEntityDeletions']['entities'][$key]['id'] = $entity->getId();
			$data['deletions']['getScheduledEntityDeletions']['entities'][$key]['nom'] = $entity->getNom();
			$data['deletions']['getScheduledEntityDeletions']['entities'][$key]['class'] = get_class($entity);
			$data['deletions']['getScheduledEntityDeletions']['entities'][$key]['state'] = $uow->getEntityState($entity);
			$infos = array();
			if($uow->isScheduledForDelete($entity)) $infos[] = 'Up';
			if($uow->isScheduledForDirtyCheck($entity)) $infos[] = 'Dc';
			$data['deletions']['getScheduledEntityDeletions']['entities'][$key]['infos'] = $infos;
		}
		// COLLECTIONS DELETIONS
		$data['deletions']['getScheduledCollectionDeletions']['count'] = count($uow->getScheduledCollectionDeletions());
		foreach($uow->getScheduledCollectionDeletions() as $key => $collection) {
			$data['deletions']['getScheduledCollectionDeletions']['collections'][$key]['class'] = get_class($collection);
			$data['deletions']['getScheduledCollectionDeletions']['collections'][$key]['mapping'] = $collection->getMapping();
		}

		$this->container->get(aeData::PREFIX_CALL_SERVICE.'aeDebug')->debugNamedFile('onFlushSubscriber', $data);
	}

	// private function printChangeSets($event) {
	// 	$em = $event->getEntityManager();
	// 	$uow = $em->getUnitOfWork();
	// 	echo('<h3>Identify Map in Unit of Work</h3>');
	// 	foreach ($uow->getIdentityMap() as $key1 => $value) {
	// 		if(is_object($value)) echo('<p style="margin:0px;">- Object#'.$key1.' '.get_class($value).' #'.$value->getId().'</p>');
	// 		if(is_array($value)) {
	// 			echo('<p style="margin:0px;">- Array#'.$key1.' '.count($value).'</p>');
	// 			foreach ($value as $key2 => $entity2) {
	// 				// if(!$uow->isEntityScheduled($entity2)) $uow->computeChangeSet($em->getClassMetadata(get_class($entity2)), $entity2);
	// 				// $mappd = count($uow->getEntityChangeSet($entity2)) > 0; $title = 'changeSets > 0 ?';
	// 				$mappd = $uow->isEntityScheduled($entity2); $title = 'isEntityScheduled :';
	// 				// $mappd = $uow->isScheduledForDirtyCheck($entity2); $title = 'isScheduledForDirtyCheck :';
	// 				// $mappd = $uow->isInIdentityMap($entity2); $title = 'isInIdentityMap :';
	// 				$color = $mappd ? 'lightgreen' : 'red';
	// 				if(is_array($entity2)) echo('<p style="margin:0px;"><i>--- Array '.count($entity2).'</i></p>');
	// 				if(is_string($entity2)) echo('<p style="margin:0px;"><i>--- String '.$entity2.'</i></p>');
	// 				if(is_object($entity2)) {
	// 					$default = null;
	// 					if(method_exists($entity2, 'getDefault')) $default = $entity2->getDefault();
	// 					echo('<p style="margin:0px;">--- Object - '.$title.' <span style="color:'.$color.';">'.json_encode($mappd).'</span><i> | Changes : <span style="color:orange;">'.count($uow->getEntityChangeSet($entity2)).'</span> | '.get_class($entity2).' #'.$entity2->getId().' => default : '.json_encode($default).'</i></p>');
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	public function postFlush(PostFlushEventArgs $event) {
		// $em = $event->getEntityManager();
		// $uow = $em->getUnitOfWork();
		// echo('<p>postFlush insertions ('.count($uow->getScheduledEntityInsertions()).') : '.implode(', ', $uow->getScheduledEntityInsertions()).'</p>');
		// foreach($uow->getScheduledEntityInsertions() as $scheduledEntityInsertion) {
		// 	$em->persist($scheduledEntityInsertion);
		// }
		// $em->flush();
		// die('<p style="color:red;">END >>> onFlushSubscriber::postFlush</p>');
	}




}
<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeSubentity;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\nested;

use \Exception;

/** call in controller with $this->get('aetools.aeServiceNested'); */
class aeServiceNested extends aeServiceSubentity {

	const NAME                  = 'aeServiceNested';			// nom du service
	const CALL_NAME             = 'aetools.aeServiceNested';	// comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\nested';

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
	    parent::__construct($container, $EntityManager);
	    $this->defineEntity(self::CLASS_ENTITY);
	    return $this;
	}


	/**
	 * Check entity integrity in context
	 * @param nested $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
	 * @param $eventArgs = null
	 * @return aeServiceNested
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		parent::checkIntegrity($entity, $context, $eventArgs);
		// if($entity instanceOf nested) {
			switch(strtolower($context)) {
				case 'edit':
					// $this->addNestedAttributes($entity);
					break;
				case 'new':
					$this->addNestedAttributes($entity);
					break;
				case 'postload':
					// add nested attributes
					$this->addNestedAttributes($entity);
					break;
				case 'prepersist':
					break;
				case 'postpersist':
					break;
				case 'preupdate':
					break;
				case 'postupdate':
					break;
				case 'preremove':
					break;
				case 'postremove':
					break;
				default:
					break;
			}
		// }
		return $this;
	}

	/**
	 * Add nested attributes to entity
	 * @param nested $entity
	 * @param string $context ('new', 'postLoad', 'prePersist', 'postPersist', 'preUpdate', 'postUpdate', 'preRemove', 'postRemove')
	 * @return aeServiceNested
	 */
	public function addNestedAttributes(nested &$entity) {
		// echo('<pre>');
		$info_entites = $this->container->getParameter('info_entites');
		$attributes = array();
		foreach ($this->aeClasses->getParentClassShortNames($entity, true) as $parent) {
			if(isset($info_entites[$parent]['nestedAttributes'])) {
				// add attributes
				foreach ($info_entites[$parent]['nestedAttributes'] as $lib => $values) {
					foreach ($values as $name => $value) if(is_string($value)) {
						if(preg_match('#^method::#', $value)) {
							// not simple value, but method
							$method = preg_replace('#^method::#', '', $value);
							$info_entites[$parent]['nestedAttributes'][$lib][$name] = $entity->$method();
							// echo('<p style="color:darkblue;margin:0px;">'.$method.'() : '.json_encode($info_entites[$parent]['nestedAttributes'][$lib][$name]).'</p>');
						}
					}
				}
				$attributes = array_merge($attributes, $info_entites[$parent]['nestedAttributes']);
			}
		}
		// echo('<h2>'.$entity->getShortName().' '.json_encode($entity->getNom()).'</h2>');var_dump($attributes);echo('</pre>');
		$entity->setNestedAttributesParameters($attributes);
		return $this;
	}

	// /**
	//  * Check entity after change (edit…)
	//  * @param baseEntity $entity
	//  * @return aeServiceNested
	//  */
	// public function checkAfterChange(&$entity, $butEntities = []) {
	// 	parent::checkAfterChange($entity, $butEntities);
	// 	return $this;
	// }

	// public function checkAll() {
	// 	$results = array();
	// 	$entities = $this->getRepo()->findAll();
	// 	foreach($entities as $entity) {
	// 		$results[$entity->getId().'_'.$entity->getNom()]['entity'] = $entity->checkNestedpositionMap()->getNestedpositionMap();
	// 		$save = $this->save($entity);
	// 		$results[$entity->getId().'_'.$entity->getNom()]['saving']['result'] = $save->getResult();
	// 		$results[$entity->getId().'_'.$entity->getNom()]['saving']['message'] = $save->getMessage();
	// 	}
	// 	return $results;
	// }

	public function sortChildren($data) {
		$return = array();
		$return['ranging'] = array();
		// find nested
		$nested = $this->getRepo()->find($data['entity'][1]);
		// find children
		$children = $nested->getChildsByGroup($data['group']);
		// $children = $this->getRepo()->findItemsByGroup($data['entity'][1], $data['group']);
		$return['childrenFound'] = array();
		foreach($children as $child) {
			$return['childrenFound'][(string)$child->getId()] = $data['entity'][1].'-'.$data['group'].'-'.json_encode($child->getPositionFromHisParent($data['entity'][1], $data['group']));
		}
		// change order
		$step = 0;
		foreach($data['children'] as $ord => $value) {
			foreach($children as $child) {
				if($value[1] == $child->getId()) {
					if($child->setNestedPosition_position($data['entity'][1], $data['group'], $ord)) {
						$return['ranging'][(string)$child->getId()] = $data['entity'][1].'_'.$data['group'].'_'.$ord;
						$changes = array('set' => 'Step '.$step++.' : '.$child->getId().' to position '.$ord);
						foreach($children as $changeChild) {
							$changes[(string)$changeChild->getId()] = json_encode($changeChild->getPositionFromHisParent($data['entity'][1], $data['group']));
						}
						$return['changes'][(string)$child->getId()] = $changes;
					}
					else throw new Exception('Position not found with '.$child->getNom().' #'.$child->getId().' with parent '.$data['entity'][1].' !', 1);
					// $return[$ord] = $child->getId().'_'.$child->getNom();
				}
			}
		}
		// $nested->checkChildsPositions($data['group']);
		// if(count($children) !== count($return)) return 'Impossible de modifier la liste : les éléments fournis ne sont pas corrects.';
		
		$this->getEm()->flush();
		// real children of nested
		// $return = array();
		$return['result'] = array();
		foreach($nested->getChildsByGroup($data['group']) as $child) {
			$return['result'][$child->getId().'_'.$child->getPositionFromHisParent($data['entity'][1], $data['group'])] = $child->getId().'_'.$child->getNom();
		}
		// ksort($return);
		return $return;
	}


}
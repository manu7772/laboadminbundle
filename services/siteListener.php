<?php

namespace Labo\Bundle\AdminBundle\services;

// use Labo\Bundle\AdminBundle\Controller\baseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Labo\Bundle\AdminBundle\Controller\baseController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
// use Symfony\Component\DependencyInjection\ContainerInterface;
// use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\services\aeClasses;

use \Exception;
use \DateTime;

class siteListener {

	const NAME = 'siteListener';

	protected $container;
	protected $aeDebug;

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}

	/**
	* @param FilterControllerEvent $event
	*/
	public function load(FilterControllerEvent $event) {
		$this->container = $event->getController()[0];
		if($this->container instanceOf Controller) {
			$route = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes');
			$route->verifParamsFolder();
			$_controller = $route->getControllerClassName();
			// is in Labo
			$isInLabo = $this->container instanceOf baseController;
			// $isInLabo = false;
			// $parent_ctrl_classes = [];
			// if(class_exists($_controller)) {
			// 	$aeClasses = new aeClasses();
			// 	$parent_ctrl_classes = $aeClasses->getParentClassNames($_controller);
			// 	foreach($parent_ctrl_classes as $parent) if(preg_match('#^Labo\\\Bundle\\\AdminBundle#', $parent)) $isInLabo = true;
			// }
			// $this->aeDebug = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeDebug');
			// only master request
			if($event->isMasterRequest()) {
				// log
				// $this->aeDebug->deleteDebugNamedFile(aeData::LOG_NAME);
				// $this->aeDebug->debugNamedFile(aeData::LOG_NAME, array(
				// 	'called_class' => get_called_class(),
				// 	// 'parent_ctrl_classes' => $parent_ctrl_classes,
				// 	'controller' => $route->getController(),
				// 	'controller_class' => $_controller,
				// 	'controller_name' => $route->getControllerName(),
				// 	'controller_shortname' => $route->getControllerShortName(),
				// 	'isInLabo' => $isInLabo,
				// 	'bundle' => $route->getBundleName(),
				// 	'route' => $route->getRoute(),
				// 	'ip' => $route->getIp(),
				// 	'resquest_type' => $event->getRequestType(),
				// 	'resquest_master' => $event->isMasterRequest(),
				// 	)
				// );
				// trash categorie control in is in Labo
				if($isInLabo) {
					$aeServiceCategorie = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceCategorie');
					$aeServiceCategorie->controlTrashCategorie();
				}
				// sitedata
				$sitedata = $this->container->get('aetools.aeServiceSite')->getRepo()->findSiteData(); // default : all 15 minutes
				// if(
				// 	!isset($sitedata['id'])
				// 	|| $this->container->get('kernel')->getEnvironment() == 'dev'
				// ) {
					// $sitedata = $this->container->get('aetools.aeServiceSite')->getRepo()->findSiteData(); // default : all 15 minutes
					// if(is_array($sitedata)) {
					// 	$date = new DateTime();
					// 	$sitedata['time'] = $date->format('Y-m-d H:i:s');
					// 	$sitedata['DateTime'] = $date;
					// }
					 // else {
						// throw new Exception("SiteListener::load() : no site data found (found ".json_encode(gettype($sitedata)).")", 1);
						// $sitedata = null;
					// }
					// save site data
					$event->getRequest()->getSession()->set(aeData::SITE_DATA, $sitedata);

					// user info
					$olddata = (array)$event->getRequest()->getSession()->get('user');
					$olddata['ip'] = $event->getRequest()->getClientIp();
					$event->getRequest()->getSession()->set('user', $olddata);
				// }
			}
		}
	}


}
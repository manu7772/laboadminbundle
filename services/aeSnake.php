<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeData;
// use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
// use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Service aeSnake
 * - Gestion des attributs nesteds
 * class: Labo\Bundle\AdminBundle\services\aeSnake
 * arguments: array $nestedAttributesParameters
 */
class aeSnake {

	const VIRTUAL_PROPERTIES_PREFIX = 'group';
	const PARENT_NAME = 'parent';
	const CHILD_NAME = 'child';
	const PARENTS_NAME = 'parents';
	const CHILDS_NAME = 'childs';
	const VIRTUALGROUPS_STRIP = '#^((set|add|remove|get)?_?((group)?_?(.{3,})))(_(parent|child|parents|childs))$#';

	protected $nestedAttributesParameters;
	protected $VIRTUALGROUPS_STRIP;
	protected $actions;
	protected $hierarchy;
	protected $hierarchyParent;
	protected $hierarchyChild;
	protected $propPrefix;
	protected $keywords;

	/*
	NAMES OF VARIABLES :
	$group								nesteds / categorie_nesteds / … (short name)
	$Ggroup								group_nesteds (long name)
	$Ggroup_hierarchy / $attribute		group_nesteds_parents (-> virtual attribute)
	$set_Ggroup_hierarchy				setGroup_nesteds_parents / addGroup_nesteds_parents (!! « s »)
	$hierarchy							parent(s) / child(s)
	*/

	public function __construct($nestedAttributesParameters) {
		$this->actions = array('set', 'add', 'remove', 'get');
		$this->hierarchyParent = array(self::PARENT_NAME, self::PARENTS_NAME);
		$this->hierarchyChild = array(self::CHILD_NAME, self::CHILDS_NAME);
		$this->hierarchy = array_merge($this->hierarchyParent, $this->hierarchyChild);
		$this->propPrefix = self::VIRTUAL_PROPERTIES_PREFIX;
		$this->keywords = array_merge($this->actions, $this->hierarchy, array($this->propPrefix));
		$this->VIRTUALGROUPS_STRIP = '#^(('.implode('|', $this->actions).')?_?(('.$this->propPrefix.')?_?(.{3,})))(_('.implode('|', $this->hierarchy).'))$#';
		$this->setNestedAttributesParameters($nestedAttributesParameters);
	}

	public function setNestedAttributesParameters($nestedAttributesParameters) {
		$this->nestedAttributesParameters = array();
		foreach ($nestedAttributesParameters as $name => $values) {
			$forbidden = preg_match('#('.implode('|', $this->getKeywords()).')#i', $name);
			// echo('<p style="margin:0px;">Creating nested attribute '.json_encode($name).' => <strong>'.aeData::decamelize($name).'</strong> <em><i>(Valid : '.json_encode($forbidden).')</i></em></p>');
			if($forbidden) {
				throw new Exception("Bad name for ".$name." attribute : can not have these keywords : ".json_encode($this->getKeywords()), 1);
			}
			$this->nestedAttributesParameters[aeData::decamelize($name)] = $values;
		}
		return $this;
	}

	// GETTERS

	public function getNestedAttributesParameters() {
		return is_array($this->nestedAttributesParameters) ? $this->nestedAttributesParameters : array();
	}

	public function getNestedAttributeNames($Ggroups = true) {
		$nestedAttributesParameters = array_keys($this->getNestedAttributesParameters());
		if($Ggroups) foreach($nestedAttributesParameters as $key => $value) {
			$nestedAttributesParameters[$key] = $this->propPrefix.'_'.$value;
		}
		return $nestedAttributesParameters;
	}

	public function getNestedAttributesClasses($attribute) {
		aeData::decamelize($attribute);
		$attribute = $this->getGroupName_internal($attribute, false);
		return isset($this->getNestedAttributesParameters()[$attribute]['class']) ? $this->getNestedAttributesParameters()[$attribute]['class'] : false;
	}

	public function getPropPrefix() {
		return $this->propPrefix;
	}

	public function getKeywords() {
		return $this->keywords;
	}

	public function getHierarchyNames() {
		return $this->hierarchy;
	}

	public function getParentsNames() {
		return $this->hierarchyParent;
	}

	public function getChildsNames() {
		return $this->hierarchyChild;
	}

	// VERIF IF EXIST

	public function hasGgroupOrGroup($attribute) {
		return $this->hasGgroup($attribute) || $this->hasGroup($attribute);
	}

	public function hasGgroup($Ggroup) {
		return in_array($this->getGroupName_internal($Ggroup, null), $this->getNestedAttributeNames(true));
	}

	public function hasGroup($group) {
		return in_array($this->getGroupName_internal($group, null), $this->getNestedAttributeNames(false));
	}

	// GET PARTS OF NAME

	protected function getGroupName_internal($property, $Ggroups = false) {
		aeData::decamelize($property);
		if(!preg_match('#('.implode('|', $this->hierarchy).')$#i', $property)) $property = $property.'_'.self::PARENTS_NAME;
		if($Ggroups === null) {
			return preg_replace($this->VIRTUALGROUPS_STRIP, '${3}', $property);
		}
		$property = preg_replace($this->VIRTUALGROUPS_STRIP, '${5}', $property);
		return $Ggroups ? $this->propPrefix.'_'.$property : $property;
	}

	public function getGroupName($property, $Ggroups = false) {
		$groupName = $this->getGroupName_internal($property, $Ggroups);
		return !$this->hasGroup($groupName) ? false : $groupName;
	}

	public function getHierarchy($property, $firstlc = false) {
		aeData::decamelize($property);
		if(!$this->isHierarchy($property)) return false;
		$result = preg_replace($this->VIRTUALGROUPS_STRIP, '${7}', $property);
		return $firstlc ? lcfirst($result) : $result;
	}

	public function getAction($property) {
		aeData::decamelize($property);
		$motif = '#^('.implode('|', $this->actions).')(.)+#i';
		if(preg_match($motif, $property)) return strtolower(preg_replace($motif, '${1}', $property));
		return false;
	}

	// NAMES TESTS

	public function isGroupName($group) {
		aeData::decamelize($group);
		return $this->getGroupName_internal($group) == $group;
	}

	public function isGgroup($Ggroup) {
		aeData::decamelize($Ggroup);
		return preg_replace($this->VIRTUALGROUPS_STRIP, '${3}', $Ggroup) == $Ggroup && !$this->isGroupName($Ggroup);
	}

	public function isAction($action) {
		aeData::decamelize($action);
		return preg_replace($this->VIRTUALGROUPS_STRIP, '${2}', $action) == $action && !$this->isGroupName($action);
	}

	public function isHierarchy($hierarchy) {
		aeData::decamelize($hierarchy);
		return preg_match('#^('.implode("|", $this->hierarchy).')$#i', preg_replace($this->VIRTUALGROUPS_STRIP, '${7}', $hierarchy)) && !$this->isGroupName($hierarchy);
	}

	public function isNestParent($parent) {
		aeData::decamelize($parent);
		return preg_match('#^('.implode("|", $this->hierarchyParent).')$#i', preg_replace($this->VIRTUALGROUPS_STRIP, '${7}', $parent)) && !$this->isGroupName($parent);
	}

	public function isNestChild($child) {
		aeData::decamelize($child);
		return preg_match('#^('.implode("|", $this->hierarchyChild).')$#i', preg_replace($this->VIRTUALGROUPS_STRIP, '${7}', $child)) && !$this->isGroupName($child);
	}

	public function isParentName($parentName) {
		// return in_array($parentName, $this->getParentsNames);
		return preg_match('#^('.implode('|', $this->getParentsNames()).')$#', $parentName);
	}

	public function isChildName($childName) {
		// return in_array($childName, $this->getChildsNames);
		return preg_match('#^('.implode('|', $this->getChildsNames()).')$#', $childName);
	}

	// STRIPS

	public function stripActionAndHierarchy($method) {
		aeData::decamelize($method);
		return lcfirst(preg_replace($this->VIRTUALGROUPS_STRIP, '${3}', $method));
	}

	public function stripHierarchy($method) {
		aeData::decamelize($method);
		return lcfirst(preg_replace($this->VIRTUALGROUPS_STRIP, '${1}', $method));
	}

	public function stripAction($method) {
		aeData::decamelize($method);
		return lcfirst(preg_replace($this->VIRTUALGROUPS_STRIP, '${3}${6}', $method));
	}




}


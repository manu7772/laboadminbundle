<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeData;

use \ReflectionObject;
use \ReflectionClass;
use \ReflectionMethod;
use \Exception;

/**
 * Service aeClasses
 * - Gestion des classes
 * class: Labo\Bundle\AdminBundle\services\aeClasses
 * arguments: null
 */
class aeClasses {

	const NAME					= 'aeClasses'; 			// nom du service
	const CALL_NAME				= 'aetools.aeClasses';	// comment appeler le service depuis le controller/container

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IDENTIFICATION CLASSE
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}

	//////////////////////////////////
	// CLASS
	//////////////////////////////////

	/**
	 * Get classname
	 * @param mixed $className
	 * @return string
	 */
	public function getClassName($className) {
		$ReflectionClass = new ReflectionClass($className);
		return $ReflectionClass->getName();
	}

	/**
	 * Get namespace
	 * @param mixed $className
	 * @return string
	 */
	public function getNamespaceName($className) {
		$ReflectionClass = new ReflectionClass($className);
		return $ReflectionClass->getNamespaceName();
	}

	/**
	 * Get class shortname
	 * @param mixed $className
	 * @return string
	 */
	public function getShortName($className) {
		$ReflectionClass = new ReflectionClass($className);
		return $ReflectionClass->getShortName();
	}



	//////////////////////////////////
	// PARENT
	//////////////////////////////////

	/**
	 * Get parent class
	 * @param mixed $className
	 * @return ReflectionClass
	 */
	public function getParentClass($className) {
		$ReflectionClass = new ReflectionClass($className);
		return $ReflectionClass->getParentClass();
	}

	/**
	 * Get parent classname
	 * @param mixed $className
	 * @return string
	 */
	public function getParentClassName($className) {
		return $this->getParentClass($className)->getName();
	}

	/**
	 * Get parents class shortname
	 * @param mixed $className
	 * @return string
	 */
	public function getParentClassShortName($className) {
		return $this->getParentClass($className)->getShortName();
	}


	//////////////////////////////////
	// ALL PARENTS
	//////////////////////////////////

	/**
	 * Get parents classes
	 * @param mixed $className
	 * @param boolean $includeIt = true
	 * @return array of ReflectionClass
	 */
	public function getParentClasses($className, $includeIt = true) {
		$className = new ReflectionClass($className);
		// echo('<h3>getParentClasses : '.$className->getShortName().' / '.$className->getName().'</h3>');
		$parents = array();
		if($includeIt) $parents[] = $className->getName();
		while($parent = $className->getParentClass()) {
			$parents[] = $parent->getName();
			$className = $parent;
		}
		// echo('<pre><h3>getParentClasses</h3>');var_dump($parents);echo('</pre>');
		return $parents;
	}

	/**
	 * Get parents classnames
	 * @param mixed $className
	 * @param boolean $includeIt = true
	 * @return array of string
	 */
	public function getParentClassNames($className, $includeIt = true) {
		// $parents = array();
		// foreach ($this->getParentClasses($className, $includeIt) as $parent) $parents[] = $this->getClassName($parent);
		// return $parents;
		return $this->getParentClasses($className, $includeIt);
	}

	/**
	 * Get parents class shortnames
	 * @param mixed $className
	 * @param boolean $includeIt = true
	 * @return array of string
	 */
	public function getParentClassShortNames($className, $includeIt = true) {
		$parents = array();
		foreach ($this->getParentClasses($className, $includeIt) as $parent) $parents[] = $this->getShortName($parent);
		// echo('<pre><h3>getParentClassShortNames</h3>');var_dump($parents);echo('</pre>');
		return $parents;
	}




	//////////////////////////////////
	// INFO
	//////////////////////////////////

	/**
	 * Is instantiable
	 * @param mixed $className
	 * @return boolean
	 */
	public function isInstantiable($className) {
		$ReflectionClass = new ReflectionClass($className);
		return $ReflectionClass->isInstantiable();
	}

	/**
	 * Is abstract
	 * @param mixed $className
	 * @return boolean
	 */
	public function isAbstract($className) {
		$ReflectionClass = new ReflectionClass($className);
		return $ReflectionClass->isAbstract();
	}

	/**
	 * Is insterface
	 * @param mixed $className
	 * @return boolean
	 */
	public function isInterface($className) {
		$ReflectionClass = new ReflectionClass($className);
		return $ReflectionClass->isInterface();
	}

	public function getReflexionMethodConstants() {
		return array(
			ReflectionMethod::IS_STATIC => 'static',
			ReflectionMethod::IS_PUBLIC => 'public',
			ReflectionMethod::IS_PROTECTED => 'protected',
			ReflectionMethod::IS_PRIVATE => 'private',
			ReflectionMethod::IS_ABSTRACT => 'abstract',
			ReflectionMethod::IS_FINAL => 'final',
			);
	}

	public function getReflexionClassConstants() {
		return array(
			ReflectionClass::IS_IMPLICIT_ABSTRACT => 'implicit abstract',
			ReflectionClass::IS_EXPLICIT_ABSTRACT => 'explicit abstract',
			ReflectionClass::IS_FINAL => 'final',
			);
	}

	/**
	 * Get properties of $object
	 * Important for objects to get new dynamic properties !!
	 * 
	 * 
	 */
	public function getPropertiesOfObject($object) {
		if(!is_object($object)) throw new Exception("Object must be an object.", 1);
		$ReflectionClass = new ReflectionObject($object);
		$list = array();
		foreach($ReflectionClass->getProperties() as $key => $value) $list[] = $value->getName();
		return $list;
	}

	public function getClassProperties($object) {
			$result = array();
			// $ReflectionClass = new ReflectionObject($object);
			if(is_object($object)) {
				$result['object'] = $object;
				$ReflectionClass = new ReflectionObject($object);
			} else if(is_string($object)) {
				$result['object'] = new $object();
				$ReflectionClass = new ReflectionClass($object);
			}
			$result['class']['comments'] = $this->commentsParser($ReflectionClass->getDocComment(), false);
			$result['class']['filename'] = $ReflectionClass->getFileName();
			$methods = $ReflectionClass->getMethods();
			foreach ($methods as $key => $method) {
				// access
				$result['methods'][$method->getName()]['comments'] = $this->commentsParser($method->getDocComment());
				$result['methods'][$method->getName()]['access'] = array();
				if($method->isStatic()) $result['methods'][$method->getName()]['access'][] = 'static';
				if($method->isPublic()) $result['methods'][$method->getName()]['access'][] = 'public';
				if($method->isProtected()) $result['methods'][$method->getName()]['access'][] = 'protected';
				if($method->isPrivate()) $result['methods'][$method->getName()]['access'][] = 'private';
				if($method->isAbstract()) $result['methods'][$method->getName()]['access'][] = 'abstract';
				if($method->isFinal()) $result['methods'][$method->getName()]['access'][] = 'final';
				// constructor
				$result['methods'][$method->getName()]['constructor'] = false;
				if($method->isConstructor()) $result['methods'][$method->getName()]['constructor'] = true;
				// desctructor
				$result['methods'][$method->getName()]['destructor'] = false;
				if($method->isDestructor()) $result['methods'][$method->getName()]['destructor'] = true;
			}
			$properties = $ReflectionClass->getProperties();
			foreach ($properties as $key => $property) {
				// access
				$result['properties'][$property->getName()]['comments'] = $this->commentsParser($property->getDocComment());
				$result['properties'][$property->getName()]['access'] = array();
				if($property->isStatic()) $result['properties'][$property->getName()]['access'][] = 'static';
				if($property->isPublic()) $result['properties'][$property->getName()]['access'][] = 'public';
				if($property->isProtected()) $result['properties'][$property->getName()]['access'][] = 'protected';
				if($property->isPrivate()) $result['properties'][$property->getName()]['access'][] = 'private';
				if($property->isDefault()) $result['properties'][$property->getName()]['access'][] = 'default';
				// if($property->isAbstract()) $result['properties'][$property->getName()]['access'][] = 'abstract';
				// if($property->Final()) $result['properties'][$property->getName()]['access'][] = 'final';
			}
			$constants = $ReflectionClass->getConstants();
			foreach ($constants as $key => $constant) {
				$result['constants'][$key] = $constant;
			}
			return $result;
	}

	/**
	 * Parse des commentaires PHP    		
	 * @param string $comments
	 * @param boolean $findKeys = true
	 * @return array
	 */
	public function commentsParser($comments, $findKeys = true) {
		$comments = preg_split('#(\\r|\\n)#', $comments);
		$lines = array();
		foreach ($comments as $comment) {
			if(!preg_match('#^[[:space:]]*(\/\*\*|\*\/)[[:space:]]*#', $comment)) {
				// ligne ok
				$line = trim(preg_replace('#^[[:space:]]*\*[[:space:]]*#', '', $comment));
				if(strlen($line) > 0) $lines[] = $line;
			}
		}
		if(!$findKeys) return $lines;
		$result = array();
		$result['texts'] = array();
		$result['keys'] = array();
		foreach ($lines as $line) {
			if(preg_match('#^@[A-Za-z]{2,}\s*#', $line)) {
				// clé détectée
				$split = preg_split('#[[:space:]]+#', $line, 3);
				if(!isset($split[1])) $split[1] = '';
				if(!isset($split[2])) $split[2] = '';
				$tmp = array();
				$tmp['key'] = '';
				$tmp['var'] = '';
				$tmp['type'] = '';
				// key
				$tmp['key'] = preg_replace('#^@#', '', $split[0]);
				unset($split[0]);
				// type
				if(preg_match('#^\$#', $split[2])) {
					$tmp['var'] = $split[2];
					$tmp['type'] = $split[1];
				} else if(preg_match('#^\$#', $split[1])) {
					// pas de type mais une var
					$tmp['var'] = implode(' ', $split);
				} else {
					// ni type ni var, juste un commentaire
					$tmp['var'] = implode(' ', $split);
				}
				$result['keys'][] = $tmp;
			} else {
				$result['texts'][] = preg_replace('#(\\t|\\s)+#', ' ', $line);
			}
		}
		return $result;
	}




	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// STRUCTURES DE CLASSES
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Renvoie un array de la hiérarchie de la classe
	 * @param mixed $className - nom de la classe (AVEC namespace !!) ou objet
	 * @param string $format - 'string' ou 'array' (défaut)
	 * @return array
	 */
	public function getClassHierarchy($className, $format = 'array') {
		if(!is_string($format)) $format = 'array';
		if(is_object($className)) $className = get_class($className);
		$parents = array();
		$treeB = $this->getClassTree($className);
		do {
			$treeB = reset($treeB);
			$parents[] = $treeB['shortName'];
			$treeB = $treeB['parent'];
		} while ($treeB !== false);
		unset($treeB);
		return strtolower($format) === 'string' ? implode(" ".aeData::SLASH." ", $parents) : $parents;
	}

	public function getClassTree($className) {
		if(is_object($className)) $className = get_class($className);
		$tree = array();
		$ReflectionClass = new ReflectionClass($className);
		$tree[$className]['shortName'] = $this->getClassShortName($className);
		$tree[$className]['longName'] = $className;
		$tree[$className]['abstract'] = $ReflectionClass->isAbstract();
		// parents
		$tree[$className]['parent'] = false;
		$parentClassName = get_parent_class($className);
		if($parentClassName !== false) $tree[$className]['parent'] = $this->getClassTree($parentClassName);
		return $tree;
	}





	//////////////////////////////////
	// DIVERS
	//////////////////////////////////

	/**
	 * Renvoie le nom de la méthode en fonction de l'attribut et du préfix
	 * On peut vérifier si la méthode est dans l'objet $testEntity en ajoutant cet objet ou sa classe (pas de shortname)
	 * @param string $attribute
	 * @param string $prefix = "set"
	 * @param mixed $testEntity = null
	 * @param mixed $filter (put ReflectionMethod constants here if you want to filter result)
	 * @return string / false
	 */
	public function getMethodNameWith($attribute, $prefix = "set", $testEntity = null, $filter = null) {
		$validMethods = null;
		if(in_array($prefix, array("remove", "add"))) $attribute = preg_replace("#s$#i", "", $attribute);
		$method = $prefix.ucfirst($attribute);
		if($testEntity !== null) {
			$validMethods = array();
			$ReflectionClass = new ReflectionClass($testEntity);
			return $ReflectionClass->hasMethod($method) ? $method : false;
		} else {
			return $method;
		}
		return false;
	}


}
<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\Entity\rawfile;
use Labo\Bundle\AdminBundle\Entity\baseEntity;

// call in controller with $this->get('aetools.aeRawfile');
class aeServiceRawfile extends aeServiceBaseEntity {

	const NAME                  = 'aeServiceRawfile';        // nom du service
	const CALL_NAME             = 'aetools.aeRawfile'; // comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\rawfile';
	const CLASS_SHORT_ENTITY    = 'rawfile';

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		parent::__construct($container, $EntityManager);
		$this->defineEntity(self::CLASS_ENTITY);
		return $this;
	}


	/**
	 * Check entity integrity in context
	 * @param rawfile $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
	 * @param $eventArgs = null
	 * @return aeServiceRawfile
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		parent::checkIntegrity($entity, $context, $eventArgs);
		// if($entity instanceOf rawfile) {
			switch(strtolower($context)) {
				case 'new':
				case 'postload':
					$entity->setCropperInfo($this->container->getParameter('cropperInfo'));
					break;
				case 'prepersist':
					break;
				case 'postpersist':
					break;
				case 'preupdate':
					break;
				case 'postupdate':
					break;
				case 'preremove':
					break;
				case 'postremove':
					break;
				default:
					break;
			}
		// }
		return $this;
	}



	/**
	 * Renvoie une nouvelle entité rawfile remplie avec les données de $data
	 * @param array $data
	 * @return rawfile
	 */
	public function getNewRawfileWithData($data) {
		$rawfile = $this->getNewEntity(self::CLASS_ENTITY);
		$rawfile->setOriginalnom($data['file']['name']);
		$rawfile->setNom($data['file']['name']);
		$rawfile->setFormat($data['file']['type']);
		$rawfile->setFileSize($data['file']['size']);
		$rawfile->setHeight($data['height']);
		$rawfile->setWidth($data['width']);
		$rawfile->setBinaryFile($data['raw']);
		$this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceStatut')->setTemp($rawfile);
		return $rawfile;
	}


}
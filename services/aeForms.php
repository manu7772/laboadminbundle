<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Labo\Bundle\AdminBundle\services\aeData;

use \Exception;
use \NotFoundHttpException;

class aeForms extends aeServiceBaseEntity {

	const NAME 				= 'aeForms';
	const CALL_NAME 		= 'aetools.aeForms';

	protected $container;			// container
	protected $router;				// router
	protected $formFactory;			// form.factory

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
		$this->formFactory = $this->container->get('form.factory');
		$this->router = $this->container->get('router');
		return $this;
	}



	/**
	 * Get closer formType for $entity
	 * @param mixed $entity
	 * @return string
	 */
	public function getEntityTypename($entity) {
		foreach ($this->aeClasses->getParentClassNames($entity, true) as $typename) {
			$typeclass = preg_replace('#Entity(\\\\)?$#', 'Form\\', $this->aeClasses->getNamespaceName($typename)).$this->aeClasses->getShortName($entity).'Type';
			// echo('<p>Entity : '.$typename.' => '.$typeclass.'</p>');
			if(class_exists($typeclass)) return $typeclass;
		}
		throw new Exception('aeForms error on line '.__LINE__.' : no formType found for this entity '.json_encode($entity).'!', 1);
	}

	/**
	 * Renvoie la vue du formulaire de l'entité $entite
	 * @param object $entite
	 * @param string $action
	 * @param array $data
	 * @return Symfony\Component\Form\FormView
	 */
	public function getEntityFormView(&$data, $typeClassname = null) {
		return $this->getEntityForm($data, $typeClassname, true);
	}

	/**
	 * Renvoie le formulaire de l'entité $entite
	 * @param object $entite
	 * @param string $action
	 * @param array $data
	 * @return Symfony\Component\Form\Form
	 */
	public function getEntityForm(&$data, $typeClassname = null, $getViewNow = false) {
		if(!is_array($data)) throw new Exception("getEntityForm : data doit être défini !", 1);
		$types_valid = array(aeData::LIST_ACTION, aeData::SHOW_ACTION, aeData::CREATE_ACTION, aeData::EDIT_ACTION, aeData::COPY_ACTION, aeData::DELETE_ACTION);
		if(!in_array($data['action'], $types_valid)) {
			// throw new Exception("Action ".$action." invalide, doit être ".json_encode($types_valid, true), 1);
			throw new Exception("getEntityForm => invalid action form : ".$data['action'], 1);
		}
		// récupère les directions en fonction des résultats
		$this->addContextActionsToData($data);
		$form = false;
		// define Type
		if($typeClassname == null || !class_exists($typeClassname))
			$typeClassname = $this->getEntityTypename($data['entite']);
		switch ($data['action']) {
			case aeData::SHOW_ACTION:
			case aeData::LIST_ACTION:
			case aeData::CREATE_ACTION:
				// echo('<p>Nom form : '.$typeClassname.'</p>');
				$form = $this->formFactory->create(new $typeClassname($this->container, $data), $data['entite']);
				break;
			case aeData::EDIT_ACTION:
				$form = $this->formFactory->create(new $typeClassname($this->container, $data), $data['entite']);
				break;
			case aeData::COPY_ACTION:
				throw new Exception("Ce formulaire ".$data['action']." n'est pas encore supporté.", 1);
				break;
			case aeData::DELETE_ACTION:
				throw new Exception("Ce formulaire ".$data['action']." n'est pas encore supporté.", 1);
				break;
			default:
				$this->container->get('flash_messages')->send(array(
					'title'		=> 'Erreur formulaire',
					'type'		=> flashMessage::MESSAGES_ERROR,
					'text'		=> 'Ce type de formulaire <strong>'.json_encode($typeClassname).'</strong> n\'est pas reconnu.',
				));
				break;
		}
		if($form == false) {
			$this->container->get('flash_messages')->send(array(
				'title'		=> 'Erreur de génération du formalaire',
				'type'		=> flashMessage::MESSAGES_ERROR,
				'text'		=> 'Le formulaire n\'a pas pu être généré. Veuillez contacter le webmaster.',
			));
		} else {
			if($getViewNow != false) return $form->createView();
		}
		return $form;
	}

	/**
	 * Renvoie les url selon résultats (pour formulaires)
	 * @param array $data = null
	 * @return array
	 */
	public function addContextActionsToData(&$data) {
		if(!is_array($data)) throw new Exception("addContextActionsToData : data doit être défini !", 1);
		switch ($data['action']) {
			case 'delete_linked_image':
				if(!isset($data['form_action'])) {
					$data['form_action'] = $this->router->generate('siteadmin_form_action', array(
						'classname'	=> $data['entite_name'],
						), true);
				}
				if(!isset($data['onSuccess'])) {
					if($data['type']['type_related'] != null) {
						$data['onSuccess'] = $this->router->generate('siteadmin_entite_type', array(
							'entite'		=> $data['entite_name'],
							'type_related'	=> $data['type']['type_related'],
							'type_field'	=> $data['type']['type_field'],
							'type_values'	=> $this->typeValuesToString($data['type']['type_values']),
							'action'		=> aeData::SHOW_ACTION,
							'id'			=> $data['entite']->getId(),
							), true);
					} else {
						$data['onSuccess'] = $this->router->generate('siteadmin_entite', array(
							'entite'	=> $data['entite_name'],
							'id'		=> $data['entite']->getId(),
							'action'	=> aeData::SHOW_ACTION,
							), true);
					}
				}
				if(!isset($data['onError'])) {
					$data['onError'] = null;
				}
				break;
			case aeData::CREATE_ACTION:
				if(!isset($data['form_action'])) {
					$data['form_action'] = $this->router->generate('siteadmin_form_action', array(
						'classname'	=> $data['entite_name'],
						), true);
				}
				if(!isset($data['onSuccess'])) {
					if($data['type']['type_related'] != null) {
						$data['onSuccess'] = $this->router->generate('siteadmin_entite_type', array(
							'entite'		=> $data['entite_name'],
							'type_related'	=> $data['type']['type_related'],
							'type_field'	=> $data['type']['type_field'],
							'type_values'	=> $this->typeValuesToString($data['type']['type_values']),
							'action'		=> aeData::SHOW_ACTION,
							'id'			=> $data['entite']->getId(),
							), true);
					} else {
						$data['onSuccess'] = $this->router->generate('siteadmin_entite', array(
							'entite'	=> $data['entite_name'],
							'id'		=> $data['entite']->getId(),
							'action'	=> aeData::SHOW_ACTION,
							), true);
					}
				}
				if(!isset($data['onError'])) {
					$data['onError'] = null;
				}
			case aeData::EDIT_ACTION:
				if(!isset($data['form_action'])) {
					$data['form_action'] = $this->router->generate('siteadmin_form_action', array(
						'classname'	=> $data['entite_name'],
						), true);
				}
				if(!isset($data['onSuccess'])) {
					if($data['type']['type_related'] != null) {
						$data['onSuccess'] = $this->router->generate('siteadmin_entite_type', array(
							'entite'		=> $data['entite_name'],
							'type_related'	=> $data['type']['type_related'],
							'type_field'	=> $data['type']['type_field'],
							'type_values'	=> $this->typeValuesToString($data['type']['type_values']),
							'action'		=> aeData::SHOW_ACTION,
							'id'			=> $data['entite']->getId(),
							), true);
					} else {
						$data['onSuccess'] = $this->router->generate('siteadmin_entite', array(
							'entite'	=> $data['entite_name'],
							'action'	=> aeData::SHOW_ACTION,
							'id'		=> $data['entite']->getId(),
							), true);
					}
				}
				if(!isset($data['onError'])) {
					$data['onError'] = null;
				}
				break;
			case aeData::COPY_ACTION:
				if(!isset($data['form_action'])) {
					$data['form_action'] = $this->router->generate('siteadmin_form_action', array(
						'classname'	=> $data['entite_name'],
						), true);
				}
				if(!isset($data['onSuccess'])) {
					$data['onSuccess'] = $this->router->generate('siteadmin_entite', array(
						'entite'	=> $data['entite_name'],
						'id'		=> null,
						'action'	=> aeData::SHOW_ACTION,
						), true);
				}
				if(!isset($data['onError'])) {
					$data['onError'] = null;
				}
				break;
			case aeData::DELETE_ACTION:
				if(!isset($data['form_action'])) {
					$data['form_action'] = $this->router->generate('siteadmin_form_action', array(
						'classname'	=> $data['entite_name'],
						), true);
				}
				if(!isset($data['onSuccess'])) {
					$data['onSuccess'] = $this->router->generate('siteadmin_entite', array(
						'entite'	=> $data['entite_name'],
						), true);
				}
				if(!isset($data['onError'])) {
					$data['onError'] = $this->router->generate('siteadmin_entite', array(
						'entite'	=> $data['entite_name'],
						'id'		=> $data['entite']->getId(),
						'action'	=> aeData::SHOW_ACTION,
						), true);
				}
				break;
			
			default:
				if(!isset($data['form_action'])) {
					$data['form_action'] = $this->router->generate('siteadmin_form_action', array(
						'classname'	=> $data['entite_name'],
						), true);
				}
				break;
		}
		// return $data;
	}

	protected function typeValuesToArray($type_values = null) {
		if($type_values != null) $type_values = explode(aeData::TYPE_VALUE_JOINER, $type_values);
		return $type_values;
	}
	protected function typeValuesToString($type_values = null) {
		if($type_values != null) $type_values = implode(aeData::TYPE_VALUE_JOINER, $type_values);
		return $type_values;
	}


}
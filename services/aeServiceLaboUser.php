<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Role\Role;

use Labo\Bundle\AdminBundle\Entity\message;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \DateTime;

class aeServiceLaboUser extends aeServiceBaseEntity {

	const NAME                  = 'aeServiceLaboUser';				// nom du service
	const CALL_NAME             = 'aetools.aeServiceLaboUser';		// comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\LaboUser';
	const PASSWORD				= 'password';

	protected $UserManager;           // UserManager
	protected $EncoderFactory;        // EncoderFactory
	protected $listOfRoles;        	  // List of roles

	// public function __construct(UserManager $UserManager, EncoderFactory $EncoderFactory) {
	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
	    parent::__construct($container, $EntityManager);
	    $this->defineEntity(self::CLASS_ENTITY);
	    $this->listOfRoles = null;
		$this->UserManager = $this->container->get('fos_user.user_manager');
		$this->EncoderFactory = $this->container->get('security.encoder_factory');
		return $this;
	}

	public function getRolesHierarchy() {
		if($this->listOfRoles === null) {
			$yamlService = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeYaml');
			$this->listOfRoles = $yamlService->getYmlContent('security.yml')['security']['role_hierarchy'];
		}
		return $this->listOfRoles;
	}

	/**
	 * Check entity integrity in context
	 * @param baseEntity $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove', 'PreFlush')
	 * @param $eventArgs = null
	 * @return aeServiceLaboUser
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		parent::checkIntegrity($entity, $context, $eventArgs);
		// if($entity instanceOf LaboUser) {
			switch(strtolower($context)) {
				case 'new':
					// break;
				case 'postload':
					if($entity->isCollaborator()) $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceMessage')->checkMessagesCollaborator($entity, true);
					// echo('<p>User loaded. ('.$entity->getUsername().')</p>');
					$granted_roles = array();
					foreach ($entity->getRoles() as $role) {
						$roles = $this->container->get('security.role_hierarchy')->getReachableRoles(array(new Role($role)));
						foreach ($roles as $granted_role) {
							$granted_roles[$granted_role->getRole()] = $granted_role->getRole();
						}
					}
					$entity->setGrantedRoles($granted_roles);
					$entity->setRolesHierarchy($this->getRolesHierarchy());
					// echo('<pre>');var_dump($this->listOfRoles);die('</pre>');
					break;
				case 'prepersist':
					break;
				case 'postpersist':
					break;
				case 'preupdate':
					break;
				case 'postupdate':
					break;
				case 'preremove':
					break;
				case 'postremove':
					break;
				case 'preflush':
					break;
				default:
					break;
			}
		// }
		return $this;
	}


	public function usersExist($createIfNobody = false) {
		$users = $this->UserManager->findUsers();
		$u = true;
		if($createIfNobody === true && count($users) < 1) {
			// crée si aucun utilisateur
			$u = $this->createUsers(true);
		}
		return count($u) > 0 ? $u : false ;
	}

	/**
	 * Efface tous les utilisateurs
	 * @return integer
	 */
	public function deleteAllUsers() {
		$users = $this->UserManager->findUsers();
		$n = count($users);
		if($n > 0) {
			foreach($users as $key => $user) {
				$this->UserManager->deleteUser($user);
			}
		}
		return $n;
	}

	/**
	 * Hydrate avec les utilisateurs de base
	 * @param boolean $deleteAll
	 * @return array
	 */
	public function createUsers($deleteAll = false) {
		if($deleteAll === true) {
			// efface tous les utilisateur existants
			$this->deleteAllUsers();
		}
		$users = $this->getCreateUsers();
		// crée les users par défaut
		$newUsers = array();
		$attrMethods = array('set', 'add');
		foreach ($users as $key => $user) {
			$newUsers[$key] = $this->UserManager->createUser();
			foreach ($user as $attribute => $value) {
				foreach ($attrMethods as $method) {
					$m = $method.ucfirst($attribute);
					if(method_exists($newUsers[$key], $m)) {
						if($attribute == self::PASSWORD) {
							$encoder = $this->EncoderFactory->getEncoder($newUsers[$key]);
							$value = $encoder->encodePassword($value, $newUsers[$key]->getSalt());
						}
						$newUsers[$key]->$m($value);
					}
				}
			}
			$this->UserManager->updateUser($newUsers[$key]);
		}
		return $newUsers;
	}

	public function getUserNumberOfMessages($user) {
		if($user instanceOf LaboUser) $user = $user->getId();
		return $this->getEm()->createQuery("SELECT COUNT(element.collaborator) FROM Labo\Bundle\AdminBundle\Entity\messageuser element WHERE element.collaborator = ".$user)->getSingleScalarResult();
	}


	public function setUserMessageSent(LaboUser $user, message $message, $sent = true, $flush = true) {
		$aeReponse = new aeReponse();
		if($sent === false) $sent = null;
		if($sent !== null && !($sent instanceOf DateTime)) $sent = new DateTime('NOW');
		$message_user = $user->getMessageuser($message);
		if($message_user instanceOf messageuser) $message_user->setSent($sent);
		if($flush !== false) $this->getEm()->flush();
	}

	protected function getCreateUsers() {
		$users = array(
			array(
				'username' => 'sadmin',
				self::PASSWORD => 'sadmin',
				'nom' => 'Dujardin',
				'prenom' => 'Emmanuel',
				'email' => 'manu7772@gmail.com',
				'telephone' => '06 13 14 35 15',
				'role' => 'ROLE_SUPER_ADMIN',
				'enabled' => true,
				),
			array(
				'username' => 'manu7772',
				self::PASSWORD => 'azetyu123',
				'nom' => 'Dujardin',
				'prenom' => 'Emmanuel',
				'email' => 'emmanuel@aequation-webdesign.fr',
				'telephone' => '06 13 14 35 15',
				'role' => 'ROLE_ADMIN',
				'enabled' => true,
				),
			);
		return $users;
	}

}



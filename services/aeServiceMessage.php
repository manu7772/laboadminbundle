<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
// use Doctrine\ORM\Event\PreUpdateEventArgs;
// use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializationContext;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Labo\Bundle\AdminBundle\services\aeData;

use Labo\Bundle\AdminBundle\services\aeServiceBaseEntity;
use Labo\Bundle\AdminBundle\services\aeServiceMessageuser;

use Labo\Bundle\AdminBundle\Entity\LaboUser;
use Labo\Bundle\AdminBundle\Entity\message;
use Labo\Bundle\AdminBundle\Entity\messageuser;

class aeServiceMessage extends aeServiceBaseEntity {

    const NAME                  = 'aeServiceMessage';        // nom du service
    const CALL_NAME             = 'aetools.aeServiceMessage'; // comment appeler le service depuis le controller/container
    const CLASS_ENTITY          = 'Labo\Bundle\Bundle\AdminBundle\Entity\message';

    public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
        parent::__construct($container, $EntityManager);
        // $this->defineEntity(self::CLASS_ENTITY);
        return $this;
    }

    // $users = $this->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getAllCollaborateurs($this->getRequest()->getSession()->get('sitedata')['id']);
    /**
     * Check entity integrity in context
     * @param message $entity
     * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
     * @param $eventArgs = null
     * @return aeServiceMessage
     */
    public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
        parent::checkIntegrity($entity, $context, $eventArgs);
        // if($entity instanceOf message) {
            switch(strtolower($context)) {
                case 'new':
                    break;
                case 'postload':
                    break;
                case 'prepersist':
                    break;
                case 'postpersist':
                    $this->checkMessagesCollaborators(true);
                    // $users = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite')->getAllCollaborateurs($this->container->get('request')->getSession()->get('sitedata')['id']);
                    // foreach ($users as $user) {
                    //     $newMu = $this->getNewEntity('Labo\Bundle\AdminBundle\Entity\messageuser');
                    //     $newMu
                    //         ->setCollaborator($user)
                    //         ->setMessageuser($entity)
                    //         ->setSent(null)
                    //         ->setRead(null)
                    //         ;
                    //     $this->getEm()->persist($newMu);
                    //     $this->getEm()->flush();
                    // }
                    break;
                case 'preupdate':
                    break;
                case 'postupdate':
                    break;
                case 'preremove':
                    break;
                case 'postremove':
                    break;
                default:
                    break;
            }
        // }
        return $this;
    }

    public function getSerialized($requirements = [], LaboUser $user = null, $shortname = null) {
        $this->initSerializeRequirements($requirements, $user, $shortname);
        if($requirements['user'] instanceOf LaboUser && $requirements['user_condition']) {
            $entities = $this->getRepo()->{$requirements['QueryMethod']['method']}($requirements['user'], $requirements['QueryMethod']['shortCutContext']);
            return $this->container->get('jms_serializer')->serialize($entities, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups($requirements['JmsGroups']));
        }
        return json_encode(array());
    }

    /**
     * Get array of unread messages (with … if $all == true)
     * @param boolean $all = false
     * @return array
     */
    public function getInfosMessages(LaboUser $user, $all = false) {
        $messages = $this->getRepo()->findNotRead($user, $all);
        foreach ($messages as $key => $message) {
            $messages[$key] = array(
                'id' => $message->getId(),
                'read' => $message->getRead(),
                'nom' => $message->getNom(),
                'prenom' => $message->getPrenom(),
                'email' => $message->getEmail(),
                'telephone' => $message->getTelephone(),
                'objet' => $message->getObjet(),
                'message' => $message->getMessage(),
                'ip' => $message->getIp(),
                );
        }
        return $messages;
    }

    public function getCountMessagesInBDD() {
        return $this->getEm()->createQuery("SELECT COUNT(element.id) FROM site\AdminsiteBundle\Entity\message element")->getSingleScalarResult();
    }

    public function checkMessagesCollaborator(LaboUser $user, $repar = false) {
        if($user->isCollaborator()) {
            $aeReponse = new aeReponse();
            $messagesUser = $this->getRepo()->findAllOfUser($user);
            $result = array();
            $result['status'] = true;
            $result['nbmessages'] = count($messagesUser);
            $result['repaired'] = false;
            $result['nb_saved'] = 0;
            $result['nb_before'] = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceUser')->getUserNumberOfMessages($user);
            $result['nb_after'] = $result['nb_before'];
            if($result['nbmessages'] > $result['nb_before']) {
                // repar
                $result['status'] = false;
                if($repar) {
                    // repair user messages, here…
                    $result['repaired'] = true;
                    foreach ($messagesUser as $message) if(!$user->hasMessageUserMessage($message)) {
                        $newMu = $this->getNewEntity(aeServiceMessageuser::CLASS_ENTITY);
                        $newMu
                            ->setCollaborator($user)
                            ->setMessageuser($message)
                            ->setSent(null)
                            ->setRead(null)
                            ;
                        if($this->getRepo(aeServiceMessageuser::CLASS_ENTITY)->findByUserAndMessage($user, $message) == null) { 
                            $this->getEm()->persist($newMu);
                            $result['nb_saved']++;
                        }
                    }
                    if($result['nb_saved'] > 0) $this->getEm()->flush();
                    //
                    $result['nb_after'] = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceUser')->getUserNumberOfMessages($user);
                    if($result['nbmessages'] > $result['nb_after']) {
                        $aeReponse->setMessage('User '.$user->getUsername().' had '.$result['nbmessages'] - $result['nb_before'].' missing message(s). And still missing '.$result['nbmessages'] - $result['nb_after'].' message(s).');
                    } else {
                        $result['status'] = true;
                        $aeReponse->setMessage('User '.$user->getUsername().' had '.$result['nbmessages'] - $result['nb_before'].' missing message(s). He si now OK.');
                    }
                } else {
                    // no repair…
                    $aeReponse->setMessage('User '.$user->getUsername().' has '.$result['nbmessages'] - $result['nb_before'].' missing message(s).');
                }
            } else {
                $aeReponse->setMessage('User '.$user->getUsername().' is allready ok.');
            }
            $aeReponse->setResult($result['status']);
            $aeReponse->setData($result);
            return $aeReponse;
        } else {
            $messageusers = $user->getMessageusers();
            $nbmu = $messageusers->count();
            foreach($messageusers as $messageuser) $this->getEm()->remove($messageuser);
            if($nbmu > 0) $this->getEm()->flush();
            return null;
        }
    }

    public function checkMessagesCollaborators($repar = false) {
        $aeReponse = new aeReponse();
        $result = array();
        $result['users'] = array();
        $aeServiceSite = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceSite');
        $collaborators = $aeServiceSite->getAllCollaborateurs();
        $result['nbmessinBDD'] = $this->getCountMessagesInBDD();
        foreach($collaborators as $collaborator) {
            $check = $this->checkMessagesCollaborator($collaborator, $repar);
            $result['users'][$collaborator->getUsername()] = $check->getData();
            $aeReponse->addResult($check->getResult());
        }
        $action = $repar ? 'réparés' : 'listés';
        $message = '<div>Les messages ont été '.$action.'.<br><strong>'.$result['nbmessinBDD'].' messages</strong> en base de données.<br>-------------</div>';
        foreach ($result['users'] as $name => $status) {
            $icon = $status['status'] ? '<i class="fa fa-check"></i>' : '<i class="fa fa-ban"></i>';
            $message .= '<div>'.$name.' => '.$icon.' '.$status['nb_after'].' / '.$status['nbmessages'].' message(s)</div>';
        }
        $aeReponse->setMessage($message);
        $aeReponse->setData($result);
        return $aeReponse;
    }



}
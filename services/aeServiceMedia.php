<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Labo\Bundle\AdminBundle\services\aeServiceNested;
use Labo\Bundle\AdminBundle\services\aeReponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Labo\Bundle\AdminBundle\Entity\media;
use Labo\Bundle\AdminBundle\Entity\baseEntity;

/** call in controller with $this->get('aetools.aeServiceMedia'); */
class aeServiceMedia extends aeServiceNested {

	const NAME                  = 'aeServiceMedia';				// nom du service
	const CALL_NAME             = 'aetools.aeServiceMedia';		// comment appeler le service depuis le controller/container
	const CLASS_ENTITY          = 'Labo\Bundle\AdminBundle\Entity\media';

	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
	    parent::__construct($container, $EntityManager);
	    $this->defineEntity(self::CLASS_ENTITY);
	    return $this;
	}


	/**
	 * Check entity integrity in context
	 * @param media $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
	 * @param $eventArgs = null
	 * @return aeServiceMedia
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		parent::checkIntegrity($entity, $context, $eventArgs);
		// if($entity instanceOf media) {
			// echo('<p>checkIntegrity '.$context.'::'.self::NAME.'/'.json_encode($entity->getNom()).'/'.$this->getEntityShortName($entity).'</p>');
			switch(strtolower($context)) {
				case 'new':
				case 'postload':
					$entity->setPreferedStockage($this->container->getParameter('info_entites')['media']['preferedStockage']);
					break;
				case 'prepersist':
					$entity->setPreferedStockage($this->container->getParameter('info_entites')['media']['preferedStockage']);
					break;
				case 'postpersist':
					break;
				case 'preupdate':
					break;
				case 'postupdate':
					break;
				case 'preremove':
					break;
				case 'postremove':
					break;
				case 'preflush':
					$entity->setPreferedStockage($this->container->getParameter('info_entites')['media']['preferedStockage']);
					break;
				default:
					break;
			}
		// }
		return $this;
	}

	/**
	 * Change image source mode ('file' or 'database')
	 * @param integer $id
	 * @param string $mode
	 * @return aeReponse
	 */
	public function changeImageSource($id, $mode) {
		$aeReponse = new aeReponse();
		$result = false;
		$img = $this->getRepo(self::CLASS_ENTITY)->find($id);
		if(is_object($img)) {
			// image trouvée
			if($img->getStockage() == $mode) return new aeReponse(false, $img, 'image '.$id.' is already in '.$mode.' mode.');
			$stockageList = $img->getStockageList();
			if(!in_array($mode, $stockageList)) return new aeReponse(false, $img, 'image '.$id.' has not '.$mode.' mode in his stockage list.');
			switch ($mode) {
				case 'file':
					$result = true;
					break;
				case 'database':
					$result = true;
					break;
			}
			return $result ? new aeReponse(true, $img, 'image '.$id.' has been successfully changed to '.$mode.' mode.') : new aeReponse(false, $img, 'Error : image '.$id.' has NOT been changed to '.$mode.' mode.');
		}
		return new aeReponse(false, $id, 'Image '.$id.' not found. Can not get it into '.$mode.' mode !');
	}

	/**
	 * Persist en flush a media
     * @dev désactivée
	 * @param baseEntity $entity
	 * @return aeReponse
	 */
	// public function save(baseEntity &$entity, $flush = true) {
	// 	return parent::save($entity, $flush);
	// }

	public function imageB64sized($id, $x = null, $y = null, $mode = 'in', $format = null) {
		$error = false;
		// echo('<h1>2 Image id : '.$id.'</h1>');
		$img = $this->getRepo()->getImgbase64data($id);
		if(count($img) < 1) return null;
		if(isset($img[0]['binaryFile'])) $img = $img[0];
		if(!isset($img['binaryFile'])) return null;
		if($format == null && isset($img['format'])) {
			$exp = explode('/', $img['format']);
			$format = end($exp);
		}
		return 'data:image/'.$format.';base64,'.base64_encode($this->container->get('aetools.aeImagesTools')->thumb_image($img['binaryFile'], $x, $y, $mode, true, $format));
	}

}
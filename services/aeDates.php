<?php
namespace Labo\Bundle\AdminBundle\services;

use \DateTime;
use \Exception;

class aeDates {

	const NAME					= 'aeDates'; 			// nom du service
	const CALL_NAME				= 'aetools.aeDates';		// comment appeler le service depuis le controller/container

	const FORMAT_DATETIME		= 'Y-m-d H:i:s';
	const FORMAT_DATEFILE		= 'Ymd_His';
	const PREG_MOTIF_DATETIME	= '^(\d{4})-(\d{2})-(\d{2})( (\d{2}):(\d{2}):(\d{2})(\.\d{5})?)?$';



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IDENTIFICATION CLASSE
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}




	/**
	 * Renvoie la date SUPÉRIEURE à celle indiquée par $refdate
	 * @param array $dates
	 * @param mixed $refdate = null (si null, considère la date la plus proche par rapport à NOW)
	 * @return mixed
	 */
	public function findDate($dates, $refdate = null) {
		$this->getDateTimeObject($refdate);
		$bestdate = null;

		$now = new DateTime('NOW');
		foreach($dates as $id => $date) if($this->getDateTimeObject($date) instanceOf DateTime) {
			if($refdate === null) {
				if($bestdate === null) $bestdate = $id;
					else if($date >= $this->getDateTimeObject($dates[(string)$bestdate])) $bestdate = $id;
			} else if($refdate instanceOf DateTime) {
				if($bestdate === null) {
					if($date >= $refdate) $bestdate = $id;
				} else {
					if($date > $this->getDateTimeObject($dates[(string)$bestdate])) $bestdate = $id;
				}
			} else {
				throw new Exception('Error aeDates::findDate() : $refdate must be string(date format '.self::FORMAT_DATETIME.' or DateTime object, or null.', 1);
			}
		}
		return isset($dates[(string)$bestdate]) && $bestdate !== null ? $dates[(string)$bestdate] : null;
	}

	public function getDateTimeObject(&$date) {
		if(is_string($date)) {
			if(preg_match("#".self::PREG_MOTIF_DATETIME."#", $date)) {
				$date = new DateTime($date);
			} else {
				$memdate = $date;
				$date = new DateTime('NOW');
				$date->modify($memdate);
			}
		}
		return $date;
	}

}
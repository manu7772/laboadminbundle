<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Labo\Bundle\AdminBundle\services\aeData;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Exception\ParseException;

use \Exception;
use \DateTime;

/**
 * Service aeUrlroutes
 * - fonctionnalités de routes et URL
 * class: Labo\Bundle\AdminBundle\services\aeUrlroutes
 * arguments: [@service_container]
 */
class aeUrlroutes {

    const NAME					= 'aeUrlroutes';			// nom du service
    const CALL_NAME				= 'aetools.aeUrlroutes';	// comment appeler le service depuis le controller/container

	protected $router;
	protected $route;
	protected $aeSystemfiles;
	protected $serviceRequ;

	/**
	 * Constructeur
	 * @return aeUrlroutes
	 */
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->serviceRequ = $this->container->get('request');
		$this->router = $this->container->get('router');
		$this->route = null;
		$this->aeSystemfiles = $this->container->get('aetools.aeSystemfiles');
		// vérification des dossiers params
		// $this->verifParamsFolder();
		return $this;
	}





	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}



	public function verifParamsFolder() {
		$tree = $this->container->getParameter('webfolders')['tree'];
		$this->aeSystemfiles->createTreeFoldersInWeb($tree);
		return $this;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ROUTES & URL
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Renvoie l'url de base
	 * @return string
	 */
	public function getBaseUrl() {
		return $this->serviceRequ->getBaseUrl();
	}

	/**
	 * Renvoie le path (string)
	 * @return string
	 */
	public function getURL() {
		return $this->serviceRequ->getPathInfo();
	}

	/**
	 * Renvoie l'URL entier
	 * @return string
	 */
	public function getURLentier() {
		return $this->serviceRequ->getUri();
	}

	/**
	 * Renvoie un array des routes contenant le préfixe $prefix
	 * @param $prefix
	 * @return array
	 */
	public function getAllRoutes($prefix = null) {
		if(is_string($prefix)) $pattern = '/^'.$prefix.'/'; // commence par $prefix
			else $pattern = '/.*/';
		$this->allRoutes = array();
		foreach($this->router->getRouteCollection()->all() as $nom => $route) {
			if(preg_match($pattern, $nom)) $this->allRoutes[] = $nom;
		}
		return $this->allRoutes;
	}

	/**
	 * Renvoie la route actuelle
	 * @return string
	 */
	public function getRoute() {
		return $this->route;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IP
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Renvoie l'adresse IP utilisateur
	 * @return string
	 */
	public function getIP() {
		return $this->serviceRequ->getClientIp();
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// BUNDLES
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Renvoie la liste des bundles disponibles
	 * array[BundleShortName] = class_name
	 * @param boolean $all = false
	 * @param boolean $inverse = false
	 * @return array
	 */
	public function getBundlesList($all = false, $inverse = false) {
		$bundles = $this->container->getParameter('kernel.bundles');
		if($all == false) $this->selectBundles($bundles);
		return $inverse ? array_flip($bundles) : $bundles;
	}

	/**
	 * Renvoie la liste des paths de bundles disponibles
	 * array[BundleShortName] = path
	 * @param boolean $all = false
	 * @return array
	 */
	public function getBundlesPath($all = false, $forPregTest = false) {
		$bundles = $this->container->getParameter('kernel.bundles');
		if($all == false) $this->selectBundles($bundles);
		// select
		foreach ($bundles as $key => $bundle) {
			$bundles[$key] = preg_replace('#[a-zA-Z0-9-_]+$#', '', $bundle);
			if($forPregTest) $bundles[$key] = preg_replace('#(\\\)#', "\\\\\\", $bundles[$key]);
		}
		return $bundles;
	}


	/**
	 * Enregistre la liste des bundles disponibles dans un fichier YAML
	 * dans : web/'.aeData::PARAMS_FOLDER.'/bundles.yml
	 * @return boolean
	 */
	public function updateBundlesInConfig() {
		$r = false;
		$bundles = $this->container->getParameter('kernel.bundles');
		$this->selectBundles($bundles);
		$pathToConfig = $this->aeSystemfiles->getRootServer().aeData::WEB_PATH.aeData::PARAMS_FOLDER.'/bundles.yml';
		$dumper = new Dumper();
		$r = @file_put_contents($pathToConfig, $dumper->dump($bundles, aeData::MAX_YAML_LEVEL));
		if($r == false) throw new Exception("Mise à jour des bundles dans web/".aeData::PARAMS_FOLDER."/bundles.yml : impossible d'écrire dans le fichier.", 1);
		return $r;
	}

	/**
	 * Sélectionne uniquement les bundles du dossier src/
	 * @param array &$bundles
	 */
	protected function selectBundles(&$bundles) {
		$grps = array();
		$bnds = array();
		$groupes = $this->aeSystemfiles->exploreDir(aeData::SOURCE_FILES, null, 'dossiers', false);
		foreach ($groupes as $groupe) $grps[] = $groupe['nom'];
		$motif = '#^('.implode('|', $grps).')#';
		foreach($bundles as $key => $bundle) if(preg_match($motif, $bundle)) $bnds[$key] = $bundle;
		$bundles = $bnds;
	}

	/**
	 * Renvoie le nom du bundle courant
	 * @return string
	 */
	public function getBundleName() {
		$exp = explode('Controller', $this->getController());
		$bundleName = str_replace('\\', '', reset($exp));
		$bundle = explode(':', $bundleName, 2);
		return count($bundle) > 0 ? reset($bundle) : null;
		return null;
	}

	/**
	 * Affiche la liste des bundles
	 */
	protected function afficheBundles() {
		$this->writeTableConsole('Liste des Bundles présents détectés par Symfony2', $this->getBundlesList());
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// CONTROLLER
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Renvoie le controller complet
	 * @return string
	 */
	public function getController() {
		return $this->serviceRequ->attributes->get('_controller');
	}

	/**
	 * Renvoie le dossier du controller
	 * @return string
	 */
	// public function getCtrlFolder() {
	// 	return $this->ctrlFolder;
	// }

	/**
	 * Renvoie le nom du controller
	 * @return string
	 */
	public function getControllerClassName() {
		$name = preg_replace('#^([A-Za-z0-9\\\\_-]+)(:{1,2})(.+)$#', '${1}', $this->getController());
		return $name;
	}

	/**
	 * Renvoie le nom du controller
	 * @return string
	 */
	public function getControllerName() {
		$name = preg_replace('#^(.+)(\\\)([A-Za-z0-9\_-]+)(:{1,2})(.+)$#', '${3}', $this->getController());
		return $name;
	}

	/**
	 * Renvoie le nom du controller
	 * @return string
	 */
	public function getControllerShortName() {
		$name = preg_replace('#^(.+)(\\\)([A-Za-z0-9\_-]+)(Controller:{1,2})(.+)$#', '${3}', $this->getController());
		return $name;
	}

	/**
	 * Renvoie le nom du groupeName
	 * @return string
	 */
	public function getGroupeName() {
		return $this->groupeName;
	}

	/**
	 * Renvoie le nom de la méthode appelée dans le controller
	 * @return string
	 */
	public function getActionName() {
		$controller = $this->getController();
		$d = explode("::", $controller."");
		if(count($d) < 2)
			$d = explode(":", $controller."");
		return $d[1];
	}

	/**
	 * Renvoie le nom de la méthode, sans "Action" appelée dans le controller
	 * @return string
	 */
	public function getSingleActionName() {
		return preg_replace("#Action$#", "", $this->getActionName());
	}

	/**
	 * Renvoie mode d'environnement (dev, test, prod…)
	 * @return string
	 */
	public function getEnv() {
		return $this->container->get('kernel')->getEnvironment();
	}

	/**
	 * is DEV env ?
	 * @return boolean
	 */
	public function isDev() {
		return $this->getEnv() === 'dev';
	}

	/**
	 * is TEST env ?
	 * @return boolean
	 */
	public function isTest() {
		return $this->getEnv() === 'test';
	}

	/**
	 * is TEST or TEST env ?
	 * @return boolean
	 */
	public function isDevTest() {
		return $this->isDev() || $this->isTest();
	}

	public function isGranted($role) {
		return $this->container->get('security.authorization_checker')->isGranted($role);
	}

	/**
	 * Mode d'environnement ROLE_SUPER_ADMIN ou DEV/TEST (dev, test, prod…)
	 * @return boolean
	 */
	public function isDevOrSadmin() {
		return $this->isGranted('ROLE_SUPER_ADMIN') || $this->isDevTest();
	}

}
<?php
namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\DBAL\Types\Type;
use Doctrine\Common\Collections\ArrayCollection;
// use Labo\Bundle\AdminBundle\services\aeData;
// use Labo\Bundle\AdminBundle\services\aeSnake;
// yaml parser
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Exception\ParseException;
// aetools
use Labo\Bundle\AdminBundle\Entity\nested;

use \Exception;
use \DateTime;

/**
 * Service aeFixtures
 * - Gestion des fixtures
 */
class aeFixtures extends aeServiceBaseEntity {

	const NAME                  	= 'aeFixtures';        // nom du service
	const CALL_NAME             	= 'aetools.aeFixtures'; // comment appeler le service depuis le controller/container

	protected $languages; 			// array des langues disponibles --> config.yml --> default_locales: "fr|en|en_US|es|it|de"
	protected $bundlesLanguages;	// array des langues disponibles par bundles --> config.yml --> list_locales: "fr|en", etc.
	protected $default_locale; 		// string locale par défaut --> config.yml --> locale: "en"
	protected $fold_ORM;			// liste des dossiers contenant les fichiers de traduction
	protected $bundles_list;		// array des bundles/path : $array(bundle => path)
	protected $files_list;			// array des fichiers, classés par bundles
	protected $rootPath;			// Dossier root du site

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
		// $this->container = $container;
		$this->aeSystemfiles = $this->container->get('aetools.aeSystemfiles');
		$this->aeSystemfiles->setRootPath("/");
		// récupération de fichiers et check
		$this->initFiles();
		// $this->verifData();
	}

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}



	protected function initFiles() {
		// initialisation
		$this->bundles_list = array();
		$this->files_list = array();
		// récupération des dossiers "translations", enfants DIRECTS des dossiers "Resources", uniquement dans "src"
		$fold_datafixtures = $this->aeSystemfiles->exploreDir(aeData::SOURCE_FILES, aeData::FOLD_DATAFIXTURES, "dossiers");
		$this->fold_ORM = array();
		foreach($fold_datafixtures as $fR) {
			$res = $this->aeSystemfiles->exploreDir($fR['sitepath'].$fR['nom'], aeData::FOLD_ORM, "dossiers", false); // false --> enfants directs
			if(count($res) > 0) foreach($res as $folder) {
				$this->fold_ORM[] = $folder;
			}
		}
		foreach($this->fold_ORM as $folder) {
			$path = $folder['sitepath'].$folder['nom'];
			// constitution de la liste des bundles
			$bundle = $this->getBundle($path);
			$this->bundles_list[$bundle] = $path;
			// recherche des fichiers
			$listOfFiles = $this->getFixturesFiles($path);
			// liste des domaines
			$this->fixturesFiles[$bundle] = array();
			foreach($listOfFiles as $key => $file) {
				$entityName = $this->fileGetEntity($file['nom']);
				if(!in_array($entityName, $this->fixturesFiles[$bundle]))
					$this->fixturesFiles[$bundle][] = $entityName;
				// ajout de données
				$listOfFiles[$key]['statut_message'] = 'fixtures.file_found';
				$listOfFiles[$key]['statut'] = 1;
			}
			// initialisation des fichiers du domaine
			foreach($this->fixturesFiles[$bundle] as $entityName) {
				foreach($this->getListOfEnties(true) as $class => $short) {
					// if($entityName == $short && preg_match('#^'.$bundle.'#', str_replace('\\', '', $class))) {						
					if($entityName == $short) {						
						$this->files_list[$class]['name'] = $short;
						$this->files_list[$class]['classname'] = $class;
						$this->files_list[$class]['bundle'] = $bundle;
						$this->files_list[$class]['fullpath'] = $this->aeSystemfiles->getRootPath().$path."/".$entityName."s.yml";
						// $this->files_list[$entityName][$bundle]['data'] = $this->parse_yaml_fromFile($this->aeSystemfiles->getRootPath().$path."/".$entityName."s.yml");
					}
				}
			}
		}
		// echo('<pre>');
		// var_dump($this->files_list);
		// die('</pre>');
		return $this->files_list;
	}

	/**
	 * Renvoie le domaine du fichier
	 * @param string $filename
	 * @return string
	 */
	protected function fileGetEntity($filename) {
		return preg_replace("#s\.yml$#", "", $filename);
	}

	/**
	 * Liste des bundles
	 * @return array
	 */
	public function getBundles() {
		return array_keys($this->bundles_list);
	}

	/**
	 * Renvoie le nom du bundle d'après le path
	 * @param string $path
	 * @return array
	 */
	public function getBundle($path) {
		return strtolower(str_replace(array(aeData::FOLD_DATAFIXTURES, aeData::FOLD_ORM, aeData::SOURCE_FILES, aeData::BUNDLE_EXTENSION, aeData::SLASH), '', $path));
	}

	/**
	 * Renvoie la liste des fichiers de translation (yaml) contenus dans le dossier $path
	 * @param string $path
	 * @return array
	 */
	protected function getFixturesFiles($path) {
		return $this->aeSystemfiles->exploreDir($path, "s\.yml$", "fichiers", false, true);
	}


	public function getInfoFiles() {
		$data = array();
		foreach($this->files_list as $className => $info) {
			$temp = $this->parse_yaml_fromFile($info['fullpath']);
			$ord = $temp[$info['name']]['order'];
			$data[$ord]['ord'] = $ord;
			$data[$ord]['nb'] = count($temp[$info['name']]['data']);
			$data[$ord]['name'] = $info['name'];
			$data[$ord]['classname'] = $className;
		}
		ksort($data);
		// echo('<pre>');
		// var_dump($data);
		// die('</pre>');
		return $data;
	}

	/**
	 * 
	 * 
	 * 
	 */
	public function fillDataWithFixtures($entity) {
		// echo('<h3>Fill Entity : '.$entity.'</h3>');
		$className = $this->getEntityClassName($entity);
		$this->getCurrentEntityIfEmpty($className);
		if(array_key_exists($className, $this->files_list)) {
			if(count($this->files_list[$className] > 0)) {
				// echo('<pre>');var_dump($this->files_list[$className]);die('</pre>');
				$data = $this->parse_yaml_fromFile($this->files_list[$className]['fullpath']);
				return $this->fillEntity($data[$entity]['data'], $entity);
			}
		}
		return false;
	}

	/**
	 * Lecture d'un fichier d'après son path
	 * @param string $path
	 * @return array
	 */
	protected function parse_yaml_fromFile($path) {
		if(!file_exists($path)) return false;
		$yaml = new Parser();
		try {
			$parse = $yaml->parse(file_get_contents($path));
		} catch (ParseException $e) {
			$parse = $e->getMessage();
		}
		return $parse;
	}


	/**
	 * Vide l'entité $entity
	 * @param string $entity
	 * @return integer
	 */
	public function emptyEntity($entity) {
		$className = $this->getEntityClassName($entity);
		$this->getCurrentEntityIfEmpty($className);
		if($className) {
			$em = $this->container->get('doctrine')->getManager();
			$number = 0;
			switch ($entity) {
				case 'User':
					$number = $this->container->get('aetools.aeServiceUser')->deleteAllUsers();
					break;
				case 'fileFormat':
					$number = $this->container->get('aetools.media')->eraseAllFormats();
					break;
				default:
					$entities = $em->getRepository($className)->findAll();
					foreach($entities as $ent) {
						$em->remove($ent);
						$number++;
					}
					$em->flush();
					break;
			}
			// remise à zéro de l'index de la table
			$em->getConnection()->executeUpdate("ALTER TABLE ".$entity." AUTO_INCREMENT = 1;");
			return $number;
		} else {
			throw new Exception("Entity ".$entity." does not exist or is not instantiable !", 1);
		}
	}

	/**
	 * Hydrate l'entité $entite avec $data
	 * @param array $data
	 * @param string $entity
	 * @return array
	 */
	protected function fillEntity($data, $entity, $empty = true) {
		$className = $this->getEntityClassName($entity);
		$this->getCurrentEntityIfEmpty($className);
		if($className) {
			if($empty === true) $this->emptyEntity($entity);
			$em = $this->container->get('doctrine')->getManager();
			$newdata = array();
			// echo('<pre>');
			$service = $this->getEntityService($className);
			foreach($data as $key => $dat) {
				$newdata[$key] = $service->getNewEntity($className);
				// echo('<h3 style="color:orange;">Entity '.$className.' / Service : '.$service->getNom().' <i>(call : '.$service->callName().')</i></h3>');
				foreach($dat as $attribute => $value) {
					$m = $this->getMethodOfSetting($attribute, $newdata[$key], false);
					// $assoc = $this->hasAssociation($attribute, $newdata[$key]) ? '<i>(assoc.)</i>' : '<i>(field)</i>';
					if(is_string($m)) {
						// if(is_array($value))
							// echo('<p style="margin:0px;">Method for <span style="color:blue;"><i>'.$attribute.'</i></span> : <span style="color:blue;"><strong>'.$m.'</strong></span> = '.json_encode($value).'</p>');
							// else if(is_string($value))
								// echo('<p style="margin:0px;">Method for <span style="color:blue;"><i>'.$attribute.'</i></span> : <span style="color:blue;"><strong>'.$m.'</strong></span> = '.json_encode(htmlspecialchars($value)).'</p>');
						$isNested = method_exists($newdata[$key], 'hasNestedAttribute') ? $newdata[$key]->hasNestedAttribute($attribute) : false;
						if(!$this->hasAssociation($attribute, $newdata[$key]) && !$isNested) {
							// echo('<p style="margin:0px;color:green;">Attribute : '.$attribute.'</p>');
							// champ simple
							switch ($this->getTypeOfField($attribute, $newdata[$key])) {
								case Type::BOOLEAN:
									$newdata[$key]->$m((boolean)$value);
									break;
								case Type::DATETIME:
								case Type::DATETIMETZ:
								case Type::DATE:
								case Type::TIME:
									$datetime = new DateTime($value);
									$newdata[$key]->$m($datetime->format(aeData::FORMAT_DATETIME_SQL));
									break;
								default:
									$newdata[$key]->$m($value);
									break;
							}
						} else {
							// entité liée
							if($isNested) {
								// echo('<p style="margin:0px;color:green;">Nested : '.$attribute.'</p>');
								// nested
								$otherSideEntities = $newdata[$key]->getSnake()->getNestedAttributesClasses($attribute);
								if(!$otherSideEntities) throw new Exception('Error : '.$attribute.' is not a valid attribute.', 1);
								foreach($otherSideEntities as $keyEntity => $entity) {
									$otherSideEntities[$keyEntity] = $this->getEntityClassName($entity);
								}
								// echo('<h3>- List of entities :</h3>');
								// var_dump($this->getListOfEnties());
								// echo('<h3>- OtherSideEntities :</h3>');
								// var_dump($otherSideEntities);
							} else {
								// echo('<p style="margin:0px;color:green;">Association : '.$attribute.'</p>');
								$otherSideEntities = array($this->getTargetEntity($attribute, $newdata[$key]));
							}
							foreach($otherSideEntities as $otherSideEntity) {
								// echo('<p style="margin:0px;">- Target class = '.$otherSideEntity.'</p>');
								$repo = $em->getRepository($otherSideEntity);
								$ml = 'findBy'.ucfirst($value['field']);
								$linkeds = new ArrayCollection();
								foreach((array)$value['value'] as $value2) {
									$founds = $repo->$ml($value2);
									if(is_object($founds)) {
										// echo('<p style="margin:0px;">- found 1 and add : '.$founds.'</p>');
										$linkeds->add($founds);
									}
									if(is_array($founds)) foreach($founds as $found) {
										// echo('<p style="margin:0px;">- found in array, and add : '.$found.'</p>');
										$linkeds->add($found);
									}
								}
								if($linkeds->count() > 0) {
									if(preg_match('#^set#', $m)) {
										// echo('<p style="margin:0px;color:#999;"><i>- setting as '.json_encode($this->getTypeOfAssociation($attribute, $newdata[$key])));
										if($this->getTypeOfAssociation($attribute, $newdata[$key]) === self::SINGLE_NAME) {
											// single
											// echo(' : single ('.$m.')</i></p>');
											$newdata[$key]->$m($linkeds->first());
											break 1;
										} else {
											// collection
											// echo(' : collection ('.$m.')</i></p>');
											$newdata[$key]->$m($linkeds, 'test');
										}
									} else if(preg_match('#^add#', $m)) {
										// echo('<p style="margin:0px;color:#999;"><i>- setting as '.json_encode($this->getTypeOfAssociation($attribute, $newdata[$key])).'</i></p>');
										foreach($linkeds as $linked) {
											$newdata[$key]->$m($linked);							
										}
									}
								}
							}
						}
					} else {
						// echo('<p style="margin:0px;color:red;">Method for '.$attribute.' does not exist !');
						// echo('<p style="margin:0px;">Available methods :</p>');
						// echo('<ul style="margin:0px;">');
						// foreach($newdata[$key]->getNestedAttributesParameters() as $attribute => $values) {
							// echo('<li>'.json_encode($attribute).'</li>');
						// }
						// echo('</ul>');
						// echo('</p>');
					}
				}
				$result = $service->save($newdata[$key]);
				// echo('<p style="margin:0px 0px 0px 40px;border-left:3px solid #999;padding-left:12px;">Save result for "'.get_class($newdata[$key]).'" <strong>'.$newdata[$key]->getNom().'/Id:'.json_encode($newdata[$key]->getId()).'</strong> :<br>');
				// echo('<span title="'.json_encode($result->getData()).'"><strong>'.json_encode($result->getResult()).'</strong> : '.$result->getMessage().'</span>');
				// echo('</p>');
			}
			// echo('<p style="margin:0px;color:red;">END</p>');
			// echo('</pre>');
			return $newdata;
		} else {
			throw new Exception("Entity ".$entity." does not exist or is not instantiable !", 1);
		}
	}


}


<?php

namespace Labo\Bundle\AdminBundle\services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use \Twig_Extension;
use \Twig_SimpleFilter;
use \Twig_SimpleFunction;

use site\adminsiteBundle\Entity\article;
use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

class twigToolsArticlePanier extends Twig_Extension {

    const NAME                  = 'twigToolsArticlePanier';			// nom du service
    const CALL_NAME             = 'aetools.twigToolsArticlePanier';	// comment appeler le service depuis le controller/container


	private $container;
	private $trans;
	private $session;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->trans = $this->container->get('translator');
		$this->session = $this->container->get('request')->getSession();
	}

	public function getFunctions() {
		return array(
			// article
			new Twig_SimpleFunction('articlePrix', array($this, 'articlePrix')),
			new Twig_SimpleFunction('articlePrixHT', array($this, 'articlePrixHT')),
			new Twig_SimpleFunction('shortUnit', array($this, 'shortUnit')),
			new Twig_SimpleFunction('siteFilter', array($this, 'siteFilter')),
			// panier
			// new Twig_SimpleFunction('dataForPanier', array($this, 'dataForPanier')),
			// new Twig_SimpleFunction('getInfosPanier', array($this, 'getInfosPanier')),
			new Twig_SimpleFunction('getSerialized', array($this, 'getSerialized')),
			);
	}

	public function getFilters() {
		return array(
			// filters
			new Twig_SimpleFilter('price', array($this, 'price')),
			new Twig_SimpleFilter('articlePrice', array($this, 'articlePrice')),
			);
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// IDENTIFICATION CLASSE
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function __toString() {
		try {
			$string = $this->getNom();
		} catch (Exception $e) {
			$string = '…';
		}
		return $string;
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}







	public function articlePrix($article) {
		// object
		if($article instanceOf article) {
			if($article->getPrix() == null) return $this->trans->trans('price.noprice', [], 'article');
			if($article->getPrix() <= 0) return $this->trans->trans('price.noprice', [], 'article');
			return $article->getUnitprix() != null ? number_format($article->getPrix(), 2, ',', '').'<small><sup>€</sup><span>/'.$article->getUnitprix().'</span></small>' : number_format($article->getPrix(), 2, ',', '').'<small><sup>€</sup></small>';
		}
		// array
		if(!isset($article['prix'])) return $this->trans->trans('price.noprice', [], 'article');
		if(floatval($article['prix']) <= 0) return $this->trans->trans('price.noprice', [], 'article');
		return isset($article['unitprix']) ? number_format($article['prix'], 2, ',', '').'<small><sup>€</sup><span>/'.$article['unitprix'].'</span></small>' : number_format($article['prix'], 2, ',', '').'<small><sup>€</sup></small>';
	}

	public function articlePrixHT($article) {
		// object
		if($article instanceOf article) {
			if($article->getPrixHT() == null) return $this->trans->trans('price.noprice', [], 'article');
			if($article->getPrixHT() <= 0) return $this->trans->trans('price.noprice', [], 'article');
			return $article->getUnitprix() != null ? number_format($article->getPrixHT(), 2, ',', '').'<small><sup>€</sup><span>/'.$article->getUnitprix().'</span></small>' : number_format($article->getPrixHT(), 2, ',', '').'<small><sup>€</sup></small>';
		}
		// array
		if(!isset($article['prixHT'])) return $this->trans->trans('price.noprice', [], 'article');
		if(floatval($article['prixHT']) <= 0) return $this->trans->trans('price.noprice', [], 'article');
		return isset($article['unitprix']) ? number_format($article['prixHT'], 2, ',', '').'<small><sup>€</sup><span>/'.$article['unitprix'].'</span></small>' : number_format($article['prixHT'], 2, ',', '').'<small><sup>€</sup></small>';
	}

	public function shortUnit($unit) {
		return strlen($unit) > 4 ? $unit[0].'.' : $unit;
	}

	// optionArticlePhotosOnly
	// optionArticlePriceOnly
	public function siteFilter($article) {
		$articleTest = array();
		if($article instanceOf article) {
			// object
			$articleTest['class_name'] = $article->getShortName();
			$articleTest['imageId'] = $article->getImage();
			$articleTest['prix'] = $article->getPrix();
		} else if(is_array($article)) {
			// array
			$articleTest['class_name'] = isset($article['class_name']) ? $article['class_name'] : null;
			$articleTest['imageId'] = isset($article['imageId']) ? $article['imageId'] : null;
			$articleTest['prix'] = isset($article['prix']) ? $article['prix'] : null;
		} else {
			return false;
		}
		$test = '#^(article)#'; // '#^(article|autres…)#'
		if(preg_match($test, $articleTest['class_name'])) {
			$options = $this->session->get(aeData::SITE_DATA);
			// echo('<p style="margin:0px;color:blue;">'.$articleTest['class_name'].'</p>');
			foreach($options as $option => $value) {
				// if(is_bool($value)) echo('<p style="margin:0px;">'.$option.' : '.json_encode($value).'</p>');
				switch($option) {
					case 'optionArticlePhotosOnly':
						if($value === true && $articleTest['imageId'] == null) return false;
						break;
					case 'optionArticlePriceOnly':
						if($value === true && floatval($articleTest['prix']) <= 0) return false;
						break;
				}
			}
		}
		return true;
	}

	// public function dataForPanier($item) {
	// 	if(is_object($item)) {
	// 		return json_encode(
	// 			array(
	// 				// 'nom' => $item->getNom(),
	// 				'id' => $item->getId(),
	// 				// 'prix' => $item->getPrix(),
	// 				// 'prixHT' => $item->getPrixHT(),
	// 				// 'unitprix' => $item->getUnitprix(),
	// 				'unit' => $item->getUnit(),
	// 				'defaultquantity' => $item->getDefaultquantity(),
	// 				'maxquantity' => $item->getMaxquantity(),
	// 				'minquantity' => $item->getMinquantity(),
	// 				'increment' => $item->getIncrement(),
	// 				)
	// 			);
	// 	} else if(is_array($item)) {
	// 		if(!isset($item['defaultquantity'])) $item['defaultquantity'] = null;
	// 		if(!isset($item['maxquantity'])) $item['maxquantity'] = null;
	// 		if(!isset($item['minquantity'])) $item['minquantity'] = 1;
	// 		if(!isset($item['increment'])) $item['increment'] = 1;
	// 		return json_encode(
	// 			array(
	// 				// 'nom' => $item['nom'],
	// 				'id' => $item['id'],
	// 				// 'prix' => $item['prix'],
	// 				// 'prixHT' => $item['prixHT'],
	// 				// 'unitprix' => $item['unitprix'],
	// 				'unit' => $item['unit'],
	// 				'defaultquantity' => $item['defaultquantity'],
	// 				'maxquantity' => $item['maxquantity'],
	// 				'minquantity' => $item['minquantity'],
	// 				'increment' => $item['increment'],
	// 				)
	// 			);
	// 	}
	// 	return json_encode([]);
	// }

	/**
	 * getInfosPanier
	 * Renvoie le nombre d'articles dans un array :
	 * -> "bytype" = nombre d'articles différents
	 * -> "quantite" = nombre total d'articles
	 * @param LaboUser $user
	 * @param boolean $groups = []
	 * @return array
	 */
	// public function getInfosPanier(LaboUser $user = null, $complete = false) {
	// 	return $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServicePanier')->getInfosPanier($user, $complete);
	// }

    public function getSerialized($requirements = [], LaboUser $user = null, $shortname = null) {
    	if(!isset($requirements['shortname'])) $requirements['shortname'] = $shortname;
		return $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeService'.ucfirst($requirements['shortname']))->getSerialized($requirements, $user, $requirements['shortname']);
	}


	////////////////////////////// FILTERS /////////////////

	public function price($prix, $unit = null, $devise = null, $locale = null) {
		if($locale === null) $locale = $this->container->get('request')->getLocale();
		$devise_info = $this->container->getParameter('marketplace');
		$devise_info = isset($devise_info['devises'][$locale]) ? $devise_info['devises'][$locale] : reset($devise_info['devises']);
		$unit = $unit != null ? $unit = '<span>/'.$unit.'</span>' : '';
		$prix = isset($devise_info['ratio']) ? $prix * $devise_info['ratio'] : $prix;
		switch ($locale) {
			case 'en':
				$price = $devise_info['symb'].number_format($prix, 2, '.', ',').'<small>'.$unit.'</small>';
				break;
			default:
				$price = number_format($prix, 2, ',', '').'<small><sup>'.$devise_info['symb'].'</sup>'.$unit.'</small>';
				break;
		}
		return $price;
	}

	public function articlePrice($article, $ht = false) {
		$prix = $ht === true ? $article->getPrixHT() : $article->getPrix();
		return $this->price($prix, $article->getUnitprix());
	}


}









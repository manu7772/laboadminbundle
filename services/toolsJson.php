<?php
namespace Labo\Bundle\AdminBundle\services;

/**
 * Service JSON
 */
class toolsJson {

	/**
	 * compile les données $params sérializées
	 * @param string $params
	 * @return array
	 */
	public function JSonExtract($params) {
		if(is_array($params)) return $params;
		$returnPdata = array();
		$pd = json_decode($params, true);
		if($pd !== null) $returnPdata = $pd;
		$params = $returnPdata;
		return $params;
	}


}
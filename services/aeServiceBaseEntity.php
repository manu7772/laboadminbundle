<?php
namespace Labo\Bundle\AdminBundle\services;

use Labo\Bundle\AdminBundle\services\aeData;
use Labo\Bundle\AdminBundle\services\aeSnake;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializationContext;
// aeReponse
use Labo\Bundle\AdminBundle\services\aeReponse;

// informations classes
use Labo\Bundle\AdminBundle\services\flashMessage;

use Labo\Bundle\AdminBundle\Entity\baseEntity;
use Labo\Bundle\AdminBundle\Entity\statut;
use Labo\Bundle\AdminBundle\Entity\LaboUser;

use \ReflectionClass;
use \Exception;

/**
 * @dev classe temporairement en travaux
 */
class aeServiceBaseEntity {

	const NAME					= 'aeServiceBaseEntity';			// nom du service
	const CALL_NAME				= 'aetools.aeServiceBaseEntity';	// comment appeler le service depuis le controller/container

	const REPO_DEFAULT_VAL		= "defaultVal";				// méthode repository pour récupération des entités par défaut

	const ASSOCIATION_NAME		= "association";
	const COLLECTION_NAME		= "collection";
	const SINGLE_NAME			= "single";

	const STATUT_CLASS			= 'Labo\Bundle\AdminBundle\Entity\statut';

	// ENTITÉS / ENTITÉ COURANTE
	protected $current = null;					// className (nom long) de l'entité courante
	protected $entities = null;					// all entities

	protected $_em = false;						// entity_manager
	protected $repo;							// repository
	protected $aeClasses;						// service classes
	protected $aeArrays;						// service arrays
	protected $aeDebug;							// service debug
	protected $aeCache;							// service cache
	protected $aeUrlroutes;						// service aeUrlroutes
	protected $detailedListOfEntities;			// detailed list of entities
	protected $listOfEnties;					// list of entities
	protected $_locale;							// user locale
	// protected $translator;					// service translator

	/**
	 * Constructeur
	 * @param ContainerInterface $container
	 * @return aeServiceBaseEntity
	 */
	public function __construct(ContainerInterface $container, EntityManager $EntityManager = null) {
		$this->container = $container;
		// locale
		$this->_locale = $this->container->get('request')->getLocale();
		// services
		$this->aeClasses = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeClasses');
		$this->aeArrays = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeArrays');
		$this->aeDebug = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeDebug');
		$this->aeCache = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeCache');
		$this->aeUrlroutes = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeUrlroutes');
		// $this->translator = $this->container->get('translator');
		// EntityManager
		if($EntityManager instanceOf EntityManager) $this->setEm($EntityManager);
			else $this->setEm($this->container->get('doctrine')->getManager());
		// $this->aeDebug->debugNamedFile(aeData::LOG_NAME, array(
		// 	'EntityManager' => get_class($this->getEm()),
		// 	'called_class' => get_called_class(),
		// 	'this_class' => $this->getNom(),
		// 	'parent_classes' => $this->aeClasses->getParentClassNames($this),
		// 	// 'getAllEntitiesInfo' => $this->getAllEntitiesInfo(),
		// 	)
		// );
		// others…
		$this->current = null;
		$this->entities = null;
		$this->detailedListOfEntities = null;
		$this->repo = array();
		// $this->getAllEntitiesInfo();
		return $this;
	}

	public function __toString() {
		return $this->getNom();
	}

	public function getNom() {
		return self::NAME;
	}

	public function callName() {
		return self::CALL_NAME;
	}

	/**
	 * Renvoie le nom de la classe
	 * @return string
	 */
	public function getName() {
		return get_called_class();
	}



	public function getEntityMetadataInfo($className) {
		return $this->getEm()->getClassMetadata($className);
	}

	public function getCurrentEntityIfEmpty(&$className = null) {
		if($className === null) $className = $this->current;
		if(is_object($className)) $className = $this->cleanClassIfProxy(get_class($className));
		if(!($className2 = $this->hasEntity($className))) $this->errorEntityDoesNotExist($className2);
		$className = $className2;
	}

	/**
	 * Renvoie le nom du service approprié de l'entité
	 * Renvoie les services/entités parents dans l'ordre, puis aeServiceBaseEntity en dernier recours
	 * @param mixed $className = null
	 * @return string / null
	 */
	public function getEntityServiceName($className = null) {
		if(is_object($className)) $className = $this->cleanClassIfProxy(get_class($className));
		$this->getCurrentEntityIfEmpty($className);
		foreach($this->aeClasses->getParentClassShortNames($className, true) as $parentClassName) {
			$aeServiceName = aeData::PREFIX_CALL_SERVICE.'aeService'.ucfirst($parentClassName);
			// echo('<p>Search service : '.$aeServiceName.'</p>');
			if($this->container->has($aeServiceName)) {
				// echo('<p><strong>Found : '.$aeServiceName.' !!</strong></p>');
				return $aeServiceName;
			}
		}
		return aeData::PREFIX_CALL_SERVICE.$this->aeClasses->getShortName($this->getName());
	}

	/**
	 * Renvoie le service de l'entité
	 * Recherche les services/entités parents dans l'ordre, puis aeServiceBaseEntity en dernier recours
	 * @param mixed $className = null
	 * @return service
	 */
	public function getEntityService($className = null) {
		if(is_object($className)) $className = $this->cleanClassIfProxy(get_class($className));
		$aeServiceName = $this->getEntityServiceName($className);
		return $this->container->get($aeServiceName);
	}

	public function cleanClassIfProxy($className) {
		return preg_replace(aeData::PROXY_PREFIX, '', $className);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// JMS / Serializer
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected function initSerializeRequirements(&$requirements = [], LaboUser $user = null, $shortname = null) {
		$serializer = $this->container->getParameter('serializer');
        $requirements['JmsGroups'] = isset($requirements['JmsGroups']) ? $requirements['JmsGroups'] : $serializer['default']['JmsGroups'];
        $requirements['QueryMethod']['method'] = isset($requirements['QueryMethod']['method']) ? $requirements['QueryMethod']['method'] : $serializer['default']['QueryMethod']['method'];
        $requirements['QueryMethod']['shortCutContext'] = isset($requirements['QueryMethod']['shortCutContext']) ? $requirements['QueryMethod']['shortCutContext'] : $serializer['default']['QueryMethod']['shortCutContext'];
		if($shortname != null && !isset($requirements['shortname'])) $requirements['shortname'] = $shortname;
		$requirements['classname'] = $requirements['shortname'];
		$this->getCurrentEntityIfEmpty($requirements['classname']);
		if(isset($requirements['user'])) {
			if(!($requirements['user'] instanceOf LaboUser)) {
				if((integer)$requirements['user'] > 0) $requirements['user'] = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $requirements['user']));
			}
		} else if(!($user instanceOf LaboUser)) {
			if($user == null) $requirements['user'] = $this->container->get('security.token_storage')->getToken()->getUser();
				else if((integer)$user > 0) $requirements['user'] = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $user));
		} else if($user instanceOf LaboUser) {
			$requirements['user'] = $user;
		}
		// user_condition
		if(!in_array($requirements['user_condition'], ['all'])) {
			if($requirements['user'] instanceOf LaboUser) {
				$method = preg_replace('#\(\)$#', '', $requirements['user_condition']);
				aeData::camelize($method);
				if(method_exists($requirements['user'], $method)) {
					$requirements['user_condition'] = $requirements['user']->$method();
				} else {
					// try getter…
					$method = 'get'.ucfirst($method);
					if(method_exists($requirements['user'], $method)) {
						$requirements['user_condition'] = $requirements['user']->$method();
					} else
						$requirements['user_condition'] = false;
						// throw new Exception('User condition as User method '.json_encode($method).' for getSerialized does not exist.', 1);
				}
			} else {
				// no user
				$requirements['user_condition'] = false;
			}
		} else {
			// all
			$requirements['user_condition'] = true;
		}
		// $data = $this->container->get('jms_serializer')->serialize($requirements, 'json');
		// echo('<pre>');var_dump(json_decode($data));die('</pre>');
	}

    public function getSerialized($requirements = [], LaboUser $user = null, $shortname = null) {
    	$this->initSerializeRequirements($requirements, $user, $shortname);
        if($requirements['user'] instanceOf LaboUser && $requirements['user_condition']) {
        	$repo = $this->getRepo($requirements['classname']);
        	if(!method_exists($repo, $requirements['QueryMethod']['method'])) $requirements['QueryMethod']['method'] = 'findAll';
            $entities = $repo->{$requirements['QueryMethod']['method']}($requirements['user'], $requirements['QueryMethod']['shortCutContext']);
            return $this->container->get('jms_serializer')->serialize($entities, 'json', SerializationContext::create()->enableMaxDepthChecks()->setGroups($requirements['JmsGroups']));
        }
        return json_encode(array());
    }


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// CLASSES D'ENTITES
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get all entities informations in array
	 * @return array
	 */
	public function getAllEntitiesInfo() {
		if($this->entities === null) {
			$this->entities = array();
			$entitiesClassNames = $this->getEm()->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
			foreach($entitiesClassNames as $className) {
				$CMD = $this->getEntityMetadataInfo($className);
				$RC = $CMD->getReflectionClass();
				$className = $RC->getName();
				$this->entities[$className] = array();
				$this->entities[$className]['shortName']			= $RC->getShortName();
				$this->entities[$className]['className']			= $className;
				$this->entities[$className]['namespaceName']		= $RC->getNamespaceName();
				// $this->entities[$className]['classMetadata']		= $CMD;
				// associations
				// $associationNames = $CMD->getAssociationNames();
				// $this->entities[$className][self::ASSOCIATION_NAME] = array();
				// foreach($associationNames as $associationName) {
				// 	// find target
				// 	$target = $CMD->getAssociationTargetClass($associationName);
				// 	$this->entities[$className][self::ASSOCIATION_NAME][$associationName]['target'] = $target;
				// 	// find type
				// 	$type = $CMD->getTypeOfAssociation($associationName);
				// 	$this->entities[$className][self::ASSOCIATION_NAME][$associationName]['type'] = $type;
				// }
			}
		}
		return $this->entities;
	}

	public function getAllEntitiesNames() {
		$entities = $this->getAllEntitiesInfo();
		return array_keys($entities);
	}

	public function getAllEntitiesShortNames() {
		$entities = array();
		foreach ($this->getAllEntitiesInfo() as $name => $info) $entities[$name] = $info['shortName'];
		// echo('<pre>');var_dump($entities);echo('</pre>');
		return $entities;
	}

	public function getEntityInfo($className) {
		$this->getCurrentEntityIfEmpty($className);
		return $this->getAllEntitiesInfo()[$className];
	}

	public function hasEntity($className) {
		$result = false;
		// echo('<p>hasEntity '.json_encode($className).' ?');
		if(isset($this->getAllEntitiesInfo()[$className])) $result = $className;
		foreach ($this->getAllEntitiesShortNames() as $className2 => $shortName) {
			if($shortName == $className) {
				$result = $className2;
				break 1;
			}
		}
		// echo(' => <strong>'.json_encode($result).'</strong></p>');
		return $result;
	}

	/**
	 * Get the instantiable className of $className, or false
	 * @param string $className
	 * @return string
	 */
	public function getInstantiable($className) {
		$result = false;
		// echo('<p>getInstantiable '.json_encode($shortName).' ?');
		if(isset($this->getAllEntitiesInfo()[$className])) if($this->isInstantiable($className)) $result = $className;
		foreach ($this->getAllEntitiesShortNames() as $className2 => $shortName) {
			if($shortName == $className && $this->isInstantiable($className2)) {
				$result = $className2;
				break 1;
			}
		}
		// echo(' => <strong>'.json_encode($result).'</strong></p>');
		return $result;
	}

	public function isInstantiable($className) {
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->getReflectionClass()->isInstantiable();
	}

	public function getAssociationTargetClass($className, $fieldName) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->getAssociationTargetClass($fieldName);
	}



	/**
	 * Put entity as default entity in service
	 * @param string $className
	 * @return aeServiceBaseEntity
	 */
	public function defineEntity($className) {
		$this->getCurrentEntityIfEmpty($className);
		$this->current = $className;
		// $this->aeDebug->debugNamedFile(aeData::LOG_NAME, array('current_entity' => json_encode($this->current)));
		return $this;
	}

	/**
	* get current entity className
	* @return string
	*/
	public function getCurrent() {
		return $this->current;
	}

	/**
	 * Get new entity controlled by labo (filled with default values and relations)
	 * @param string $className
	 * @return entity of $className
	 */
	public function getNewEntity($className) {
		$className = $this->getInstantiable($className);
		if($className) {
			$newEntity = new $className();
			$this->checkIntegrity($newEntity, 'new');
			return $newEntity;
		}
		$this->errorEntityIsNotInstantiable($className);
	}

	/**
	 * Check entity integrity in context
	 * @param baseEntity $entity
	 * @param string $context ('new', 'PostLoad', 'PrePersist', 'PostPersist', 'PreUpdate', 'PostUpdate', 'PreRemove', 'PostRemove')
	 * @param $eventArgs = null
	 * @return aeServiceBaseEntity
	 */
	public function checkIntegrity(&$entity, $context = null, $eventArgs = null) {
		if(is_object($eventArgs)) if(method_exists($eventArgs, 'getEntityManager')) $this->setEm($eventArgs->getEntityManager());
		// echo('<p>aeServiceBaseEntity::checkIntegrity with '.get_class($entity).' in '.json_encode($context).' context.</p>');
		// if($entity instanceOf baseEntity) {
			// echo('<p>checkIntegrity '.$context.'::'.self::NAME.'/'.json_encode($entity->getNom()).'/'.$this->getEntityShortName($entity).'</p>');
			switch(strtolower($context)) {
				case 'edit':
					$this->defineIcons($entity);
					break;
				case 'new':
					$this->setDefaultSingleValues($entity);
					$this->fillAllAssociatedFields($entity);
					$this->defineIcons($entity);
					if(method_exists($entity, 'setLocale')) $entity->setLocale($this->_locale);
					break;
				case 'postload':
					if(method_exists($entity, 'setLocale')) $entity->setLocale($this->_locale);
					break;
				case 'prepersist':
					// $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceImage')->detachCropperImage($entity);
					break;
				case 'postpersist':
					break;
				case 'preupdate':
					// $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceImage')->detachCropperImage($entity);
					break;
				case 'postupdate':
					break;
				case 'preremove':
					break;
				case 'postremove':
					break;
				case 'preflush':
					break;
				case 'onflush::updates':
					$this->defineIcons($entity);
					$this->verifyDefault($entity, $eventArgs);
					$this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceImage')->detachCropperImage($entity);
					break;
				case 'onflush::insertions':
					$this->defineIcons($entity);
					$this->checkStatuts($entity);
					$this->verifyDefault($entity, $eventArgs);
					$this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceImage')->detachCropperImage($entity);
					break;
				case 'onflush::deletions':
					$this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceImage')->detachCropperImage($entity);
					break;
				case 'postflush':
					break;
				default:
					break;
			}
		// }
		return $this;
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ERRORS
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected function errorEntityDoesNotExist($className) {
		if(is_object($className)) $className = get_class($className);
		throw new Exception('Entity '.json_encode($className).' does not exist in '.json_encode($this->getAllEntitiesNames()).'.', 1);
	}

	protected function errorEntityIsNotInstantiable($className) {
		if(is_object($className)) $className = get_class($className);
		throw new Exception('Error : entity '.json_encode($className).' is not instantiable.', 1);
	}

	protected function errorEntityHasNoAssociation($className, $fieldName) {
		if(is_object($className)) $className = get_class($className);
		throw new Exception('Entity '.json_encode($className).' has no association named '.$fieldName.'.', 1);
	}

	protected function errorEntityHasNoField($className, $fieldName) {
		if(is_object($className)) $className = get_class($className);
		throw new Exception('Entity '.json_encode($className).' has no field named '.$fieldName.'.', 1);
	}

	protected function errorEntityHasNoFieldOrAssociation($className, $fieldName) {
		if(is_object($className)) $className = get_class($className);
		throw new Exception('Entity '.json_encode($className).' has no field or association named '.$fieldName.'.', 1);
	}




	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ICONS
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Add icon list to $entity
	 * @param object &$entity
	 */
	public function defineIcons(&$entity) {
		if($entity instanceOf baseEntity) {
			// set list of icons
			$entity->setListIcons($this->container->getParameter('icons'));
			// set default icon
			if($entity->getIcon() === null) {
				$shortName = $entity->getShortName();
				$info = $this->container->getParameter('info_entites');
				if(isset($info[$shortName]['icon'])) $icon = $info[$shortName]['icon'];
					else $icon = $info['default']['icon']; // default icon
				$entity->setIcon($icon);
			}
			// echo('<pre><h3>Icon for entity '.json_encode($entity->getNom()).' : '.json_encode($entity->getIcon()).'</h3>');
			// var_dump($this->container->getParameter('icons'));
			// echo('</pre>');
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OPERATIONS ON ENTITIES
	////////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Fill single fields values of $entity with parameters
	 * @param object &$entity
	 * @return aeServiceBaseEntity
	 */
	public function setDefaultSingleValues(&$entity) {
		$params = $this->container->getParameter('info_entites');
		$className = $this->getEntityClassName($entity);
		$shortName = $this->getEntityShortName($entity);
		if(isset($params[$shortName]['singleFields'])) {
			foreach ($params[$shortName]['singleFields'] as $field => $value) {
				$set = $this->getMethodOfSetting($field, $className);
				if($set) $entity->$set($value);
			}
		}
		return $this;
	}

	/**
	 * Fill related fields of $entity with default relations
	 * @param object &$entity
	 * @return aeServiceBaseEntity
	 */
	public function fillAllAssociatedFields(&$entity) {
		$className = $this->cleanClassIfProxy(get_class($entity));
		$this->getCurrentEntityIfEmpty($className);
		foreach($this->getAssociationNames($className) as $associationName) {
			$this->fillAssociatedField($associationName, $entity);
		}
		return $this;
	}

	/**
	 * Fill $associationName with default relations
	 * DEV@not_perfect : verify is are allready the same entities than default (so, not necessary to change… it should be faster)
	 * @param string $associationName
	 * @param object &$entity
	 */
	public function fillAssociatedField($associationName, &$entity) {
		$className = $this->cleanClassIfProxy(get_class($entity));
		$this->getCurrentEntityIfEmpty($className);
		$targetClassName = $this->get_OtherSide_sourceField($associationName, $className);
		$this->getCurrentEntityIfEmpty($targetClassName);
		$typeOfAssociation = $this->getTypeOfAssociation($associationName, $className);
		$targetRepo = $this->getRepo($targetClassName);
		if($targetRepo !== false) {
			$defaultMethod = self::REPO_DEFAULT_VAL;
			if(method_exists($targetRepo, $defaultMethod)) {
				$items = $targetRepo->$defaultMethod();
				if(is_array($items)) {
					if(!is_object(reset($items))) $items = false;
				}
				// echo('<p>items type for field '.json_encode($associationName).' : ('.gettype($items).')'.implode(', ',(array)$items).' with '.json_encode($this->getMethodOfSetting($associationName, $entity)).'</p>');
				if(is_array($items) || is_object($items)) {
					// echo('<p>- items : '.json_encode(gettype($items)).'</p>');
					$items = (array)$items;
					foreach ($items as $key => $item) if(!is_object($item)) unset($items[$key]);
					// echo('<p>Repository '.json_encode(get_class($targetRepo).'::'.$defaultMethod.'()').' result : '.count($items).' '.implode(', ', $items).'</p>');
					if($typeOfAssociation === self::SINGLE_NAME) array_splice($items, 1);
						else $items = new ArrayCollection($items);
					$set = $this->getMethodOfSetting($associationName, $entity);
					// echo('<p>- Setting '.$typeOfAssociation.' : '.$set.'('.gettype(reset($items)).');</p>');
					switch($typeOfAssociation) {
						case self::SINGLE_NAME:
							if(preg_match('#^set#', $set)) {
								$entity->$set(reset($items));
							} else if(preg_match('#^add#', $set)) {
								foreach($items as $item) $entity->$set($item);
							}
							break;
						case self::COLLECTION_NAME:
							if(preg_match('#^set#', $set)) {
								// set en priorité
								$entity->$set($items);
							} else if(preg_match('#add#', $set)) {
								foreach($items as $item) $entity->$set($item);
							}
							break;
						default:
							// echo('<p>Unknown type of association '.json_encode($typeOfAssociation).' !</p>');
							break;
					}
				}
			}
		}
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ENTITIES INFORMATIONS
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get shortName of entity
	 * @param mixed $className (can be object)
	 * @return string
	 */
	public function getEntityShortName($className) {
		$this->getCurrentEntityIfEmpty($className);
		return $this->aeClasses->getShortName($className);
	}

	/**
	 * Get name of entity
	 * @param mixed $className (can be object)
	 * @return string
	 */
	public function getEntityClassName($className) {
		// echo('<p>getEntityClassName : '.$className.'…</p>');
		$this->getCurrentEntityIfEmpty($className);
		return $className;
	}

	/**
	 * Renvoie la liste des champs (sans association)
	 * @param string $className
	 * @return array
	 */
	public function getFieldNames($className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->getFieldNames();
	}

	/**
	 * Renvoie la liste des noms des associations (sans champs)
	 * Optionally filter by targetEntity
	 * @param string $className
	 * @param string $targetEntity = null
	 * @return array
	 */
	public function getAssociationNames($className, $targetEntity = null) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		$associationNames = $CMD->getAssociationNames();
		if(is_string($targetEntity)) {
			// filter by target entity
			$list = array();
			foreach($associationNames as $association) {
				$otherSideTarget = $this->getTargetEntity($association, $className);
				$parents = $this->aeClasses->getParentClassNames($otherSideTarget);
				if($targetEntity == null || in_array($targetEntity, $parents)) $list[] = $association;
			}
			$associationNames = $list;
			unset($list);
		}
		return $associationNames;
	}

	/**
	 * Renvoie la liste des noms des associations de type SINGLE
	 * Optionally filter by targetEntity
	 * @param string $className
	 * @param string $targetEntity = null
	 * @return array
	 */
	public function getSingleAssociationNames($className, $targetEntity = null) {
		$singleAssociationNames = $this->getAssociationNames($className, $targetEntity);
		$list = array();
		foreach($singleAssociationNames as $association) {
			if($this->isAssociationWithSingleJoinColumn($association, $className)) $list[] = $association;
		}
		return $list;
	}

	/**
	 * Renvoie la liste des noms des associations de type COLLECTION
	 * Optionally filter by targetEntity
	 * @param string $className
	 * @param string $targetEntity = null
	 * @return array
	 */
	public function getCollectionAssociationNames($className, $targetEntity = null) {
		$singleAssociationNames = $this->getAssociationNames($className, $targetEntity);
		$list = array();
		foreach($singleAssociationNames as $association) {
			if($this->isCollectionValuedAssociation($association, $className)) $list[] = $association;
		}
		return $list;
	}

	/**
	 * Renvoie la liste des noms des champs + associations
	 * @param string $className
	 * @return array
	 */
	public function getAllFieldAndAssociationNames($className) {
		$this->getCurrentEntityIfEmpty($className);
		return array_merge($this->getFieldNames($className), $this->getAssociationNames($className));
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// FIELDS OF ENTITIES INFORMATIONS
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get if $fieldName existes
	 * // deleted : ONLY if $className is not abstract or interface 
	 * @param string $fieldName
	 * @param string $className
	 * @return boolean
	 */
	public function hasField($fieldName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		// $isAbstract = $CMD->getReflectionClass()->isAbstract();
		// $isInterface = $CMD->getReflectionClass()->isInterface();
		// return (!$isAbstract && !$isInterface) ? $CMD->hasField($fieldName) : false;
		return $CMD->hasField($fieldName);
	}

	/**
	 * Renvoie le className l'entité associée
	 * @param string $fieldName
	 * @param string $className
	 * @param boolean $shortname = false
	 * @return string
	 */
	public function getTargetEntity($fieldName, $className, $shortName = false) {
		$this->getCurrentEntityIfEmpty($className);
		if(in_array($fieldName, $this->getAssociationNames($className))) {
			$CMD = $this->getEntityMetadataInfo($className);
			$target = $CMD->getAssociationMapping($fieldName)['targetEntity'];
			return $shortName ? $this->getEntityShortName($target) : $target;
		}
		$this->errorEntityHasNoAssociation($className, $fieldName);
	}

	/**
	 * Renvoie si le champ doit être unique
	 * @param string $field
	 * @param object $entite
	 * @return boolean
	 */
	public function isUniqueField($fieldName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		if(in_array($fieldName, $this->getFieldNames($className))) {
			// field
			return $CMD->isUniqueField($fieldName);
		} else if(in_array($fieldName, $this->getAssociationNames($className))) {
			// association
			$mapping = $CMD->getAssociationMapping($fieldName);
			/** DEV@not_perfect : cas d'association type collection… à améliorer */
			return isset($mapping["joinColumns"][0]["unique"]) ? $mapping["joinColumns"][0]["unique"] : true;
		}
		$this->errorEntityHasNoFieldOrAssociation($className, $fieldName);
	}

	/**
	 * Renvoie si le champ doit être unique
	 * @param string $field
	 * @param object $entite
	 * @return boolean
	 */
	public function isNullableField($fieldName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		if(in_array($fieldName, $this->getFieldNames($className))) {
			// field
			return $CMD->isUniqueField($fieldName);
		} else if(in_array($fieldName, $this->getAssociationNames($className))) {
			// association
			$mapping = $CMD->getAssociationMapping($fieldName);
			/** DEV@not_perfect : cas d'association type collection… à améliorer */
			return isset($mapping["joinColumns"][0]["nullable"]) ? $mapping["joinColumns"][0]["nullable"] : true;
		}
		$this->errorEntityHasNoFieldOrAssociation($className, $fieldName);
	}

	/**
	 * Get type of field
	 * @param string $fieldName
	 * @param string $className
	 * @return string
	 */
	public function getTypeOfField($fieldName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		if(in_array($fieldName, $this->getFieldNames($className))) {
			return $CMD->getTypeOfField($fieldName);
		}
		// $this->errorEntityHasNoField($className, $fieldName);
		return false;
	}

	/**
	 * Get type of association
	 * @param string $associationName
	 * @param string $className
	 * @return string
	 */
	public function getTypeOfAssociation($associationName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		if(in_array($associationName, $this->getAssociationNames($className))) {
			if($CMD->isCollectionValuedAssociation($associationName)) return self::COLLECTION_NAME;
			if($CMD->isSingleValuedAssociation($associationName)) return self::SINGLE_NAME;
			throw new Exception('Unknown type of association for attribute '.$className.'::'.$associationName.'.', 1);
		}
		// $this->errorEntityHasNoAssociation($className, $associationName);
		return false;
	}

	/**
	 * Get if association is SINGLE
	 * @param string $associationName
	 * @param string $className
	 * @return boolean
	 */
	public function isAssociationWithSingleJoinColumn($associationName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->isAssociationWithSingleJoinColumn($associationName, $className);
	}

	/**
	 * Get if association is COLLECTION
	 * @param string $associationName
	 * @param string $className
	 * @return boolean
	 */
	public function isCollectionValuedAssociation($associationName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->isCollectionValuedAssociation($associationName, $className);
	}

	/**
	 * Get if entity has this $associationName association
	 * @param string $associationName
	 * @param object $className
	 * @return boolean
	 */
	public function hasAssociation($associationName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->hasAssociation($associationName);
	}

	/**
	 * Get if $fieldName is identifier
	 * @param string $fieldName
	 * @param object $className
	 * @return boolean
	 */
	public function isIdentifier($fieldName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->isIdentifier($fieldName);
	}

	/**
	 * Get if $associationName is bidirectional
	 * True if yes
	 * false if no or if has no association 
	 * @param string $associationName
	 * @param object $className
	 * @return boolean
	 */
	public function isBidirectional($associationName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		if(!$this->hasAssociation($associationName, $className)) return false;
		$tar_entity = $this->getTargetEntity($associationName, $className);
		$tar_field = $this->get_OtherSide_sourceField($associationName, $className);
		return ($this->isAssociationInverseSide($associationName, $className) || $this->isAssociationInverseSide($tar_field, $tar_entity));
	}

	/**
	 * Renvoie si une relation bidirectionnelle est propriétaire
	 * true si oui
	 * false si non ou si pas d'association 
	 * @param string $associationName
	 * @param object $className
	 * @return boolean
	 */
	public function isAssociationMappedSide($associationName, $className) {
		return !$this->isAssociationInverseSide($associationName, $className);
	}

	/**
	 * Renvoie si une relation bidirectionnelle est inverse
	 * true si oui
	 * false si non ou si pas d'association 
	 * @param string $associationName
	 * @param object $className
	 * @return boolean
	 */
	public function isAssociationInverseSide($associationName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		return $CMD->isAssociationInverseSide($associationName);
	}


	// ASSOCIATION : ENTITÉS INVERSES OU MAPPED

	/**
	 * Get other side source associationName
	 * @param string $associationName
	 * @param object $className
	 * @return string
	 */
	public function get_OtherSide_sourceField($associationName, $className) {
		$this->getCurrentEntityIfEmpty($className);
		$CMD = $this->getEntityMetadataInfo($className);
		if($this->hasAssociation($associationName, $className)) {
			$association = $CMD->getAssociationMapping($associationName);
			// echo('<p>get_OtherSide_sourceField '.json_encode($associationName).' on '.json_encode($className).' : '.json_encode($association).'</p>');
			// if(is_string($association["inversedBy"])) return $association["inversedBy"];
			return $association["targetEntity"];
		}
		return false;
	}




	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// GETTING & SETTING METHODS
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Renvoie le nom du setter / false si la méthode est manquante
	 * @param string $fieldName
	 * @param object $className = null
	 * @param boolean $testIfExists = true
	 * @return string / false
	 */
	public function getMethodOfSetting($fieldName, $className = null, $testIfExists = true) {
		$this->getCurrentEntityIfEmpty($className);
		$test = $testIfExists ? $className : null;
		$TOF = $this->getTypeOfField($fieldName, $className);
		if($TOF !== false) {
			if($TOF === Type::TARRAY) {
				// Type arrayCollection -> priority to 'set'
				$test = $this->aeClasses->getMethodNameWith($fieldName, 'set', $test);
				return $test !== false ? $test : $this->aeClasses->getMethodNameWith($fieldName, 'add', $test);
			} else {
				return $this->aeClasses->getMethodNameWith($fieldName, 'set', $test);
			}
		} else {
			switch ($this->getTypeOfAssociation($fieldName, $className)) {
				case self::COLLECTION_NAME:
					// collection -> priority to 'set'
					$test = $this->aeClasses->getMethodNameWith($fieldName, 'set', $test);
					return $test !== false ? $test : $this->aeClasses->getMethodNameWith($fieldName, 'add', $test);
					break;
				case self::SINGLE_NAME:
					// single
					return $this->aeClasses->getMethodNameWith($fieldName, 'set', $test);
					break;
			}
		}
		if($test == null) return $this->aeClasses->getMethodNameWith($fieldName, 'set', null);
		// throw new Exception("L'objet entité ".json_encode($className->getClassName())." n'est pas une entité Doctrine !", 1);		
		return false;
	}

	/**
	 * Renvoie le nom du remover / false si la méthode est manquante
	 * @param string $fieldName
	 * @param object $entite
	 * @return string / false
	 */
	public function getMethodOfRemoving($fieldName, $entite = null, $testIfExists = true) {
		$this->getCurrentEntityIfEmpty($className);
		$test = $testIfExists ? $entite : null;
		$TOF = $this->getTypeOfField($fieldName, $entite);
		if($TOF !== false) {
			if($TOF === Type::TARRAY) {
				// Type arrayCollection
				return $this->aeClasses->getMethodNameWith($fieldName, 'remove', $test);
			} else {
				return $this->aeClasses->getMethodNameWith($fieldName, 'set', $test);
			}
		} else {
			switch ($this->getTypeOfAssociation($fieldName, $entite)) {
				case self::COLLECTION_NAME:
					// collection
					return $this->aeClasses->getMethodNameWith($fieldName, 'remove', $test);
					break;
				case self::SINGLE_NAME:
					// single
					return $this->aeClasses->getMethodNameWith($fieldName, 'set', $test);
					break;
			}
		}
		return false;
	}

	/**
	 * Renvoie le nom du getter / false si la méthode est manquante
	 * @param string $fieldName
	 * @param object $className
	 * @return string / false
	 */
	public function getMethodOfGetting($fieldName, $className = null, $testIfExists = true) {
		$this->getCurrentEntityIfEmpty($className);
		$test = $testIfExists ? $className : null;
		$TOF = $this->getTypeOfField($fieldName, $className);
		if($TOF !== false) {
			// is field
			if($TOF === Type::TARRAY) {
				// Type arrayCollection
				return $this->aeClasses->getMethodNameWith($fieldName, 'get', $test); // s !
			} else {
				return $this->aeClasses->getMethodNameWith($fieldName, 'get', $test);
			}
		} else if($this->hasAssociation($fieldName, $className)) {
			// is association
			switch ($this->getTypeOfAssociation($fieldName, $className)) {
				case self::COLLECTION_NAME:
					// collection
					return $this->aeClasses->getMethodNameWith($fieldName, 'get', $test); // s !
					break;
				case self::SINGLE_NAME:
					// single
					return $this->aeClasses->getMethodNameWith($fieldName, 'get', $test);
					break;
			}
		}
		return false;
	}




	// DEFAULT

	/**
	 * Définit l'entité par défaut
	 * @param object &$entity
	 * @param boolean $set = null
	 * @param boolean $flush = true
	 * @return boolean / false
	 */
	public function setAsDefault(&$entity, $set = null, $flush = true) {
		$className = $this->cleanClassIfProxy(get_class($entity));
		if($this->hasField('default', $className)) {
			if($entity->getDefault() === $set) return null;
			$entity->setDefault(!($set === false || ($set == null && $entity->getDefault() === true)));
			// $this->verifyDefault($entity);
			// flush
			if($flush) $this->getEm()->flush();
			return $entity->getDefault();
		}
	}

	/**
	 * Check default
	 * for entities have 'default' field of 'boolean' doctrine type
	 * @param object &$entity
	 * @return aeReponse where result is default value of $entity
	 */
	public function verifyDefault(&$entity) {
		$messages = array();
		$reponse = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeReponse');
		$className = $this->cleanClassIfProxy(get_class($entity));
		if($this->hasField('default', $className) && $this->getTypeOfField('default', $className) == 'boolean') {
			if(!is_bool($entity->getDefault())) $entity->setDefault(false);
			$dm = $entity->isDefaultMultiple(); // $dm = nombre max de defaults
			$nu = $entity->isDefaultNullable(); // $nu = aucun default authorisé ?
			if($dm !== true) { // si $dm != de infini
				$items = $this->getRepo($className)->findByDefault(true);
				if(count($items) > 0) foreach($items as $key => $oneItem) if($entity->getId() == $oneItem->getId()) unset($items[$key]);
				if(is_int($dm)) {
					if($dm < 1) $dm = 1;
				} else $dm = 1;
				if((count($items) + (integer)$entity->getDefault()) > $dm) {
					if($entity->getDefault() && $dm > 1) {
						$entity->setDefault(false);
						// nombre max dépassé, so print a message !
						$messages[] = $this->container->get('translator')->trans('default.nomore', array('%max%' => $dm));
						$messages[] = $this->container->get('translator')->trans('default.notsettotrue', array('%item%' => $entity->getNom()));
					}
					$cpt = count($items) - $dm + (integer)$entity->getDefault();
					$nb = 0;
					if($cpt > 0) {
						foreach($items as $oneItem) {
							$oneItem->setDefault(false);
							$cpt--;
							$nb++;
							if($cpt < 1) break;
						}
					}
					if($nb > 0) $messages[] = $this->container->get('translator')->transChoice('default.deleted', $nb, array('%number%' => $nb));
				}
			}
			$reponse->initAeReponse($entity->getDefault(), null, implode(' ', $messages));
			if(count($messages) > 0)
				$this->container->get('flash_messages')->send(array(
					'title'		=> $this->container->get('translator')->trans('default.bydefault'),
					'type'		=> flashMessage::MESSAGES_WARNING,
					'text'		=> $reponse->getMessage(),
				));
		} else {
			// $reponse->initAeReponse(false, null, $this->container->get('translator')->trans('default.notDefaultEntity'));
		}
		return $reponse;
	}

	// DELETIONS

	/**
	 * Supprime une entité sans la retirer de la base (sauf si 'deleted')
	 * 4 États : actif => inactif => deleted => et enfin, suppression de la base
	 * @param baseEntity &$entity
	 * @return boolean
	 */
	public function softDeleteEntity(&$entity) {
		$className = $this->cleanClassIfProxy(get_class($entity));
		$this->getCurrentEntityIfEmpty($className);
		$get = $this->getMethodOfGetting('statut', $entity, true);
		if($get !== false) {
			$aeServiceStatut = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceStatut');
			// si un champ statut existe : Règle :
			// actif ---> inactif
			// inactif ou expired ---> deleted (uniquement visible du SUPER ADMIN)
			if(in_array($entity->$get()->getNiveau(), array('IS_AUTHENTICATED_ANONYMOUSLY', 'ROLE_USER'))) {
				$result = $aeServiceStatut->setInactif($entity);
				if($result) {
					$this->setAsDefault($entity, false, false);
				}
			} else if(in_array($entity->$get()->getNiveau(), array('ROLE_TRANSLATOR', 'ROLE_EDITOR', 'ROLE_ADMIN'))) {
				$result = $aeServiceStatut->setDeleted($entity);
			} else {
				$this->getEm()->remove($entity);
			}
		} else {
			// sinon on la supprime
			$this->getEm()->remove($entity);
		}
		// flush
		return $this->getEm()->flush();
	}

	/**
	 * Supprime toutes les entités qui ont un statut temporaire
	 * option : array $listOfId - si null, supprime tous
	 * @param string $className
	 * @param array $listOfId = null
	 * @return boolean
	 */
	public function deleteAllTemp($className, $listOfId = null) {
		$this->getCurrentEntityIfEmpty($className);
		$items = $this->getRepo($className)->findTempStatut($listOfId);
		if(is_array($items)) foreach($items as $item) $this->getEm()->remove($item);
		// flush
		return $this->getEm()->flush();
	}

	/**
	 * Rétablit une entité inactive
	 * @param baseEntity &$entity
	 * @return boolean
	 */
	public function softActivateEntity(&$entity) {
		$className = $this->cleanClassIfProxy(get_class($entity));
		$this->getCurrentEntityIfEmpty($className);
		$aeServiceStatut = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeServiceStatut');
		// si un champ statut existe
		$result = $aeServiceStatut->setActif($entity);
		if($result) {
			return $this->getEm()->flush();
		}
		return false;
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// CHECKS ON ENTITIES
	////////////////////////////////////////////////////////////////////////////////////////////////////////



	// CHAMPS LIÉS NULLABLE FALSE

	// ALL
	/**
	 * Check $association and fill it with default if null
	 * @param object &$entity
	 * @param string $association
	 * @param boolean $fillEvenIfNotEmpty = false
	 * @param boolean $evenIfIsNullable = true
	 * @return object $entity
	 */
	public function checkAssociation(&$entity, $association, $fillEvenIfNotEmpty = false, $evenIfIsNullable = true) {
		$className = $this->cleanClassIfProxy(get_class($entity));
		$this->getCurrentEntityIfEmpty($className);
		if($this->hasAssociation($association, $className)) {
			$associationTargetClass = $this->getAssociationTargetClass($className, $association);
			$this->getCurrentEntityIfEmpty($associationTargetClass);
			// test if not nullable or fillEvenIfNotEmpty
			if(!$this->isNullableField($association, $className) || ($evenIfIsNullable)) {
				// test if empty or fillEvenIfNotEmpty
				$get = $this->getMethodOfGetting($association, $entity, true);
				$items = $this->aeArrays->asArray($entity->$get());
				// echo('<p>checkAssociation : '.json_encode($association).' on '.json_encode($entity->getShortName()).' = '.json_encode($get).' (nullable: '.json_encode($this->isNullableField($association, $className)).')</p>');
				// echo('<p>- Found: '.count($items).' = '.implode(', ', $items).'</p>');
				if(count($items) < 1 || $fillEvenIfNotEmpty) {
					// echo('<p>- So, fill it…</p>');
					$this->fillAssociatedField($association, $entity);
				}
			}
		}
		return $entity;
	}

	// STATUT
	/**
	 * Check statut field and fill it with default if null
	 * @param object &$entity
	 * @return boolean
	 */
	public function checkStatuts(&$entity, $fillEvenIfNotEmpty = false) {
		return $this->checkAssociation($entity, 'statut', $fillEvenIfNotEmpty);
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ENTITYMANAGER & REPOSITORY
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * set EntityManager
	 * @param EntityManager $em
	 * @return aeServiceBaseEntity
	 */
	public function setEm(EntityManager $em) {
		$this->_em = $em;
		$this->getAllEntitiesInfo();
		return $this;
	}

	/**
	 * get EntityManager
	 * @return EntityManager
	 */
	public function getEm() {
		if(!$this->_em instanceOf EntityManager) {
			if(is_object($this->container)) $this->setEm($this->container->get('doctrine')->getManager());
		}
		return $this->_em;
	}

	/**
	 * Get Repository of $className (or default entity)
	 * @param mixed $className
	 * @param boolean $context = true
	 * @return repository / false
	 */
	public function getRepo($className = null, $context = true) {
		$this->getCurrentEntityIfEmpty($className);
		if(!isset($this->repo[$className])) {
			try {
				$this->repo[$className] = $this->getEm()->getRepository($className);
			} catch (Exception $e) {
				return false;
			}
			// initialisation du repository
			if(method_exists($this->repo[$className], "declareContext") && $context) {
				$this->repo[$className]->declareContext($this->container);
			}
		}
		// $this->aeDebug->debugNamedFile(aeData::LOG_NAME, array('Repository' => json_encode($className).' : '.json_encode(get_class($this->repo[$className]))));
		return $this->repo[$className];
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// FLUSH/PERSIST ENTITY
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Persist en flush a baseEntity
	 * @param object $entity
	 * @return aeReponse
	 */
	public function NOsave(&$entity) {
		$aeReponse = $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeReponse');
		$classname = $this->getEntityClassName($entity);
		if($classname) {
			$response = true;
			$message = 'Entity saved.';
			$action = '(persist) ';
			if($entity->getId() == null) {
				$context = 'Create entity : ';
				try {
					$this->_em->persist($entity);
				} catch (Exception $e) {
					$response = false;
					if($this->aeUrlroutes->isDevOrSadmin()) $message = $context.$action.$e->getMessage();
						else $message = $context.$action.'system error (persist).';
				}
			} else $context = 'Update entity '.json_encode($entity->getNom()).' : ';
			// flush
			$action = '(flush) ';
			try {
				$this->getEm()->flush();
			} catch (Exception $e) {
				$response = false;
				if($this->aeUrlroutes->isDevOrSadmin()) $message = $context.$action.$e->getMessage();
					else $message = $context.$action.'system error.';
			}
			return $aeReponse
				->setResult($response)
				->setMessage($message)
				->setData(array('id' => $entity->getId()))
				;
		} else {
			return $aeReponse
				->setResult(false)
				->setMessage('This entity is not valid for flush. Type '.json_encode(gettype($entity)).' is not a class.')
				->setData(array('entity' => $entity))
				;
		}
	}

	/**
	 * Persist en flush a baseEntity / pour tests
	 * @param baseEntity $entity
	 * @return aeReponse
	 */
	public function save(baseEntity &$entity) {
		$response = true;
		$message = 'Entité enregistrée.';
		if($entity->getId() == null) $this->_em->persist($entity);
		$this->_em->flush();
		return $this
			->container->get(aeData::PREFIX_CALL_SERVICE.'aeReponse')
			->setResult($response)
			->setMessage($message)
			->setData(array('id' => $entity->getId()))
			;
	}




	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// FUNCTIONS TO OPTIMIZE
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	/** DEV@to_replace : old function to replace */
	public function getDetailedListOfEnties() {
		if($this->detailedListOfEntities == null) {
			$this->detailedListOfEntities = array();
			$entitiesNameSpaces = $this->getEm()->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
			foreach($entitiesNameSpaces as $ENS) {
				$CMD = $this->getEm()->getClassMetadata($ENS);
				if(is_object($CMD)) {
					$reflectionClass = $CMD->getReflectionClass();
					// $EE = $reflectionClass->getShortName();
					// $NS = $reflectionClass->getNamespaceName();
					// $classname = $NS.'\\'.$EE;
					$classname = $reflectionClass->getName();
					$this->detailedListOfEntities[$ENS]['class'] = $reflectionClass->getShortName();
					$this->detailedListOfEntities[$ENS]['classname'] = $classname;
					$this->detailedListOfEntities[$ENS]['is_abstract'] = $reflectionClass->isAbstract();
					$this->detailedListOfEntities[$ENS]['is_interface'] = $reflectionClass->isInterface();
					$this->detailedListOfEntities[$ENS]['is_instantiable'] = $reflectionClass->isInstantiable();
					$this->detailedListOfEntities[$ENS]['service'] = $this->getEntityServiceName($classname);
					// field names
					$this->detailedListOfEntities[$ENS]['single'] = array();
					$fieldNames = $CMD->getFieldNames();
					foreach($fieldNames as $fieldName) {
						$this->detailedListOfEntities[$ENS]['single'][$fieldName] = array();
					}
					$this->detailedListOfEntities[$ENS]['association'] = array();
					$associationNames = $CMD->getAssociationNames();
					foreach($associationNames as $associationName) {
						$this->detailedListOfEntities[$ENS]['association'][$associationName] = array();
					}
					// $this->detailedListOfEntities[$ENS]['object'] = $this->getNewEntity($classname);
				}
			}
		}
		return $this->detailedListOfEntities;
	}

	/** DEV@to_replace : old function to replace */
	public function getCompleteDetailedListOfEnties() {
		$result = array();
		foreach ($this->detailedListOfEntities as $classname => $entity) {
			$result[$classname] = $entity;
			$result[$classname]['object'] = $this->getNewEntity($classname);
		}
		return $result;
	}

	/**
	 * DEV@to_replace : old function to replace
	 * Get array des entités doctrine
	 * Sous la forme liste[nameSpace] = shortName (ou inverse si $fliped = true)
	 * @param boolean $onlyInstantiable = true
	 * @param boolean $fliped = false
	 * @return array
	 */
	public function getListOfEnties($onlyInstantiable = true, $fliped = false) {
		$this->listOfEnties = array();
		$list = $this->getDetailedListOfEnties();
		foreach($list as $key => $entity) {
			if($entity['is_instantiable'] == true || $onlyInstantiable == false) {
				$this->listOfEnties[$key] = $entity['class'];
			}
		}
		// $this->container->get(aeData::PREFIX_CALL_SERVICE.'aeDebug')->debugNamedFile(
		// 	'listOfEntities',
		// 	array(
		// 		"call" => "Fist call : define data / ".count($this->listOfEnties)." entity(ies)",
		// 		'instantiable' => $onlyInstantiable,
		// 		'listOfEnties' => $this->listOfEnties,
		// 		)
		// 	);
		return $fliped ? array_flip($this->listOfEnties) : $this->listOfEnties;
	}



}
jQuery(document).ready(function ($) {

    var environment = $('body').data('environnementMode');
    environment = 'prod';

    if ($('#ajaxlive > [ajaxlive-entity]').length) {

        if (environment !== 'prod') {console.log('• Loading : ', 'ajaxlive (' + environment + ')'); }

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        // toastr["success"]("My name is Inigo Montoya. You killed my father. Prepare to die!");
        // toastr["success"]('<div><input class="input-small" value="textbox"/>&nbsp;<a href="http://johnpapa.net" target="_blank">This is a hyperlink</a></div><div><button type="button" id="okBtn" class="btn btn-primary">Close me</button><button type="button" id="surpriseBtn" class="btn" style="margin: 0 8px 0 8px">Surprise me</button></div>');


        ////////////////////////////////
        // AJAX LIVE
        ////////////////////////////////

        var ajaxlive = function () {

            var
                TemplaxZone = '[TemplaxZone]',
                onedata;

            if ($(TemplaxZone).length) {
                var ajaxliveBalise          = '#ajaxlive';
                var getAjaxlive_params      = function () { return $.parseJSON($(ajaxliveBalise + ' > [ajaxlive-params]').first().attr('ajaxlive-params')); };
                var getajaxliveurl          = function () { return $(ajaxliveBalise + ' > [ajaxlive-url]').first().attr('ajaxlive-url'); };
                // var getajaxliveentiteurl = function () { return $(ajaxliveBalise + ' > [ajaxlive-entity-url]').first().attr('ajaxlive-entity-url'); };
                var getajaxliveuser         = function () {
                    return $.parseJSON($(ajaxliveBalise + ' > [ajaxlive-user]').first().attr('ajaxlive-user'));
                };

                var templax_main = new templax(TemplaxZone, environment !== 'prod');
                // first init data
                var datas = {};
                $(ajaxliveBalise + ' [ajaxlive-entity][ajax-entity-data]').each(function () {
                    if ($(this).attr('ajax-group') !== undefined) {
                        if (datas[$(this).attr('ajax-group')] === undefined) {datas[$(this).attr('ajax-group')] = {}; }
                        datas[$(this).attr('ajax-group')][$(this).attr('ajaxlive-entity')] = $.parseJSON($(this).attr('ajax-entity-data'));
                    } else {
                        datas[$(this).attr('ajaxlive-entity')] = $.parseJSON($(this).attr('ajax-entity-data'));
                    }
                });
                for (onedata in datas) {
                    var added_data = {requirements: {
                        entities:   getAjaxlive_params().entities,
                        user:       getajaxliveuser(),
                    }};
                    if (environment !== 'prod') {console.log('First load "' + onedata + '" and added data:', {data: datas[onedata], added_data: added_data}); }
                    templax_main
                        .setNamedVar(onedata, datas[onedata])
                        .addAjaxLoad(getajaxliveurl(), onedata, added_data, 'post')
                        ;
                };
                // launch…
                templax_main
                    .refreshScreen()
                    .addRestoreDegradBind('mouseenter', 'all', 'body .allRefreshAjaxlive')
                    .startAll(getAjaxlive_params().parameters.frequency)
                    ;

            };

        }

        ajaxlive();



        ////////////////////////////////
        // AJAX PANIER
        ////////////////////////////////

        var panier = {

            controleNumber: function (panierData) {
                if (panierData.quantite < 1) {panierData.quantite = 1; }
                return panierData;
            },

            controleVolume: function (panierData) {
                if (panierData.article.maxquantity != null)
                    {if (panierData.volume > panierData.article.maxquantity) {panierData.volume = panierData.article.maxquantity; }}
                if (panierData.article.minquantity != null)
                    {if (panierData.volume < panierData.article.minquantity) {panierData.volume = panierData.article.minquantity; }}
                if (panierData.volume < 1) {panierData.volume = 1; }
                return panierData;
            },

            findBaliseData: function (item) {
                var $balisedata;
                if (!$(item).hasClass('panier-data')) {$balisedata = $(item).closest('.panier-data'); }
                    else {$balisedata = $(item); }
                if ($balisedata == undefined) {
                    // if (environment !== 'prod') console.error('Error balisedata', 'not found.');
                    $balisedata = null;
                }
                return $balisedata;
            },

            findCommand_quantity: function (item) {
                var target = $(item.find('input.command_quantity'));
                if (target.length) {return $(target).first(); }
                return null;
            },

            /**
             * Get panier data
             */
            getPanierData: function (item) {
                var $item = this.findBaliseData(item);
                if ($item !== null) {
                    var panierData = {};
                    panierData.quantite = parseInt($item.attr('data-number'));
                    panierData.action = $item.attr('data-action');
                    panierData.article = $.parseJSON($item.attr('data-article'));
                    panierData.user = $.parseJSON($item.attr('data-user'));
                    panierData.panierActionUrl = $item.attr('panier-action-url');
                    var $command_quantity = this.findCommand_quantity($item);
                    if ($command_quantity != null) {
                        var command_quantity = $command_quantity.first().val();
                        // control values number and volumes
                        if (panierData.article != null) {
                            if (command_quantity == undefined || command_quantity == null) {
                                panierData.volume = panierData.article.defaultquantity;
                                panierData.unit = panierData.article.unit;
                            } else {
                                if (this.is_number_mode(item)) {
                                    // NUMBER MODE
                                    // var numbers = command_quantity.split(' ');
                                    // panierData.quantite = numbers[1];
                                    panierData.quantite = Math.floor(parseFloat(command_quantity.replace(new RegExp("^(x\s?)?[^0-9]", "g"), '')));
                                    if (panierData.quantite < 1 || isNaN(panierData.quantite)) {panierData.quantite = 1; }
                                    $command_quantity.data('number', panierData.quantite);
                                    // if ($command_quantity.data('volume') != undefined) {
                                        panierData.volume = $command_quantity.data('volume')['volume'];
                                        panierData.unit = $command_quantity.data('volume')['unit'];
                                    // }
                                } else {
                                    // VOLUME MODE
                                    var volumes = command_quantity.split(' ');
                                    if (volumes[0] == undefined || isNaN(volumes[0]) || volumes[0] == null) {panierData.volume = panierData.article.defaultquantity; }
                                    var neg = false;
                                    if (volumes[0][0] == '-') {neg = true; }
                                    panierData.volume = Math.floor(parseFloat(volumes[0].replace(new RegExp("[^(0-9\.,)]", "g"), '')));
                                    if (isNaN(panierData.volume)) {panierData.volume = panierData.article.defaultquantity; }
                                    if (neg == true) {panierData.volume = 0 - panierData.volume; }
                                    panierData.unit = panierData.article.unit;
                                    var pnb = $command_quantity.data('number');
                                    if (pnb != undefined) {panierData.quantite = pnb; }
                                }
                                panierData = this.controleNumber(panierData);
                                panierData = this.controleVolume(panierData);
                            }
                        }
                    }
                    return panierData;
                }
                return null;
            },

            /**
             * Set panier data (in html)
             */
            setPanierData: function (item, panierData) {
                var
                    $item = this.findBaliseData(item),
                    panierData;
                if (panierData == undefined) {panierData = this.getPanierData(item); }
                    else {panierData = panierData; }
                if ($item !== null) {
                    $item.attr('data-number', panierData.quantite);
                    $item.attr('data-action', panierData.action);
                    $item.attr('data-article', JSON.stringify(panierData.article));
                    $item.attr('data-user', JSON.stringify(panierData.user));
                    $item.attr('panier-action-url', panierData.panierActionUrl);
                    panierData = this.controleNumber(panierData);
                    panierData = this.controleVolume(panierData);
                    var $command_quantity = this.findCommand_quantity($item);
                    if ($command_quantity != null) {
                        if (this.is_number_mode(item)) {
                            // NUMBER MODE
                            // console.log('setPanierData : ', 'NUMBER MODE');
                            $($command_quantity).first().val('x ' + panierData.quantite).data('volume', {volume: panierData.volume, unit: panierData.unit});
                        } else {
                            // VOLUME MODE
                            // console.log('setPanierData : ', 'VOLUME MODE');
                            $($command_quantity).first().val(panierData.volume + ' ' + panierData.unit).data('number', panierData.quantite);
                        }
                    }
                } else {
                    if (environment !== 'prod') {console.error('Error : ', 'panierData balise not found (panier-data).'); }
                }
                return;
            },

            is_number_mode: function (item) {
                var $item = this.findBaliseData(item);
                if ($item != null) {
                    $quantity = $('input.command_quantity', $item);
                    if ($quantity.length) {
                        return $quantity.hasClass('number');
                    }
                }
                return false;
            },

            number_option_on: function (item) {
                var
                    $item = this.findBaliseData(item),
                    panierData = this.getPanierData(item);
                if (panierData.article != null) {
                    if ($item != null && !this.is_number_mode(item) && (panierData.article.groupbasket == false)) {
                        $quantity = $('input.command_quantity', $item);
                        if ($quantity.length) {
                            $quantity
                                .addClass('number')
                                .val('x ' + panierData.quantite)
                                .data('volume', {volume: panierData.volume, unit: panierData.unit})
                                ;
                            this.setPanierData(item);
                        }
                    }
                }
            },

            number_option_off: function (item) {
                var
                    $item = this.findBaliseData(item),
                    panierData = this.getPanierData(item);
                if (panierData.article != null) {
                    if ($item != null && this.is_number_mode(item) && (panierData.article.groupbasket == false)) {
                        $quantity = $('input.command_quantity', $item);
                        if ($quantity.length) {
                            $quantity
                                .removeClass('number')
                                .val(panierData.volume + ' ' + panierData.unit)
                                .data('number', panierData.quantite)
                                ;
                            this.setPanierData(item);
                        }
                    }
                }
            },

            increment: function (item) {
                // var $item = this.findBaliseData(item);
                var panierData = this.getPanierData(item);
                if (this.is_number_mode(item)) {
                    // NUMBER MODE
                    panierData.quantite++;
                } else {
                    // VOLUME MODE
                    panierData.volume += panierData.article.increment;
                }
                this.setPanierData(item, panierData);
            },

            decrement: function (item) {
                // var $item = this.findBaliseData(item);
                var panierData = this.getPanierData(item);
                if (this.is_number_mode(item)) {
                    // NUMBER MODE
                    panierData.quantite--;
                } else {
                    // VOLUME MODE
                    panierData.volume -= panierData.article.increment;
                }
                this.setPanierData(item, panierData);
            },

            rollicons: function (item, action) {
                var
                    $item = this.findBaliseData(item),
                    action = action;
                $item.find('.fa.for-pulse, .fa.fa-spinner, .fa.fa-warning').each(function (item) {
                    if ($(this).data('class') == undefined) {$(this).data('class', $(this).attr('class')); }
                    switch(action) {
                        case 'error':
                            $(this).removeClass().addClass('fa fa-warning fa-fw');
                            break;
                        case 'roll':
                            $(this).removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
                            break;
                        case 'stop':
                        default:
                            $(this).removeClass().addClass($(this).data('class'));
                            break;
                    }                   
                });
            },

            init: function () {
                var parent = this;
                // $('body .panier-data').each(function (index) { parent.getPanierData(this); });

                $('body').on('click', '.panier-action', function (event) {
                    event.preventDefault();
                    var 
                        parent2 = this,
                        panierData = parent.getPanierData(this),
                        conf = true;
                    // if (environment !== 'prod') console.log('panier send data : ', panierData);
                    parent.rollicons(parent2, 'roll');
                    if (panierData.action == 'empty') {
                        conf = confirm('Confirmez la suppression de tous les articles du panier, svp.');
                        // toastr["warning"]('Confirmez la suppression de tous les articles du panier, svp.');
                    }
                    if (conf) {
                        $.ajax({
                            method: 'POST',
                            url: panierData.panierActionUrl,
                            data: {params: panierData},
                            statusCode: {
                                404: function (jqXHR, textStatus) { toastr["warning"]('Error 404: ' + jqXHR.responseText); },
                                // 500: function () { toastr["error"]('Error 500'); },
                            },
                        })
                        .fail(function (jqXHR, textStatus) {
                            // if (environment !== 'prod') console.error(jqXHR);
                            parent.rollicons(parent2, 'error');
                            if (environment !== 'prod') {
                                toastr["error"](jqXHR.statusText, 'See at bottom of this page <a href="#devinfo"><i class="fa fa-level-down fa-3x"></i></a>');
                                if ($('#devinfo').length) {$('#devinfo').html($(jqXHR.responseText)); }
                            }
                            toastr["warning"]('Une erreur est survenue.');
                        })
                        .done(function (aeReponse) {
                            // if (environment !== 'prod') console.log('panier return brut : ', aeReponse);
                            aeReponse = $.parseJSON(aeReponse);
                            // if (environment !== 'prod') console.log('panier return data : ', aeReponse.data);
                            if (aeReponse.result) {toastr["success"](aeReponse.message); }
                                else {toastr["warning"](aeReponse.message); }
                        })
                        .always(function (aeReponse) {
                            parent.rollicons(parent2, 'stop');
                        });
                        // ajaxlive.startUpdate();
                        // var $item = parent.findBaliseData(this);
                        // $('.fa-spin', $item).each(function (item) {
                        //  var icon_wait = $(this).data('icon_wait');
                        //  if (icon_wait != undefined) $(this).removeClass().addClass("fa " + icon_wait.oldIcon);
                        // });
                    }
                });

                $('body').on('mouseenter', '.panier-action', function (oneevent) {
                    oneevent.preventDefault();
                    if (!parent.is_number_mode(this)) {
                        parent.number_option_on(this);
                        // if (environment !== 'prod') console.log('panier data : ', parent.getPanierData(this));
                    }
                });

                $('body').on('mouseleave', '.commande-cmd', function (oneevent) {
                    oneevent.preventDefault();
                    if (parent.is_number_mode(this)) {
                        parent.number_option_off(this);
                        // if (environment !== 'prod') console.log('panier data : ', parent.getPanierData(this));
                    }
                });

                $('body').on('click', '.quantity-top', function (oneevent) {
                    oneevent.preventDefault();
                    parent.increment(this);
                    // if (environment !== 'prod') console.log('panier data : ', parent.getPanierData(this));
                });

                $('body').on('click', '.quantity-bottom', function (oneevent) {
                    oneevent.preventDefault();
                    parent.decrement(this);
                    // if (environment !== 'prod') console.log('panier data : ', parent.getPanierData(this));
                });

                $('body').on('change', '.commande-cmd input.command_quantity', function (item) {
                    parent.setPanierData(this);
                    // if (environment !== 'prod') console.log('panier data : ', parent.getPanierData(this));
                });

            },

        }
        panier.init();

    } else {
        if (environment !== 'prod') {console.log('• NOT LOADING : ', 'ajaxlive has no data (' + environment + ')'); }
    }





});










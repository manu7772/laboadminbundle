jQuery(document).ready(function ($) {

    var
        environment = $('body').data('environnementMode'),
        CALENDAR = $('#calendar');

    if (CALENDAR.length) {

        var
            date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            URL = CALENDAR.attr('cal-url'),
            eventFeed = CALENDAR.attr('cal-feed'),
            itemSlug = CALENDAR.attr('cal-itemslug'),
            locale = CALENDAR.attr('cal-locale'),
            owner = JSON.parse(CALENDAR.attr('cal-owner')),
            slug = CALENDAR.attr('cal-slug'),
            current_event = null,
            TEMP_ITEM = 'TEMP_ITEM';

        var camelizeAllDay = function (events) {
            if ($.type(events) === 'array') {
                for (var i in events) {
                    if (events[i].all_day !== undefined) {
                        events[i].allDay = events[i].all_day;
                        delete events[i].all_day;
                    }
                }
            } else if ($.type(events) === 'object') {
                if (events.all_day !== undefined) {
                    events.allDay = events.all_day;
                    delete events.all_day;
                }
            }
        }

        var checkEditButton = function () {
            $edit = $('#editevent');
            if (current_event === null) {
                $edit.hide();
                $edit.removeAttr('href');
            } else if (current_event === TEMP_ITEM) {
                $edit.hide();
                $edit.removeAttr('href');
            } else {
                var oneevent = CALENDAR.fullCalendar('clientEvents', current_event)[0];
                var href = $edit.attr('cal-href').replace(/(__slug__)/g, oneevent.slug);
                $edit.attr('href', href);
                $edit.show();
            }
        }
        checkEditButton();

        var simplifyEventObject = function (oneevent) {
            var simplyEvent = {};
            simplyEvent.id = oneevent.id;
            simplyEvent.start = oneevent.start.toString();
            simplyEvent.end = null;
            if ($.type(oneevent.end) === 'object') simplyEvent.end = oneevent.end.toString();
            simplyEvent.title = oneevent.title;
            simplyEvent.allDay = oneevent.allDay;
            simplyEvent.url = oneevent.url;
            simplyEvent.slug = oneevent.slug;
            simplyEvent.color = oneevent.color;
            simplyEvent.owner = oneevent.owner;
            return simplyEvent;
        }

        var displayView = function () {
            var view = CALENDAR.fullCalendar('getView');
            // console.log('Changed view:', view);
            $("#calview").text(view.title);
        }

        $("#external-events div.external-event").each(function (item) {
            // store data so the calendar knows to render an event upon drop
            var
                color = $(this).css('backgroundColor'),
                url = $(this).attr('cal-url');
            if (color === undefined) color = "#2572a9";
            // var view = CALENDAR.fullCalendar('getView');
            if (url === undefined) url = null;
            $(this).data("event", {
                stick: true, // maintain when user navigates (see docs on the renderEvent method)
                title: $.trim($(this).text()), // use the elements text as the event title
                // allDay: view.intervalUnit === 'month',
                owner: owner,
                color: color,
                id: TEMP_ITEM,
                slug: TEMP_ITEM,
                url: url,
                end: null,
            });
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1111999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
            // console.log('new dragged event:', $(this).data("event"));
        });

        var fcal = CALENDAR.fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultTimedEventDuration: '01:00:00',
            locale: locale,
            timezone: 'local',
            timeFormat: 'H:mm',
            defaultView: 'agendaWeek',
            droppable: true, // this allows things to be dropped onto the calendar
            eventLimit: true,
            editable: true,
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
             var title = prompt('Event Title:');
             var eventData;
             if (title) {
                 eventData = {
                     title: title,
                     start: start,
                     end: end,
                     // id: 'fc_'+Math.floor((Math.random() * 100000) + 1),
                     id: TEMP_ITEM,
                 };
                 CALENDAR.fullCalendar('renderEvent', eventData, true); // stick? = true
             }
             CALENDAR.fullCalendar('unselect');
            },
            // selectOverlap: function (oneevent) {
            //  return oneevent.rendering === 'background';
            // },
            eventClick: function (oneevent, element) {
                // console.log('Click on:', {event: oneevent, element: element});
                if (current_event !== null) {
                    var oldEvent = CALENDAR.fullCalendar('clientEvents', current_event)[0];
                    if ($.type(oldEvent) === 'object') {
                        oldEvent.borderColor = 'transparent';
                        CALENDAR.fullCalendar('updateEvent', oldEvent);
                    }
                }
                current_event = oneevent.id;
                oneevent.borderColor = "red";
                CALENDAR.fullCalendar('updateEvent', oneevent);
                checkEditButton();
            },
            eventDragStart: function (oneevent, jsEvent, ui, view) {
                // console.log('eventDragStart:', {event: oneevent, jsEvent: jsEvent, ui: ui, view: view});
            },
            eventResize: function (oneevent, delta, revertFunc) {
                var newEvent = simplifyEventObject(oneevent);
                newEvent.mode = 'resize';

                // console.log('newEvent eventResize fullcalendar BEFORE:', newEvent);
                $.when(
                    $.ajax({
                        method: 'POST',
                        url: URL,
                        data: {event: newEvent},
                    }).fail(function (err) {
                        // console.log('Event eventResize fullcalendar ERROR:', err);
                        $('#devwin').append($(err.responseText));
                        alert('Une erreur est survenue : annulation.');
                        revertFunc();
                    })
                ).done(function (data) {
                    // console.log('Event eventResize fullcalendar RESULT parsed:', data);
                    if (data.result === false) {
                        alert(data.message);
                        revertFunc();
                    } else {
                        // CALENDAR.fullCalendar('updateEvent', data.data);
                    }
                    CALENDAR.fullCalendar('refetchEvents');
                });
            },
            eventDrop: function (oneevent, delta, revertFunc) {
                var newEvent = simplifyEventObject(oneevent);
                newEvent.mode = 'move';

                // console.log('newEvent eventDrop fullcalendar BEFORE:', newEvent);
                $.when(
                    $.ajax({
                        method: 'POST',
                        url: URL,
                        data: {event: newEvent},
                    }).fail(function (err) {
                        // console.log('Event eventDrop fullcalendar ERROR:', err);
                        $('#devwin').append($(err.responseText));
                        alert('Une erreur est survenue : annulation.');
                        revertFunc();
                    })
                ).done(function (data) {
                    // console.log('Event eventDrop fullcalendar RESULT parsed:', data);
                    if (data.result === false) {
                        alert(data.message);
                        revertFunc();
                    } else {
                        // CALENDAR.fullCalendar('updateEvent', data.data);
                    }
                    CALENDAR.fullCalendar('refetchEvents');
                });
            },
            eventReceive: function (oneevent) {
                var
                    tempEvent = oneevent.id,
                    newEvent = simplifyEventObject(oneevent);
                CALENDAR.fullCalendar('removeEvents', oneevent.id);
                var view = CALENDAR.fullCalendar('getView');
                newEvent.allDay = view.intervalUnit === 'month';
                // mode
                newEvent.mode = 'new';

                // console.log('Event eventReceive fullcalendar BEFORE:', {newEvent: newEvent, tempEvent: tempEvent});
                $.ajax({
                    method: 'POST',
                    url: URL,
                    data: {event: newEvent},
                })
                .fail(function (err) {
                    // console.log('Event eventReceive fullcalendar ERROR:', err);
                    // $('body').append($(err.responseText));
                    alert('Une erreur est survenue : annulation.');
                })
                .done(function (data) {
                    // console.log('Event eventReceive fullcalendar RESULT:', {aeReponse: data, tempEvent: tempEvent, PHPdata: JSON.parse(data.message)});
                    if (data.result === false) {
                        alert(data.message);
                    } else {
                        // CALENDAR.fullCalendar('renderEvent', data.data, true);
                        CALENDAR.fullCalendar('refetchEvents');
                    }
                });
            },
            filterResourcesWithEvents: true,
            eventDataTransform: function (eventData) {
                camelizeAllDay(eventData);
                // console.log('eventDataTransform', eventData);
                return eventData;
            },
            events: eventFeed,
        });

        // TRASH & TRASH SELECTED
        $('#trash').on('click', function (item) {
            if (current_event === null) {
                if (confirm('ATTENTION : tous les éléments de cet agenda vont être effacés !\nTout effacer ?')) { // removeAll
                    $.ajax({
                        method: 'POST',
                        url: URL,
                        data: {global: 'removeAll'},
                    })
                    .fail(function (err) {
                        // console.log('removeAll ERROR:', err);
                        alert('Une erreur est survenue : annulation.');
                    })
                    .done(function (data) {
                        // console.log('removeAll RESULT parsed:', data);
                        if (data.result === true) CALENDAR.fullCalendar('removeEvents');
                        alert(data.message);
                    });
                    // CALENDAR.fullCalendar('refetchEvents'):
                }
            } else {
                var
                    oneevent = CALENDAR.fullCalendar('clientEvents', current_event)[0],
                    newEvent = simplifyEventObject(oneevent);
                newEvent.mode = 'remove';
                $.when(
                    $.ajax({
                        method: 'POST',
                        url: URL,
                        data: {oneevent: newEvent},
                    }).fail(function (err) {
                        // console.log('Event remove fullcalendar ERROR:', err);
                        $('#devwin').append($(err.responseText));
                        alert('Une erreur est survenue : annulation.');
                    })
                ).done(function (data) {
                    // console.log('Event remove fullcalendar RESULT parsed:', data);
                    if (data.result === false) {
                        alert(data.message);
                    } else {
                        // alert(data.message);
                        CALENDAR.fullCalendar('removeEvents', oneevent.id);
                    }
                    // CALENDAR.fullCalendar('refetchEvents');
                });
            }
        });
        $(CALENDAR).on('mouseleave', function (item) {
            if (current_event !== null) {
                $.when($('body').delay(3000)).done(function () {
                    if (current_event !== null) {
                        var oneevent = CALENDAR.fullCalendar('clientEvents', current_event)[0];
                        if ($.type(oneevent) === 'object') {
                            oneevent.borderColor = 'transparent';
                            CALENDAR.fullCalendar('updateEvent', oneevent);
                        }
                    }
                    current_event = null;
                    checkEditButton();
                });
            }
        });

        // REFRESH CALENDAR
        $('#refresh_calendar').on('click', function (item) {
            $('> i.fa', $(this)).addClass('fa-spin');
            $.when(CALENDAR.fullCalendar('refetchEvents')).done(function () {
                $('> i.fa', $(this)).removeClass('fa-spin');
            });
        });

        $('.fc-button-group > button.fc-button').on('click', function (item) { displayView(); });
        displayView();

    }





});










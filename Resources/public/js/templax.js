
/************************************************************/
/* TEMPLAX v1.3                                             */
/* by Emmanuel Dujardin                                     */
/* creation 2017/01                                         */
/*                                                          */
/************************************************************/

var templax = function (html, devmode) {

	var
		parent_pla = this,

		html = html === undefined ? 'body' : html,
		$current_main_html = null,
		devmode = devmode === undefined ? false : devmode,
		$main_html = null,
		initialized = false,
		flags = false,

		ajax_LoadData = {},
		max_ajax_LoadData = 3,

		is_balise = /^\n*\[\[\s*([-_;:,'"&\+\*\/\w.\(\)\[\]]+)\s*\]\]\n*$/,
		has_balise = /\[\[/g,
		strip_balises = /^(\n*\[\[\s*)(.+[^\s])(\s*\]\]\n*)$/,
		search_balises = /\[\[(.+)\]\]/g,
		search_vars = /\[\[\s*([-_;:,'"&\+\*\/\w.\(\)\[\]]+)\s*\]\]/g, // --->  temporaire : n'accepte pas les espaces :-( !!!
		RegExp1 = /(\w+(\.\w+)*)(\.\w+[^\W])/g,
		RegExp2 = /("|')?([\w]+)((\.[\w]+)*)("|')?/g,
		base_ext = /(\w+)((\.\w+)*)/,

		forbidden_balises_body = new RegExp('^(body|html|head|meta|title|style|script)$', 'ig'),
		forbidden_balises = new RegExp('^(html|head|meta|title|style|script)$', 'ig'),

		errVar = '### var ###',
		waiting_index = '_#index#',
		defaultModelValue = null,
		noChangesReturnValueFromAjax = null // single value returned from ajax, indicates that there is no change in data returned
		pla_attr_params = {
			// public pla attributes
			'pla-repeat': 	{ model: false, empty: false, is_condition: false, stopWalk: false },
			'pla-if': 		{ model: false, empty: false, is_condition: true, stopWalk: true },
			'pla-if-not': 	{ model: false, empty: false, is_condition: true, stopWalk: true },
			'pla-enable': 	{ model: false, empty: false, is_condition: true, stopWalk: false },
			'pla-disable': 	{ model: false, empty: false, is_condition: true, stopWalk: false },
			// system pla attribures
			'pla-model': 	{ model: true, empty: true, is_condition: false, stopWalk: false },
		},
		pla_attr_names = Object.keys(pla_attr_params)
		;

	// devmode = false;



	/* BASICS / TOOLS */

	Number.prototype.format = function(n, x, s, c) {
		var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
			num = this.toFixed(Math.max(0, ~~n));
		return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s));
	};

	var timenow = function() {
		// http://www.finalclap.com/faq/158-javascript-date-heure-courante
		var now = new Date();
		return parseInt(now.getHours());
	};

	/* CRC32 */
	var
		doEncodeForVerif = true,
		makeCRCTable = function () {
			var c, crcTable = [];
			for (var n =0; n < 256; n++){
				c = n;
				for (var k =0; k < 8; k++){
					c = ((c&1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
				}
				crcTable[n] = c;
			}
		return crcTable;
		},

		crc32 = function (str) {
			if (!doEncodeForVerif) return JSON.stringify(str);
			var str = JSON.stringify(str);
			var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
			var crc = 0 ^ (-1);
			for (var i = 0; i < str.length; i++ ) {
				crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
			}
			return (crc ^ (-1)) >>> 0;
		}
	;

	/* VARS function */
	var vars_tool = function (parent_name) {
		// BEWARE : use nodeContext, so do not forget to actualise it with jQuery $item !
		var parent_vars = this;
		var parent_name = parent_name;
		var variables = {};
		var vars_alias = {};

		var init = function () {
			if (parent_name !== undefined) {
				if ($.type(parent_name) === 'string' && parent_name.length > 0) return;
			}
			console.error('New vars error:', 'You must define parent name (string) in parameter when creating a new vars instance!');
		}
		init();

		this.hasChanged = function (stringVar) {
			var var_name = stringVar.replace(base_ext, '$1');
			return variables[var_name].hasChanged();
		}

		this.hasSubChanged = function (stringVar) {
			var result = true;
			if (stringVar === undefined) return result;
			var var_name = stringVar.replace(base_ext, '$1');
			var var_sub = stringVar.replace(base_ext, '$2');
			// if (devmode) console.log('TEST SUB:', '"'+var_sub+'"');
			if (var_sub === '') return parent_vars.hasChanged(var_name);
			try {
				var result = variables[var_name].hasSubChanged(var_sub.replace(/^\./, ''));
			} catch (err) {
				// do nothing
			} finally {
				return result;
			}
			// return result;
		}

		var variable = function (name, new_variables) {
			var parentVariable = this;
			this.name = name;
			this.data = {};
			this.type = null;
			this.control = {
				global: { // global control
					changed: true,
					uniq_key: null,
				},
				sub: {}, // if object or array -> controls on first level
					// has :
					// - changed
					// - uniq_key
					// - sub
			};
			// gettes / setters
			this.hasChanged = function () { return this.control.global.changed; };
			this.hasSubChanged = function (subName) {
				// var subName = subName.replace(base_ext, '$1');
				var error = false;
				var ControlSub;
				var subName = subName.split('.');
				var joinedName = subName.join('.sub.');
				try {
					ControlSub = eval('this.control'+joinedName);
				} catch (err) {
					error = true;
				} finally {
					return error ? true : ControlSub.changed;
				}
			};
			this.computeChanges = function (new_variables, ControlSub) {
				var new_variables = new_variables === undefined ? [] : new_variables;
				var uniq_key;
				if (ControlSub === undefined) {
					this.type = $.type(new_variables);
					this.data = new_variables;
					// control
					uniq_key = crc32(new_variables);
					this.control.global.changed = this.control.global.uniq_key !== uniq_key;
					this.control.global.uniq_key = uniq_key;
					if ($.type(this.data) === 'array' || $.type(this.data) === 'object') {
						for (var vars in this.data) {
							if (this.control.sub[vars] === undefined) this.control.sub[vars] = {changed: true, uniq_key: null, sub: {}};
							this.computeChanges(this.data[vars], this.control.sub[vars]);
						}
					}
				} else {
					uniq_key = crc32(new_variables);
					ControlSub.changed = ControlSub.uniq_key !== uniq_key;
					ControlSub.uniq_key = uniq_key;
					if ($.type(new_variables) === 'array' || $.type(new_variables) === 'object') {
						for (var vars in new_variables) {
							if (ControlSub.sub[vars] === undefined) ControlSub.sub[vars] = {changed: true, uniq_key: null, sub: {}};
							this.computeChanges(new_variables[vars], ControlSub.sub[vars]);
						}
						var new_variables_keys = Object.keys(new_variables);
						for (var vars in ControlSub.sub) {
							if (!new_variables_keys.find(function (element) { return element === vars; })) delete ControlSub.sub[vars];
						}
					}
				}
			};
			this.setChangeOnAll = function (change, ControlSub) {
				var change = change !== false;
				if (ControlSub === undefined) {
					this.control.global.changed = change;
					if ($.type(this.control.sub) === 'array' || $.type(this.control.sub) === 'object')
						for (var vars in this.control.sub) this.setChangeOnAll(change, this.control.sub[vars]);
				} else {
					ControlSub.changed = change;
					if ($.type(ControlSub.sub) === 'array' || $.type(ControlSub.sub) === 'object')
						for (var vars in ControlSub.sub) this.setChangeOnAll(change, ControlSub.sub[vars]);
				}
			}
			this.setNewdata = function (new_variables) {
				// if no changes from ajax
				if (new_variables === noChangesReturnValueFromAjax) parentVariable.setChangeOnAll(false);
					else parentVariable.computeChanges(new_variables);
			}
			this.getCrc32 = function () { return crc32(parentVariable.data); }

			// INIT
			// initialize if new_variables is defined
			if (new_variables !== undefined) this.setNewdata(new_variables);
				else this.setNewdata([]);

		};

		this.getVar = function (name) {
			return variables[name] !== undefined ? variables[name].data : undefined;
		}
		this.getVarCrc32 = function (name) {
			return variables[name] !== undefined ? variables[name].getCrc32() : undefined;
		}
		/* full variables */
		this.getVariables = function () { return variables; };
		/* only data of variables */
		this.getVars = function () {
			var vars = new Array;
			for (var v in variables) vars[v] = variables[v].data;
			return vars;
		};
		/* var alias */
		this.getVarsAlias = function () { return $.extend(true, {}, vars_alias); }
		/* set data in variable name (create or replace if name exists) */
		this.setNamedVar = function (name, new_variables) {
			// init if not exists
			if (variables[name] === undefined) {
				variables[name] = new variable(name, new_variables);
				vars_alias[name] = name+'.data';
			} else {
				variables[name].setNewdata(new_variables);
			}
			// if (devmode) console.log('Set vars["'+name+'"]', {complete: variables[name], changed: variables[name].hasChanged()});
			if (devmode) console.log('Set named variables "'+name+'":', {crc32: this.getVarCrc32(name), variable: variables[name], control: variables[name].control});
			return parent_vars;
		};
		this.getVarNames = function (index) {
			if (index === undefined) return Object.keys(variables);
			var variableByIndex = [];
			for (var name in Object.keys(variables)) {
				try {
					variableByIndex = eval('variables.'+name+'.data.'+index);
				} catch (err) {
					// do nothing…
				} finally {
					if ($.type(variableByIndex) === 'object' || $.type(variableByIndex) === 'array') return Object.keys(variableByIndex);
				}
			}
			return variableByIndex;
		};

		/**
		 * Get vars_alias by data of nodeContext (or repeatParams of nodeContext)
		 * @param object repeatParams
		 * @return object
		 */
		this.getContextVarAlias = function (repeatParams) {
			var getParentAlias = function (repeatParams) {
				var sourceVar_base;
				var sourceVar_ext;
				var new_vars_alias;
				if ($.type(repeatParams.sourceVar) === 'string') {
					new_vars_alias = repeatParams.parent === null ? repeatParams.vars_alias : getParentAlias(repeatParams.parent);
					sourceVar_base = repeatParams.sourceVar.replace(base_ext, '$1');
					sourceVar_ext = repeatParams.sourceVar.replace(base_ext, '$2');
					if (Object.keys(new_vars_alias).find(function (element) { return element === sourceVar_base; }) !== undefined) {
						// new alias
						new_vars_alias[repeatParams.alias] = new_vars_alias[sourceVar_base]+sourceVar_ext+'["'+repeatParams.current_index+'"]';
					} else {
						if (devmode) console.error('Alias error: did not find source var "'+repeatParams.sourceVar+'" in "'+repeatParams.alias+'" of self or parents!', {repeatParams: repeatParams});
					}
				}
				return new_vars_alias;
			}
			if ($.type(repeatParams) === 'object') return getParentAlias(repeatParams);
				else return [];
		}

		this.replaceVarAsString = function (stringVar_2, nodeContext) {
			return stringVar_2.replace(RegExp2, function(m0, m1, m2, m3, m4, m5) {
				if (m1 !== undefined && m5 !== undefined) return m0;
				return nodeContext.repeatParams.vars_alias[m2] === undefined ? m0 : parent_name+'.getVariables().'+nodeContext.repeatParams.vars_alias[m2]+m3;
			});
		};

		/**
		 * Get result corresponding to model's expression, with nodeContext
		 * @param string stringVar_1
		 * @param object nodeContext
		 * @return mixed
		 */
		this.computeVariables = function (stringVar_1, nodeContext) { // ***** find variables *****
			// var stringVar_1 = stringVar_1;
			// var nodeContext = nodeContext;
			if ($.type(nodeContext) === 'object') {
				return parent_vars.replaceVarAsString(stringVar_1, nodeContext).replace(search_vars, function (m0, m1) {
					// index in repeat
					if (m1.match(/(parent\.loop\.index0)/g) && $.type(nodeContext.repeatParams) === 'object') {
						if ($.type(nodeContext.repeatParams.parent) === 'object') m1 = m1.replace(/(parent\.loop\.index0)/g, nodeContext.repeatParams.parent.loopindex);
					}
					if (m1.match(/(parent\.loop\.index)/g) && $.type(nodeContext.repeatParams) === 'object') {
						if ($.type(nodeContext.repeatParams.parent) === 'object') m1 = m1.replace(/(parent\.loop\.index)/g, nodeContext.repeatParams.parent.loopindex + 1);
					}
					if (m1.match(/(loop\.index0)/g) && $.type(nodeContext.repeatParams) === 'object') m1 = m1.replace(/(loop\.index0)/g, nodeContext.repeatParams.loopindex);
					if (m1.match(/(loop\.index)/g) && $.type(nodeContext.repeatParams) === 'object') m1 = m1.replace(/(loop\.index)/g, nodeContext.repeatParams.loopindex + 1);

					// find name in repeat parens
					var parentrepeatParams = $.extend(true, {}, nodeContext.repeatParams);
					while($.type(parentrepeatParams) === 'object') {
						if (parentrepeatParams.name !== null && m1 === parentrepeatParams.name) return parentrepeatParams.current_index;
						parentrepeatParams = parentrepeatParams.parent;
					}

					var result = undefined;
					var error = undefined;
					var new_test;
					var prefix_tests = ['','parent_pla.', 'parent_vars.'];
					for (var prefix in prefix_tests) {
						new_test = prefix_tests[prefix]+m1;
						// if (devmode) console.log('new_test', new_test);
						try { result = eval(new_test); }
						catch(err) { error = err; }
						finally {
							if (result !== undefined) return result;
						}
					}
					return errVar;
				});
			}
			return false;
		};

		this.computeVariablesResult = function (stringVar_1, nodeContext) {
			var computed = this.computeVariables(stringVar_1, nodeContext);
			if (computed === false || computed === errVar) return errVar;
			var result = undefined;
			var error = undefined;
			try { var result = eval(computed); }
			catch(err) { return errVar; }
			finally {
				return result;
			}
		}

	};



	/* TIMEOUT & DEGRAD */ 

	var SessionTimeouts_tool = function () {
		var parent_sts = this;
		var name_sts = 'all';
		var sessions = {};
		var activated_style = 'btn-success';

		this.createSession = function (params) {
			if (params === undefined) if (devmode) console.error('Error data creation:', 'You must define params (at least "params.name" and "params.action").');
			if ($.type(params) !== 'object') if (devmode) console.error('Error data creation:', 'params must be an object of parameters (with at least "params.name" and "params.action").');
			if (params.name === undefined) if (devmode) console.error('Error data creation:', 'You must define a name!');
			if (params.action === undefined) if (devmode) console.error('Error data creation:', 'You must define an action!');
			sessions[params.name] = new SessionTimeout(params);
			if (devmode) console.log('Create session:', {name: params.name, params: params})
			return sessions[params.name];
		}
		// this.removeSession = function (name) {
		// 	if (sessions[name] !== undefined) delete sessions[name];
		// 	return parent_sts;
		// }
		this.getSessionNames = function (name) { return Object.keys(sessions); }
		this.getSession = function (name) {
			return sessions[name] !== undefined ? sessions[name] : null;
		}
		this.getSessionsIndex = function () {
			var sessionsIndex = {};
			for (var sessionName in sessions) sessionsIndex[sessionName] = sessions[sessionName].getSessionIndex();
			return sessionsIndex;
		}

		this.getSessionIndexByName = function (name) {
			if (sessions[sessionName] !== undefined) return sessions[name].getSessionIndex();
			return null;
		}

		/* COMMANDS */
		var commands_start = function (name) {
			$('.pla-'+name+'-start', $('body')).each(function (item, e) {
				text = $(this).attr('pla-'+name+'-timelaps-enabled');
				$(this).addClass('pla-'+name+'-stop').removeClass('pla-'+name+'-start').addClass(activated_style).text(text === undefined ? 'Stop '+name : text);
			});
			return parent_sts;
		}
		var commands_stop = function (name) {
			$('.pla-'+name+'-stop', $('body')).each(function (item, e) {
				text = $(this).attr('pla-'+name+'-timelaps-disabled');
				$(this).addClass('pla-'+name+'-start').removeClass('pla-'+name+'-stop').removeClass(activated_style).text(text === undefined ? 'Start '+name : text);
			});
		}

		/* DEGRAD */
		this.enableAllDegrad = function () {
			for (var sessionName in sessions) sessions[sessionName].enableDegrad();
			return parent_sts;
		}
		this.disableAllDegrad = function () {
			for (var sessionName in sessions) sessions[sessionName].disableDegrad();
			return parent_sts;
		}
		this.restoreDegrad = function (sessionName) {
			var sessionName = sessionName === undefined ? 'all' : sessionName;
			if (sessionName == 'all') {
				for (var allSessionName in sessions) sessions[allSessionName].restoreDegrad();
				// if (devmode) console.log('• Restore degrad:', 'session '+sessionName);
			} else if (sessions[sessionName] !== undefined) {
				sessions[sessionName].restoreDegrad();
				// if (devmode) console.log('• Restore degrad:', 'session '+sessionName);
			} else {
				// if (console.error('Error restoreDegrad:', JSON.stringify(sessionName)+' does not exist!'));
			}
			return parent_sts;
		}
		this.restoreAllDegrad = function () {
			return parent_sts.restoreDegrad();
		}

		/* ALL SESSIONS */

		this.startAll = function (new_timelaps, now) {
			// var now = now === undefined ? false : now;
			commands_start(name_sts);
			for (var sessionName in sessions) sessions[sessionName].start(new_timelaps, now);
			return parent_sts;
		}

		this.stopAll = function () {
			commands_stop(name_sts);
			for (var sessionName in sessions) sessions[sessionName].stop();
			return parent_sts;
		}

		this.verifAllStopped = function () {
			var allStopped = true;
			for (var sessionName in sessions) if (sessions[sessionName].isStart()) allStopped = false;
			allStopped ? commands_stop(name_sts) : commands_start(name_sts);
		}

		/* Define a session */

					var SessionTimeout = function (params) {
						var parent_st = this;
						// data parameters defined by params
						var name = params.name;
						var action = params.action;
						var defaultTimelaps = !isNaN(parseInt(params.defaultTimelaps)) ? params.defaultTimelaps : 3000;
						var minTimelaps = !isNaN(parseInt(params.minTimelaps)) ? params.minTimelaps : 1000;
						var maxTimelaps = !isNaN(parseInt(params.maxTimelaps)) ? params.maxTimelaps : 180000;
							// degrad : slow ajax while long time inactivity
						var degrad_status = degrad_status !== undefined ? degrad_status : true;
						var degrad_session_limit = !isNaN(parseInt(params.degrad_session_limit)) ? params.degrad_session_limit : 10; // calculated with modulo on session
						var degrad_increment = !isNaN(parseInt(params.degrad_increment)) ? params.degrad_increment : 1000;
						var degrad_max = !isNaN(parseInt(params.degrad_max)) ? params.degrad_max : 180000;
						// Object variables
						var session = 0;
						var session_degrad = 0;
						var timeout = null;
						var timelaps = null;
						var saved_timelaps = null;
						// time during refresh operation
						var lasts = null;
						
						this.enableDegrad = function () { degrad_status = true; parent_st.restoreDegrad(); return parent_st; };
						this.disableDegrad = function () { degrad_status = false; return parent_st; };
						this.isStart = function () { return timeout !== null; };
						this.isStop = function () { return timeout === null; };

						this.setAction = function (new_action) { action = new_action; };

						// PRIVATE
						var applyDegrad = function () {
							session_degrad++;
							if (degrad_status && timeout !== null) {
								// degrag controls
								if ((session_degrad % degrad_session_limit) >= (degrad_session_limit - 1)) {
									if (saved_timelaps === null) saved_timelaps = timelaps;
									var old_timelaps = timelaps;
									var new_timelaps = timelaps + degrad_increment;
									if (new_timelaps > degrad_max) new_timelaps = degrad_max;
									if (new_timelaps !== old_timelaps) {
										parent_st.start(new_timelaps, false);
										// if (devmode) console.log('Degrad '+name, 'new timelaps is '+timelaps);
									}
								}
							}
							return parent_st;
						};
						this.restoreDegrad = function () {
							if (!isNaN(saved_timelaps) && timeout !== null) {
								if (saved_timelaps !== timelaps) {
									session_degrad = 0;
									parent_st.setTimelaps(saved_timelaps, true);
									saved_timelaps = null;
								}
							}
						}
						// PUBLIC
						this.getSessionIndex = function () { return session; };
						this.incrementSession = function () {
							session++;
							if (isNaN(session)) session = 0; // prevent session overflow…
							applyDegrad();
							flagSession();
							return parent_st;
						};

						this.setTimelaps = function (new_timelaps, now) {
							var now = now === undefined ? false : now;
							var new_timelaps = parseInt(new_timelaps);
							timelaps = isNaN(new_timelaps) ? timelaps : new_timelaps;
							if (timelaps < minTimelaps || isNaN(timelaps)) timelaps = minTimelaps;
							if (timelaps > maxTimelaps) timelaps = maxTimelaps;
							if (now) parent_st.start();
							flagTimelaps();
							return parent_st;
						};
						this.getTimelaps = function () { return timelaps; };
						this.getTimelapsAsSeconds = function () { return parseInt(timelaps / 1000); };

						this.refresh = function () {
							var d = new Date();
							var start_time = d.getTime();

							eval(action);
							parent_st.incrementSession();

							d = new Date();
							var ends_time = d.getTime();
							lasts = ends_time - start_time;
							flaglasts();

							return parent_st;
						}

						this.start = function (new_timelaps, now) {
							var now = now === undefined ? false : now;
							commands_start(name);
							parent_st.setTimelaps(new_timelaps, false);
							// define timeout
							if (timeout !== null) clearInterval(timeout);
							if (now) parent_st.refresh();
							timeout = setInterval(function () { parent_st.refresh(); }, timelaps);
							parent_sts.verifAllStopped();
							flagTimelaps();
							return parent_st;
						}

						this.stop = function () {
							commands_stop(name);
							if (timeout !== null) clearInterval(timeout);
							timeout = null;
							flagTimelaps();
							lasts = null;
							flaglasts();
							parent_sts.verifAllStopped();
							return parent_st;
						}

						/* FLAGS */
						var flagSession = function () {
							if (flags) $('.'+name+'_session', $('body')).text(session); // Math.floor((Math.random() * 9999) + 1)
						}
						var flagTimelaps = function () {
							if (flags) $('.'+name+'_timelaps', $('body')).text((!isNaN(timelaps) && timeout !== null) ? timelaps/1000+'s.' : '___');
						}
						var flaglasts = function () {
							if (flags) $('.'+name+'_lasts', $('body')).text((!isNaN(lasts) && lasts !== null) ? lasts/1000+'s.' : '___');
						}

						var initSession = function () {
							/* INIT SESSION */
							if (timelaps == null) parent_st.setTimelaps(defaultTimelaps, false);
							$('body').on('click', '.pla-'+name+'-start', function (event) {
								var new_timelaps = $(this).attr('pla-'+name+'-timelaps-value');
								new_timelaps = new_timelaps === undefined ? timelaps : parseInt(new_timelaps);
								parent_st.start(new_timelaps, true);
							});
							$('body').on('click', '.pla-'+name+'-stop', function (event) {
								parent_st.stop();
							});
							$('body').on('click', '.pla-'+name+'-timelaps-define', function (event) {
								new_timelaps = parseInt($(this).attr('pla-'+name+'-timelaps-value'));
								if (!isNaN(new_timelaps)) parent_st.setTimelaps(new_timelaps, true);
							});
						}
						initSession();

					}

		var initSessions = function () {
			/* ALL */
			$('body').on('click', '.pla-'+name_sts+'-start', function (event) {
				var new_all_timelaps = $(this).attr('pla-'+name_sts+'-timelaps-value');
				if (new_all_timelaps !== undefined) new_all_timelaps = parseInt(new_all_timelaps);

				if (Object.keys(sessions).length > 0) {
					commands_start(name_sts);
					for (var sessionName in sessions) {
						if (isNaN(new_all_timelaps)) {
							var new_timelaps = $(this).attr('pla-'+sessionName+'-timelaps-value');
							if (new_timelaps !== undefined) new_timelaps = parseInt(new_timelaps);
						} else {
							new_timelaps = new_all_timelaps;
						}
						sessions[sessionName].start(new_timelaps, true);
					}
				}
			});
			$('body').on('click', '.pla-'+name_sts+'-stop', function (event) {
				parent_sts.stopAll();
			});
		}
		initSessions();
	}



	/* FLAGS UPDATES */

	var flagError = function (string, add) {
		if (flags) {
			var add = add == undefined ? false : add;
			if (add === false) $('.pla-errors', $('body')).html($('<div style="color:darkred;background-color:orange;padding:20px;">'+string+'</div>'));
				else $('.pla-errors', $('body')).append($('<div style="color:darkred;background-color:orange;padding:20px;">'+string+'</div>'));
		}
	}

	var flagErrorRemove = function () {
		$('.pla-errors', $('body')).empty();
	}


	/* TIMEOUT ACTIONS */

	var refreshTemplate = function ($htmlItem) {
		// if (devmode) console.log('Refresh template…');
		var $htmlItem = $htmlItem === undefined ? $main_html : $htmlItem;
		var parent_rtpl = this;

		var isValidPlarepeat = function ($item) {
			var attrPla = $item.attr('pla-repeat');
			if (attrPla === undefined || $item.get(0).nodeName.match(forbidden_balises_body) !== null) {
				$item.removeAttr('pla-repeat');
				return false;
			}
			if (!attrPla.match(/^(\w+\s*,\s*)?\w+\s+in\s+(\w+)(\.\w+)*$/)) {
				$item.removeAttr('pla-repeat');
				return false;
			}
			return true;
		};

		var repeats = function (nodeContext) {
			var $clone;
			var nodeContext = nodeContext;
			// if ($.type(nodeContext) === 'object') {
				if (!nodeContext.repeatParams.is_plarepeat_updated()) {
					// init clone in params
					if (nodeContext.repeatParams.repeat_model === undefined) {
						$clone = $($.clone(nodeContext.$item.get(0)))
							.removeAttr('id')
							.attr('pla-clone', nodeContext.repeatParams.id)
							.removeAttr('pla-repeat')
							;
						$('*', $clone).each(function () {
							$(this).removeAttr('id');
							// $.removeData($(this), 'nodeContext');
						});
						nodeContext.repeatParams.repeat_model = $clone.get(0);
					}
					try {
						var sourceVar = eval(vars_master.replaceVarAsString(nodeContext.repeatParams.sourceVar, nodeContext));
					} catch (err) {
						sourceVar = errVar;
						if (devmode) console.error('Repeat error: var reference "'+nodeContext.repeatParams.sourceVar+'" does not exist!', {params: nodeContext.repeatParams});
					} finally {
						var index = 0;
						if (($.type(sourceVar) === 'array' || $.type(sourceVar) === 'object')) {
							// if (devmode) console.log('   • sourceVar during repeats:', {sourceVar: $.extend(true, {}, sourceVar)});
							if (Object.keys(sourceVar).length < 1) {
								// aucune donnée
								nodeContext.$item.hide().attr('pla-clone-loop', index).attr('pla-clone-name', 'undefined');
							} else {
								for (var name in sourceVar) {
									// clone
									if (index === 0) {
										$clone = nodeContext.$item;
										$clone.show();
										nodeContext.repeatParams.current_index = name;
										nodeContext.repeatParams.loopindex = index;
										$clone.attr('pla-clone-loop', index);
										$clone.attr('pla-clone-name', name);
										$clone.attr('pla-refresh-session', REFRESHtimeout.getSessionIndex());
										for (var vars_name in nodeContext.repeatParams.vars_alias)
											nodeContext.repeatParams.vars_alias[vars_name] = nodeContext.repeatParams.vars_model[vars_name].replace(new RegExp('\\["'+waiting_index+'"\\]$'), '["'+name+'"]');
										nodeContext.saveData();
									} else {
										$old_clone = $clone;
										$clone = $(nodeContext.$item.parent().find('[pla-clone="'+nodeContext.repeatParams.id+'"]:eq('+(index-1)+')'));
										if (!$clone.length) {
											$clone = $($.clone(nodeContext.repeatParams.repeat_model));
											$old_clone.after($clone);
											// computeBalises($clone, false);
											// if (devmode) console.log('--------- new repeat item "'+name+'":', $.extend(true, {}, $clone.data('nodeContext')));
										}
										$clone.attr('pla-clone-loop', index);
										$clone.attr('pla-clone-name', name);
										$clone.attr('pla-refresh-session', REFRESHtimeout.getSessionIndex());
									}
									index++;
								}
							}
						} else {
							if (devmode) console.error('Repeating error: var reference "'+nodeContext.repeatParams.sourceVar+'" is not an array or object!', {params: nodeContext.repeatParams});
						}
						// detach all non updated elements
						nodeContext.$item.parent().find('[pla-clone="'+nodeContext.repeatParams.id+'"][pla-refresh-session!="'+nodeContext.repeatParams.actual_session+'"]').detach();
					}
				}
			// }
		};

		var getBaseRepeatParams = function (id) {
			return {
				id: 				id,
				actual_session:		REFRESHtimeout.getSessionIndex(),
				parent:				null,
				vars_alias:			vars_master.getVarsAlias(),
				current_index:		waiting_index, // for replace '_#index#' (waiting_index)
				repeat_session:		null,
				plarepeat:			true,
				updated:			false,
				is_plarepeat:		function () { return this.plarepeat; },
				// pla-repeat updated
				is_plarepeat_updated: function () {
					this.updated = this.actual_session === this.repeat_session;
					return this.updated;
				},
				plarepeat_update: function () {
					this.repeat_session = this.actual_session;
					return this.is_plarepeat_updated();
				},
			}
		}

		/**
		 * 
		 * @return object / null if not valid
		 */
		var computeRepeatParams = function (nodeContext) {
			var nodeContext = nodeContext;
			var parentParams;
			if (nodeContext.is_plarepeat()) {
				if (nodeContext.repeatParams === null) {
					nodeContext.repeatParams = getBaseRepeatParams(nodeContext.id);
				} else {
					nodeContext.repeatParams.current_index = waiting_index;
					// nodeContext.repeatParams.updated = false;
					nodeContext.repeatParams.actual_session = REFRESHtimeout.getSessionIndex();
				}
				if (!nodeContext.repeatParams.is_plarepeat_updated()) {
					// element pla-repeat
					nodeContext.repeatParams.complete = nodeContext.$item.attr('pla-repeat');
					values = nodeContext.repeatParams.complete.split(/\s+in\s+/);
					attribs = values[0].split(',');
					if (attribs[1] === undefined) { nodeContext.repeatParams.name = null; nodeContext.repeatParams.alias = attribs[0]; }
						else { nodeContext.repeatParams.name = attribs[0]; nodeContext.repeatParams.alias = attribs[1]; }
					nodeContext.repeatParams.sourceVar = values[1];
					if (values.length < 2) return null;
					// parent
					nodeContext.repeatParams.parent = null;
					var parent_repeat = nodeContext.$item.parent().closest('[pla-clone],[pla-repeat]');
					if (parent_repeat.length) {
						// find parent
						nodeContext.repeatParams.parent = $.extend(true, {}, $(parent_repeat).data('nodeContext').repeatParams);
					}
					nodeContext.repeatParams.vars_alias = vars_master.getContextVarAlias(nodeContext.repeatParams);
					nodeContext.repeatParams.vars_model = $.extend(true, {}, nodeContext.repeatParams.vars_alias);
					repeats(nodeContext);
					// update and save…
					nodeContext.repeatParams.plarepeat_update();
					nodeContext.saveData();
				}
				return nodeContext.repeatParams;
			}
			return null;
		};

		var refreshNodeContext = function ($item) {
			var $item = $item;
			var oldContext = $item.data('nodeContext');
			var nodeContext_model = {
				$item: null,
				id: null,
				nodeName: null,
				nodeType: null,
				// conditions
				valid: true,
				continueCompute: true,
				// model
				models: [],
				// session
				item_session: null,
				actual_session: REFRESHtimeout.getSessionIndex(),
				// has pla condition
				placonditions: {},
				plaattributes: {},
				plarepeat: false,
				repeatParams: null,
				placlone: false, // false ou id du parent pla-repeat
				// functions
				is_valid: function () {
					return this.valid;
				},
				is_continueCompute: function () {
					return this.continueCompute;
				},
				has_models: function () {
					return this.models.length > 0;
				},
				// pla-repeat
				is_plarepeat: function () {
					return this.plarepeat;
				},
				// pla-clone
				is_placlone: function () {
					return this.placlone !== false;
				},
				// save nodeContext in $item data
				saveData: function () {
					this.$item.data('nodeContext', this);
					// this.$item.css('borderLeft', "3px solid pink");
				},
				// remove nodeContext from $item data
				removeData: function () {
					this.$item.data('nodeContext', undefined);
					// this.$item.css('borderLeft', "none");
				},
				getModel: function (index) {
					return this.models[index] !== undefined ? this.models[index] : null;
				},
				addModel: function (model, index, replace) {
					var replace = replace === undefined ? true : replace;
					if (this.models[index] === undefined) {
						this.models[index] = {model: model, last_value: defaultModelValue};
					} else if (replace) {
						this.models.splice(index, 1, {model: model, last_value: defaultModelValue});
					} else {
						this.models.splice(index, 0, {model: model, last_value: defaultModelValue});
					}
					// this.save();
					return this;
				},
				setModelLastValue: function (value, index) {
					if (this.models[index] !== undefined) {
						this.models[index].last_value = value;
					}
				},
				removeModel: function (index) {
					if (this.models[index] !== undefined) this.models.splice(index, 1);
					return this;
				},
				preComputeConditions: function () {
					var conditionCode;
					for (var condition in pla_attr_params) if (pla_attr_params[condition].is_condition) {
						conditionCode = this.$item.attr(condition);
						if (conditionCode !== undefined) this.placonditions[condition] = conditionCode;
							else delete this.placonditions[condition];
					}
				},
				preComputeAttributes: function () {
					var attributes = this.$item.get(0).attributes;
					for (var attr in attributes) if ($.type(attributes[attr].value) === 'string') {
						if (attributes[attr].value.match(search_balises)) {
							// if (devmode) console.log('--- Attr "'+attributes[attr].name+'" of '+this.$item.attr('id')+':', attributes[attr].value);
							this.plaattributes[attributes[attr].name] = attributes[attr].value;
						}
					}
					for (var name in this.plaattributes) if (!this.$item.attr(name) === undefined) delete this.plaattributes[name];
				},
				// pla-condition
				has_placonditions: function () {
					return Object.keys(this.placonditions).length > 0;
				},
				// pla-attributes
				has_plaattributes: function () {
					return Object.keys(this.plaattributes).length > 0;
				},
				preComputeModels: function () {
					var parenthis = this;
					var counter = 0;
					var search;
					this.$item.contents()
						.filter(function () { return this.nodeType === 3; })
						.each(function () {
							search = this.textContent.match(search_vars);
							if ($.type(search) === 'array')
								if (search.length > 0) parenthis.addModel(this.textContent, counter);
							counter++;
						});
				},
				checkValidity: function (fast) {
					var fast = fast === undefined ? false : fast;
					var forbidden = null;
					if (!fast) {
						this.nodeName = this.$item.get(0).nodeName; // node name
						forbidden = this.nodeName.match(forbidden_balises);
						this.nodeType = this.$item.get(0).nodeType; // node type
						this.plarepeat = isValidPlarepeat(this.$item);
						this.placlone = this.$item.attr('pla-clone') !== undefined ? this.$item.attr('pla-clone') : false;
						this.preComputeModels();
						this.preComputeConditions();
						this.preComputeAttributes();
					}
					this.valid = (this.is_plarepeat() || this.is_placlone() || this.has_models() || this.has_placonditions() || this.has_plaattributes()) && this.nodeType === 1 && forbidden === null;
					this.continueCompute = forbidden === null;
					return this.is_valid();
				},
				// updated
				update: function () { this.item_session = this.actual_session; },
				is_updated: function () { return this.item_session === this.actual_session; },
				// compute
				computeModels: function () {
					var parenthis = this;
					var new_value;
					// attributes
					if (this.has_plaattributes()) {
						// if (devmode) console.log('--- Compute attributes:', this.plaattributes);
						for (var name in this.plaattributes) {
							new_value = vars_master.computeVariables(this.plaattributes[name], parenthis);
							// if (devmode) console.log('   --- Compute attribute "'+name+'":', {value: this.plaattributes[name], new_value: new_value, nodeContext: $.extend(true, {}, parenthis)});
							if (new_value !== errVar) this.$item.attr(name, new_value);
						}
					}
					var counter = 0;
					var itemModel;
					this.$item.contents()
						.filter(function () { return this.nodeType === 3; })
						.each(function () {
							itemModel = parenthis.getModel(counter);
							if (itemModel !== null) {
								new_value = vars_master.computeVariables(itemModel.model, parenthis);
								if (new_value !== itemModel.last_value) $(this).replaceWith(new_value);
								// if (devmode) console.log('ComputeModels:', {model: itemModel.model, new_value: new_value, nodeContext: parenthis});
								parenthis.setModelLastValue(new_value, counter);
							}
							counter++;
						});
				},
				refresh: function ($item) {
					// Fill conditions of validity
					this.$item = $item;
					var cloneParent;
					if (this.checkValidity()) {
						// is valid, so…
						this.id = this.$item.attr('id');
						if (this.is_plarepeat()) {
							// pla-repeat
							computeRepeatParams(this);
							// if (devmode) console.log('*** repeat item:', {item: this.$item.get(0), nodeContext: $.extend(true, {}, this)});
							if (this.repeatParams === null) {
								// if not a valid pla-repeat…
								this.$item.removeAttr('pla-repeat');
								if (!this.checkValidity(true)) return this.is_valid();
							}
						} else if (this.is_placlone()) {
							// pla-clone
							cloneParent = this.$item.closest('[pla-clone],[pla-repeat]');
							if (cloneParent.length) {
								var $repeatParent = $(cloneParent);
								if ($repeatParent.attr('pla-clone') !== undefined) {
									$repeatParent = $('#'+$repeatParent.attr('pla-clone'));
								}
								this.repeatParams = $.extend(true, {}, $repeatParent.data('nodeContext').repeatParams);
								this.repeatParams.current_index = $(cloneParent).attr('pla-clone-name');
								this.repeatParams.loopindex = parseInt($(cloneParent).attr('pla-clone-loop'));
								if (this.repeatParams.vars_model !== undefined) {
									for (var varalias in this.repeatParams.vars_model) this.repeatParams.vars_alias[varalias] = this.repeatParams.vars_model[varalias].replace(new RegExp('\\["'+waiting_index+'"\\]$'), '["'+this.repeatParams.current_index+'"]');
									delete this.repeatParams.vars_model;
								}
							}
						} else {
							// others…
							cloneParent = this.$item.closest('[pla-clone],[pla-repeat]');
							if (cloneParent.length) {
								var $repeatParent = $(cloneParent);
								if ($repeatParent.attr('pla-clone') !== undefined) {
									$repeatParent = $('#'+$repeatParent.attr('pla-clone'));
								}
								this.repeatParams = $.extend(true, {}, $repeatParent.data('nodeContext').repeatParams);
								this.repeatParams.current_index = $(cloneParent).attr('pla-clone-name');
								this.repeatParams.loopindex = parseInt($(cloneParent).attr('pla-clone-loop'));
								if (this.repeatParams.vars_model !== undefined) {
									for (var varalias in this.repeatParams.vars_model) this.repeatParams.vars_alias[varalias] = this.repeatParams.vars_model[varalias].replace(new RegExp('\\["'+waiting_index+'"\\]$'), '["'+this.repeatParams.current_index+'"]');
									delete this.repeatParams.vars_model;
								}
							} else {
								this.repeatParams = {vars_alias: vars_master.getVarsAlias()};
							}
						}
						// if (devmode) console.log('Before computeModels on item:', {item: this.$item.get(0), nodeContext: $.extend(true, {}, this)});
						this.computeModels();
						// and update session $item :
						this.update();
					}
					// else if (devmode) console.error('Invalid item:', {item: this.$item.get(0)});
					return this.is_valid();
				}
			};
			// init
			var nodeContext_data;
			if ($.type(oldContext) === 'object') {
				// existing nodeContext on $item
				oldContext.actual_session = REFRESHtimeout.getSessionIndex();
				if (oldContext.is_updated()) {
					oldContext.saveData();
					return oldContext;
				} else {
					nodeContext_data = $.extend(true, {}, oldContext);
				}
			} else {
				// new nodeContext on $item
				// $item.uniqueId();
				nodeContext_data = $.extend(true, {}, nodeContext_model);
			}
			// refresh
			nodeContext_data.refresh($item) ? nodeContext_data.saveData() : nodeContext_data.removeData();
			// beware : can continue compute if not valid !! (for computing valid children)
			return nodeContext_data.is_continueCompute();
		}

		var computeBalises = function ($items) {
			// if (devmode) console.log('---> computeBalises items:', {items: $items, type: $.type($items)});
			var $item;
			$items.each(function () {
				$item = $(this);
				// if (devmode) console.log('   ---> computeBalises item:', {item: $item, type: $.type($item)});
				$item.uniqueId();
				id = '#'+$item.attr('id');
				result = refreshNodeContext($item);
				// if (devmode) console.log('    ---> computeBalises on ', {id: $item.attr('id'), item: $items[i], 'continue': result});
				if (result !== false) {
					computeBalises($(id+' > [pla-repeat]'));
					computeBalises($('> *:not([pla-repeat])', $item));
				}
			});
			return parent_rtpl;
		}

		/**
		 * Search condition-balises in element and all his children, and do action
		 * @param JQuery $item = $current_main_html
		 * @return refreshTemplate
		 */
		var plaConditions = function ($item) {
			var $item = $item === undefined ? $current_main_html : $item;
			var test_if;
			// pla-if / pla-if-not
			$('[pla-if]', $item).each(function (item) {
				test_if = vars_master.computeVariablesResult($(this).attr('pla-if'), $(this).data('nodeContext'));
				// if (devmode) console.log('Test pla-if:', {result: test_if, item: this});
				test_if ? $(this).show().attr('pla-status', true) : $(this).hide().attr('pla-status', false);
			});
			$('[pla-if-not]', $item).each(function (item) {
				test_if = vars_master.computeVariablesResult($(this).attr('pla-if-not'), $(this).data('nodeContext'));
				// if (devmode) console.log('Test pla-if-not:', {result: test_if, item: this});
				test_if ? $(this).hide().attr('pla-status', false) : $(this).show().attr('pla-status', true);
			});

			// pla-enable / pla-disable
			$('[pla-enable]', $item).each(function (item) {
				test_if = vars_master.computeVariablesResult($(this).attr('pla-enable'), $(this).data('nodeContext'));
				// if (devmode) console.log('Test pla-enable:', {result: test_if, item: this});
				test_if ? $(this).removeClass('disabled').removeAttr('disabled').attr('pla-status', true) : $(this).addClass('disabled').attr('disabled', 'disabled').attr('pla-status', false);
			});
			$('[pla-disable]', $item).each(function (item) {
				test_if = vars_master.computeVariablesResult($(this).attr('pla-disable'), $(this).data('nodeContext'));
				// if (devmode) console.log('Test pla-disable:', {result: test_if, item: this});
				test_if ? $(this).addClass('disabled').attr('disabled', 'disabled').attr('pla-status', false) : $(this).removeClass('disabled').attr('disabled', 'disabled').attr('pla-status', true);
			});
		}

		computeBalises($htmlItem);
		// pla-conditions
		plaConditions($htmlItem);

		return parent_pla;
	};

	var ajaxLoad = function (do_refresh) { // false par défaut
		var do_refresh = do_refresh === undefined ? false : do_refresh;
		var params = [];
		var idx = 0;
		var testReturn = function (data) {
			var data = data;
			if ($.type(data) === 'string') {
				data = JSON.parse(data);
				if (devmode) console.log('*** Ajax return data:', data);
				if (data['data'][0] === "no-changes") return false;
				for (var i in data['data']) {
					data['data'][i] = JSON.parse(data['data'][i]);
					if (devmode) console.log('Parse result data.'+i+':', $.extend(true, {}, data['data'][i]));
				}
			}
			return data;
		}
		if (Object.keys(ajax_LoadData).length < 1 && do_refresh) {
			REFRESHtimeout.refresh();
		} else {
			for (var name in ajax_LoadData) {
				params[idx] = {
					url: ajax_LoadData[name].url,
					method: ajax_LoadData[name].method,
					data: ajax_LoadData[name].data,
					name: name,
					ajax_session: AJAXtimeout.getSessionIndex(),
				};
				idx++;
			}
			if (Object.keys(ajax_LoadData).length == 1) {
				$.when($.ajax(params[0]))
					.done(function (r1) {
						r1 = testReturn(r1);
						if (devmode) console.log('*** Ajax return parsed (1):', $.extend(true, {}, r1));
						if (params[0] !== undefined && r1) vars_master.setNamedVar(params[0].name, r1.data);
						// refresh
						var changed = vars_master.getVar(params[0].name).hasChanged;
						if (do_refresh && changed) REFRESHtimeout.refresh();
					});
			} else if (Object.keys(ajax_LoadData).length == 2) {
				$.when($.ajax(params[0]), $.ajax(params[1]))
					.done(function (r1,r2) {
						r1 = testReturn(r1);
						r2 = testReturn(r2);
						if (devmode) console.log('*** Ajax return (2):', {r1: r1, r2: r2});
						if (params[0] !== undefined && r1.data[0] !== "no-changes") vars_master.setNamedVar(params[0].name, r1.data);
						if (params[1] !== undefined && r2.data[0] !== "no-changes") vars_master.setNamedVar(params[1].name, r2.data);
						// refresh
						var changed = vars_master.getVar(params[0].name).hasChanged || vars_master.getVar(params[1].name).hasChanged;
						if (do_refresh && changed) REFRESHtimeout.refresh();
					});
			} else if (Object.keys(ajax_LoadData).length == 3) {
				$.when($.ajax(params[0]), $.ajax(params[1]), $.ajax(params[2]))
					.done(function (r1,r2,r3) {
						r1 = testReturn(r1);
						r2 = testReturn(r2);
						r3 = testReturn(r3);
						if (devmode) console.log('*** Ajax return (3):', {r1: r1, r2: r2, r3: r3});
						if (params[0] !== undefined && r1.data[0] !== "no-changes") vars_master.setNamedVar(params[0].name, r1.data);
						if (params[1] !== undefined && r2.data[0] !== "no-changes") vars_master.setNamedVar(params[1].name, r2.data);
						if (params[2] !== undefined && r3.data[0] !== "no-changes") vars_master.setNamedVar(params[2].name, r3.data);
						// refresh
						var changed = vars_master.getVar(params[0].name).hasChanged || vars_master.getVar(params[1].name).hasChanged || vars_master.getVar(params[2].name).hasChanged;
						if (do_refresh && changed) REFRESHtimeout.refresh();
					});
			}
		}
		return parent_pla;
	}




	////////////////////////
	// PUBLIC ACCESS
	////////////////////////

	/* Public direct access to vars methods and properties */

	this.getName = function () { return 'Templax' };
	this.getDescription = function () { return 'Javascript/Ajax templater.' };
	this.getVersion = function () { return '1.2' };
	this.isDevmode = function () { return devmode };

	this.refreshScreen = function () { REFRESHtimeout.refresh(); return parent_pla; };
	this.refreshAjax = function () { AJAXtimeout.refresh(); return parent_pla; };
	this.getRefreshSession = function () { return REFRESHtimeout.getSessionIndex(); };
	this.getAjaxSession = function () { return AJAXtimeout.getSessionIndex(); };

	this.disableAllDegrad = function () { SessionTimeouts.disableAllDegrad(); return parent_pla; };
	this.enableAllDegrad = function () { SessionTimeouts.enableAllDegrad(); return parent_pla; };
	// by session
	this.enableRefreshDegrad = function () { REFRESHtimeout.enableDegrad(); return parent_pla; };
	this.disableRefreshDegrad = function () { REFRESHtimeout.disableDegrad(); return parent_pla; };
	this.enableAjaxDegrad = function () { AJAXtimeout.enableDegrad(); return parent_pla; };
	this.disableAjaxDegrad = function () { AJAXtimeout.disableDegrad(); return parent_pla; };
	// binds
	this.addRestoreDegradBind = function (action, sessionName, target) { 
		var action = action === undefined ? 'click' : action;
		var sessionName = sessionName === undefined ? 'all' : sessionName;
		target = target === undefined ? '' : target.replace(/^\s*body\s+/i, '');
		$target = target.trim().length < 1 ? $('body') : $(target, $('body'));
		if ($target.length) {
			$target.bind(action, function () { SessionTimeouts.restoreDegrad(sessionName) });
		}
		return parent_pla;
	};

	this.getHtml = function () { return $main_html.html(); };
	this.getJQueryObject = function () { return $main_html; };

	this.startAll = function(new_timelaps, now) { SessionTimeouts.startAll(new_timelaps, now); };

	this.addAjaxLoad = function (url, name, data, method) {
		if ((Object.keys(ajax_LoadData).length >= max_ajax_LoadData) && (ajax_LoadData[name] === undefined)) {
			// Error. No more ajax data
			if (devmode) console.error('Max ajax loads', 'you can not add AjaxLoad. Maximum is 3.');
		} else {
			ajax_LoadData[name] = {
				url: url,
				method: method === undefined ? 'post' : method,
				data: data === undefined ? null : data,
			}
			// if (devmode) console.log('addAjaxLoad "'+name+'" '+Object.keys(ajax_LoadData).length+' :', ajax_LoadData[name]);
		}
		return parent_pla;
	};
	// ajax return value to indicate there is no changes in data with a single value
	this.setNoChangesReturnValueFromAjax = function (noChangesValue) {
		noChangesReturnValueFromAjax = noChangesValue;
		return parent_pla;
	}
	this.getNoChangesReturnValueFromAjax = function () {
		return noChangesReturnValueFromAjax;
	}

	this.setNamedVar = function (name, new_variables) { vars_master.setNamedVar(name, new_variables); return parent_pla; };
	this.getVar = function (name) { return vars_master.getVar(name); };
	this.getVars = function () { return vars_master.getVars(); };
	this.getVarNames = function (index) { return vars_master.getVarNames(index); };

	this.enableFlags = function () {
		$('.pla-flags-enable', $('body')).each(function (item, e) {
			text = $(this).attr('pla-flags-enabled');
			$(this).addClass('pla-flags-disable').removeClass('pla-flags-enable').addClass('btn-danger').text(text === undefined ? 'Disable flags' : text);
		});
		$('.pla-flags-disable', $('body')).each(function (item, e) {
			text = $(this).attr('pla-flags-enabled');
			$(this).addClass('btn-danger').text(text === undefined ? 'Disable flags' : text);
		});
		flags = true;
		flagErrorRemove();
		if (devmode) console.log('• flags:', 'activated');
		return parent_pla;
	};
	this.disableFlags = function () {
		$('.pla-flags-disable', $('body')).each(function (item, e) {
			text = $(this).attr('pla-flags-disabled');
			$(this).addClass('pla-flags-enable').removeClass('pla-flags-disable').removeClass('btn-danger').text(text === undefined ? 'Enable flags' : text);
		});
		$('.pla-flags-enable', $('body')).each(function (item, e) {
			text = $(this).attr('pla-flags-disabled');
			$(this).removeClass('btn-danger').text(text === undefined ? 'Enable flags' : text);
		});
		flags = false; flagErrorRemove();
		if (devmode) console.log('• flags:', 'disactivated');
		return parent_pla;
	};

	this.enableDevmode = function () {
		devmode = true;
		if (devmode) console.log('• dev mode:', 'activated');
		parent_pla.enableFlags();
		$('.pla-flags-disabled, .pla-flags-enable').show();
		return parent_pla;
	};
	this.disableDevmode = function () {
		devmode = false;
		if (devmode) console.log('• dev mode:', 'disactivated');
		parent_pla.disableFlags();
		$('.pla-flags-disabled, .pla-flags-enable').hide();
		return parent_pla;
	};



	////////////////////////
	// INITIALIZATION
	////////////////////////

	var vars_master = new vars_tool('vars_master');

	var SessionTimeouts = new SessionTimeouts_tool();
	var REFRESH_data = {
		name: 'refresh',
		action: 'refreshTemplate()',
		minTimelaps: 2000,
		maxTimelaps: 30000,
		degrad_session_limit: 10,
		degrad_increment: 1000,
		degrad_max: 8000, // 8 secondes
	}
	var REFRESHtimeout = SessionTimeouts.createSession(REFRESH_data);
	var AJAX_data = {
		name: 'ajax',
		action: 'ajaxLoad()',
		minTimelaps: 2000,
		maxTimelaps: 180000,
		degrad_session_limit: 5,
		degrad_increment: 2000,
		degrad_max: 180000, // 3 minutes
	}
	var AJAXtimeout = SessionTimeouts.createSession(AJAX_data);

	var init = function () {
		$main_html = $(html);
		if (devmode) parent_pla.enableDevmode();
		if (!initialized) {
			initialized = true;
			// parent_pla.disableDevmode();
			if (!devmode) parent_pla.disableFlags();
			/* FLAGS */
			$('body').on('click', '.pla-flags-enable', function (event) {
				parent_pla.enableFlags();
			});
			$('body').on('click', '.pla-flags-disable', function (event) {
				parent_pla.disableFlags();
			});
			/* DEGRAD ALL */
			parent_pla.addRestoreDegradBind('mousemove', 'refresh', 'body');
			// parent_pla.addRestoreDegradBind('click', 'refresh');
			// parent_pla.addRestoreDegradBind('click', 'ajax');
			parent_pla.addRestoreDegradBind('click', 'all');
			// SessionTimeouts.restoreDegrad();
		}

		return parent_pla;
	};

	init();

}

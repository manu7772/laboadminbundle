<?php

namespace Labo\Bundle\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use FOS\UserBundle\Util\LegacyFormHelper;

// Paramétrage de formulaire
// use Symfony\Component\Form\FormEvents;
// use Symfony\Component\Form\FormEvent;

// use Labo\Bundle\AdminBundle\Entity\userevenement;
use Labo\Bundle\AdminBundle\Entity\inviteduserevenement;
use Labo\Bundle\AdminBundle\Entity\LaboUser;
use site\adminsiteBundle\Form\inviteduserevenementType;
use site\UserBundle\Form\Type\ProfileForUserevenementType;
use site\UserBundle\Form\Type\RegistrationForUserevenementType;

use \Exception;

abstract class userevenementType extends AbstractType {

    const USER_CLASS = 'Labo\Bundle\AdminBundle\Entity\LaboUser';

    protected $userevenement;
    protected $user;

    protected function defineUser() {
        throw new Exception("Vous devez surcharger cette méthode dans votre classe qui étend userevenementType, svp.", 1);
        // MODEL :
        // $this->user = $this->userevenement->getUser();
        // if(!($this->user instanceOf LaboUser)) {
        //     $this->user = new LaboUser();
        //     $this->user->setEnabled(true);
        //     $this->userevenement->setUser($this->user);
        // }
    }

	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);

        $this->userevenement = $builder->getData();
        $this->defineUser();

        if($this->userevenement->getParrain() !== null) {
            $builder
                ->add('parrain', 'entity', array(
                    'label' => 'fields.parrain',
                    'disabled' => true,
                    // 'label_attr' => array('class' => 'text-muted'),
                    'translation_domain' => 'userevenement',
                    'attr'      => array(
                        'class'         => 'input-sm form-control',
                        'placeholder'   => 'fields.parrain',
                        ),
                    ))
            ;
        }

        if($this->user->getId() !== null) {
            $builder
                ->add('user', new ProfileForUserevenementType(get_class($this->user), $this->user), array(
                    'label' => 'fields.parrain',
                    'data_class' => self::USER_CLASS,
                    'translation_domain' => 'userevenement',
                    'attr'      => array(
                        'class'         => 'input-sm form-control',
                        'placeholder'   => 'fields.parrain',
                        ),
                    ))
            ;
        } else {
            $builder
                ->add('user', new RegistrationForUserevenementType(get_class($this->user), $this->user), array(
                    'label' => 'fields.parrain',
                    'data_class' => self::USER_CLASS,
                    'translation_domain' => 'userevenement',
                    'attr'      => array(
                        'class'         => 'input-sm form-control',
                        'placeholder'   => 'fields.parrain',
                        ),
                    ))
            ;
        }

		$builder
            ->add('evenementSlug', 'hidden', array(
                'mapped' => false,
                'data' => $this->userevenement->getEvenement()->getSlug(),
                ))
            ->add('parrainmailorname', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array(
                'label'     => 'fields.parrainmailorname',
                'label_attr' => array('class' => 'text-muted'),
                'translation_domain' => 'userevenement',
                'required'  => false,
                'attr'      => array(
                    'class'         => 'input-sm form-control text-center live_verif',
                    'placeholder'   => 'fields.parrainmailorname',
                    'style'         => 'width:280px;',
                    ),
                ))
            ->add('inviteduserevenements', 'collection', array(
                'type' => new inviteduserevenementType(),
                'required'  => false,
                'allow_add' => function() { return $this->userevenement->getEvenement()->isReservable(); },
                'allow_delete' => function() { return $this->userevenement->getEvenement()->isReservable(); },
                'attr' => array(
                    'class' => 'well well-sm addremoveelementsintype',
                    ),
                ))
        ;
	}

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Labo\Bundle\AdminBundle\Entity\userevenement',
            // 'attr'       => array(
                // 'class' => 'form-horizontal',
                // ),
        ));
    }

	public function getName() {
		return 'labo_adminbundle_userevenement';
	}

}
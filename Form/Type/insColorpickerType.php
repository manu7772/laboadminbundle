<?php
namespace Labo\Bundle\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class insColorpickerType extends AbstractType {

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			// 'widget' => 'single_text',
			'empty_value' => '#ffffff',
			// 'format' => $this->formatDate
		));
	}

	public function getParent() {
		return 'text';
	}

	public function getName() {
		return 'insColorpicker';
	}
}
<?php
namespace Labo\Bundle\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class bs_buttonType extends AbstractType {

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			"attr"	=> array("class" => 'btn btn-primary btn-xs btn-outline'),
			"label"	=> '<i class="fa fa-times icon-wait-on-click"></i> Supprimer',
		));
	}

	public function getParent() {
		return 'button';
	}

	public function getName() {
		return 'bs_button';
	}
}
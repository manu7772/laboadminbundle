<?php
namespace Labo\Bundle\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class insRichtextType extends AbstractType {

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
		));
	}

	public function getParent() {
		return 'textarea';
	}

	public function getName() {
		return 'insRichtext';
	}
}
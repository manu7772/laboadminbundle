<?php
namespace Labo\Bundle\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class insCheckType extends AbstractType {

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
		));
	}

	public function getParent() {
		return 'checkbox';
	}

	public function getName() {
		return 'insCheck';
	}
}
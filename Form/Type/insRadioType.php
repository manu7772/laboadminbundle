<?php
namespace Labo\Bundle\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class insRadioType extends AbstractType {

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
		));
	}

	public function getParent() {
		return 'choice';
	}

	public function getName() {
		return 'insRadio';
	}
}